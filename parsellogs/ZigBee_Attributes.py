#!/usr/bin/env python3

# Парсим имя атрибута (только для используемых сейчас аттрибутов)
def parse_attribute_name( output_text, cluster, attribute ):
	if cluster == "0000": # Basic cluster
		if attribute == "0000":
			output_text.insert(output_text.index('insert'), "ZCLVersion")
		elif attribute == "0001":	
			output_text.insert(output_text.index('insert'), "ApplicationVersion")
		elif attribute == "0002":	
			output_text.insert(output_text.index('insert'), "StackVersion")
		elif attribute == "0003":	
			output_text.insert(output_text.index('insert'), "HWVersion")
		elif attribute == "0004":
			output_text.insert(output_text.index('insert'), "ManufacturerName")
		elif attribute == "0005":
			output_text.insert(output_text.index('insert'), "ModelIdentifier")
		elif attribute == "0006":	
			output_text.insert(output_text.index('insert'), "DateCode")
		elif attribute == "0007":	
			output_text.insert(output_text.index('insert'), "PowerSource")
		elif attribute == "0010":	
			output_text.insert(output_text.index('insert'), "LocationDescription")
		elif attribute == "0011":
			output_text.insert(output_text.index('insert'), "PhysicalEnvironment")
		elif attribute == "0012":
			output_text.insert(output_text.index('insert'), "DeviceEnabled")
		elif attribute == "0013":
			output_text.insert(output_text.index('insert'), "AlarmMask")
		elif attribute == "0014":
			output_text.insert(output_text.index('insert'), "DisableLocalConfig")
		else:
			output_text.insert(output_text.index('insert'), "Unknown")
	elif cluster == "0001": # PowerConfiguration cluster
		if attribute == "0020":
			output_text.insert(output_text.index('insert'), "BatteryVoltage")	
		elif attribute == "0030":	
			output_text.insert(output_text.index('insert'), "BatteryManufacturer")
		elif attribute == "0031":	
			output_text.insert(output_text.index('insert'), "BatterySize")
		elif attribute == "0032":	
			output_text.insert(output_text.index('insert'), "BatteryAHrRating")
		elif attribute == "0033":	
			output_text.insert(output_text.index('insert'), "BatteryQuantity")
		elif attribute == "0034":	
			output_text.insert(output_text.index('insert'), "BatteryRatedVoltage")
		elif attribute == "0035":	
			output_text.insert(output_text.index('insert'), "BatteryAlarmMask")
		elif attribute == "0036":	
			output_text.insert(output_text.index('insert'), "BatteryVoltageMinThreshold")
		else:
			output_text.insert(output_text.index('insert'), "Unknown")
	elif cluster == "0003": # Identify cluster
		if attribute == "0000":
			output_text.insert(output_text.index('insert'), "IdentifyTime")	
		else:
			output_text.insert(output_text.index('insert'), "Unknown")
	elif cluster == "000a" or cluster == "000A": # Time cluster
		if attribute == "0000":
			output_text.insert(output_text.index('insert'), "Time")	
		elif attribute == "0001":	
			output_text.insert(output_text.index('insert'), "TimeStatus")
		elif attribute == "0002":
			output_text.insert(output_text.index('insert'), "TimeZone")
		elif attribute == "0003":
			output_text.insert(output_text.index('insert'), "DstStart")
		elif attribute == "0004":
			output_text.insert(output_text.index('insert'), "DstEnd")
		elif attribute == "0005":
			output_text.insert(output_text.index('insert'), "DstShift")
		elif attribute == "0006":
			output_text.insert(output_text.index('insert'), "StandardTime")
		elif attribute == "0007":
			output_text.insert(output_text.index('insert'), "LocalTime")
		elif attribute == "0008":
			output_text.insert(output_text.index('insert'), "LastSetTime")
		elif attribute == "0009":
			output_text.insert(output_text.index('insert'), "ValidUntilTime")
		else:
			output_text.insert(output_text.index('insert'), "Unknown")
	elif cluster == "0020": # PollControl cluster
		if attribute == "0000":
			output_text.insert(output_text.index('insert'), "CheckinInterval")	
		elif attribute == "0001":	
			output_text.insert(output_text.index('insert'), "LongPollInterval")
		elif attribute == "0002":
			output_text.insert(output_text.index('insert'), "ShortPollInterval")
		elif attribute == "0003":
			output_text.insert(output_text.index('insert'), "FastPollTimeout")
		elif attribute == "0004":
			output_text.insert(output_text.index('insert'), "CheckinIntervalMin")
		elif attribute == "0005":
			output_text.insert(output_text.index('insert'), "LongPollIntervalMin")
		elif attribute == "0006":
			output_text.insert(output_text.index('insert'), "FastPollTimeoutMax")
		else:
			output_text.insert(output_text.index('insert'), "Unknown")
	elif cluster == "0201": # Thermostat cluster
		if attribute == "0000":
			output_text.insert(output_text.index('insert'), "LocalTemperature")	
		elif attribute == "0001":	
			output_text.insert(output_text.index('insert'), "OutdoorTemperature")
		elif attribute == "0002":
			output_text.insert(output_text.index('insert'), "Ocupancy")
		elif attribute == "0003":
			output_text.insert(output_text.index('insert'), "AbsMinHeatSetpointLimit")
		elif attribute == "0004":
			output_text.insert(output_text.index('insert'), "AbsMaxHeatSetpointLimit")
		elif attribute == "0005":
			output_text.insert(output_text.index('insert'), "AbsMinCoolSetpointLimit")
		elif attribute == "0006":
			output_text.insert(output_text.index('insert'), "AbsMaxCoolSetpointLimit")
		elif attribute == "0007":
			output_text.insert(output_text.index('insert'), "PICoolingDemand")
		elif attribute == "0008":
			output_text.insert(output_text.index('insert'), "PIHeatingDemand")
		elif attribute == "0009":
			output_text.insert(output_text.index('insert'), "HVACSystemTypeConfiguration")		
		elif attribute == "0010":
			output_text.insert(output_text.index('insert'), "LocalTemperatureCalibration")
		elif attribute == "0011":
			output_text.insert(output_text.index('insert'), "OccupiedCoolingSetpoint")
		elif attribute == "0012":
			output_text.insert(output_text.index('insert'), "OccupiedHeatingSetpoint")
		elif attribute == "0013":
			output_text.insert(output_text.index('insert'), "UnoccupiedCoolingSetpoint")
		elif attribute == "0014":
			output_text.insert(output_text.index('insert'), "UnoccupiedHeatingSetpoint")
		elif attribute == "0015":
			output_text.insert(output_text.index('insert'), "MinHeatSetpointLimit")
		elif attribute == "0016":
			output_text.insert(output_text.index('insert'), "MaxHeatSetpointLimit")
		elif attribute == "0017":
			output_text.insert(output_text.index('insert'), "MinCoolSetpointLimit")
		elif attribute == "0018":
			output_text.insert(output_text.index('insert'), "MaxCoolSetpointLimit")
		elif attribute == "0019":
			output_text.insert(output_text.index('insert'), "MinSetpointDeadBand")
		elif attribute == "001a" or attribute == "001A":
			output_text.insert(output_text.index('insert'), "RemoteSensing")
		elif attribute == "001b" or attribute == "001B":
			output_text.insert(output_text.index('insert'), "ControlSequenceOfOperation")
		elif attribute == "001c" or attribute == "001C":
			output_text.insert(output_text.index('insert'), "SystemMode")
		elif attribute == "001d" or attribute == "001D":
			output_text.insert(output_text.index('insert'), "AlarmMask")
		elif attribute == "001e" or attribute == "001E":
			output_text.insert(output_text.index('insert'), "ThermostatRunningMode")	
		elif attribute == "0020":
			output_text.insert(output_text.index('insert'), "StartOfWeek")
		elif attribute == "0021":
			output_text.insert(output_text.index('insert'), "NumberOfWeeklyTransitions")
		elif attribute == "0022":
			output_text.insert(output_text.index('insert'), "NumberOfDailyTransitions")
		elif attribute == "0023":
			output_text.insert(output_text.index('insert'), "TemperatureSetpointHold")
		elif attribute == "0024":
			output_text.insert(output_text.index('insert'), "TemperatureSetpointHoldDuration")
		elif attribute == "0025":
			output_text.insert(output_text.index('insert'), "ThermostatProgrammingOperationMode")
		elif attribute == "0029":
			output_text.insert(output_text.index('insert'), "ThermostatRunningState")
		elif attribute == "0030":
			output_text.insert(output_text.index('insert'), "SetpointChangeSource")
		elif attribute == "0031":
			output_text.insert(output_text.index('insert'), "SetpointChangeAmount")
		elif attribute == "0032":
			output_text.insert(output_text.index('insert'), "SetpointChangeSourceTimestamp")
		else:
			output_text.insert(output_text.index('insert'), "Unknown")
	elif cluster == "0204": # Thermostat User Interface Configuration cluster
		if attribute == "0000":
			output_text.insert(output_text.index('insert'), "TemperatureDisplayMode")	
		elif attribute == "0001":	
			output_text.insert(output_text.index('insert'), "KeypadLockout")
		elif attribute == "0002":
			output_text.insert(output_text.index('insert'), "ScheduleProgrammingVisibility")
		else:
			output_text.insert(output_text.index('insert'), "Unknown")
	elif cluster == "0402": # Temperature Measurement cluster
		if attribute == "0000":
			output_text.insert(output_text.index('insert'), "MeasuredValue")	
		elif attribute == "0001":	
			output_text.insert(output_text.index('insert'), "MinMeasuredValue")
		elif attribute == "0002":
			output_text.insert(output_text.index('insert'), "MaxMeasuredValue")
		elif attribute == "0003":
			output_text.insert(output_text.index('insert'), "Tolerance")
		else:
			output_text.insert(output_text.index('insert'), "Unknown")
	else:
		output_text.insert(output_text.index('insert'), "Unknown")
	