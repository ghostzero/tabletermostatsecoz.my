#!/usr/bin/env python3

# Парсим статус в ZigBee команде
def ZigBee_Enumerated_Status( output_text, status ):
	if status == "00":
		output_text.insert(output_text.index('insert'), "SUCCESS")
		# "orange" "red" "OliveDrab1" "cyan" "light gray" "lime green" "DarkOrchid1"
		return 0x00
	elif status == "01": 
		Bad_status( output_text, "FAILURE" )
		return 0x01
	elif status == "7e" or status == "7E":
		Bad_status( output_text, "NOT_AUTHORIZED" )
		return 0x7e 
	elif status == "7F" or status == "7F":
		Bad_status( output_text, "RESERVED_FIELD_NOT_ZERO" )
		return 0x7f 
	elif status == "80":
		Bad_status( output_text, "MALFORMED_COMMAND" )
		return 0x80 
	elif status == "81":
		Bad_status( output_text, "UNSUP_CLUSTER_COMMAND" )
		return 0x81 
	elif status == "82":
		Bad_status( output_text, "UNSUP_GENERAL_COMMAND" )
		return 0x82 
	elif status == "83":
		Bad_status( output_text, "UNSUP_MANUF_CLUSTER_COMMAND" )
		return 0x83 
	elif status == "84":
		Bad_status( output_text, "UNSUP_MANUF_GENERAL_COMMAND" )
		return 0x84
	elif status == "85":
		Bad_status( output_text, "INVALID_FIELD" )
		return 0x85
	elif status == "86":
		Bad_status( output_text, "UNSUPPORTED_ATTRIBUTE" )
		return 0x86
	elif status == "87":
		Bad_status( output_text, "INVALID_VALUE" )
		return 0x87
	elif status == "88":
		Bad_status( output_text, "READ_ONLY" )
		return 0x88
	elif status == "89": 
		Bad_status( output_text, "INSUFFICIENT_SPACE" )
		return 0x89
	elif status == "8a" or status == "8A":
		Bad_status( output_text, "DUPLICATE_EXISTS" )		
		return 0x8a
	elif status == "8b" or status == "8B":
		Bad_status( output_text, "NOT_FOUND" )
		return 0x8b
	elif status == "8c" or status == "8C":
		Bad_status( output_text, "UNREPORTABLE_ATTRIBUTE" )
		return 0x8c
	elif status == "8d" or status == "8D":
		Bad_status( output_text, "INVALID_DATA_TYPE" )
		return 0x8d
	elif status == "8e" or status == "8E":
		Bad_status( output_text, "INVALID_SELECTOR" )
		return 0x8e
	elif status == "8f" or status == "8F":
		Bad_status( output_text, "WRITE_ONLY" )
		return 0x8f
	elif status == "90":
		Bad_status( output_text, "INCONSISTENT_STARTUP_STATE" )
		return 0x90 
	elif status == "91":
		Bad_status( output_text, "DEFINED_OUT_OF_BAND" )
		return 0x91 
	elif status == "92":
		Bad_status( output_text, "INCONSISTENT" )
		return 0x92 
	elif status == "93":
		Bad_status( output_text, "ACTION_DENIED" )
		return 0x93 
	elif status == "94":
		Bad_status( output_text, "TIMEOUT" )
		return 0x94
	elif status == "95":
		Bad_status( output_text, "ABORT" )
		return 0x95
	elif status == "96":
		Bad_status( output_text, "INVALID_IMAGE" )
		return 0x96
	elif status == "97":
		Bad_status( output_text, "WAIT_FOR_DATA" )
		return 0x97
	elif status == "98":
		Bad_status( output_text, "NO_IMAGE_AVAILABLE" )
		return 0x98
	elif status == "99":
		Bad_status( output_text, "REQUIRE_MORE_IMAGE" )
		return 0x99
	elif status == "c0" or status == "C0":
		Bad_status( output_text, "HARDWARE_FAILURE" )
		return 0xc0
	elif status == "c1" or status == "C1":
		Bad_status( output_text, "SOFTWARE_FAILURE" )
		return 0xc1
	elif status == "c2" or status == "C2":
		Bad_status( output_text, "CALIBRATION_ERROR" )
		return 0xc2
	else:
		Bad_status( output_text, "UNKNOWN" )
		return 0xff

# Выделим цветом ошибки (все кроме 0)
def Bad_status( output_text, status ):		
	tag1 = output_text.index('insert')
	output_text.insert(output_text.index('insert'), status) 
	tag2 = output_text.index('insert')
	output_text.tag_add("error", tag1, tag2)
	output_text.tag_config("error", foreground = "red")