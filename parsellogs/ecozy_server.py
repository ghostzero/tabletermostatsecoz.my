#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import datetime
import ZigBee_Profiles, ZigBee_Clusters, ZigBee_parser

array_pointer = 0 # глобальный счетчик, указывает на наше положение в строке

# Проверяем заголовок команды. Данные должны начиналься подстрокой "ECOZY"
def check_command_header( output_text, command ):
	if command.startswith("45434f5a59") == True:
		output_text.insert(output_text.index('insert'), "ECOZY - заголовок пакета\n")
		return True
	else:
		output_text.insert(output_text.index('insert'), "Неверные входные данные\n") # Слишком мало входных данных
		return False

# Пришедшая команда от концентратора на сервер должна быть '07' 
def check_cu_command( output_text, command ):
	if command[10:12] == "07":
		output_text.insert(output_text.index('insert'), command[10:12] + " - Передача данных от концентратора для журналирования\n")
		return True
	else:
		output_text.insert(output_text.index('insert'), "Неверная команда от концентратора\n") # Неверная команда от концентратора
		return False
		
# Вычисляем длину пришедшей команды
def main_command_len( output_text, command ):
	if len(command) < 16:
		output_text.insert(output_text.index('insert'), "Недостаточно данных\n") # Слишком мало входных данных
		return False
		
	length = int( command[12:16], 16)
	output_text.insert(output_text.index('insert'), str(length) + " - Длина тела сообщения, байты\n")
	return True

# Вычисляем количество связей телефонов 	
def Phone_ID_numbers( output_text, command ):
	global array_pointer
	if len(command) < 17:
		output_text.insert(output_text.index('insert'), "Недостаточно данных\n") # Слишком мало входных данных
		return False
	
	array_pointer = 0 # сбрасываем счетчик, у нас новый цыкл
	IDnumbers = command[16] + command[17] # Вычисляем количество связей телефонов 	
	IDnumbers = int( IDnumbers, 16)
	output_text.insert(output_text.index('insert'), str(IDnumbers) + " - Количество связей телефонов\n")
	array_pointer = 18 # указываем следующий байт в принятой строке для парсинка
	output_text.insert(output_text.index('insert'), "\n") # Перевод строки
	
	if IDnumbers == 0:
		return True
	else:
		id = 1 # парсим связи телефонов
		while id <= IDnumbers:
			Phone_ID_parsing( output_text, command )
			output_text.insert(output_text.index('insert'), "\n") # Перевод строки
			id += 1;
		
	if len(command) == 17:
		return False # данные закончились
		
	return True

# разбираем связи телефонов
def Phone_ID_parsing( output_text, command ):
	global array_pointer
	IdNumber = command[array_pointer] + command[array_pointer + 1] # читаем номер связи 
	array_pointer += 2
	IdLength = command[array_pointer] + command[array_pointer + 1] # читаем длину связи
	array_pointer += 2
	IdNumber = int( IdNumber, 16 ) # пребразуем строчный вид в число
	IdLength = int( IdLength, 16 ) # пребразуем строчный вид в число
	output_text.insert(output_text.index('insert'), str(IdNumber) + " - Номер связи телефона\n")
	output_text.insert(output_text.index('insert'), str(IdLength) + " - Длина связи телефона\n")	
	fin = array_pointer + (2 * IdLength)
	while array_pointer < fin:
		output_text.insert(output_text.index('insert'), chr(int(command[array_pointer:array_pointer+2],16)))
		array_pointer += 2
	output_text.insert(output_text.index('insert'), " - ID связи телефона\n") # Перевод строки
	
# Разбираем входящие ZigBee пакеты
def CU_ZigBee_incomming( output_text, command ):
	global array_pointer
	while True:
		ZigBee_parser.temperature_change_report = False
		tag1 = output_text.index('insert')
		CommLength = command[array_pointer:array_pointer + 4]
		array_pointer += 4
		CommLength = int( CommLength, 16 )
		output_text.insert(output_text.index('insert'), str(CommLength) + " - Длина ZigBee команды\n")
		MessageTime = command[array_pointer:array_pointer + 8]
		array_pointer += 8
		MessageTime = int( MessageTime, 16 )
		output_text.insert(output_text.index('insert'), str(MessageTime) + " - Время команды " + datetime.datetime.utcfromtimestamp(MessageTime).strftime('%Y-%m-%d %H:%M:%S') + "\n")
		IdNumber = command[array_pointer:array_pointer + 2]
		array_pointer += 2
		if IdNumber == "00":
			output_text.insert(output_text.index('insert'), "0x" + IdNumber + " - ID сообщения (репортинг)\n")
		else:
			output_text.insert(output_text.index('insert'), "0x" + IdNumber + " - ID сообщения\n")
		MACaddress = command[array_pointer:array_pointer + 16]
		array_pointer += 16
		MACaddress = swap64( MACaddress ) # MAC адрес перевернуть байты
		if MACaddress[0:8] == '70b3d5de':
			output_text.insert(output_text.index('insert'), MACaddress + " - MAC адрес термостата\n")
		else:
			tag1 = output_text.index('insert')
			output_text.insert(output_text.index('insert'), MACaddress + " - MAC адрес термостата\n")
			tag2 = output_text.index('insert')
			output_text.tag_add("error", tag1, tag2)
			output_text.tag_config("error", foreground = "red")
		MessageDirect = command[array_pointer:array_pointer + 2]
		array_pointer += 2
		if MessageDirect == "00":
			output_text.insert(output_text.index('insert'), MessageDirect + " - Message direct: запрос от телефона термостату\n")
		elif MessageDirect == "FF" or MessageDirect == "ff":
			output_text.insert(output_text.index('insert'), MessageDirect + " - Message direct: ответ от термостата\n")
		else:
			output_text.insert(output_text.index('insert'), MessageDirect + " - Message direct\n")
		SourceEndpoint = command[array_pointer:array_pointer + 4]
		array_pointer += 4
		output_text.insert(output_text.index('insert'), SourceEndpoint + " - " + SourceEndpoint[1::2] + " конечная точка отправителя\n")
		DestinationEndpoint = command[array_pointer:array_pointer + 4]
		array_pointer += 4
		output_text.insert(output_text.index('insert'), DestinationEndpoint + " - " + DestinationEndpoint[1::2] + " конечная точка получателя\n")
		ClusterID = command[array_pointer:array_pointer + 8]
		array_pointer += 8
		cluster = chr(int((ClusterID[0]+ClusterID[1]),16))+chr(int((ClusterID[2]+ClusterID[3]),16))+chr(int((ClusterID[4]+ClusterID[5]),16))+chr(int((ClusterID[6]+ClusterID[7]),16))
		output_text.insert(output_text.index('insert'), ClusterID + " - " + cluster + " кластер " + ZigBee_Clusters.ZigBee_clusters( cluster ) + "\n")
		ProfileID = command[array_pointer:array_pointer + 8]
		array_pointer += 8
		output_text.insert(output_text.index('insert'), ProfileID + " - " + ProfileID[1::2] + " профайл " + ZigBee_Profiles.ZigBee_profile( ProfileID[1::2] ) + "\n")
		# разбираем заголовок ZigBee сообщения
		HeaderRes = ZigBee_parser.ZigBee_header_parsing( output_text, command[array_pointer:], cluster )
		# HeaderRes[0] - Manufacturer_Specific, HeaderRes[1] - Direction, HeaderRes[2] - Frame type, HeaderRes[3] - ZCL Command
		CommLength -= 22 # отнимаем длину заголовка сообщения концентратора
		if HeaderRes[0] == False: # Manufacturer spesific
			CommLength -= 3 # отнимаем длину заголовка ZCL сообщения
			array_pointer += 6
		else:
			CommLength -= 5 # отнимаем длину заголовка ZCL сообщения
			array_pointer += 10
			
		ZCLpayload = command[array_pointer:array_pointer + 2*CommLength]
		
		# Попытаемся разобрать тело команды
		if HeaderRes[0] == True: # Manufacturer spesific
			# Не разбираем никакие команды, специфичные от производителя. Просто выведем тело команды на экран
			output_text.insert(output_text.index('insert'), "ZCL payload: " + ZCLpayload + "\n")
		else:
			if HeaderRes[2] == "00": # Profile-wide command
				ZigBee_parser.Parsing_profile_wide_command( output_text, HeaderRes[3], ZCLpayload, cluster )
			elif HeaderRes[2] == "01": # Cluster-specific command
				ZigBee_parser.Parsing_cluster_specific_command( output_text, HeaderRes[3], ZCLpayload, cluster, HeaderRes[1] )
			else: 
				# Хрен его знает что это, явная ошибка. Просто выведем тело команды на экран
				output_text.insert(output_text.index('insert'), "ZCL payload: " + ZCLpayload + "\n")
		
		if ZigBee_parser.temperature_change_report == True:
			print('123')
			tag2 = output_text.index('insert')
			output_text.tag_add("color_mark", tag1, tag2)
			output_text.tag_config("color_mark", background = "OliveDrab1")
	
		output_text.insert(output_text.index('insert'), "\n") # Перевод строки
		array_pointer += 2*CommLength
		
		if len(command) <= (array_pointer + 1):
			break;
			
# Переверовачиваем байты в MAC адресе 
def swap64( eui64 ):
	ret = ""
	for i in [14, 12, 10, 8, 6, 4, 2, 0]:
		ret += eui64[i]
		ret += eui64[i + 1]
	ret.upper()
	return ret