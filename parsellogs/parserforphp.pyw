#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import ecozy_server
from tkinter import * # импорт библиотеки GUI

# проверим входные данные, если там строчные и бинарные вместе, то отсеим
# бинарные, оставим только строчные данные для дальнейшей обработки
def Parse_received_string_data( input_data ):
	output_data = ""
	counter = start = finish = 0
	while counter < len( input_data ):
		if input_data[counter] == '\t':
			start = counter + 1
			counter += 1
		elif input_data[counter] == '\n':
			output_data += input_data[start:counter]
			start = finish = counter
			while input_data[counter] == '\n':
				counter += 1
				start = finish = counter
				if counter >= len( input_data ):
					break;
		else:
			counter += 1
	
	input_text.delete('1.0', END)
	input_text.insert(1.0, output_data)
	return output_data

# сюда попадаем, когда пользователь нажимает Enter в окне входных данных"
def received_data(event):
	input_data = ""
	input_data = input_text.get("1.0", END) # Читаем введенные данные
	output_text.delete('1.0', END) # Очищаем поле выходных данных
	
	""" Здесь еще должен быть обработчик, который отделит бинарную часть
	данных от строчной. Дальше будем работать только с строчной частью"""
	input_data = Parse_received_string_data( input_data )
	file = open('zigbeebegincommand.txt', encoding='utf-8')
	input_data = file.read()
	
	try:
		if len(input_data) <= 6:
			output_text.insert(INSERT, "Недостаточно данных\n") # Слишком мало входных данных
			input_text.delete('1.0', END) # Очищаем поле входных данных
			return
		
		if ecozy_server.check_command_header( output_text, input_data ) == False:
			# Данные должны начиналься подстрокой "ECOZY"
			input_text.delete('1.0', END) # Очищаем поле входных данных
			return
		
		# проверяем команду концентратора
		if ecozy_server.check_cu_command( output_text, input_data ) == False:
			input_text.delete('1.0', END) # Очищаем поле входных данных
			return
		
		if ecozy_server.main_command_len( output_text, input_data ) == False:
			return # Проблема с вычислением длины пришедшей команды
		
		if ecozy_server.Phone_ID_numbers( output_text, input_data ) == False:
			return
		
		ecozy_server.CU_ZigBee_incomming( output_text, input_data )
	except:
		tag1 = output_text.index('insert')
		output_text.insert(output_text.index('insert'), "Возникла непредвиденная ошибка\nПарсинг команды был прерван\n" +\
					"Проверьте целостность и корректность входных данных\n")
		tag2 = output_text.index('insert')
		output_text.tag_add("error", tag1, tag2)
		output_text.tag_config("error", foreground = "red")

def Read_data_from_file():
	file = open('zigbeebegincommand.txt', encoding='utf-8')
	input_data = file.read()
	if len(input_data) <= 6:
		output_text.insert(INSERT, "Недостаточно данных\n") # Слишком мало входных данных
		input_text.delete('1.0', END) # Очищаем поле входных данных
		return
		
	if ecozy_server.check_command_header( output_text, input_data ) == False:
		# Данные должны начиналься подстрокой "ECOZY"
		input_text.delete('1.0', END) # Очищаем поле входных данных
		return
		
	# проверяем команду концентратора
	if ecozy_server.check_cu_command( output_text, input_data ) == False:
		input_text.delete('1.0', END) # Очищаем поле входных данных
		return
		
	if ecozy_server.main_command_len( output_text, input_data ) == False:
		return # Проблема с вычислением длины пришедшей команды
	
	if ecozy_server.Phone_ID_numbers( output_text, input_data ) == False:
		return
		
	ecozy_server.CU_ZigBee_incomming( output_text, input_data )
	
root = Tk() # создаем главное окно программы
root.title('Logs parser (ZigBee)')

input_text = Text(root, height = 20, width = 40, font = "8", wrap = WORD) # создаем окно ввода данных
#input_text.bind('<Return>', received_data) # привязываем функцию получения введенных данных
input_scrY = Scrollbar(root, command = input_text.yview) # вертикальная полоса прокрутки
input_text.configure(yscrollcommand = input_scrY.set)
output_text = Text(root, height = 20, width = 60, font = "8", wrap = WORD) # создаем окно вывода данных
output_scrY = Scrollbar(root, command = output_text.yview) # вертикальная полоса прокрутки
output_text.configure(yscrollcommand = output_scrY.set)
# Отображаем все графические элементы на экране
input_scrY.pack(side = LEFT, fill = Y)
output_scrY.pack(side = RIGHT, fill = Y)
input_text.pack(side = LEFT, fill = BOTH, expand = 1)
output_text.pack(side = RIGHT, fill = BOTH, expand = 1)

output_text.insert(INSERT, "Waiting input data...") # Ждем входных данных
Read_data_from_file()
 # Сохраняем распарсенные даннные в файл
sa='outputresult.txt'
letter = output_text.get(1.0, END)
f = open(sa, "w",encoding='utf-8')
f.write(letter)
f.close()
root.mainloop() # отображаем главное окно программы