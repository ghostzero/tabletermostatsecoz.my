#!/usr/bin/env python3

# Проверко профайла на совпадение с объявленными public профайлами в ZigBee
def ZigBee_profile ( profile ):
	if profile == "0101":
		return "Industrial Plant Monitoring (IPM)"
	elif profile == "0104":
		return "Home Automation (HA)"
	elif profile == "0105":
		return "Commercial Building Automation (CBA)"
	elif profile == "0107":
		return "	Telecom Applications (TA)"
	elif profile == "0108":
		return "Personal Home & Hospital Care (PHHC)"
	elif profile == "0109":
		return "	Advanced Metering Initiative (AMI)"
	else:
		return "Unknown"