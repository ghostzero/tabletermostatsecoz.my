#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from math import *
import datetime
import ZigBee_Attributes, ZigBee_Status, ZigBee_Data_Types

global temperature_change_report

# Парсим ZigBee заголовок
def ZigBee_header_parsing( output_text, command, cluster ):
	# Парсим первый байт заголовка
	Frame_control = command[0] + command[1]
	conter = 2
	output_text.insert(output_text.index('insert'), "0x" + Frame_control + " - ZCL frame control\n")
	# Представляем Frame_control в двоичном виде
	Frame_control = bin(int(Frame_control,16))
	while len(Frame_control) < 7:
		Frame_control = Frame_control[0:2] + "0" + Frame_control[2:]
	# Выводим побитный парсинг на экран
	# 1. Frame type
	if (Frame_control[-2] + Frame_control[-1]) == "00":
		output_text.insert(output_text.index('insert'), "      .... ..00 = Frame Type: Profile-wide\n")
	elif (Frame_control[-2] + Frame_control[-1]) == "01":
		output_text.insert(output_text.index('insert'), "      .... ..01 = Frame Type: Cluster-specific\n")
	else:
		output_text.insert(output_text.index('insert'), "      .... .." + Frame_control[-2] + Frame_control[-1] + " = Frame Type: Unknows\n")
	# 2. Manufacturer Specific
	if Frame_control[-3] == "0":
		Manufacturer_Specific = False
		output_text.insert(output_text.index('insert'), "      .... .0.. = Manufacturer Specific: False\n")
	else:
		Manufacturer_Specific = True
		output_text.insert(output_text.index('insert'), "      .... .1.. = Manufacturer Specific: True\n")
	# 3. Direction
	if Frame_control[-4] == "0":
		output_text.insert(output_text.index('insert'), "      .... 0... = Direction: To server\n")
	else:
		output_text.insert(output_text.index('insert'), "      .... 1... = Direction: To client\n")
	# 4. Disable Default Response
	if Frame_control[-5] == "0":
		output_text.insert(output_text.index('insert'), "      ...0 .... = Disable Default Response: False\n")
	else:
		output_text.insert(output_text.index('insert'), "      ...1 .... = Disable Default Response: True\n")
	# Если это команда производителя, то выводим код производителя
	if Manufacturer_Specific == True:
		output_text.insert(output_text.index('insert'), command[4] + command[5] + command[2] + command[3] + " - Manufacturer Code Field\n")
		conter = 6
	# Парсим второй байт заголовка
	# Счетчик транзаксии
	output_text.insert(output_text.index('insert'), command[conter] + command[conter + 1] + " - Transaction Sequence Number\n")
	conter += 2
	# Парсим третий байт заголовка
	# ZCL команда
	ZCLcommand = command[conter] + command[conter + 1]
	output_text.insert(output_text.index('insert'), ZCLcommand + " - Command: ")
	if (Frame_control[-2] + Frame_control[-1]) == "00":
		Profile_wide_command( output_text, ZCLcommand )
	elif (Frame_control[-2] + Frame_control[-1]) == "01":
		Cluster_specific_command( output_text, ZCLcommand, cluster, Frame_control[-4] )
	else:
		output_text.insert(output_text.index('insert'), "Unknown")
	output_text.insert(output_text.index('insert'), "\n") # Перевод строки
	return [Manufacturer_Specific, Frame_control[-4], Frame_control[-2] + Frame_control[-1], ZCLcommand]
	
# разбираем общие команды
def Profile_wide_command( output_text, ZCLcommand ):
	if ZCLcommand == "00":
		output_text.insert(output_text.index('insert'), "Read attributes")
	elif ZCLcommand == "01":
		output_text.insert(output_text.index('insert'), "Read attributes response")
	elif ZCLcommand == "02": 
		output_text.insert(output_text.index('insert'), "Write attributes")
	elif ZCLcommand == "03": 
		output_text.insert(output_text.index('insert'), "Write attributes undivided")
	elif ZCLcommand == "04": 
		output_text.insert(output_text.index('insert'), "Write attributes response")
	elif ZCLcommand == "05": 
		output_text.insert(output_text.index('insert'), "Write attributes no response")
	elif ZCLcommand == "06": 
		output_text.insert(output_text.index('insert'), "Configure reporting")
	elif ZCLcommand == "07": 
		output_text.insert(output_text.index('insert'), "Configure reporting response")
	elif ZCLcommand == "08": 
		output_text.insert(output_text.index('insert'), "Read reporting configuration")
	elif ZCLcommand == "09": 
		output_text.insert(output_text.index('insert'), "Read reporting configuration response")
	elif ZCLcommand == "0A" or ZCLcommand == "0a": 
		output_text.insert(output_text.index('insert'), "Report attributes")
	elif ZCLcommand == "0B" or ZCLcommand == "0b": 
		output_text.insert(output_text.index('insert'), "Default response")
	elif ZCLcommand == "0C" or ZCLcommand == "0c": 
		output_text.insert(output_text.index('insert'), "Discover attributes")
	elif ZCLcommand == "0D" or ZCLcommand == "0d": 
		output_text.insert(output_text.index('insert'), "Discover attributes response")
	elif ZCLcommand == "0E" or ZCLcommand == "0e": 
		output_text.insert(output_text.index('insert'), "Read attributes structured")
	elif ZCLcommand == "0F" or ZCLcommand == "0f": 
		output_text.insert(output_text.index('insert'), "Write attributes structured")
	elif ZCLcommand == "10": 
		output_text.insert(output_text.index('insert'), "Write attributes structured response")
	else:
		output_text.insert(output_text.index('insert'), "Unknown")

# разбираем команды, которые спецефичны для некоторых кластеров
def Cluster_specific_command( output_text, ZCLcommand, cluster, Direction ):
	if cluster == "0000": # Basic cluster
		if ZCLcommand == "00": # Reset to Factory Defaults
			output_text.insert(output_text.index('insert'), "Reset to Factory Defaults")
		else:
			output_text.insert(output_text.index('insert'), "Unknown")
	elif cluster == "0003": # Identify cluster
		if ZCLcommand == "00":
			if Direction == "0": # To server
				output_text.insert(output_text.index('insert'), "Identify")
			else:
				output_text.insert(output_text.index('insert'), "Identify Query Response")
		elif ZCLcommand == "01":
			output_text.insert(output_text.index('insert'), "Identify Query")
		else:
			output_text.insert(output_text.index('insert'), "Unknown")
	elif cluster == "0020": # PollControl cluster
		if ZCLcommand == "00":
			if Direction == "0": # To server
				output_text.insert(output_text.index('insert'), "Check-in Response")
			else:
				output_text.insert(output_text.index('insert'), "Check-in")
		elif ZCLcommand == "01":
			output_text.insert(output_text.index('insert'), "Fast Poll Stop")
		elif ZCLcommand == "02":
			output_text.insert(output_text.index('insert'), "Set Long Poll Interval")
		elif ZCLcommand == "03":
			output_text.insert(output_text.index('insert'), "Set Short Poll Interval")
		else:
			output_text.insert(output_text.index('insert'), "Unknown")
	elif cluster == "0201": # Thermostat cluster
		if ZCLcommand == "00":
			if Direction == "0": # To server
				output_text.insert(output_text.index('insert'), "Setpoint Raise/Lower")
			else:
				output_text.insert(output_text.index('insert'), "Get Weekly Schedule Response")
		elif ZCLcommand == "01":
			if Direction == "0": # To server
				output_text.insert(output_text.index('insert'), "Set Weekly Schedule")
			else:
				output_text.insert(output_text.index('insert'), "Get Relay Status Log Response")
		elif ZCLcommand == "02":
			output_text.insert(output_text.index('insert'), "Get Weekly Schedule")
		elif ZCLcommand == "03":
			output_text.insert(output_text.index('insert'), "Clear Weekly Schedule")
		elif ZCLcommand == "04":
			output_text.insert(output_text.index('insert'), "Get Relay Status Log")
		else:
			output_text.insert(output_text.index('insert'), "Unknown")
	else:
		output_text.insert(output_text.index('insert'), "Unknown")

# Будем парись тело общей ZCL команды		
def Parsing_profile_wide_command( output_text, ZCLcommand, ZCLpayload, cluster ):
	if ZCLcommand == "00": 							# Read attributes
		Parsing_Read_attributes_command( output_text, cluster, ZCLpayload )
	elif ZCLcommand == "01": 						# Read attributes response
		Parsing_Read_attributes_response_command( output_text, cluster, ZCLpayload )
	elif ZCLcommand == "02" or ZCLcommand == "03": 	# Write attributes or Write attributes undivided
		Parsing_Write_attributes_command( output_text, cluster, ZCLpayload )
	elif ZCLcommand == "04": 						# Write attributes response
		Parsing_Write_attributes_response_command( output_text, cluster, ZCLpayload )
	elif ZCLcommand == "05": 						# Write attributes no response
		Parsing_Write_attributes_command( output_text, cluster, ZCLpayload )
	elif ZCLcommand == "06": 						# Configure reporting
		Parsing_Configure_reporting_command( output_text, cluster, ZCLpayload )
	elif ZCLcommand == "07": 						# Configure reporting response
		Parsing_Configure_reporting_response_command( output_text, cluster, ZCLpayload )
	elif ZCLcommand == "0A" or ZCLcommand == "0a": 	# Report attributes
		Parsing_Report_attributes_command( output_text, cluster, ZCLpayload )
	elif ZCLcommand == "0B" or ZCLcommand == "0b": 	# Default response
		Parsing_Default_response_command( output_text, ZCLpayload )
	else: 											# Unknown Просто выведем тело команды на экран
		output_text.insert(output_text.index('insert'), "ZCL payload: " + ZCLpayload + "\n")

# Будем парись тело Read attributes command	
def Parsing_Read_attributes_command( output_text, cluster, ZCLpayload ):
	counter = 0
	while counter + 1 < len(ZCLpayload):
		Attribute_Parsing( output_text, cluster, ZCLpayload, counter )
		counter += 4

# Будем парись тело Read attributes response command	
def Parsing_Read_attributes_response_command( output_text, cluster, ZCLpayload ):
	counter = 0
	while counter + 1 < len(ZCLpayload):
		# Разбираем атрибут
		attribute = Attribute_Parsing( output_text, cluster, ZCLpayload, counter )
		counter += 4
		# Разбираем статус чтения атрибута
		status = Status_Parsing( output_text, ZCLpayload, counter )
		counter += 2
		if status == 0:
			# Разбираем тип данных
			Length = Data_Type_Parsing( output_text, ZCLpayload, counter )
			counter += 2
			# Выводим данные на экран
			if Length == 0xFF:
				output_text.insert(output_text.index('insert'), "Возникла непредвиденная ошибка\nПарсинг команды был прерван\n" +\
					"Проверьте целостность и корректность входных данных")
				return False
			else:
				counter = Payload_Parsing( output_text, cluster, attribute, Length, ZCLpayload, counter )

# Будем парись тело Write attributes command
def Parsing_Write_attributes_command( output_text, cluster, ZCLpayload ):
	counter = 0
	while counter + 1 < len(ZCLpayload):
		# Разбираем атрибут
		attribute = Attribute_Parsing( output_text, cluster, ZCLpayload, counter )
		counter += 4
		# Разбираем тип данных
		Length = Data_Type_Parsing( output_text, ZCLpayload, counter )
		counter += 2
		# Выводим данные на экран
		if Length == 0xFF:
			output_text.insert(output_text.index('insert'), "Возникла непредвиденная ошибка\nПарсинг команды был прерван\n" +\
					"Проверьте целостность и корректность входных данных")
			return False
		else:
			counter = Payload_Parsing( output_text, cluster, attribute, Length, ZCLpayload, counter )

def Parsing_Write_attributes_response_command( output_text, cluster, ZCLpayload ):
	counter = 0
	if len(ZCLpayload) == 2:
		Status_Parsing( output_text, ZCLpayload, counter )
	else:
		while counter + 1 < len(ZCLpayload):
			# Разбираем статус
			Status_Parsing( output_text, ZCLpayload, counter )
			counter += 2
			# Разбираем аттрибут
			attribute = Attribute_Parsing( output_text, cluster, ZCLpayload, counter )
			counter += 4

# Будем парись тело Configure reporting command
def Parsing_Configure_reporting_command( output_text, cluster, ZCLpayload ):
	counter = 0
	while counter + 1 < len(ZCLpayload):
		if ZCLpayload[counter:counter+2] == "00":
			output_text.insert(output_text.index('insert'), ZCLpayload[counter:counter+2] + " - Direction: Reported (0x00)\n")
			counter += 2
			# Разбираем атрибут
			attribute = Attribute_Parsing( output_text, cluster, ZCLpayload, counter )
			counter += 4
			# Разбираем тип данных
			Length = Data_Type_Parsing( output_text, ZCLpayload, counter )
			counter += 2
			# Разбираем Minimum reporting interval
			Minimum_interval = ZCLpayload[counter+2:counter+4] + ZCLpayload[counter:counter+2]
			output_text.insert(output_text.index('insert'), "0x" + Minimum_interval + " - Minimum interval: " + str(int(Minimum_interval,16)) + " seconds \n")
			counter += 4
			# Разбираем Maximum reporting interval
			Minimum_interval = ZCLpayload[counter+2:counter+4] + ZCLpayload[counter:counter+2]
			output_text.insert(output_text.index('insert'), "0x" + Minimum_interval + " - Maximum interval: " + str(int(Minimum_interval,16)) + " seconds \n")
			counter += 4
			# Разбираем Reportable change
			Reportable_change = swap( Length, ZCLpayload[counter:counter+2*Length] )
			output_text.insert(output_text.index('insert'), "0x" + Reportable_change + " - Reportable change: " + str(int(Reportable_change,16)) + "\n")
			counter += 2*Length
		elif ZCLpayload[counter:counter+2] == "01":
			output_text.insert(output_text.index('insert'), ZCLpayload[counter:counter+2] + " - Direction: Received (0x01)\n")
			counter += 2
			# Разбираем атрибут
			attribute = Attribute_Parsing( output_text, cluster, ZCLpayload, counter )
			counter += 4
			# Разбираем Timeout
			timeout = swap( 2, ZCLpayload[counter:counter+4] )
			output_text.insert(output_text.index('insert'), "0x" + timeout + " - Timeout: " + str(int(timeout,16)) + " seconds\n")
		else:
			output_text.insert(output_text.index('insert'), "Возникла непредвиденная ошибка\nПарсинг команды был прерван\n" +\
					"Проверьте целостность и корректность входных данных\n")
			return False

# Будем парись тело Configure reporting response command
def Parsing_Configure_reporting_response_command( output_text, cluster, ZCLpayload ):
	counter = 0
	if len(ZCLpayload) == 2:
		Status_Parsing( output_text, ZCLpayload, counter )
	else:
		while counter + 1 < len(ZCLpayload):
			# Разбираем статус
			status = Status_Parsing( output_text, ZCLpayload, counter )
			counter += 2
			if status == 0: # success
				# Разбираем direction
				if ZCLpayload[counter:counter+2] == "00":
					output_text.insert(output_text.index('insert'), ZCLpayload[counter:counter+2] + " - Direction: Reported (0x00)\n")
				elif ZCLpayload[counter:counter+2] == "01":
					output_text.insert(output_text.index('insert'), ZCLpayload[counter:counter+2] + " - Direction: Received (0x01)\n")
				else:
					output_text.insert(output_text.index('insert'), "Возникла непредвиденная ошибка\nПарсинг команды был прерван\n" +\
						"Проверьте целостность и корректность входных данных\n")
					return False
				counter += 2
				# Разбираем атрибут
				attribute = Attribute_Parsing( output_text, cluster, ZCLpayload, counter )
				counter += 4

# Будем парись тело Report attributes command
def Parsing_Report_attributes_command( output_text, cluster, ZCLpayload ):
	counter = 0
	while counter + 1 < len(ZCLpayload):
		# Разбираем атрибут
		attribute = Attribute_Parsing( output_text, cluster, ZCLpayload, counter )
		counter += 4
		# Разбираем тип данных
		Length = Data_Type_Parsing( output_text, ZCLpayload, counter )
		counter += 2
		# Выводим данные на экран
		if Length == 0xFF:
			output_text.insert(output_text.index('insert'), "Возникла непредвиденная ошибка\nПарсинг команды был прерван\n" +\
					"Проверьте целостность и корректность входных данных")
			return False
		else:
			counter = Payload_Parsing( output_text, cluster, attribute, Length, ZCLpayload, counter )

# Будем парись тело Default response command	
def Parsing_Default_response_command( output_text, ZCLpayload ):
	output_text.insert(output_text.index('insert'), ZCLpayload[0:2] + " - Command Identifier Field\n")
	output_text.insert(output_text.index('insert'), ZCLpayload[2:4] + " - Status: ")
	ZigBee_Status.ZigBee_Enumerated_Status( output_text, ZCLpayload[2:4] )
	output_text.insert(output_text.index('insert'), "\n") # Перевод строки

# Будем парись тело спецефичной для каждого кластера ZCL команды	
def Parsing_cluster_specific_command( output_text, ZCLcommand, ZCLpayload, cluster, Direction ):
	if cluster == "0000": # Basic cluster
		if ZCLcommand != "00": # This is not Reset to Factory Defaults command
			output_text.insert(output_text.index('insert'), "ZCL payload: " + ZCLpayload + "\n")
	elif cluster == "0003": # Identify cluster
		if ZCLcommand == "00":
			Identify_Time = swap( 2, ZCLpayload[0:4] )
			output_text.insert(output_text.index('insert'), "0x" + Identify_Time + " - Identify Time: " + str(int(Identify_Time, 16)) + " seconds\n")
		elif ZCLcommand != "01":
			output_text.insert(output_text.index('insert'), "ZCL payload: " + ZCLpayload + "\n")
	elif cluster == "0020": # PollControl cluster
		if ZCLcommand == "00":
			if len(ZCLpayload) == 6: # This is Check-in Response command
				if ZCLpayload[0:2] == "00":
					output_text.insert(output_text.index('insert'), "0x00 - Start Fast Polling: False\n")
				elif ZCLpayload[0:2] == "01":
					output_text.insert(output_text.index('insert'), "0x01 - Start Fast Polling: True\n")
				else:
					output_text.insert(output_text.index('insert'), "ZCL payload: " + ZCLpayload + "\n")
					return False
				Fast_Poll_Timeout = swap( 2, ZCLpayload[2:6] )
				output_text.insert(output_text.index('insert'), "0x" + Fast_Poll_Timeout + " - Fast Poll Timeout: " + str(int(Fast_Poll_Timeout, 16)) + "\n")
		elif ZCLcommand == "02": # Set Long Poll Interval command 
			NewLongPollInterval = swap( 4, ZCLpayload[0:8] )
			output_text.insert(output_text.index('insert'), "0x" + NewLongPollInterval + " - New Long Poll Interval\n")
		elif ZCLcommand == "03": # Set Short Poll Interval command
			New_Short_Poll_Interval = swap( 2, ZCLpayload[0:4] )
			output_text.insert(output_text.index('insert'), "0x" + New_Short_Poll_Interval + " - New Short Poll Interval\n")
		elif ZCLcommand != "01": # != Fast Poll Stop command
			output_text.insert(output_text.index('insert'), "ZCL payload: " + ZCLpayload + "\n")
	elif cluster == "0201": # Thermostat cluster
		if Direction == "0": # To server
			if ZCLcommand == "01": # Set Weekly Schedule command
				Parsing_Set_Weekly_Schedule_command( output_text, ZCLpayload )
			elif ZCLcommand == "02": # Get Weekly Schedule command
				Days_of_Week_Parsing( output_text, ZCLpayload[0:2] ) # Разбираем день недели расписания
				Mode_for_Sequence_Parsing( output_text, ZCLpayload[2:4] ) # Разбираем Mode for Sequence
			elif ZCLcommand != "03": # != Clear Weekly Schedule command
				output_text.insert(output_text.index('insert'), "ZCL payload: " + ZCLpayload + "\n")
		else: #To client
			if ZCLcommand == "00": # Get Weekly Schedule Response command
				Parsing_Set_Weekly_Schedule_command( output_text, ZCLpayload )
			else:
				output_text.insert(output_text.index('insert'), "ZCL payload: " + ZCLpayload + "\n")
	else:
		output_text.insert(output_text.index('insert'), "ZCL payload: " + ZCLpayload + "\n")

# Будем парись тело Set Weekly Schedule command	
def Parsing_Set_Weekly_Schedule_command( output_text, ZCLpayload ):
	counter = 0
	# Разбираем Number of Transitions for Sequence
	output_text.insert(output_text.index('insert'), "0x" + ZCLpayload[counter:counter+2] + " - Number of Transitions for Sequence\n")
	counter += 2
	# Разбираем день недели расписания
	Days_of_Week_Parsing( output_text, ZCLpayload[counter:counter+2] )
	counter += 2
	# Разбираем Mode for Sequence
	Mode_for_Sequence_Parsing( output_text, ZCLpayload[counter:counter+2] )
	mode = ZCLpayload[counter:counter+2]
	counter += 2
	if mode == "01":
		# Разбираем время и температуру
		while (counter + 1) < len(ZCLpayload):
			# Разбираем время
			time = swap( 2, ZCLpayload[counter:counter+4] )
			output_text.insert(output_text.index('insert'), "0x" + time + " - Transition Time: " + str(int(time, 16)) + " minutes ")
			time = int(time, 16)
			hours = floor(time / 60)
			minutes = time % 60
			output_text.insert(output_text.index('insert'), "(" + str(hours) + " часов " + str(minutes) + " минут)\n")
			counter += 4
			# Разбираем температуру
			heat = swap( 2, ZCLpayload[counter:counter+4] )
			output_text.insert(output_text.index('insert'), "0x" + heat + " - Heat Setpoint: " + str(int(heat, 16)/100) + "\n")
			counter += 4
	else:
		output_text.insert(output_text.index('insert'), "Other ZCL payload: " + ZCLpayload[counter:] + "\n")

# Разбираем Mode for Sequence
def Mode_for_Sequence_Parsing( output_text, mode ):
	output_text.insert(output_text.index('insert'), "0x" + mode + " - Mode for Sequence\n")
	mode = bin(int(mode, 16))
	while len(mode) < 4:
		mode = mode[0:2] + "0" + mode[2:]
	if mode[-1] == '1':
		output_text.insert(output_text.index('insert'), "      .... ...1 = Heating: True\n")
	else:
		output_text.insert(output_text.index('insert'), "      .... ...0 = Heating: False\n")
	if mode[-2] == '1':
		output_text.insert(output_text.index('insert'), "      .... ..1. = Cooling: True\n")
	else:
		output_text.insert(output_text.index('insert'), "      .... ..0. = Cooling: False\n")
	
# Разбираем день недели в команде
def Days_of_Week_Parsing( output_text, day ):
	output_text.insert(output_text.index('insert'), "0x" + day + " - Day of Week for Sequence\n")
	day = bin(int(day, 16))
	while len(day) < 10:
		day = day[0:2] + "0" + day[2:]
	if day[-1] == '1':
		output_text.insert(output_text.index('insert'), "      .... ...1 = Sunday: True\n")
	else:
		output_text.insert(output_text.index('insert'), "      .... ...0 = Sunday: False\n")
	if day[-2] == '1':
		output_text.insert(output_text.index('insert'), "      .... ..1. = Monday: True\n")
	else:
		output_text.insert(output_text.index('insert'), "      .... ..0. = Monday: False\n")
	if day[-3] == '1':
		output_text.insert(output_text.index('insert'), "      .... .1.. = Tuesday: True\n")
	else:
		output_text.insert(output_text.index('insert'), "      .... .0.. = Tuesday: False\n")
	if day[-4] == '1':
		output_text.insert(output_text.index('insert'), "      .... 1... = Wednesday: True\n")
	else:
		output_text.insert(output_text.index('insert'), "      .... 0... = Wednesday: False\n")
	if day[-5] == '1':
		output_text.insert(output_text.index('insert'), "      ...1 .... = Thursday: True\n")
	else:
		output_text.insert(output_text.index('insert'), "      ...0 .... = Thursday: False\n")
	if day[-6] == '1':
		output_text.insert(output_text.index('insert'), "      ..1. .... = Friday: True\n")
	else:
		output_text.insert(output_text.index('insert'), "      ..0. .... = Friday: False\n")
	if day[-7] == '1':
		output_text.insert(output_text.index('insert'), "      .1.. .... = Saturday: True\n")
	else:
		output_text.insert(output_text.index('insert'), "      .0.. .... = Saturday: False\n")
	if day[-8] == '1':
		output_text.insert(output_text.index('insert'), "      1... .... = Away or Vacation: True\n")
	else:
		output_text.insert(output_text.index('insert'), "      0... .... = Away or Vacation: False\n")

# Разбираем статус атрибута
def Status_Parsing( output_text, ZCLpayload, counter ):
	output_text.insert(output_text.index('insert'), ZCLpayload[counter:counter+2] + " - Status: ")
	status = ZigBee_Status.ZigBee_Enumerated_Status( output_text, ZCLpayload[counter:counter+2] )
	output_text.insert(output_text.index('insert'), "\n") # Перевод строки
	return status
		
# Разбираем данные атрибута
def Payload_Parsing( output_text, cluster, attribute, Length, ZCLpayload, counter ):
	if Length == 0xFE: # Строчный тип данных
		Length = int(ZCLpayload[counter:counter + 2], 16)
		output_text.insert(output_text.index('insert'), "Length: " + str(Length) + "\n")
		counter += 2
		output_text.insert(output_text.index('insert'), "String: ")
		# Подготовительные операции для вывода самой строки на экран
		fin = counter + (2 * Length)
		while counter < fin:
			output_text.insert(output_text.index('insert'), chr(int(ZCLpayload[counter:counter+2],16)))
			counter += 2
		output_text.insert(output_text.index('insert'), "\n") # Перевод строки
	else:
		val = ZCLpayload[counter:counter + (2 * Length)]
		val = swap( Length, val )
		output_text.insert(output_text.index('insert'), "Data: 0x" + val)
		parse_attribute_payload( output_text, cluster, attribute, val )
		output_text.insert(output_text.index('insert'), "\n") # Перевод строки
		counter += (2 * Length)
	return counter
	
# Разбираем тип данных
def Data_Type_Parsing( output_text, ZCLpayload, counter ):
	output_text.insert(output_text.index('insert'), ZCLpayload[counter:counter+2] + " - Data Type: ")
	Length = ZigBee_Data_Types.ZigBee_data_type( output_text, ZCLpayload[counter:counter+2] )
	output_text.insert(output_text.index('insert'), "\n") # Перевод строки
	return Length
			
# Разбираем значение атрибута
def Attribute_Parsing( output_text, cluster, ZCLpayload, counter ):
	# Разбираем атрибут
	attribute = ZCLpayload[counter+2:counter+4] + ZCLpayload[counter:counter+2]
	output_text.insert(output_text.index('insert'), ZCLpayload[counter:counter+4] + " - атрибут 0х" + attribute + " - ")
	ZigBee_Attributes.parse_attribute_name(output_text, cluster, attribute) # Выведем название атрибута на экран
	output_text.insert(output_text.index('insert'), "\n") # Перевод строки
	return attribute

# Переварачиваем payload
def swap( Length, val ):
	if Length == 2:
		return val[2:4] + val[0:2]
	elif Length == 4:
		return val[6:8] + val[4:6] + val[2:4] + val[0:2]
	elif Length == 8:
		return val[14:16] + val[12:14] + val[10:12] + val[8:10] + val[6:8] + val[4:6] + val[2:4] + val[0:2]
	else:
		return val

# Выведем значения температуры и батареек на экран		
def parse_attribute_payload( output_text, cluster, attribute, val ):
	global temperature_change_report
	if cluster == "0001" and attribute == "0020":
		output_text.insert(output_text.index('insert'), " (батарейки " + str(int(val, 16)/10) + "В)")
	elif cluster == "0402" and attribute == "0000":
		output_text.insert(output_text.index('insert'), " (температура " + str(int(val, 16)/100) + " градусов)")
	elif cluster == "0201":
		if attribute == "0000":
			output_text.insert(output_text.index('insert'), " ( текущая температура " + str(int(val, 16)/100) + " градусов)")
		elif attribute == "0012":
			output_text.insert(output_text.index('insert'), " ( выставленая температура " + str(int(val, 16)/100) + " градусов)")
		elif attribute == "0008":
			output_text.insert(output_text.index('insert'), " (состояние плунжера " + str(int(val, 16)) + ")")
		elif attribute == "0030":
			temperature_change_report = True
			if val == "00":
				output_text.insert(output_text.index('insert'), " - температура изменена вручную на термостате")
			elif val == "01":
				output_text.insert(output_text.index('insert'), " - температура изменена расписанием на термостате")
			elif val == "02":
				output_text.insert(output_text.index('insert'), " - температура изменена записью по ZigBee")
		elif attribute == "0031":
			if len(bin(int(val,16))) == 18:
				# Добавить парсинг отрицательной температуры
				val1='0b'
				val = bin(int(val, 16))
				mass = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
				for i in mass:
					if val[i] == '0':
						val1 += '1'
					else:
						val1 += '0'
				output_text.insert(output_text.index('insert'), " (температура -" + str((int(val1,2)+1)/100) + " градусов)")
			else:
				output_text.insert(output_text.index('insert'), " (температура +" + str(int(val, 16)/100) + " градусов)")
		elif attribute == "0032":
			output_text.insert(output_text.index('insert'), "    (" + \
			datetime.datetime.utcfromtimestamp(int(val, 16) + 946684800).strftime('%Y-%m-%d %H:%M:%S') + " - Local time)")
	elif cluster == "000a" or cluster == "000A":
		if attribute == "0000" or attribute == "0003" or attribute == "0004":
			output_text.insert(output_text.index('insert'), "    (" + \
			datetime.datetime.utcfromtimestamp(int(val, 16) + 946684800).strftime('%Y-%m-%d %H:%M:%S') + " - UTC time)")
		elif attribute == "0005":
			output_text.insert(output_text.index('insert'), " (сдвиг летнего времени " + str(int(val, 16)/3600) + " часов)")
		elif attribute == "0002":
			TimeZone = int(val, 16)
			if TimeZone > 86400:
				# Добавляем парсинг отрицательной временной зоны
				val1='0b'
				val = bin(TimeZone)
				mass = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33]
				for i in mass:
					if val[i] == '0':
						val1 += '1'
					else:
						val1 += '0'
				output_text.insert(output_text.index('insert'), " (временная зона -" + str((int(val1,2)+1)/3600) + " часов)")
			else:
				output_text.insert(output_text.index('insert'), " (временная зона +" + str(TimeZone/3600) + " часов)")