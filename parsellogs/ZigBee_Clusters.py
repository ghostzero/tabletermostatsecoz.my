#!/usr/bin/env python3

# Проверка кластера на совпадение с объявленными в ZCL и HA
def ZigBee_clusters( cluster ):
	# General clusters
	if cluster == "0000":
		return "Basic"
	elif cluster == "0001":
		return "Power Configuration"
	elif cluster == "0002":
		return "Device Temperature Configuration"
	elif cluster == "0003":
		return "Identify"
	elif cluster == "0004":
		return "Groups"
	elif cluster == "0005":
		return "Scenes"
	elif cluster == "0006":
		return "On/Off"
	elif cluster == "0007":
		return "On/Off Switch Configuration"
	elif cluster == "0008":
		return "Level Control "
	elif cluster == "0009":
		return "Alarms"
	elif cluster == "000a" or cluster == "000A":
		return "Time"
	elif cluster == "000f" or cluster == "000F":
		return "Binary Input (Basic) "
	elif cluster == "0016":
		return "Partition"
	elif cluster == "001a" or cluster == "001A":
		return "Power Profile "
	elif cluster == "001b" or cluster == "001B":
		return "EN50523Appliance Control"
	elif cluster == "0020":
		return "Poll Control"
	# Closures clusters
	elif cluster == "0100":
		return "Shade Configuration"
	elif cluster == "0101":
		return "Door Lock"
	elif cluster == "0102":
		return "Window Covering"
	# HVAC clusters
	elif cluster == "0200":
		return "Pump Configuration and Control"
	elif cluster == "0201":
		return "Thermostat" 
	elif cluster == "0202":
		return "Fan Control"
	elif cluster == "0204":
		return "Thermostat User Interface Configuration"
	# Lighting cluster
	elif cluster == "0300":
		return "Color Control"
	# Measurement & Sensing clusters
	elif cluster == "0400":
		return "Illuminance Measurement"
	elif cluster == "0401":
		return "Illuminance Level Sensing"
	elif cluster == "0402":
		return "Temperature Measurement"
	elif cluster == "0403":
		return "Pressure Measurement"
	elif cluster == "0404":
		return "Flow Measurement"
	elif cluster == "0405":
		return "Relative Humidity Measurement"
	elif cluster == "0406":
		return "Occupancy Sensing"
	# Security and Safety clusters
	elif cluster == "0500":
		return "IAS Zone"
	elif cluster == "0501":
		return "IAS ACE"
	elif cluster == "0502":
		return "IAS WD"
	# Smart Energy cluster
	elif cluster == "0702":
		return "Metering"
	# Home Automation clusters
	elif cluster == "0b00":
		return "EN50523 Appliance Identification"
	elif cluster == "0b01" or cluster == "0B01":
		return "Meter Identification"
	elif cluster == "0b02" or cluster == "0B02":
		return "EN50523 Appliance events and Alert"
	elif cluster == "0b03" or cluster == "0B03":
		return "Appliance statistics"
	elif cluster == "0b04" or cluster == "0B04":
		return "Electricity Measurement"
	elif cluster == "0b05" or cluster == "0B05":
		return "Diagnostics"
	else:
		return "Unknown"
