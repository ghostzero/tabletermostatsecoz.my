#!/usr/bin/env python3

# Парсим тип данных ZigBee и возвращаем длину данных, если это возможно
def ZigBee_data_type( output_text, data_type ):
	if data_type == "00": 
		output_text.insert(output_text.index('insert'), "No data")
		return 0
	elif data_type == "08": 
		output_text.insert(output_text.index('insert'), "8-bit data")
		return 1
	elif data_type == "09": 
		output_text.insert(output_text.index('insert'), "16-bit data")
		return 2
	elif data_type == "0a" or data_type == "0A": 
		output_text.insert(output_text.index('insert'), "24-bit data")
		return 3
	elif data_type == "0b" or data_type == "0B": 
		output_text.insert(output_text.index('insert'), "32-bit data")
		return 4
	elif data_type == "0c" or data_type == "0C": 
		output_text.insert(output_text.index('insert'), "40-bit data")
		return 5
	elif data_type == "0d" or data_type == "0D": 
		output_text.insert(output_text.index('insert'), "48-bit data")
		return 6
	elif data_type == "0e" or data_type == "0E": 
		output_text.insert(output_text.index('insert'), "56-bit data")
		return 7
	elif data_type == "0f" or data_type == "0F": 
		output_text.insert(output_text.index('insert'), "64-bit data")
		return 8
	elif data_type == "10": 
		output_text.insert(output_text.index('insert'), "Boolean")
		return 1
	elif data_type == "18": 
		output_text.insert(output_text.index('insert'), "8-bit bitmap")
		return 1
	elif data_type == "19": 
		output_text.insert(output_text.index('insert'), "16-bit bitmap")
		return 2
	elif data_type == "1a" or data_type == "1A": 
		output_text.insert(output_text.index('insert'), "24-bit bitmap")
		return 3
	elif data_type == "1b" or data_type == "1B": 
		output_text.insert(output_text.index('insert'), "32-bit bitmap")
		return 4
	elif data_type == "1c" or data_type == "1C": 
		output_text.insert(output_text.index('insert'), "40-bit bitmap")
		return 5
	elif data_type == "1d" or data_type == "1D": 
		output_text.insert(output_text.index('insert'), "48-bit bitmap")
		return 6
	elif data_type == "1e" or data_type == "1E": 
		output_text.insert(output_text.index('insert'), "56-bit bitmap")
		return 7
	elif data_type == "1f" or data_type == "1F": 
		output_text.insert(output_text.index('insert'), "64-bit bitmap")
		return 8
	elif data_type == "20": 
		output_text.insert(output_text.index('insert'), "Unsigned 8-bit integer")
		return 1
	elif data_type == "21": 
		output_text.insert(output_text.index('insert'), "Unsigned 16-bit integer")
		return 2
	elif data_type == "22": 
		output_text.insert(output_text.index('insert'), "Unsigned 24-bit integer")
		return 3
	elif data_type == "23": 
		output_text.insert(output_text.index('insert'), "Unsigned 32-bit integer")
		return 4
	elif data_type == "24": 
		output_text.insert(output_text.index('insert'), "Unsigned 40-bit integer")
		return 5
	elif data_type == "25": 
		output_text.insert(output_text.index('insert'), "Unsigned 48-bit integer")
		return 6
	elif data_type == "26": 
		output_text.insert(output_text.index('insert'), "Unsigned 56-bit integer")
		return 7
	elif data_type == "27": 
		output_text.insert(output_text.index('insert'), "Unsigned 64-bit integer")
		return 8
	elif data_type == "28": 
		output_text.insert(output_text.index('insert'), "Signed 8-bit integer")
		return 1
	elif data_type == "29": 
		output_text.insert(output_text.index('insert'), "Signed 16-bit integer")
		return 2
	elif data_type == "2a" or data_type == "2A": 
		output_text.insert(output_text.index('insert'), "Signed 24-bit integer")
		return 3
	elif data_type == "2b" or data_type == "2B": 
		output_text.insert(output_text.index('insert'), "Signed 32-bit integer")
		return 4
	elif data_type == "2c" or data_type == "2C": 
		output_text.insert(output_text.index('insert'), "Signed 40-bit integer")
		return 5
	elif data_type == "2d" or data_type == "2D": 
		output_text.insert(output_text.index('insert'), "Signed 48-bit integer")
		return 6
	elif data_type == "2e" or data_type == "2E": 
		output_text.insert(output_text.index('insert'), "Signed 56-bit integer")
		return 7
	elif data_type == "2f" or data_type == "2F": 
		output_text.insert(output_text.index('insert'), "Signed 64-bit integer")
		return 8
	elif data_type == "30": 
		output_text.insert(output_text.index('insert'), "8-bit enumeration")
		return 1
	elif data_type == "31": 
		output_text.insert(output_text.index('insert'), "16-bit enumeration")
		return 2
	elif data_type == "38": 
		output_text.insert(output_text.index('insert'), "Semi-precision")
		return 2
	elif data_type == "39": 
		output_text.insert(output_text.index('insert'), "Single precision")
		return 4
	elif data_type == "3a"  or data_type == "3A": 
		output_text.insert(output_text.index('insert'), "Double precision")
		return 8
	elif data_type == "41": 
		output_text.insert(output_text.index('insert'), "Octet string")
		return 0xFE
	elif data_type == "42": 
		output_text.insert(output_text.index('insert'), "Character string")
		return 0xFE
	elif data_type == "43": 
		output_text.insert(output_text.index('insert'), "Long octet string")
		return 0xFF		
	elif data_type == "44": 
		output_text.insert(output_text.index('insert'), "Long character string")
		return 0xFF
	elif data_type == "48": 
		output_text.insert(output_text.index('insert'), "Array")
		return 0xFF
	elif data_type == "4c"  or data_type == "4C": 
		output_text.insert(output_text.index('insert'), "Structure")
		return 0xFF
	elif data_type == "50": 
		output_text.insert(output_text.index('insert'), "Set")
		return 0xFF
	elif data_type == "51": 
		output_text.insert(output_text.index('insert'), "Bag")
		return 0xFF
	elif data_type == "e0" or data_type == "E0": 
		output_text.insert(output_text.index('insert'), "Time of day")
		return 4
	elif data_type == "e1" or data_type == "E1": 
		output_text.insert(output_text.index('insert'), "Date")
		return 4
	elif data_type == "e2" or data_type == "E2": 
		output_text.insert(output_text.index('insert'), "UTCTime")
		return 4
	elif data_type == "e8" or data_type == "E8": 
		output_text.insert(output_text.index('insert'), "Cluster ID")
		return 2
	elif data_type == "e9" or data_type == "E9": 
		output_text.insert(output_text.index('insert'), "Attribute ID")
		return 2
	elif data_type == "f0" or data_type == "F0": 
		output_text.insert(output_text.index('insert'), "IEEE address")
		return 8
	elif data_type == "f1" or data_type == "F1": 
		output_text.insert(output_text.index('insert'), "128-bit security key")
		return 16
	else:
		output_text.insert(output_text.index('insert'), "Unknown")
		return 0xFF
