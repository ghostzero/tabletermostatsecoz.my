$(document).ready(function() {
 var toggleImage = function(elem) {
 if ($(elem).hasClass("shown")) {
 $(elem).removeClass("shown").addClass("hidden");
 $("img", elem).attr("src", "./images/notes-add.gif");
 }
 else {
 $(elem).removeClass("hidden").addClass("shown");
 $("img", elem).attr("src", "./images/notes-reject.gif");
 }
 };

 
 $("#usernotes .head").each(function() {
 $(this).prepend("<a class='toggler shown' href='#'><img src='./images/notes-reject.gif' /></a> ");
 });
 $(".refsect1 h3.title .toggler").click(function() {
 $(this).parent().siblings().slideToggle("slow");
 toggleImage(this);
 return false;
 });
 $("#usernotes .head .toggler").click(function() {
 $(this).parent().next().slideToggle("slow");
 toggleImage(this);
 return false;
 });
}); 
