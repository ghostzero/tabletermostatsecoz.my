-- phpMyAdmin SQL Dump
-- version 3.3.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Авг 26 2012 г., 20:21
-- Версия сервера: 5.1.63
-- Версия PHP: 5.3.5-1ubuntu7.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `livephp`
--

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `href` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `name`, `title`, `href`) VALUES
(1, 'array', 'массивы', 'array'),
(2, 'cryptography', 'криптография', 'cryptography'),
(4, 'date_and_time', 'дата и время', 'date_and_time'),
(5, 'general', 'основные', 'general'),
(6, 'math', 'математические', 'math'),
(7, 'regular_expression', 'регулярные выражения', 'regular_expression'),
(8, 'string', 'строки', 'string'),
(9, 'url', 'урл', 'url'),
(10, 'cookbook', 'php рецепты', 'news'),
(12, 'сheat_sheet', 'справочники', 'сheat_sheet'),
(13, 'feedback', 'отзывы', 'feedback');

-- --------------------------------------------------------

--
-- Структура таблицы `function`
--

CREATE TABLE IF NOT EXISTS `function` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category_id` int(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `declaration` text,
  `count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=134 ;

--
-- Дамп данных таблицы `function`
--

INSERT INTO `function` (`id`, `category_id`, `name`, `description`, `declaration`, `count`) VALUES
(1, 1, 'array_search', 'осуществляет поиск данного значения в массиве и возвращает соответствующий ключ в случае удачи.', 'array_search ( mixed $needle , array $haystack [, bool $strict ] )', 70),
(2, 1, 'array_splice', 'удалить последовательность элементов массива и заменить её другой последовательностью.', 'array array_splice ( array &$input , int $offset [, int $length ] [, mixed $replacement ] )', 30),
(3, 1, 'array_values', 'возвращает индексный массив, содержащий все значения массива input. ', 'array array_values ( array input )\r\n', 13),
(4, 1, 'count', 'возвратить количество элементов переменной var, которая обычно является array, или любым другим объектом, который может содержать хотя бы один элемент. ', 'int count ( mixed var [, int mode] )', 7),
(5, 1, 'in_array', 'ищет в haystack значение needle и возвращает TRUE в случае удачи, FALSE в противном случае.Если третий параметр strict установлен в TRUE тогда функция in_array() также проверит соответствие types параметра needle и соответствующего значения массива haystack. ', 'bool in_array ( mixed needle, array haystack [, bool strict] )', 5),
(6, 1, 'ksort', 'сортирует массив по ключам, сохраняя отношения между ключами и значениями. Функция полезна, в основном, для работы с ассоциативными массивами.\r\nВозвращает TRUE в случае успешного завершения или FALSE в случае возникновения ошибки. ', 'bool ksort ( array &array [, int sort_flags] )', 7),
(7, 1, 'krsort', 'сортирует массив по ключам в обратном порядке, сохраняя отношения между ключами и значениями. Функция полезна, в основном, для работы с ассоциативными массивами. ', 'bool krsort ( array &array [, int sort_flags] )\r\n', 3),
(8, 1, 'natcasesort', 'реализует алгоритм сортировки, при котором порядок буквенно-цифровых строк будет привычным для человека. Такой алгоритм называется "natural ordering". ', 'void natcasesort ( array &array )\r\n', 3),
(9, 1, 'natsort', 'реализует алгоритм сортировки, при котором порядок буквенно-цифровых строк будет привычным для человека. Такой алгоритм называется "natural ordering". Отличие алгоритма "natural ordering" от обычных алгоритмов сортировки, применяемых, например, функцией sort() ', 'void natsort ( array &array )', 3),
(10, 1, 'range', 'возвращает массив элементов с low по high, включительно. Если low > high, последовательность будет убывающей. ', 'array range ( number low, number high [, number step] )\r\n', 3),
(11, 1, 'shuffle', 'перемещает элементы массива в случайном порядке.\r\n\r\n', 'bool shuffle ( array &array )', 2),
(12, 1, 'rsort', 'сортирует массив в обратном порядке (от большего к меньшему).Возвращает TRUE в случае успешного завершения или FALSE в случае возникновения ошибки.\r\n\r\n\r\n', 'bool rsort ( array &array [, int sort_flags] )\r\n', 4),
(13, 1, 'sort', 'сортирует массив. После завершения работы функции элементы массива будут расположены в порядке возрастания.Замечание: Эта функция назначает новые ключи для элементов array. Все ранее назначенные значения ключей будут удалены, вернее переназначены.Возвращает TRUE в случае успешного завершения или FALSE в случае возникновения ошибки. ', 'bool sort ( array &array [, int sort_flags] )', 5),
(14, 2, 'convert_uudecode', 'преобразует строку из формата uuencode в обычный вид. ', 'string convert_uudecode ( string data )', 6),
(15, 2, 'convert_uuencode', 'преобразует строку data в формат uuencode.\r\n\r\n', 'string convert_uuencode ( string data )\r\n', 3),
(16, 2, 'crc32', 'функция вычисляет контрольную сумму по алгоритму CRC32 для строки str. Это обычно используется для контроля правильности передачи данных. ', 'int crc32 ( string str )\r\n', 3),
(17, 2, 'crypt', 'возвращает строку, зашифрованную по стандартному алгоритму UNIX, основанному на DES, или другому алгоритму, имеющемуся в системе. Аргументами являются строка, которую требуется зашифровать, и необязательная salt-последовательность, на которой основывается шифрование. Для получения дополнительной информации обратитесь к руководству UNIX по функции crypt. Если аргумент salt не передан, он будет выбран случайным образом. ', 'string crypt ( string str [, string salt] )\r\n', 1),
(18, 2, 'hash', 'returns a string containing the calculated message digest as lowercase hexits unless raw_output is set to true in which case the raw binary representation of the message digest is returned. ', 'string hash ( string algo, string data [, bool raw_output] )', 8),
(19, 2, 'md5', 'вычисляет MD5 хэш строки str используя алгоритм MD5 RSA Data Security, Inc. и возвращает этот хэш.', 'string md5 ( string str [, bool raw_output] )\r\n', 4),
(20, 2, 'mhash', 'применяет хэш-функцию hash к данным data и возвращает результирующий хэш (называемый также digest/дайджест). Если key специфицирован, возвращается результирующий HMAC. HMAC это хэширование с ключам для аутентификации сообщения, либо просто дайджест сообщения, который зависит от специфицированного ключа. Не все алгоритмы, поддерживаемые в mhash, могут использоваться в режиме HMAC. При ошибке возвращает FALSE.', 'string mhash (int hash, string data [, string key])', 10),
(21, 2, 'sha1', 'возвращает хэш строки str, вычисленный по алгоритму US Secure Hash Algorithm 1. Хэш представляет собой 40-разрядное шестнадцатиричное число. Если необязательный аргумент raw_output имет значение TRUE, хэш возвращается в виде двоичной строки из 20 символов. ', 'string sha1 ( string str [, bool raw_output] )\r\n', 4),
(22, 4, 'date', 'возвращает время, отформатированное в соответствии с аргументом format, используя метку времени, заданную аргументом timestamp или текущее системное время, если timestamp не задан. Другими словами, timestamp является необязательным и по умолчанию равен значению, возвращаемому функцией time(). ', 'string date ( string format [, int timestamp] )', 2),
(23, 4, 'idate', 'returns a number formatted according to the given format string using the given integer timestamp or the current local time if no timestamp is given. In other words, timestamp is optional and defaults to the value of time(). ', 'int idate ( string format [, int timestamp] )', 8),
(24, 4, 'jdtounix', 'функция возвратит UNIX timestamp, соответствующий Julian Day jday, или FALSE, если jday не находится в пределах UNIX-эпохи (Григорианские годы от 1970 до 2037, или 2440588 <= jday <= 2465342). Возвращается локальное время (а не GMT).', 'int jdtounix (int jday)', 5),
(25, 4, 'jewishtojd', 'конвертирует дату Иудейского календаря в Julian Day Count.', 'int jewishtojd ( int month, int day, int year )\r\n', 1),
(26, 4, 'mktime', 'функция возвращает метку времени Unix, соответствующую дате и времени, заданным аргументами. Метка времени - это цело число, равное разнице в секундах между заданной датой/временем и началом Эпохи Unix (The Unix Epoch, 1 января 1970 г).\r\n\r\nАргументы могут быть опущены в порядке справа налево. Опущенные аргументы считаются равными соответствующим компонентам локальной даты/времени.\r\n\r\nАргумент is_dst может быть установлен в 1, если заданной дате соответствует летнее время, 0 в противном случае, или -1 (значение по умолчанию), если неизвестно, действует ли летнеее время на заданную дату. В последнем случае PHP пытается определить это самостоятельно. Это можно привести к неожиданному результату (который, тем не менее, не будет неверным). ', 'int mktime ( [int hour [, int minute [, int second [, int month [, int day [, int year [, int is_dst]]]]]]] )', 1),
(27, 4, 'strftime', 'возвращает строку, отформатированную в соответствии с аргументом format, используя аргумент timestamp или текущее системное время, если этот аргумент не передан. Названия месяцев, дней недели и другие строки, зависящие от языка, соответствуют текущей локали, установленной функцией setlocale(). ', 'string strftime ( string format [, int timestamp] )', 5),
(28, 4, 'strptime', 'returns an array with the timestamp parsed, or FALSE on error. ', 'array strptime ( string $date , string $format )', 6),
(29, 4, 'strtotime', 'преобразует текстовое представление даты на английском языке в метку времени Unix ', 'int strtotime ( string time [, int now] )', 15),
(30, 4, 'unixtojd', 'конвертирует UNIX timestamp в Julian Day.', 'int unixtojd ([int timestamp])', 2),
(31, 5, 'intval', 'возвращает integer-значение переменной var, используя для конвертации специфицированную базу/base (по умолчанию base равна 10).', 'int intval (mixed var [, int base])', 10),
(32, 5, 'json_decode', 'преобразование данных из формата JSON в массив ', 'mixed json_decode ( string json [, bool assoc] )', 5),
(33, 5, 'json_encode', 'конвертация данных в формат JSON ', 'string json_encode ( mixed value )', 7),
(34, 5, 'serialize', 'генерирует хранимое представление значения.', 'string serialize (mixed value)', 5),
(35, 5, 'strval', 'получает строковое значение переменной.', 'string strval (mixed var)', 3),
(36, 5, 'unserialize', 'создаёт PHP-значение из хранимого представления', 'mixed unserialize (string str)', 2),
(37, 6, 'abs', 'модуль числа ', 'number abs ( mixed number )\r\n', 1),
(38, 6, 'acos', 'возвращает арккосинус аргумента arg в радианах. acos() это дополнительная функция к cos(), что означает, что a==cos(acos(a)) для каждого значения в диапазоне acos()', 'float acos ( float arg )', 8),
(39, 6, 'acosh', 'инверсный гиперболический косинус', 'float acosh ( float arg) ', 3),
(40, 6, 'asin', 'возвращает арксинус аргумента arg в радианах. asin() это дополнительная функция к sin(), т.е. a==sin(asin(a)) для каждого значения в диапазоне asin().', 'float asin ( float arg)', 1),
(41, 6, 'asinh', 'возвращает инверсный гиперболический синус аргумента arg, т.е. значение, гиперболический синус которого является arg.', 'float asinh ( float arg)', 1),
(42, 6, 'atan2', 'вычисляет арктангенс переменных x и y. Это напоминает вычисление арктангенса y / x, за исключением того, что знаки обоих аргументов используются для определения квадранта результата.', 'float atan2 ( float y, float x)', 1),
(43, 6, 'atan', 'возвращает арктангенс аргумента arg в радианах. atan() это дополнительная функция к tan(), то есть a==tan(atan(a)) для каждого значения в диапазоне atan().\r\n\r\n', 'float atan ( float arg)', 1),
(44, 6, 'atanh', 'возвращает инверсный гиперболический тангенс аргумента arg, т.е. значение, чей гиперболический тангенс равен arg.', 'float atanh ( float arg)', 1),
(45, 6, 'base_convert', 'конвертирует число между различными базами. ', 'string base_convert ( string number, int frombase, int tobase)', 8),
(46, 6, 'bindec', 'возвращает 10-теричный эквивалент двоичного числа, представленного аргументом binary_string. ', 'int bindec ( string binary_string)', 4),
(47, 6, 'ceil', 'возвращает ближайшее большее целое от value. Тип возвращаемого значения остаётся float т.к. диапазон float больше integer. ', 'float ceil ( float value )\r\n', 4),
(48, 6, 'cos', 'возвращает косинус параметра arg. Параметр arg задан в радианах.', 'float cos ( float arg)', 0),
(49, 6, 'cosh', 'возвращает гиперболический косинус arg, определённый как (exp(arg) + exp(-arg))/2.', 'float cosh ( float arg)', 0),
(50, 6, 'decbin', 'возвращает строку - двоичное представление данного аргумента number. Наибольшее возможное число для конвертации, 4294967295 в 10-ричном выражении, результирует в строку из 32 единиц.', 'string decbin ( int number)', 1),
(51, 6, 'dechex', 'возвращает последовательность, содержащую шестнадцатеричное представление данного аргумента числа. Наибольшее число, которое может быть преобразовано, 4294967295 в десятичном числе, заканчивающемся к "ffffffff".', 'string dechex ( int number )', 1),
(52, 6, 'decoct', 'возвращает строку - 8-ричное представление данного аргумента number. Наибольшее 10-ричное число, которое может быть конвертировано, это 2147483647, дающее "17777777777".', 'string decoct ( int number)', 1),
(53, 6, 'deg2rad', 'конвертирует градусы в радианы.', 'float deg2rad ( float number)', 1),
(54, 6, 'exp', 'e в степени', 'float exp ( float arg)', 2),
(55, 6, 'expm1', 'возвращает exp(number) - 1, высчитанное способом, обеспечивающим точность, даже если значение близко к нулю.', 'float expm1 ( float number)', 0),
(56, 6, 'floor', 'округляет дробь в меньшую сторону', 'float floor ( float value )', 2),
(57, 6, 'fmod', 'возвращает дробный остаток от деления десятичных дробей x и y). Остаток (r) определяется так: x = i * y + r, где i - некоторое целое. r всегда имеет такоей же знак, как и x и модуль, меньший или равный модулю y.\r\n\r\n\r\n', 'float fmod ( float x, float y )', 1),
(58, 6, 'hexdec', 'возвращает десятеричный эквивалент 16-ричного числа, заданного аргументом hex_string. hexdec() конвертирует 16-ричную строку в 10-ричное число. Наибольшее конвертируемое число это 7fffffff, или 2147483647 в 10-ричном выражении.', 'int hexdec (string hex_string)', 1),
(59, 6, 'hypot', 'возвращает sqrt( num1*num1 + num2*num2)', 'float hypot (float num1, float num2)', 0),
(60, 6, 'is_finite', 'возвращает TRUE, если val является допустимым конечным числом в пределах диапазона чисел с плавающей точкой PHP на данной платформе.', 'bool is_finite ( float $val )', 1),
(61, 6, 'is_infinite', 'возвращает TRUE, если val является допустимым конечным числом в пределах диапазона чисел с плавающей точкой PHP на данной платформе.', 'bool is_finite (float val)\r\n\r\n', 0),
(62, 6, 'is_nan', 'возвращает TRUE, если val является ''not a number/не-числом'', вроде результата acos(1.01).', 'bool is_nan (float val)', 14),
(63, 6, 'log10', 'возвращает логарифм base-10 аргумента arg.', 'float log10 (float arg)', 3),
(64, 6, 'log1p', 'возвращает log(1 + number), вычисленный способом, который точен даже когда значение number близко к нулю.', 'float log1p (float number)', 1),
(65, 6, 'log', 'возвращает значение натурального логарифма.', 'float log ( float $arg [, float $base = M_E ] )', 1),
(66, 6, 'max', 'возвращает наибольшее из предложенных чисел.', 'mixed max ( number arg1, number arg2 [, number ...] ) or \r\nmixed max ( array numbers )\r\n', 10),
(67, 6, 'min', 'возвращает наименьшее из предложенных чисел. ', 'mixed min ( number arg1, number arg2 [, number ...] ) or\r\n\r\nmixed min ( array numbers )', 5),
(68, 6, 'mt_rand', 'генерирует наилучшее случайное значение.', 'int mt_rand ([int min, int max])', 2),
(69, 6, 'octdec', 'возвращает десятеричный эквивалент 8-ричного числа, представленного аргументом octal_string. Наибольшее число, которое может быть конвертировано, 17777777777, или 2147483647 в десятеричном выражении.', 'int octdec (string octal_string)', 1),
(70, 6, 'pow', 'возвращает base, возведённую в степень exp. Если возможно, эта функция возвращает integer.', 'number pow (number base, number exp)', 6),
(71, 6, 'rad2deg', 'конвертирует количество радиан в эквивалентное количество градусов.', 'float rad2deg (float number)', 1),
(73, 6, 'rand', 'возвращает псевдослучайное целое в диапазоне от 0 до RAND_MAX. Например, если вам нужно случайное число между 5 и 15 (включительно), вызовите rand (5, 15). ', 'int rand ( [int min, int max] )\r\n', 1),
(74, 6, 'round', 'возвращает округлённое значение val с указанной точностью precision (количество цифр после запятой). Последняя может быть отрицательной или нулём (по умолчанию). ', 'float round ( float val [, int precision] )', 1),
(75, 6, 'sin', 'возвращает синус аргумента arg.Параметр arg задан в радианах.', 'float sin (float arg)', 1),
(76, 6, 'sinh', 'возвращает гиперболический синус аргумента arg, определённый как (exp(arg) - exp(-arg))/2.', 'float sinh (float arg)', 1),
(77, 6, 'sqrt', 'возвращает квадратный корень аргумента arg.', 'float sqrt (float arg)', 3),
(78, 6, 'tan', 'возвращает тангенс параметра arg.Параметр arg задан в радианах.', 'float tan (float arg)', 1),
(79, 7, 'preg_filter', 'выполняет поиск в $subject с помошью регулярных выражении $pattern и вставляет найденное в шаблон $replace найденное\r\n', 'mixed preg_filter ( mixed $pattern , mixed $replacement , mixed $subject [, int $limit = -1 [, int &$count ]] )', 10),
(80, 7, 'preg_grep', 'возвращает массив, состоящий из элементов входящего массива input, которые соответствуют заданному шаблону pattern. \r\n', 'array preg_grep ( string $pattern , array $input [, int $flags ] )', 10),
(81, 7, 'preg_match', 'ищет в заданном тексте subject совпадения с шаблоном pattern\r\nВ случае, если дополнительный параметр matches указан, он будет заполнен результатами поиска. Элемент $matches[0] будет содержать часть строки, соответствующую вхождению всего шаблона, $matches[1] - часть строки, соответствующую первой подмаске, и так далее. ', 'mixed preg_match ( string pattern, string subject [, array &matches [, int flags [, int offset]]] )\r\n', 54),
(82, 7, 'preg_match_all', 'ищет в строке subject все совпадения с шаблоном pattern и помещает результат в массив matches в порядке, определяемом комбинацией флагов flags.После нахождения первого соответствия последующие поиски будут осуществляться не с начала строки, а от конца последнего найденного вхождения. ', 'int preg_match_all ( string pattern, string subject, array &matches [, int flags [, int offset]] )', 80),
(83, 7, 'preg_quote', ' принимает строку str и добавляет обратный слеш перед каждым служебным символом. Это бывает полезно, если в составлении шаблона участвуют строковые переменные, значение которых в процессе работы скрипта может меняться. ', 'string preg_quote ( string str [, string delimiter] )', 8),
(84, 7, 'preg_split', 'возвращает массив, состоящий из подстрок заданной строки subject, которая разбита по границам, соответствующим шаблону pattern. ', 'array preg_split ( string pattern, string subject [, int limit [, int flags]] )\r\n', 1),
(85, 8, 'addslashes', 'возвращает сроку str, в которой перед каждым спецсимволом добавлен обратный слэш (\\), например для последующего использования этой строки в запросе к базе данных. Экранируются одиночная кавычка (''), двойная кавычка ("), обратный слэш (\\) и NUL (байт NULL). ', 'string addslashes ( string str )', 3),
(86, 8, 'chr', 'возвращает строку из одного символа, код которого задан аргументом ascii.', 'string chr ( int ascii )', 8),
(87, 8, 'chunk_split', 'функция используется для разбиения строки на фрагменты, например, для приведения результата функции base64_encode() в соответствие с требованиями RFC 2045. Она вставляет строку end (по умолчанию "\\r\\n") после каждых chunklen символов (по умолчанию 76). Возвращает преобразованную строку без изменения исходной.', 'string chunk_split ( string body [, int chunklen [, string end]] )\r\n', 10),
(88, 8, 'count_chars', 'возвращает информацию о символах, входящих в строку', 'mixed count_chars ( string string [, int mode] )', 5),
(89, 8, 'explode', 'возвращает массив строк, полученных разбиением строки string с использованием separator в качестве разделителя. Если передан аргумент limit передан, массив будет содержать максимум limit элементов, при этом последний элемент будет содержать остаток строки string.', 'array explode ( string separator, string string [, int limit] )\r\n', 4),
(90, 8, 'html_entity_decode', 'преобразует HTML сущности в соответствующие символы ', 'string html_entity_decode ( string string [, int quote_style [, string charset]] )\r\n', 13),
(91, 8, 'htmlentities', 'функция идентична htmlspecialchars() за исключением того, что htmlentities() преобразует все символы в соответствющие HTML сущности (для тех символов, для которых HTML сущности существуют). ', 'string htmlentities ( string string [, int quote_style [, string charset]] )\r\n', 14),
(92, 8, 'htmlspecialchars', 'преобразует специальные символы в HTML сущности ', 'string htmlspecialchars ( string string [, int quote_style [, string charset]] )', 24),
(93, 8, 'implode', 'возвращает строку, полученную объединением строковых представлений элементов массива pieces, со вставкой строки glue между соседними элементами. ', 'string implode ( string glue, array pieces )\r\n', 1),
(94, 8, 'levenshtein', 'вычисляет расстояние Левенштейна между двумя строками\r\n', 'int levenshtein ( string str1, string str2, int cost_ins, int cost_rep, int cost_del )', 2),
(95, 8, 'parse_str', 'Разбирает строку str,которая должна иметь формат строки запроса URL и присваивает значения переменным в текущем контексте, если не передан второй аргумент arr. В последнем случае значения будкт сохранены в этой переменной как элементы массива. ', 'void parse_str ( string str [, array arr] )\r\n', 6),
(96, 8, 'similar_text', 'вычисляет степень похожести двух строк ', 'int similar_text ( string first, string second [, float percent] )', 1),
(97, 8, 'soundex', 'возвращает ключ soundex для строки', 'string soundex ( string str )', 6),
(98, 8, 'str_pad', 'возвращает строку input, дополненную слева, справа или с обоих сторон до заданной аргументом pad_length длины строкой pad_string. По умолчанию pad_string содержит пробел. ', 'string str_pad ( string input, int pad_length [, string pad_string [, int pad_type]] )', 2),
(99, 8, 'str_replace', 'функция возвращает строку или массив subject, в котором все вхождения search заменены на replace. Если не нужны сложные правила поиска/замены, использование этой функции предпочтительнее ereg_replace() или preg_replace(). ', 'mixed str_replace ( mixed search, mixed replace, mixed subject [, int &count] )\r\n', 7),
(100, 8, 'str_rot13', 'выполняет над строкой str преобразование ROT13 и возвращает полученную строку. Преобразование ROT13 заключается в простом сдвиге каждой латинской буквы на 13 позиций в алфавите, остальные символы не изменяются. Обратное преобразование выполняется той же функцией. ', 'string str_rot13 ( string str )', 1),
(101, 8, 'stripos', 'возвращает позицию первого вхождения подстроки needle в строку haystack. В отличие от strpos(), эта функция не учитывает регистр символов. ', 'int stripos ( string haystack, string needle [, int offset] )\r\n', 2),
(102, 8, 'stristr', 'возвращает подстроку строки haystack начиная с первого вхождения needle до конца строки. Эта функция не учитывает регистр сиволов. ', 'string stristr ( string haystack, string needle )', 8),
(103, 8, 'strlen', 'возвращает длину строки', 'int strlen ( string string )', 2),
(104, 8, 'strncmp', 'функция подобна strcmp(), за исключением того, что можно указать максимальное количество символов в обоих строках, которые будут участвовать в сравнении.\r\nВозвращает отрицательное число, если str1 меньше, чем str2; положительное число, если str1 больше, чем str2, и 0 если строки равны. ', 'int strncmp ( string str1, string str2, int len )', 13),
(105, 8, 'strpos', 'возвращает позицию первого вхождения подстроки needle в строку haystack. В отличие от strrpos(), использует всю строку needle, а не только первый символ. ', 'int strpos ( string haystack, string needle [, int offset] )\r\n', 12),
(106, 8, 'strstr', 'возвращает подстроку строки haystack начиная с первого вхождения needle до конца строки.Если подстрока needle не найдена, возвращает FALSE.Если needle не является строкой, он приводится к целому и трактуется как код символа. ', 'string strstr ( string haystack, string needle )', 7),
(107, 8, 'strtolower', 'возвращает строку string, в которой все буквенные символы переведены в нижний регистр.Принадлежность того или иного символа к буквенным определяется с учетом текущей локали. Это означает, что, например, в используемой по умолчанию локали "C" locale, символ ? не будет преобразован. ', 'string strtolower ( string str )', 1),
(108, 8, 'strtoupper', 'Возвращает строку string, в которой все буквенные символы переведены в верхний регистр.Принадлежность того или иного символа к буквенным определяется с учетом текущей локали. Это означает, что, например, в используемой по умолчанию локали "C" locale, символ ? не будет преобразован. ', 'string strtoupper ( string string )', 1),
(109, 8, 'strtr', 'возвращает строку str, в которой каждое вхождение любого символа из перечисленных в from заменено на соответствующий символ из строки to.\r\nЕсли длины строк from и to отличаются, "лишние" символы в более длинной строке не используются.\r\n\r\n', 'string strtr ( string str, string from, string to )\r\n', 5),
(110, 8, 'substr', 'возвращает подстроку строки string длиной length, начинающегося с start символа по счету. ', 'string substr ( string string, int start  [, int length] )', 1),
(111, 8, 'substr_count', 'возвращает число вхождений подстроки needle в строку haystack. Заметьте, что поиск ведется с учетом регистра символов. ', 'int substr_count ( string $haystack , string $needle [, int $offset= 0  ] [, int $length ] )', 5),
(112, 8, 'trim', 'удаляет пробелы из начала и конца строки ', 'string trim ( string str [, string charlist] )', 15),
(113, 8, 'ucfirst', 'преобразует первый символ строки в верхний регистр', 'string ucfirst ( string str )', 3),
(114, 8, 'ucwords', 'преобразует в верхний регистр первый символ каждого слова в строке ', 'string ucwords ( string str )', 3),
(115, 8, 'vsprintf', 'возвращает отформатированную строку', 'string vsprintf ( string format, array args )\r\n', 1),
(116, 8, 'wordwrap', 'возвращает строку str с переносом в столбце с номером, заданном аргументом width. Строка разбивется с помощью аргумента break.\r\nАргументы width и break необязательны и по умолчанию равны 75 и ''\\n'' соответственно. ', 'string wordwrap ( string str [, int width [, string break [, boolean cut]]] )\r\n', 2),
(117, 9, 'base64_decode', 'декодирует данные, кодированные способом MIME base64. ', 'string base64_decode ( string encoded_data)', 2),
(118, 9, 'base64_encode', 'кодирует данные способом MIME base64.', 'string base64_encode ( string data)', 1),
(119, 9, 'http_build_query', 'генерирует URL-кодированную строку с ассоциативного (или индексированного) массивов.', 'string http_build_query ( array formdata [, string numeric_prefix] )', 5),
(120, 9, 'parse_url', 'функция возвращает ассоциативный массив, возвращающий любые имеющиеся компоненты URL', 'array parse_url (string url)', 4),
(121, 9, 'rawurldecode', 'возвращает строку, в которой последовательности из знака процентов (%) и последующих двух 16-ричных цифр заменяются литеральными символами. Например, строка', 'string rawurldecode (string str)', 1),
(122, 9, 'rawurlencode', 'URL-кодирование в соответствии с RFC1738', 'string rawurlencode (string str)', 2),
(123, 9, 'urldecode', 'декодирует URL-кодированную строку', 'string urldecode (string str)', 7),
(124, 9, 'urlencode', 'URL-кодирует строку', 'string urlencode (string str)', 3),
(126, 4, 'time', 'возвращает текущую метку времени', 'int time ( void )', 7),
(127, 8, 'strip_tags', 'функция возвращает строку str, из которой удалены HTML и PHP тэги. Для удаления тэго используется автомат, аналогичный примененному в функции fgetss(). ', 'string strip_tags ( string str [, string allowable_tags] )', 1),
(128, 7, 'preg_replace', 'выполняет поиск в строке subject совпадений с шаблоном pattern и заменяет их на replacement. В случае, если параметр limit указан, будет произведена замена limit вхождений шаблона; в случае, если limit опущен либо равняется -1, будут заменены все вхождения шаблона.', 'mixed preg_replace ( mixed pattern, mixed replacement, mixed subject [, int limit] )', 25),
(129, 5, 'uniqid', 'возвращает уникальный идентификатор с префиксом на основе текущего времени в микросекундах. Это prefix может использоваться, например, если вы генерируете идентификаторы одновременно на нескольких хостах, что может сгенерировать идентификатор в ту же микросекунду. Prefix может быть длиной до 114 символов.', 'string uniqid ([ string $prefix = "" [, bool $more_entropy = false ]] )', 19),
(130, 1, 'array_slice', 'возвращает последовательность элементов массива array, определённую параметрами offset и length. Если параметр offset положителен, последовательность начнётся на расстоянии offset от начала array. Если offset отрицателен, последовательность начнётся на расстоянии offset от конца array. Обратите внимание, что array_slice() сбрасывает ключи массива. Начиная с PHP 5.0.2 вы можете переопределить это поведение, установив параметр preserve_keys в TRUE.\r\n', 'array array_slice ( array array, int offset [, int length [, bool preserve_keys]] )', 19),
(131, 1, 'array_diff', 'вычисляет расхождение в массивах. Возвращает массив, состоящий из значений массива, которые отсутствуют в любом другом массиве, перечисленном в последующих аргументах. Обратите внимание, что ключи массивов сохраняются. ', 'array array_diff ( array array1, array array2 [, array ...] )', 4),
(132, 1, 'array_slice', 'возвращает последовательность элементов массива array, определённую параметрами offset и length. Если параметр offset положителен, последовательность начнётся на расстоянии offset от начала array. Если offset отрицателен, последовательность начнётся на расстоянии offset от конца array. Обратите внимание, что array_slice() сбрасывает ключи массива. Начиная с PHP 5.0.2 вы можете переопределить это поведение, установив параметр preserve_keys в TRUE.\r\n', 'array array_slice ( array array, int offset [, int length [, bool preserve_keys]] )', NULL),
(133, 1, 'array_diff', 'вычисляет расхождение в массивах. Возвращает массив, состоящий из значений массива, которые отсутствуют в любом другом массиве, перечисленном в последующих аргументах. Обратите внимание, что ключи массивов сохраняются. ', 'array array_diff ( array array1, array array2 [, array ...] )', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `content` longtext,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `title`, `content`, `date`) VALUES
(1, 'проверка модулей php', 'Иногда при смене хостинга или установке с нуля на сервер php, возникает необходимость проверить установлены ли все необходимые модули php.\nМожно проверить это через phpinfo, можно не проверять, натыкаясь на\nошибки после запуска сайта, а можно использовать например такой скрипт:\n[code]\n$libs = array("gd", "dom", "curl", "hash", "iconv", "json", "mbstring", "mysql", "openssl", "pcre", "session", "simplexml");\n\n$i = 0;\n$all_is_ok = true;\nforeach ($libs as $lib)\n{\n	$i++;\n	if (!extension_loaded($lib)) {\n		echo "$i. Library <b style=''color:red;''>$lib</b> IS NOT LOADED<br>";\n		$all_is_ok = false;\n	} else {\n		echo "$i. <b>$lib</b> is loaded<br>";\n	}\n}\n\n\nif ($all_is_ok) echo "<br> SUCCESS. All needed modules of PHP was loaded"; else echo "<br> Some needed modules was not loaded";\n\n[/code]', '2011-02-17 11:51:28'),
(2, 'сортировка двумерного ассоциативного массива', 'Иногда возникает вопрос как отсортировать вложенный ассоциативный массив по определенному ключу в порядке возрастания или убывания его значения - в этом нам поможет функция uasort. \nДопустим имеем массив пользователей, который содержит в себе "Имя" и "Рейтинг",\nнам необходимо отсортировать список от меньшего к большему рейтингов.\n\n[code]\n$array = array(\n	array(\n			''name'' => ''Andrey'',\n			''rate'' => 10\n		),\n	array(\n			''name'' => ''Sergey'',\n			''rate'' => 1,\n		),\n	array(\n			''name'' => ''Dmitriy'',\n			''rate'' => 5\n	),\n);\n\n\nuasort($array, ''sortrate'');//второй параметр - имя функции "сортировщика"\n\nfunction sortrate($a, $b)\n{\n	$key = ''rate'';//имя ключа, по которому производим сортировку\n	if ($a[$key] == $b[$key]) {\n		return 0;\n	}\n	return ($a[$key] < $b[$key]) ? -1 : 1;//меняем знак, если хотим по убыванию сортировать\n}\n\nprint_r($array);\n[/code]\n\n\n', '2011-02-21 13:11:03'),
(3, 'fsockopen и использование другого сетевого интерфейса.', 'Иногда возникают ситуации при работе с сокетами, когда необходимо выполнить подключение через другую сетевую карту на сервере(если таковая имеется). Как известно, fsockopen не поддерживает возможность указания ip адреса, поэтому воспользуемся функциями stream_context_create и stream_socket_client.\nПример:\n[code]\n$opts = array( ''socket'' => array( ''bindto'' => ''xx.xx.xx.xx'' ) );//ip сетевой карты\n$context = stream_context_create( $opts );\n$fp = stream_socket_client( "ssl://server.com:443", $errno, $errstr, 30, STREAM_CLIENT_CONNECT, $context);\n[/code]', '2011-02-23 13:32:21'),
(4, 'phpmyadmin количество выводимых записей в таблице по умолчанию', 'Иногда умолчальное количество выводимых записей в таблице (30) \nочень не хватает.\nПоменять его можно зайдя в файл /etc/phpmyadmin/config.inc.php\nи добавив строку\n[code]\n$cfg[''MaxRows''] = 500;\n[/code]\n', '2011-02-23 18:02:53');

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`) VALUES
(1, 'login', 'Разрешён вход в систему.'),
(2, 'admin', 'Пример специфичной роли - Администратор');

-- --------------------------------------------------------

--
-- Структура таблицы `roles_users`
--

CREATE TABLE IF NOT EXISTS `roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `roles_users`
--

INSERT INTO `roles_users` (`user_id`, `role_id`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `static`
--

CREATE TABLE IF NOT EXISTS `static` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `content` longtext,
  `status` tinyint(1) DEFAULT NULL,
  `title` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `static`
--

INSERT INTO `static` (`id`, `name`, `content`, `status`, `title`) VALUES
(1, 'cheat_sheet', '<meta content="text/html; charset=utf-8" http-equiv="content-type" />\n<ol>\n	<li>\n		<h3>\n			Таблица кодов уровня ошибок php <a href="http://livephp.net/static/error_table.html">перейти</a></h3>\n	</li>\n	<li>\n		<h3>\n			Таблица url кодов <a href="http://livephp.net/static/url_encoding_table.html">перейти</a></h3>\n	</li>\n	<li>\n		<h3>\n			Таблица магических констант <a href="http://livephp.net/static/magic_constants_php.html">перейти</a></h3>\n	</li>\n	<li>\n		<h3>\n			Русская документация php&nbsp;<a href="http://livephp.net/doc/php_manual_ru.chm" title="русская документация php скачать">скачать</a></h3>\n	</li>\n</ol>\n', 1, 'Справочники'),
(2, 'def', '<p>\n	<em>Генератор</em> карты сайта Sitemap онлайн. Создавайте Sitemap быстро и удобно, используя протокол <em>Sitemap XML</em>!</p>\n', 0, 'деф'),
(3, 'error_table', '<table border="1" cellpadding="10" cellspacing="1" style="width: 500px;">\n	<tbody>\n		<tr>\n			<td>\n				Значение</td>\n			<td>\n				Константа</td>\n		</tr>\n		<tr>\n			<td>\n				1</td>\n			<td>\n				E_ERROR</td>\n		</tr>\n		<tr>\n			<td>\n				2</td>\n			<td>\n				E_WARNING</td>\n		</tr>\n		<tr>\n			<td>\n				4</td>\n			<td>\n				E_PARSE</td>\n		</tr>\n		<tr>\n			<td>\n				8</td>\n			<td>\n				E_NOTICE</td>\n		</tr>\n		<tr>\n			<td>\n				16</td>\n			<td>\n				E_CORE_ERROR</td>\n		</tr>\n		<tr>\n			<td>\n				32</td>\n			<td>\n				E_CORE_WARNING</td>\n		</tr>\n		<tr>\n			<td>\n				64</td>\n			<td>\n				E_COMPILE_ERROR</td>\n		</tr>\n		<tr>\n			<td>\n				128</td>\n			<td>\n				E_COMPILE_WARNING</td>\n		</tr>\n		<tr>\n			<td>\n				256</td>\n			<td>\n				E_USER_ERROR</td>\n		</tr>\n		<tr>\n			<td>\n				512</td>\n			<td>\n				E_USER_NOTICE</td>\n		</tr>\n		<tr>\n			<td>\n				1024</td>\n			<td>\n				E_ALL</td>\n		</tr>\n		<tr>\n			<td>\n				2048</td>\n			<td>\n				E_STRICT</td>\n		</tr>\n		<tr>\n			<td>\n				4096</td>\n			<td>\n				E_RECOVERABLE_ERROR</td>\n		</tr>\n	</tbody>\n</table>\n<p>\n	&nbsp;</p>\n', 0, 'Таблица кодов уровня ошибок'),
(4, 'dima', '<p>\n	http://livephp/static/vlad.html</p>\n', 0, 'дима'),
(5, 'name2', '<p>\n	sdfsdf</p>\n', 0, 'имя2'),
(6, '2', '<p>\n	2</p>\n', 0, '2'),
(7, 'name', '<p>\n	asdfasdfasdfsdafas</p>\n', 0, 'title'),
(8, 'url_encoding_table', '<table border="1" cellpadding="3" cellspacing="2">\n	<tbody>\n		<tr>\n			<th>\n				Символ</th>\n			<th>\n				Назначение в URL</th>\n			<th>\n				Код</th>\n		</tr>\n		<tr>\n			<td align="center">\n				:</td>\n			<td>\n				Разделять (http) от адреса</td>\n			<td>\n				%3B</td>\n		</tr>\n		<tr>\n			<td align="center">\n				/</td>\n			<td>\n				Разделять домен и каталоги</td>\n			<td>\n				%2F</td>\n		</tr>\n		<tr>\n			<td align="center">\n				#</td>\n			<td>\n				Разделять якоря</td>\n			<td>\n				%23</td>\n		</tr>\n		<tr>\n			<td align="center">\n				?</td>\n			<td>\n				Разделять строку запроса</td>\n			<td>\n				%3F</td>\n		</tr>\n		<tr>\n			<td align="center">\n				&amp;</td>\n			<td>\n				Разделять составляющие запроса</td>\n			<td>\n				%24</td>\n		</tr>\n		<tr>\n			<td align="center">\n				@</td>\n			<td>\n				Разделять логин и пароль домена</td>\n			<td>\n				%40</td>\n		</tr>\n		<tr>\n			<td align="center">\n				%</td>\n			<td>\n				Указывает на кодированный символ</td>\n			<td>\n				%25</td>\n		</tr>\n		<tr>\n			<td align="center">\n				+</td>\n			<td>\n				Указывает на пробел</td>\n			<td>\n				%2B</td>\n		</tr>\n		<tr>\n			<td align="center">\n				<space></space></td>\n			<td>\n				Недопустим в URL-ах</td>\n			<td>\n				%20 or +</td>\n		</tr>\n		<tr>\n			<td align="center">\n				;</td>\n			<td>\n				Недопустим в URL-ах</td>\n			<td>\n				%3B</td>\n		</tr>\n		<tr>\n			<td align="center">\n				=</td>\n			<td>\n				Недопустим в URL-ах</td>\n			<td>\n				$3D</td>\n		</tr>\n		<tr>\n			<td align="center">\n				$</td>\n			<td>\n				Недопустим в URL-ах</td>\n			<td>\n				%26</td>\n		</tr>\n		<tr>\n			<td align="center">\n				,</td>\n			<td>\n				Недопустим в URL-ах</td>\n			<td>\n				%2C</td>\n		</tr>\n		<tr>\n			<td align="center">\n				&lt;</td>\n			<td>\n				Недопустим в URL-ах</td>\n			<td>\n				%3C</td>\n		</tr>\n		<tr>\n			<td align="center">\n				&gt;</td>\n			<td>\n				Недопустим в URL-ах</td>\n			<td>\n				%3E</td>\n		</tr>\n		<tr>\n			<td align="center">\n				~</td>\n			<td>\n				Недопустим в URL-ах</td>\n			<td>\n				%25</td>\n		</tr>\n		<tr>\n			<td align="center">\n				^</td>\n			<td>\n				Недопустим в URL-ах</td>\n			<td>\n				%5E</td>\n		</tr>\n		<tr>\n			<td align="center">\n				`</td>\n			<td>\n				Недопустим в URL-ах</td>\n			<td>\n				%60</td>\n		</tr>\n		<tr>\n			<td align="center">\n				\\</td>\n			<td>\n				Недопустим в URL-ах</td>\n			<td>\n				%5C</td>\n		</tr>\n		<tr>\n			<td align="center">\n				[</td>\n			<td>\n				Недопустим в URL-ах</td>\n			<td>\n				%5B</td>\n		</tr>\n		<tr>\n			<td align="center">\n				]</td>\n			<td>\n				Недопустим в URL-ах</td>\n			<td>\n				%5D</td>\n		</tr>\n		<tr>\n			<td align="center">\n				{</td>\n			<td>\n				Недопустим в URL-ах</td>\n			<td>\n				%7B</td>\n		</tr>\n		<tr>\n			<td align="center">\n				}</td>\n			<td>\n				Недопустим в URL-ах</td>\n			<td>\n				%7D</td>\n		</tr>\n		<tr>\n			<td align="center">\n				|</td>\n			<td>\n				Недопустим в URL-ах</td>\n			<td>\n				%7C</td>\n		</tr>\n		<tr>\n			<td align="center">\n				&quot;</td>\n			<td>\n				Недопустим в URL-ах</td>\n			<td>\n				%22</td>\n		</tr>\n	</tbody>\n</table>\n<p>\n	&nbsp;</p>\n', 0, 'Таблица урл кодов'),
(9, 'magic_constants_php', '<table border="1" cellpadding="3" cellspacing="2" style="padding-left: 25px;">\n	<tbody>\n		<tr>\n			<th>\n				Название</th>\n			<th>\n				Содержание</th>\n		</tr>\n		<tr>\n			<td align="center">\n				__LINE__</td>\n			<td>\n				Содержит номер текущей строки в текущем файле</td>\n		</tr>\n		<tr>\n			<td align="center">\n				__FILE__</td>\n			<td>\n				Содержит полное имя текущего файла</td>\n		</tr>\n		<tr>\n			<td align="center">\n				__FUNCTION__</td>\n			<td>\n				Содержит имя текущей функции</td>\n		</tr>\n		<tr>\n			<td align="center">\n				__CLASS__</td>\n			<td>\n				Содержит имя текущего класса</td>\n		</tr>\n		<tr>\n			<td align="center">\n				__METHOD__</td>\n			<td>\n				Содержит имя текущего метода текущего класса</td>\n		</tr>\n	</tbody>\n</table>\n<p>\n	&nbsp;</p>\n', 0, 'Магические константы php'),
(10, 'function_newbie', '<table cellpadding="4" cellspacing="0" width="100%">\n	<col width="128*" />\n	<col width="128*" />\n	<tbody>\n		<tr valign="TOP">\n			<td style="border-width: 1px medium 1px 1px; border-style: solid none solid solid; border-color: rgb(0, 0, 0) -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0.1cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					Название</p>\n			</td>\n			<td style="border: 1px solid rgb(0, 0, 0); padding: 0.1cm;" width="50%">\n				<p align="CENTER">\n					Функция</p>\n			</td>\n		</tr>\n		<tr>\n			<td colspan="2" style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" valign="TOP" width="100%">\n				<p align="CENTER">\n					Основная (general)</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="LEFT" style="margin-left: 0.72cm; margin-right: -0.02cm;">\n					void <b>echo</b> ( string arg1 [, string argn...] )</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					Выводит одну или более строк</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p>\n					If(логическое_выражение)</p>\n				<p>\n					инструкция_1;</p>\n				<p>\n					else</p>\n				<p>\n					инструкция_2;</p>\n				<p>\n					Разновидность синтаксиса:</p>\n				<p>\n					1.</p>\n				<p>\n					If(логическое_выражение)</p>\n				<p>\n					{инструкция_1;}</p>\n				<p>\n					else</p>\n				<p>\n					{инструкция_2;}</p>\n				<p>\n					2.</p>\n				<p>\n					If(логическое_выражение_1)</p>\n				<p>\n					{инструкция_1;}</p>\n				<p>\n					elseif(логическое_выражение_2)</p>\n				<p>\n					{инструкция_2;}</p>\n				<p>\n					else</p>\n				<p>\n					{инструкция_3;}</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					Оператор выбора</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p>\n					Switch(выражение){</p>\n				<p>\n					case значение_1: команды_1;[break;]</p>\n				<p>\n					case значение_2: команды_2;[break;]</p>\n				<p>\n					case значение_3: команды_3;[break;]</p>\n				<p>\n					&hellip;...</p>\n				<p>\n					}</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					Оператор выбора</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<pre class="western" style="text-align: left;">\nforeach (array_expression as $value)\n    statement\nforeach (array_expression as $key =&gt; $value)\n    statement</pre>\n				<p align="LEFT">\n					&nbsp;</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					Данный цикл предназначен специально для перебора массивов.</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<pre class="western" style="text-align: left;">\nfor (expr1; expr2; expr3)\n    statement</pre>\n				<p align="LEFT">\n					&nbsp;</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					Цикл со счетчиком используется для выполнения тела цикла определенное число раз.</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="LEFT">\n					function a () {<br />\n					&nbsp; function b() {<br />\n					&nbsp;&nbsp;&nbsp; echo &quot;I am b.&quot;;<br />\n					&nbsp; }<br />\n					&nbsp; echo &quot;I am a.&lt;br/&gt;&quot;;<br />\n					}</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					Для определения пользовательской функции</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="LEFT">\n					intval()</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					<a name="lang.types.integer"></a>Возвращает <b>integer</b>-значение</p>\n				<p align="CENTER">\n					&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td colspan="2" style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" valign="TOP" width="100%">\n				<p align="CENTER">\n					Массивы(array)</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p>\n					count()</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					Возвратить количество элементов переменной <var>var</var>, которая обычно является <b>array</b>, или любым другим объектом, который может содержать хотя бы один элемент.</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p>\n					array()</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					Возвратить массив параметров.</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					array_search</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER" style="margin-bottom: 0cm;">\n					Осуществляет поиск данного значения в массиве и возвращает соответствующий ключ в случае удачи.</p>\n				<p align="CENTER">\n					&nbsp;</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					sort</p>\n				<p align="CENTER">\n					&nbsp;</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					Сортирует массив. После завершения работы функции элементы массива будут расположены в порядке возрастания.</p>\n			</td>\n		</tr>\n		<tr>\n			<td colspan="2" style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" valign="TOP" width="100%">\n				<p align="CENTER">\n					Регулярные выражения(regular_expression)</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					preg_match()</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					Ищет в заданном тексте <var>subject</var> совпадения с шаблоном <var>pattern</var></p>\n			</td>\n		</tr>\n		<tr>\n			<td colspan="2" style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" valign="TOP" width="100%">\n				<p align="CENTER">\n					Строки(string)</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td height="66" style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					explode()</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					Возвращает массив строк, полученных разбиением строки <var>string</var> с использованием <var>separator</var> в качестве разделителя.</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					int <b>strpos</b> ( string haystack, string needle [, int offset] )</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					Возвращает позицию первого вхождения подстроки <var>needle</var> в строку <var>haystack</var>. В отличие от <b>strrpos()</b>, использует всю строку <var>needle</var>, а не только первый символ.</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					string <b>chr</b> ( int ascii )</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					Возвращает строку из одного символа, код которого задан аргументом <var>ascii</var>.</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					int <b>ord</b> ( string string )</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					Возвращает ASCII код первого символа строки <var>string</var>. Эта функция дополняет функцию <b>chr()</b>.</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					string <b>trim</b> ( string str [, string charlist] )</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					Эта функция возвращает строку <var>str</var> с удаленными из начала и конца строки пробелами.</p>\n				<p>\n					<br />\n					&nbsp;</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					string <b>substr</b> ( string string, int start [, int length] )<br />\n					&nbsp;</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					<b>substr()</b> возвращает подстроку строки <var>string</var> длиной <var>length</var>, начинающегося с <var>start</var> символа по счету.</p>\n				<p>\n					<br />\n					&nbsp;</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					string <b>ucfirst</b> ( string str )</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					Возвращает строку <var>string</var>, в которой первый символ переведен в верхний регистр, если этот символ буквенный.</p>\n			</td>\n		</tr>\n		<tr>\n			<td colspan="2" style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" valign="TOP" width="100%">\n				<p align="CENTER">\n					Криптография(cryptography)</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					md5</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER" style="margin-bottom: 0cm;">\n					Вычисляет MD5 хэш строки str используя алгоритм MD5 RSA Data Security, Inc. и возвращает этот хэш.</p>\n				<p align="CENTER">\n					&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td colspan="2" style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" valign="TOP" width="100%">\n				<p align="CENTER">\n					Дата и время(date_and_tim)</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<h3 align="CENTER" class="western" style="font-weight: normal;">\n					<font size="3">date</font></h3>\n				<p align="CENTER">\n					&nbsp;</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					Возвращает время, отформатированное в соответствии с аргументом format, используя метку времени, заданную аргументом timestamp или текущее системное время, если timestamp не задан.</p>\n			</td>\n		</tr>\n		<tr>\n			<td colspan="2" style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" valign="TOP" width="100%">\n				<p align="CENTER">\n					Математические(mathematical)</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					ceil</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					возвращает ближайшее большее целое от value. Тип возвращаемого значения остаётся float т.к. диапазон float больше integer.</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					floor</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER" style="margin-bottom: 0cm;">\n					округляет дробь в меньшую сторону</p>\n				<p align="CENTER">\n					&nbsp;</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					base_convert</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					конвертирует число между различными базами.</p>\n			</td>\n		</tr>\n		<tr>\n			<td colspan="2" style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" valign="TOP" width="100%">\n				<p align="CENTER">\n					Url</p>\n				<p>\n					&nbsp;</p>\n			</td>\n		</tr>\n		<tr valign="TOP">\n			<td style="border-width: medium medium 1px 1px; border-style: none none solid solid; border-color: -moz-use-text-color -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					urldecode</p>\n				<p>\n					&nbsp;</p>\n			</td>\n			<td style="border-width: medium 1px 1px; border-style: none solid solid; border-color: -moz-use-text-color rgb(0, 0, 0) rgb(0, 0, 0); padding: 0cm 0.1cm 0.1cm;" width="50%">\n				<p align="CENTER">\n					декодирует URL-кодированную строку</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n', 0, 'Функции новичка');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL DEFAULT '',
  `password` char(50) NOT NULL DEFAULT '',
  `email` varchar(127) NOT NULL DEFAULT '',
  `join` int(10) unsigned NOT NULL DEFAULT '0',
  `last_login` int(10) unsigned NOT NULL DEFAULT '0',
  `logins` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_username` (`username`),
  UNIQUE KEY `uniq_email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `join`, `last_login`, `logins`) VALUES
(1, 'andrey', '11e6ccc7b256635ae05896207378faf5181ed9318265495804', 'alphabetghost@gmail.com', 0, 1315511559, 67);

-- --------------------------------------------------------

--
-- Структура таблицы `user_tokens`
--

CREATE TABLE IF NOT EXISTS `user_tokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(32) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_token` (`token`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `user_tokens`
--

