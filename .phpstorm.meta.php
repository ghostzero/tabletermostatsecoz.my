<?php
namespace PHPSTORM_META { // we want to avoid the pollution

    /** @noinspection PhpUnusedLocalVariableInspection */
    /** @noinspection PhpIllegalArrayKeyTypeInspection */
    $STATIC_METHOD_TYPES = [
        \ORM::factory('') => [
            "Category" instanceof \Model_Category,
            "Logssqlecozy" instanceof \Model_Logssqlecozy,
            "Logsnewsqlecozy" instanceof \Model_Logsnewsqlecozy,
            "Role" instanceof \Model_Role,
            "Device" instanceof \Model_Device,
            "Serialnamber" instanceof \Model_Serialnamber,
            "Typeofextensionmodule" instanceof \Model_Typeofextensionmodule,
            "Timestamp" instanceof \Model_Timestamp,
            "Crcdata" instanceof \Model_Crcdata,
            "Valueofextensionmodule" instanceof \Model_Valueofextensionmodule,
            "Typeofgroupsextensionmodule" instanceof \Model_Typeofgroupsextensionmodule,
            "Typeofextensionmodule" instanceof \Model_Typeofextensionmodule,
            "Serialofgroup" instanceof \Model_Serialofgroup,
            "Timestamp" instanceof \Model_Timestamp,
            "Timerpi" instanceof \Model_Timerpi,
            "Messages" instanceof \Model_Messages,
            "Synonyms" instanceof \Model_Synonyms,
            "User" instanceof \Model_User,
            "Rolesusers" instanceof \Model_Rolesusers,

        ]
    ];

//        $STATIC_METHOD_TYPES = [
            /** @noinspection PhpUnusedLocalVariableInspection */
            /** @noinspection PhpIllegalArrayKeyTypeInspection */
         /*   \myFactory('') => [
                "foo" instanceof \someFoo,
                "bar" instanceof \someBar,
                "boo" instanceof \fooBar\someBoo,
            ],
        ];*/

}