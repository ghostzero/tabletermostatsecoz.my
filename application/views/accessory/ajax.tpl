<h3 class="title">Результат</h3>
<p class="para rdfs-comment" style="display: block;">
    <div class="methodsynopsis dc-description" style="display: block;">
        {if is_array($result)}
        {foreach from=$result item=result}
        {$result}
        <br/>
        {/foreach}
        {else}
        {$result}
        {/if}
        {if $error}
        Ошибка:
        {foreach from=$error item=error}
        {$error}
        <br/>
        {/foreach} 
        {/if} 
    </div>
</p>
{if $parameters}
<br/>
<br/>
<h3 class="title">Исходный код</h3>
<p class="para rdfs-comment" style="display: block;">
    <div class="methodsynopsis dc-description" style="display: block;">
        <div id="source_code">
            {foreach from=$arraies item=array}{$array}
            <br/>
            {/foreach}
            {$functionname}
            <font id="parameter">
                ({foreach from=$parameters item=parameter name=param}{$parameter}{if $smarty.foreach.param.last}{else}, {/if}{/foreach});
            </font>
        </div>
    </div>
    {/if}
</p>
