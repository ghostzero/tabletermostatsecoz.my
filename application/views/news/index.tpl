{include file="$tpl_dir/header.tpl"}
<div class="b-body-bg">
<div class="b-body">
<div class="b-left">	
<div class="l-menu-right">
<a href="/admin/index.html">На уровень выше.</a>
</div>
</div>
<div class="b-content">
<div class="refsect1 description">
<div align="right"><a href="/admin/logout.html">{$user} Выход</a></div>
Страница управление новостями.

<table width="100%" border="1" bordercolor="#000000">
<tr><th height="40" width="10%">№</th><th width="50%">Название</th><th width="20%">Дата</th><th width="10%">Редактировать</th><th width="10%">Удалить</th></tr>
{foreach from=$list_news item=item name=f}
{assign var='number' value=`$smarty.foreach.f.iteration+$offset`}
<tr align="center"><td>{$number}</td>
<td>{$item->title}</td><td>{$item->date}</td><td><a href="edit.html?id={$item->id}">Редактировать</a></td><td><a href="delete.html?id={$item->id}">Удалить</a></td></tr>
{/foreach}
</table>
<a href="create.html">Создать</a>

<br/>
{$pagination}
</div>
</div>
</div>
</div>
</div>
</div>
{include file="$tpl_dir/footer.tpl"}