{extends file="$tpl_dir/zalud/base.tpl"}

{block name=mainbody}
    <!-- Breadcrumbs-->
    <ol class="breadcrumb pb-20">
        <li class="breadcrumb-item">
            <a href="/./">Übersicht</a>
        </li>
        <li class="breadcrumb-item active">Übersicht aller Geräte</li>
    </ol>
    <div> {trans}Page of registration {/trans}</div>




    <br/><form method="post" accept-charset="utf-8"> <br/>
    <p>{trans}Please select a user:{/trans}</p>
    <div>
        <input type="radio" id="type_of_user_1"
               name="type_of_user" value="admin">
        <label for="contactChoice1">{trans}Admin{/trans}</label><br/>

        <input type="radio" id="type_of_user_2"
               name="type_of_user" value="plumber">
        <label for="contactChoice2">{trans}Plumber{/trans}</label><br/>

        <input type="radio" id="type_of_user_3"
               name="type_of_user" value="user">
        <label for="contactChoice3">{trans}User{/trans}</label><br/>
    </div>
    <label for="username">{trans}Username{/trans}:</label> <br/>
    <input id="username" type="text" name="username" /> <br/>
    <br /> <br/>
    <label for="email">{trans}Email{/trans}:</label> <br/>
    <input id="email" type="text" name="email" /> <br/>
    <br /> <br/>
    <label for="password">{trans}Password{/trans}:</label> <br/>
    <input id="password" type="password" name="password" /> <br/>
    <br /> <br/>
    <label for="password_confirm">{trans}Repeat Password{/trans}:</label> <br/>
    <input id="password_confirm" type="password" name="password_confirm" /> <br/>
    <br /> <br/>
    <button type="submit">{trans}Register{/trans}</button> <br/></form> <br/>












    <!-- Icon Cards-->
    <div class="row ml-1">
        {*{% for device in devices %}*}
        {*<div class="col-xl-4 col-sm-5 mb-3">*}
        {*<div class="card bg-success o-hidden h-100">*}
        {*<a href="{{ device.get_absolute_url }}">*}
        {*<div class="card-body text-dark">*}
        {*<div>*}
        {*<h5>*}
        {*<b>{{ device.name }}</b>*}
        {*</h5>*}
        {*<h5>*}
        {*{{ device.serial_number }}*}
        {*</h5>*}
        {*</div>*}
        {*</div>*}
        {*</a>*}
        {*<a class="card-footer clearfix small z-1" href="{{ device.get_absolute_url }}">*}
        {*<span class="float-left pt-4 secondary-clr">Last seen 6 hours ago</span>*}
        {*<span class="float-right">*}
        {*<i class="fa fa-angle-right fa-4x clr-black"></i>*}
        {*</span>*}
        {*</a>*}
        {*</div>*}
        {*</div>*}
        {*{% endfor %}*}
    </div>
{/block}


