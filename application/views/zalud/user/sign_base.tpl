{*{% load staticfiles i18n %}*}
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ZAGmbH - {*{% trans 'Log in' %}*}</title>
    <!-- Bootstrap core CSS-->
    <link href="/css/zalud/site-static/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/css/zalud/site-static/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="/css/zalud/site-static/css/style.css" rel="stylesheet">
</head>

<body class="bg-dark">
<div class="container">
    <div class="card card-login mx-auto mt-5">
        <div class="card-header text-center">
            {block name=header}{/block}
            {*{% block header %}{% endblock %}*}
        </div>
        <div class="card-body">
            {block name=body}{/block}
            {*{% block body %}{% endblock %}*}
            <div class="text-center pt-3">
                <a class="small right_15px" href="password_forgot_page'">'Forgot your password?'</a>
                <a class="small right_15px" href="#Copyright">Copyright ZaGmbH</a>
                <a class="small right_15px" href="#Impressum">Impressum</a>
            </div>
        </div>
    </div>
</div>
<!-- Bootstrap core JavaScript-->
<script src="/css/zalud/site-static/vendor/jquery/jquery.min.js"></script>
<script src="/css/zalud/site-static/vendor/popper/popper.min.js"></script>
<script src="/css/zalud/site-static/vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="/css/zalud/site-static/vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
