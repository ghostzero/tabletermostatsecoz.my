{extends file="$tpl_dir/zalud/user/sign_base.tpl"}

{block name=header}
    <h5>'Welcome in HomeControl Portal'</h5>
    <small>'Please enter your credentials'</small>
{/block}

{block name=body}
<form action="/login/login" method="post">
    {if isset($msg) && $msg}
    <p class="error">
        { $msg }
    </p>
    {/if}
    {if isset($request.GET.wrongpwd) && $request.GET.wrongpwd }
    <p class="error">
        'Password is wrong. Either you already reset it or you just forgot it :/'
    </p>
    {/if}
    <div class="form-group">
        <label for="email small_text">'E-Mail Adresse'</label>
        <input type="text" name="username" id="id_email" required="" placeholder="E-Mail eingeben" class="form-control" maxlength="200">
        {if isset($form) && isset($form.email) }]
        {*{ $form.email }*}
        {if isset($form.email.errors) }
        <small class="error">
            { $form.email.errors.0 }
        </small>
        {/if}
        {/if}
    </div>
    <div class="form-group">
        <label for="password small_text">'Password'</label>
        <input type="password" name="password" required="" placeholder="Passwort eingeben" class="form-control" id="id_password">
        {if isset($form.password) }
        {$form.password}
            {if isset($form.password.errors)}
                <small class="error">
                    {$form.password.errors}
                </small>
            {/if}
        {/if}
    </div>
    <button class="btn btn-primary btn-block" type="submit">Login</button>
</form>
{/block}




{*{include file="$tpl_dir/zalud/user/sign_base.tpl"}*}