{extends file="$tpl_dir/zalud/base.tpl"}
{block name=mainbody}
    <script type="text/javascript" src="/script/loader.js"></script>
    <div id="chart_div" style="height:100%;width:100%;overflow:hidden;"></div>
    <br/>
    <div id="btn-group">

        <form method="post" action="/device/print_table_with_data">
            <input type="hidden" name="device_id" value="1">
            <input type="hidden" name="type" value="AinGroup1_Ain8">
            <button type="submit">print_table_with_data</button>
        </form>

    </div>
{literal}
    <script>

//        var time=new Date(2014, 0);
//        var time_1=new Date(2014, 1);
//        var time_2=new Date(2014, 2);
//        var time_3=new Date(2014, 3);

        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);



        function drawChart() {
                var array_names_of_line =[];
                var array_all_valuse_name =[];
                {/literal}{foreach from=$output_array_for_graph['array_names_of_line'] item=one_row_name}
                    array_names_of_line.push('{$one_row_name}');
                {/foreach}
            array_all_valuse_name[0] =array_names_of_line;

            {foreach from=$output_array_for_graph['array_valuse_name'] item=all_row_values name=position_value}
                        var array_valuse_name =[];
                        {foreach from=$all_row_values item=one_row_values}
                                array_valuse_name.push({$one_row_values});
                        {/foreach}
                        array_all_valuse_name[{$smarty.foreach.position_value.iteration}] = array_valuse_name;
                {/foreach}
            {literal}
            var data = google.visualization.arrayToDataTable(array_all_valuse_name);

            var options = {
                chart: {
                    title: 'Company Performance',
                    subtitle: 'Sales, Expenses, and Profit: 2014-2017',
                },
                bars: 'vertical',
                hAxis: {
//                    format: 'yyyy-MM-dd H:m:s',
//                    format: 'yyyy-MM-dd',
                    format: 'H:m:s',
                    gridlines: {count: 25}
                },
                vAxis: {format: 'decimal'},
                height: 800,
                colors: ['#1b9e77', '#d95f02', '#7570b3']
            };

            var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));

            chart.draw(data, options);
        }
    </script>
{/literal}
{/block}