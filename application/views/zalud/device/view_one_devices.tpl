{extends file="$tpl_dir/zalud/base.tpl"}

{block name=mainbody}
    <!-- Breadcrumbs-->
    <ol class="breadcrumb pb-20">
        <li class="breadcrumb-item">
            <a href="/./">Übersicht</a>
        </li>
        <li class="breadcrumb-item active">Übersicht aller Geräte</li>
    </ol>
    <div> {trans}One Device{/trans}</div>

    <br/><form method="post" action="/device/draw_graph" accept-charset="utf-8"> <br/>
    <input type="hidden" name="device_id" value="{$device_id}"/>
    <p>{trans}Please select parameter:{/trans}</p>
    <table style="width: 100%;" border="2">
        {foreach from=$output_array item=one_group}

        <tr>
                {*<input type="checkbox" id="subscribeNews" name="parameter" value="{$one_group.group.name}">*}
                <td><label for="subscribeNews">{$one_group.group.name}</label></td>
            {foreach from=$one_group.array_extension item=one_element_of_group}
            <td>
                <input type="checkbox" id="subscribeNews" name="{$one_group.group.name}_{$one_element_of_group.name}" value="{$one_element_of_group.id}">
                <label for="subscribeNews">{$one_element_of_group.name}</label>
            </td>
            {/foreach}
        </tr>
        {/foreach}
    </table>
    <button type="submit">{trans}Draw Graph{/trans}</button> <br/></form> <br/>


{/block}


