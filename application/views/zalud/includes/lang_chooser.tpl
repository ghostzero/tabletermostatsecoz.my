{*{% load i18n %}*}


<form action="{url} /user/set_language {/url}" method="post" class="ml-3 w-50 d-inline-block">
    {*{% csrf_token %}*}
    <input name="next" type="hidden" value="{*{{ redirect_to }}*}"/>
    <select name="language" title="language_chooser" id="language_chooser" class="form-control" style="
            padding-top: 1px">
        {foreach from=$language_array item=one_language}
            <option value="{$one_language['name']}" {if $one_language['name']==$smarty.session.language_code } selected{/if}>
                {$one_language['name']}
            </option>
        {/foreach}

        {*{% get_current_language as LANGUAGE_CODE %}
        {% get_available_languages as LANGUAGES %}
        {% get_language_info_list for LANGUAGES as languages %}
        {% for language in languages %}
            <option value="{{ language.code }}"{% if language.code == LANGUAGE_CODE %} selected{% endif %}>
                {{ language.name_local }}
            </option>
        {% endfor %}*}

    </select>
</form>