<div class="collapse navbar-collapse" id="navbarResponsive">
    <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        {if isset($request.user.profile.supervisor) && $request.user.profile.supervisor }
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{trans} Devices {/trans}">
                <a class="nav-link" href="{url} devices {/url}">
                    <span class="nav-link-text">{trans} Devices {/trans}</span>
                </a>
            </li>
        {else}
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{trans} Overview {/trans}">
                <a class="nav-link" href="/./">
                    <span class="nav-link-text">{trans} Overview {/trans}</span>
                </a>
            </li>
        {/if}

        <li class="nav-item" data-toggle="too*ltip" data-placement="right" title="{trans} Name user {/trans}">
                <span class="nav-link-text">{trans} Name user {/trans} </span>
        </li>

        <li class="nav-item" data-toggle="too*ltip" data-placement="right" title="{trans} Service {/trans}">
            <a class="nav-link" href="{url} service_index {/url}">
                <span class="nav-link-text">{trans} Service {/trans} </span>
            </a>
        </li>
        <li class="nav-item " data-toggle="tooltip" data-placement="right" title="{trans} User {/trans}">
            <a class="nav-link" href="{*{% url 'user_profile' user.pk %}*}">
                <span class="nav-link-text">{trans} Name user: {/trans} {$user_array['username']}</span><br>
                <span class="nav-link-text">{trans} Email: {/trans} {$user_array['email']}</span><br>
                <span class="nav-link-text">{trans} Type user: {/trans} {foreach from=$user_array['roles'] item=one_role}
                    {if $one_role=='admin'  }
                        admin
                    {/if}

                    {if $one_role=='plumber'  }
                        plumber
                    {/if}

                    {if $one_role=='user'  }
                        user
                    {/if}
                    {/foreach}
            </span>
            </a>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="{trans} Language {/trans}">
            <a class="nav-link">
                <i class="fa fa-fw fa-language"></i>
                <span class="nav-link-text">
                    {*{% include 'includes/lang_chooser.html' %}*}
                    {include file="$tpl_dir/zalud/includes/lang_chooser.tpl" }
                </span>
            </a>
        </li>
    </ul>

    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
               <div>{trans} Log out {/trans}</div>
        </li>
        <li class="nav-item">
            {*{% if request.user.profile.supervisor %}*}
            <form class="form-inline my-2 my-lg-0 mr-lg-2" action="{url} search {/url}">
                <div class="input-group">
                    <input class="form-control wd-21 search_input"
                           type="text" name="st"
                           value="{*{{ request.GET.st|default_if_none:'' }}*}"
                           placeholder="{trans} Search for S/N, email, name {/trans}">
                </div>
            </form>
            {*{% endif %}*}
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
                {trans} Log out {/trans}
            </a>
        </li>
    </ul>
</div>