<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>MyHomeVisit - ZAGmbH Produkt</title>
    <!-- Bootstrap core CSS-->
    <link href="https://fonts.googleapis.com/css?family=Palanquin" rel="stylesheet">
    <link href="/css/zalud/site-static/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="/css/zalud/site-static/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="/css/zalud/site-static/css/style.css" rel="stylesheet">
    {block name=extra_css_head }
        <link href="/css/zalud/site-static/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    {/block}
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
{*{debug}*}
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    {*{include file='page_header.tpl'}
    {include file="$tpl_dir/zalud/base.tpl"}*}


    {*{% include 'includes/top_navi.html' %}*}
    {include file="$tpl_dir/zalud/includes/top_navi.tpl" }

</nav>
<div class="content-wrapper">
    <div class="container-fluid">

        {*{% block mainbody %}{% endblock %}*}
        {block name=mainbody}{/block}



    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
        <div class="container">
            <div class="text-center">
                <small>Copyright © ZAGmbH 2017</small>
            </div>
        </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{t}Log out{/t}</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">{trans} Click on log out if you have finished your session {/trans}</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">{trans} Cancel {/trans}</button>
                    <a class="btn btn-primary" href="/login/logout">{t}Log out{/t}</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="/css/zalud/static/vendor/jquery/jquery.min.js"></script>
    <script src="/css/zalud/static/vendor/popper/popper.min.js"></script>
    <script src="/css/zalud/static/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="/css/zalud/static/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="/css/zalud/static/vendor/chart.js/Chart.min.js"></script>
    <script src="/css/zalud/static/vendor/datatables/jquery.dataTables.js"></script>
    <script src="/css/zalud/static/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="/css/zalud/site-static/js/sb-admin.js"></script>
    <!-- Custom scripts for this page-->
    <script src="/css/zalud/site-static/js/sb-admin-datatables.min.js"></script>
    {literal}
    <script type="text/javascript">
        $(function () {
            $('#language_chooser').on('change', function () {
//                $(this).closest('form').submit();
                /*$.post( "#language_chooser", { name: "John", time: "2pm" }).done(function( data ) {
                    alert( "Data Loaded: " + data );
                });*/
                var language_code = $('#language_chooser').find(':selected').attr('value');
                $.post( "/user/set_language", {language_code: language_code}).done(function( data ) {
                    var test1=1;
                    window.location.reload();
                });


            });

           /* $.post( "#language_chooser", { name: "John", time: "2pm" }).done(function( data ) {
                alert( "Data Loaded: " + data );
            });*/

            $('.search_input').keydown(function (e) {
                if (e.keyCode == 13) {
                    var search_term = $.trim($(this).val());
                    if (search_term) {
                        if (search_term.length >= 3) {
                            return true;
                        } else {
                            alert('Please give more than 3 characters');
                            return false;
                        }
                    } else {
                        return true;
                    }
                }
            })
        });
    </script>
    {/literal}
    {block name=extrajs}

    {/block}
    </div>
</body>

</html>
