<form id="execute" action="{$function}.html" method="post">
<label for="pattern">$pattern Пример: /(\w+) (\d+), (\d+)/i</label>
<br/>
<input type="text" value="{if isset($pattern)&& $pattern}{$pattern}{/if}" id="pattern" name="pattern"><br>
<br/>
<label for="replacement">$replacement Пример: {literal}$1 7,$3{/literal}</label><br>
<input type="text" value="{if isset($replacement)&& $replacement}{$replacement}{/if}" id="replacement" name="replacement">
<br/><br/>
<label for="subject">$subject Пример: April 15, 2003</label><br>
<textarea id="subject" name="subject" cols="50" rows="10">{if isset($subject)&& $subject}{$subject}{/if}</textarea><br>
<br/>
<label for="limit">$limit</label><br>
<input type="text" value="{if isset($limit)&& $limit}{$limit}{/if}" id="limit" name="limit">
<br/>
<input type="submit" value="выполнить" id="run" class="button" name="submit">
</form>
