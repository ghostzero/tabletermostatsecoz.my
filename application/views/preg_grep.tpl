<form id="execute" action="{$function}.html" method="post">
<label for="pattern">$pattern Пример: /^(\d+)?\.\d+$/</label><br/>
<input type="text" name="pattern" id="pattern" value="{if isset($pattern)&& $pattern}{$pattern}{/if}"/><br/><br/>
<label for="input">$input Пример: (0.4,5,34,0.234,12)</label><br/>
<textarea rows="10" cols="50" name="input" id="input">{if isset($input)&& $input}{$input}{/if}</textarea><br/><br/>
<label for="flags">$flags</label><br/>
<select name="flags" id="flags">
<option value="" {if $flags==""}selected{/if}></option>
<option value="PREG_GREP_INVERT" {if $flags=="PREG_GREP_INVERT"}selected{/if}>PREG_GREP_INVERT</option>
</select><br/><br/>
<input type="submit" name="submit" class="button" id="run" value="выполнить" />
</form>