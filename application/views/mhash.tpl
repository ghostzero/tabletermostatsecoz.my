<form id="execute" action="{$function}.html" method="post">
<label for="hash">$hash</label><br/>
<select name="hash" id="hash">
<option value="MHASH_ADLER32" {if $hash=="MHASH_ADLER32"}selected{/if}>MHASH_ADLER32</option>
<option value="MHASH_CRC32" {if $hash=="MHASH_CRC32"}selected{/if}>MHASH_CRC32</option>
<option value="MHASH_CRC32B" {if $hash=="MHASH_CRC32B"}selected{/if}>MHASH_CRC32B</option>
<option value="MHASH_GOST" {if $hash=="MHASH_GOST"}selected{/if}>MHASH_GOST</option>
<option value="MHASH_HAVAL128" {if $hash=="MHASH_HAVAL128"}selected{/if}>MHASH_HAVAL128</option>
<option value="MHASH_HAVAL160" {if $hash=="MHASH_HAVAL160"}selected{/if}>MHASH_HAVAL160</option>
<option value="MHASH_HAVAL192" {if $hash=="MHASH_HAVAL192"}selected{/if}>MHASH_HAVAL192</option>
<option value="MHASH_HAVAL256" {if $hash=="MHASH_HAVAL256"}selected{/if}>MHASH_HAVAL256</option>
<option value="MHASH_MD4" {if $hash=="MHASH_MD4"}selected{/if}>MHASH_MD4</option>
<option value="MHASH_MD5" {if $hash=="MHASH_MD5"}selected{/if}>MHASH_MD5</option>
<option value="MHASH_RIPEMD160" {if $hash=="MHASH_RIPEMD160"}selected{/if}>MHASH_RIPEMD160</option>
<option value="MHASH_SHA1" {if $hash=="MHASH_SHA1"}selected{/if}>MHASH_SHA1</option>
<option value="MHASH_SHA256" {if $hash=="MHASH_SHA256"}selected{/if}>MHASH_SHA256</option>
<option value="MHASH_TIGER" {if $hash=="MHASH_TIGER"}selected{/if}>MHASH_TIGER</option>
<option value="MHASH_TIGER128" {if $hash=="MHASH_TIGER128"}selected{/if}>MHASH_TIGER128</option>
<option value="MHASH_TIGER160" {if $hash=="MHASH_TIGER160"}selected{/if}>MHASH_TIGER160</option>
</select><br/><br/>
<label for="data">$data Пример: Hello</label><br>
<textarea id="data" name="data" cols="50" rows="10">{$data}</textarea><br>
<br>
<label for="key">$key Пример: 123</label><br>
<input type="text" id="key" name="key" value="{$key}" ><br>
<br>
<input type="submit" value="выполнить" id="run" class="button" name="submit">
</form>
