<form id="execute" action="{$function}.html" method="post">
<label for="pattern">$pattern Пример: /^(http:\/\/)?([^\/]+)/i</label><br/>
<input type="text" name="pattern" id="pattern" value="{if isset($pattern)&& $pattern}{$pattern}{/if}"/><br/><br/>
<label for="subject">$subject Пример: http://www.php.net/index</label><br/>
<textarea rows="10" cols="50" name="subject" id="subject">{if isset($subject)&& $subject}{$subject}{/if}</textarea><br/><br/>
<label for="flags">$flags</label><br/>
<select name="flags" id="flags">
<option value=""{if $flags==""}selected{/if}></option>
<option value="PREG_PATTERN_ORDER"{if $flags=="PREG_PATTERN_ORDER"}selected{/if}>PREG_PATTERN_ORDER</option>
<option value="PREG_SET_ORDER"{if $flags=="PREG_SET_ORDER"}selected{/if}>PREG_SET_ORDER</option>
<option value="PREG_OFFSET_CAPTURE"{if $flags=="PREG_OFFSET_CAPTURE"}selected{/if}>PREG_OFFSET_CAPTURE</option>
</select><br/><br/>
<label for="offset">$offset Пример: 0</label><br/>
<input type="text" name="offset" id="offset" value="{if isset($offset)&& $offset}{$offset}{/if}"/><br/><br/>
<input type="submit" name="submit" class="button" id="run" value="выполнить" />

</form>