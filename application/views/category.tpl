{include file="$tpl_dir/header.tpl"}
<div class="b-body-bg">
<div class="b-body">
<div class="b-left">
<div class="l-menu-right">
<ul class="b-menu">
		{foreach from=$methods item=method}
	<li><a href="/{if isset($method.name)&& $method.name}{$method.name}{/if}.html">{if isset($method.name)&& $method.name}{$method.name}{/if}</a></li>	
	{/foreach}	
</ul>
</div>
</div>
<div class="b-content">
<h1 class="b-main-descr-title">{if isset($category)&& $category}{$category}{/if}</h1>

<div class="refsect1 description">
{if $category!='feedback'}
<ul>
	{foreach from=$methods item=method}
	<li>
	<h3 class="title"><a href="/{if isset($method.name)&& $method.name}{$method.name}{/if}.html">{if isset($method.name)&& $method.name}{$method.name}{/if}</a></h3>
	<p class="para rdfs-comment" style="display: block;">
	{if isset($method.description)&& $method.description}{$method.description}{/if}</p>
	</li>
	{/foreach}
</ul>
{else}
{literal}
	<div id="disqus_thread"></div>
			<script type="text/javascript">
		/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
		var disqus_shortname = 'livephp'; // required: replace example with your forum shortname

		// The following are highly recommended additional parameters. Remove the slashes in front to use.
		var disqus_identifier = '{/literal}{if isset($category)&& $category}{$category}{/if}{literal}';
		var disqus_url = 'http://livephp.net/category/{/literal}{if isset($category)&& $category}{$category}{/if}{literal}.html';

		/* * * DON'T EDIT BELOW THIS LINE * * */
		(function() {
			var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
			dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
			(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
		})();
	</script>
	<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
	<a href="http://disqus.com" class="dsq-brlink">blog comments powered by <span class="logo-disqus">Disqus</span></a>	
{/literal}
{/if}
</div>

</div>
{include file="$tpl_dir/rightpartofbody.tpl"}
</div>
</div>
</div>
</div>

{include file="$tpl_dir/footer.tpl"}
