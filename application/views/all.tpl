{include file="$tpl_dir/header.tpl"}
<div class="b-body-bg">
<div class="b-body">
<div class="b-left">
<div class="l-menu-right">
<ul class="b-menu">
{foreach item=output from=$output_first}
{if $output.category!='feedback'}
<li>{$output.category}</li>
{foreach item=method from=$output.methods}
<li><a href="/{$method.name}.html">{$method.name}</a></li>
{/foreach}
{/if}
{/foreach}
</ul>
</div>
</div>
<div class="b-content">
<h1 class="b-main-descr-title">Все категории для php онлайн тестов</h1>
<div class="refsect1 description">
{foreach item=output from=$output_second }
{if $output.category!='feedback'}
<h3 class="title"><a href="{$output.href}.html" >{$output.category}</a></h3>
{foreach item=method from=$output.methods name=meth}
<a href="/{$method.name}.html" title="{$method.description}" class="black">{$method.name}</a>{if not $smarty.foreach.meth.last}, {/if}
{/foreach}
<br/>
<br/>
{/if}
{/foreach}
</div>
</div>
{include file="$tpl_dir/rightpartofbody.tpl"}
</div>
</div>
</div>
</div>

{include file="$tpl_dir/footer.tpl"}