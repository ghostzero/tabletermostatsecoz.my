<form id="execute" action="{$function}.html" method="post">
<label for="pattern">$pattern Пример: (/\d/,/[a-z]/,/[1a]/)</label><br>
<input type="text" value="{if isset($pattern)&& $pattern}{$pattern}{/if}" id="pattern" name="pattern"><br>
<br>
<label for="replacement">$replacement Пример: (A:$0,B:$0,C:$0)</label><br>
<input type="text" value="{if isset($replacement)&& $replacement}{$replacement}{/if}" id="replacement" name="replacement"><br>
<br>
<label for="subject">$subject Пример: (1,a,2,b,3,A,B,4)</label><br>
<textarea id="subject" name="subject" cols="50" rows="10">{if isset($subject)&& $subject}{$subject}{/if}</textarea><br>
<br>
<label for="limit">$limit</label><br>
<input type="text" value="{if $limit}{if isset($limit)&& $limit}{$limit}{/if}{else}-1{/if}" id="limit" name="limit"><br>
<br>
<input type="submit" value="выполнить" id="run" class="button" name="submit">
</form>
