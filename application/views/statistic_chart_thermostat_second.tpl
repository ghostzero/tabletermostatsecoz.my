<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>PHP <програ></програ>ммирование - тестирование функций онлайн. php строки, php функции, php справочник - все это php сайт</title>
    <link rel="stylesheet" media="screen" href="/css/common.css" type="text/css"/>
</head>
<body>
Мак адрес текущего устройства: {$mac_current_device}.
<form action="/statistic_chart_thermostat_second?mac={$mac_current_device}" method="post">
{*<form action="/statistic_chart_thermostat_second?mac={$mac_current_device}" method="post">*}
    <div>Начальная дата:<select name="select_from_datetime" id="select_from_datetime">
        {foreach from=$array_base_row_by_mac item=one_row}
        <option value="{$one_row['datetime']}" {if isset($seted_select_from_datetime) && $one_row['datetime']==$seted_select_from_datetime} selected="selected" {/if}>{$one_row['datetime']}</option>
        {/foreach}
    </select>
    Конечная дата:
        <select name="number_hours" id="number_hours">
            {for $current_hours=1 to $number_hours}
                <option value="{$current_hours}" {if isset($seted_number_hours) &&  $current_hours==$seted_number_hours} selected="selected" {/if}>{$current_hours}</option>

            {/for}
        </select>
        <input type="submit" value="Отправить" >
    </div>

</form>
{*<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>*}
<script type="text/javascript" src="./script/loader.js"></script>
<div id="chart_div" style="height:100%;width:100%;overflow:hidden;"></div>
<br/>
<div id="btn-group">
    <button class="button button-blue" id="none">No Format</button>
    <button class="button button-blue" id="scientific">Scientific Notation</button>
    <button class="button button-blue" id="decimal">Decimal</button>
    <button class="button button-blue" id="short">Short</button>
</div>
 {literal}
<script>
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['DateTime', 'Текущее состояние температуры', 'Установленная температура', 'Состояние плунжера (Х4)'],
            {/literal}{foreach from=$array_row_by_mac item=one_row}
            ['{$one_row['datetime']}', {$one_row['current_temperature']}, {$one_row['installed_temperature']}, {$one_row['condition_plunger']}],
                {/foreach}{literal}

        ]);

        var options = {
            chart: {
                title: 'Company Performance',
                subtitle: 'Sales, Expenses, and Profit: 2014-2017',
            },
            bars: 'vertical',
            vAxis: {format: 'decimal'},
            height: 800,
            colors: ['#1b9e77', '#d95f02', '#7570b3']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));

        chart.draw(data, options);
    }
</script>
 {/literal}



</body>
</html>
