{include file="$tpl_dir/header.tpl"}
<div class="b-body-bg">
    <div class="b-body">
        <div class="b-left">
            <div class="l-menu-right">
                <ul class="b-menu">
                {foreach from=$name_methods item=name_method}<li{if $name_method.name==$function} class="active"{/if} ><a href="/{$name_method.name}.html">{$name_method.name}</a>
                </li>
                {/foreach} 
            </ul>
        </div>
    </div>
    {literal}
    <script>
        $(document).ready(function(){
            /*$('label').each(function() {
             var id = $(this).attr("for");
             var value = $(this).html();
             
             //$('#'+id).val($.trim(value.split(":")[1]));
             });
             */
            $('#execute label').bind('click', function(){
                var id = $(this).attr("for");
                var value = $(this).text();
                
                
                $('#' + id).val($.trim(value.split("Пример:")[1]));
            });
			$('#run').live('click', function(event){
				event.preventDefault();
				//Если была нажата клавиша "выполнить"
				//то передать параметры с полей вввиде массива соответствующим как при передаче формы и добавить имя функции
				var this_form=$("form#execute");
				var name_function=this_form.attr('action');
				//var form_value=$("form:(#execute)");
				var parametrs=this_form.serialize();
				//var parametrs=$("form#execute").serialize();
				var send_value=parametrs + "&name_function="+name_function;
				$.post("ajax.html",send_value,function(data_out){
					$('div#result_and_source_code').html(data_out);
				});
			});
        });
    </script>
    {/literal}
    <div class="b-content">
        <h1 class="b-main-descr-title">{$function}</h1>
		{literal}
			<!-- Place this tag where you want the +1 button to render -->
			<g:plusone size="small"></g:plusone>

			<!-- Place this tag after the last plusone tag -->
			<script type="text/javascript">
			  window.___gcfg = {lang: 'ru'};

			  (function() {
				var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
				po.src = 'https://apis.google.com/js/plusone.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>			
		{/literal}
        <div class="b-main-descr">
            {$description}
        </div>
        <div class="refsect1 description">
            <h3 class="title">Объявление</h3>
            <p class="para rdfs-comment" style="display: block;">
                {$declaration}
            </p>
        </div>
        <div class="refsect1 description">
            <h3 class="title">Тест {$function} </h3>
			<p>кликайте на текст примеров для автоматической вставки</p>
            <p class="para rdfs-comment" style="display: block;">
                <div class="methodsynopsis dc-description" style="display: block;">
                    {include file="$tpl_dir/$function.tpl"}
                </div>
            </p>
        </div>
        <div class="refsect1 description" id="result_and_source_code">
            <h3 class="title">Результат</h3>
            <p class="para rdfs-comment" style="display: block;">
                <div class="methodsynopsis dc-description" style="display: block;">
                {if isset($result)&&$result}
                {if is_array($result)}
                    {foreach from=$result item=oneResult}
                    {$oneResult}
                    <br/>
                    {/foreach}
                    {else}
                    {$result}
                    {/if}
                {/if}
                    {if isset($error) && $error}
                    Ошибка:
                    {foreach from=$error item=oneError}
                    {$oneError}
                    <br/>
                    {/foreach} 
                    {/if} 
                </div>
            </p>
			{if isset($parameters)&&$parameters}
            <br/>
            <br/>
            <h3 class="title">Исходный код</h3>
            <p class="para rdfs-comment" style="display: block;">
                <div class="methodsynopsis dc-description" style="display: block;">
				<div id="source_code">
					{foreach from=$arraies item=array}{$array}<br/>{/foreach}
					{$functionname}<font id="parameter">({foreach from=$parameters item=parameter name=param}{$parameter}{if $smarty.foreach.param.last}{else}, {/if}{/foreach});</font>
					</div>
                </div>
				{/if}
            </p>
        </div>
        <div class="refsect1 description">
            {literal}
            <div id="disqus_thread">
            </div>
            <script type="text/javascript">
                /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
                var disqus_shortname = 'livephp'; // required: replace example with your forum shortname
                // The following are highly recommended additional parameters. Remove the slashes in front to use.
                var disqus_identifier = '{/literal}{$function}{literal}';
                var disqus_url = 'http://livephp.net/{/literal}{$function}{literal}.html';
                
                /* * * DON'T EDIT BELOW THIS LINE * * */
                (function(){
                    var dsq = document.createElement('script');
                    dsq.type = 'text/javascript';
                    dsq.async = true;
                    dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                })();
            </script>
            <noscript>
                Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a>
            </noscript>
            <a href="http://disqus.com" class="dsq-brlink">blog comments powered by <span class="logo-disqus">Disqus</span></a>
            {/literal}
        </div>
    </div>
    {include file="$tpl_dir/rightpartofbody.tpl"}
</div>
</div>
</div>
</div>
{include file="$tpl_dir/footer.tpl"}