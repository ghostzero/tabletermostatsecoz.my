{include file="$tpl_dir/header.tpl"}

<div class="b-body-bg">
<div class="b-body">
<div class="b-left">	
<div class="l-menu-right">
</div>
</div>
<div class="b-content">
<h1 class="b-main-descr-title">{if isset($showstatic.title)&& $showstatic.title}{$showstatic.title}{/if} ({if isset($showstatic.date)&& $showstatic.date}{$showstatic.date}{/if})</h1>

<div class="refsect1 description">
<div id="news_content">{if isset($showstatic.content)&& $showstatic.content}{$showstatic.content}{/if}</div>
<br/>
{literal}
	<div id="disqus_thread"></div>
			<script type="text/javascript">
		/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
		var disqus_shortname = 'livephp'; // required: replace example with your forum shortname

		// The following are highly recommended additional parameters. Remove the slashes in front to use.
		var disqus_identifier = 'news{/literal}{if isset($shownews.id)&& $shownews.id}{$shownews.id}{/if}{literal}';
		var disqus_url = 'http://livephp.net/shownews.html?id={/literal}{if isset($shownews.id)&& $shownews.id}{$shownews.id}{/if}{literal}';

		/* * * DON'T EDIT BELOW THIS LINE * * */
		(function() {
			var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
			dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
			(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
		})();
	</script>
	<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
	<a href="http://disqus.com" class="dsq-brlink">blog comments powered by <span class="logo-disqus">Disqus</span></a>	
{/literal}
</div>
</div>
{include file="$tpl_dir/rightpartofbody.tpl"}
</div>
</div>
</div>
</div>
{include file="$tpl_dir/footer.tpl"}
