<form id="execute" action="{$function}.html" method="post">
<label for="pattern">$pattern Пример: /[\s,]+/</label><br/>
<input type="text" name="pattern" id="pattern" value="{if isset($pattern)&& $pattern}{$pattern}{/if}"/><br/><br/>
<label for="subject">$subject Пример: hypertext language, programming</label><br/>
<textarea rows="10" cols="50" name="subject" id="subject">{if isset($subject)&& $subject}{$subject}{/if}</textarea><br/><br/>
<label for="limit">$limit Пример: -1</label><br/>
<input type="text" name="limit" id="limit" value="{if $limit==''}-1{else}{if isset($limit)&& $limit}{$limit}{/if}{/if}"/><br/><br/>
<label for="flags">$flags Пример: PREG_SPLIT_OFFSET_CAPTURE</label><br/>
<select name="flags" id="flags">
<option value="" {if  $flags==''}selected{/if}></option>
<option value="PREG_SPLIT_NO_EMPTY"{if  $flags=='PREG_SPLIT_NO_EMPTY'}selected{/if}>PREG_SPLIT_NO_EMPTY</option>
<option value="PREG_SPLIT_DELIM_CAPTURE"{if  $flags=='PREG_SPLIT_DELIM_CAPTURE'}selected{/if}>PREG_SPLIT_DELIM_CAPTURE</option>
<option value="PREG_SPLIT_OFFSET_CAPTURE"{if  $flags=='PREG_SPLIT_OFFSET_CAPTURE'}selected{/if}>PREG_SPLIT_OFFSET_CAPTURE</option>
</select><br/><br/>
<input type="submit" name="submit" class="button" id="run" value="выполнить" />
</form>
