<form id="execute" action="{$function}.html" method="post">
<label for="string">$string Пример: &lt;a&nbsp;href='test'&gt;Test&lt;/a&gt;</label><br/>
<textarea rows="10" cols="50" name="string" id="string">{if isset($string)&& $string}{$string}{/if}</textarea><br/><br/>
<label for="quote_style">$quote_style</label><br/>
<select name="quote_style" id="quote_style">
<option value="ENT_COMPAT"{if $quote_style=='ENT_COMPAT'}selected{/if}>ENT_COMPAT</option>
<option value="ENT_QUOTES"{if $quote_style=='ENT_QUOTES'}selected{/if}>ENT_QUOTES</option>
<option value="ENT_NOQUOTES" {if $quote_style=='ENT_NOQUOTES'}selected{/if}>ENT_NOQUOTES</option>
</select><br/><br/>
<label for="charset">$charset</label><br/>
<select name="charset" id="charset">
<option value=""{if $charset==''}selected{/if}></option>
<option value="ISO-8859-1"{if $charset=='ISO-8859-1'}selected{/if}>ISO-8859-1</option>
<option value="ISO-8859-15"{if $charset=='ISO-8859-15'}selected{/if}>ISO-8859-15</option>
<option value="UTF-8"{if $charset=='UTF-8'}selected{/if}>UTF-8</option>
<option value="cp866"{if $charset=='cp866'}selected{/if}>cp866</option>
<option value="cp1251"{if $charset=='cp1251'}selected{/if}>cp1251</option>
<option value="cp1252"{if $charset=='cp1252'}selected{/if}>cp1252</option>
<option value="KOI8-R"{if $charset=='KOI8-R'}selected{/if}>KOI8-R</option>
<option value="BIG5"{if $charset=='BIG5'}selected{/if}>BIG5</option>
<option value="GB2312"{if $charset=='GB2312'}selected{/if}>GB2312</option>
<option value="BIG5-HKSCS"{if $charset=='BIG5-HKSCS'}selected{/if}>BIG5-HKSCS</option>
<option value="Shift_JIS"{if $charset=='Shift_JIS'}selected{/if}>Shift_JIS</option>
<option value="EUC-JP"{if $charset=='EUC-JP'}selected{/if}>EUC-JP</option>
</select><br/><br/>
<input type="submit" name="submit" class="button" id="run" value="выполнить" />
</form>
