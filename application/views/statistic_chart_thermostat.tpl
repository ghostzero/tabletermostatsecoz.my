<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>PHP <програ></програ>ммирование - тестирование функций онлайн. php строки, php функции, php справочник - все это php сайт</title>
    <link rel="stylesheet" media="screen" href="/css/common.css" type="text/css"/>
</head>
<body>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<div id="chart_div" style="height:100%;width:100%;overflow:hidden;"></div>
<br/>
<div id="btn-group">
    <button class="button button-blue" id="none">No Format</button>
    <button class="button button-blue" id="scientific">Scientific Notation</button>
    <button class="button button-blue" id="decimal">Decimal</button>
    <button class="button button-blue" id="short">Short</button>
</div>
 {literal}
<script>
    google.charts.load('current', {'packages':['bar']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['DateTime', 'Текущее состояние температуры', 'Установленная температура', 'Состояние плунжера (Х4)'],
            {/literal}{foreach from=$array_row_by_mac item=one_row}
            ['{$one_row['datetime']}', {$one_row['current_temperature']}, {$one_row['installed_temperature']}, {$one_row['condition_plunger']}],
                {/foreach}{literal}

        ]);

        var options = {
            chart: {
                title: 'Company Performance',
                subtitle: 'Sales, Expenses, and Profit: 2014-2017',
            },
            bars: 'vertical',
            vAxis: {format: 'decimal'},
            height: 800,
            colors: ['#1b9e77', '#d95f02', '#7570b3']
        };

        var chart = new google.charts.Bar(document.getElementById('chart_div'));

        chart.draw(data, google.charts.Bar.convertOptions(options));

        var btns = document.getElementById('btn-group');

        btns.onclick = function (e) {

            if (e.target.tagName === 'BUTTON') {
                options.vAxis.format = e.target.id === 'none' ? '' : e.target.id;
                chart.draw(data, google.charts.Bar.convertOptions(options));
            }
        }
    }
</script>
 {/literal}



</body>
</html>
