<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{$title} php программирование - тестирование функций онлайн. php строки, функции, массивы - все это на php сайте</title>
<meta name="description" content='{if isset($output_second) && $output_second}PHP сайт - тестирование php функций онлайн. PHP программирование - наборы для работы со строками, функциями и справочниками.{/if}{if isset($function)&&$function}{$function} - {$description}{/if}{if isset($category) && $category}{$category} :: {foreach from=$methods item=method name=meth}{$method.name}{if $smarty.foreach.meth.last}.{else}, {/if}{/foreach}{/if}' />
<meta name="keywords" content='{if isset($output_second) && $output_second}php строки, php сайт, php функции, php справочник, php программирование{/if}{if isset($function)&&$function} {$function}{/if}{if isset($category) && $category} {$category}{/if}' />
    {if isset($cache_random_id)}
<link rel="stylesheet" type="text/css" media="screen,projection"
	href="/css/style.css?nocache={$cache_random_id}" />
    {else}
        <link rel="stylesheet" type="text/css" media="screen,projection"
              href="/css/style.css" />
    {/if}
<!--[if IE 6]>
		<script type="text/javascript" src="/script/dd_belatedpng_0.0.8a-min.js"></script>
		<script type="text/javascript" src="/script/iefix.js"></script>
		<link rel="stylesheet" type="text/css" media="all" href="/css/ie6.css" />
		<link rel="stylesheet" media="screen" href="/css/common.css" type="text/css"/>
	<![endif]-->

<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" media="all" href="/css/ie7.css" />
	<![endif]-->

<!--[if IE 8]>
		<link rel="stylesheet" type="text/css" media="all" href="/css/ie8.css" />
	<![endif]-->
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="/script/toogler.js"></script>
{if isset($ckeditor) && $ckeditor}
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
{/if}

{literal}
	<!-- PopAds.net Popunder Code for livephp.hopto.org -->
	<script type="text/javascript" data-cfasync="false">
		var _pop = _pop || [];
		_pop.push(['siteId', 1372343]);
		_pop.push(['minBid', 0.000000]);
		_pop.push(['popundersPerIP', 0]);
		_pop.push(['delayBetween', 0]);
		_pop.push(['default', false]);
		_pop.push(['defaultPerDay', 0]);
		_pop.push(['topmostLayer', false]);
		(function() {
			var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
			var s = document.getElementsByTagName('script')[0];
			pa.src = '//c1.popads.net/pop.js';
			pa.onerror = function() {
				var sa = document.createElement('script'); sa.type = 'text/javascript'; sa.async = true;
				sa.src = '//c2.popads.net/pop.js';
				s.parentNode.insertBefore(sa, s);
			};
			s.parentNode.insertBefore(pa, s);
		})();
	</script>
	<!-- PopAds.net Popunder Code End -->
{/literal}
</head>
<body>
<div class="l-wrapper">

<div class="l-container">

<div class="b-header">
<a href="/" class="b-logo" title="Livephp">Livephp</a>


<div class="l-menu">{if isset($administrative)&&$administrative} {else}
<div class="b-menu">
<ul>
	<li><a href="/category/all.html" title="все">всё</a> |</li>
	{foreach from=$all_category item=category name=categ}
	<li>
	    <a href="{$category.href}.html" title="{$category.title}">{$category.title}</a>
	{if not $smarty.foreach.categ.last} |{/if}
	</li>
	{/foreach}
</ul>
</div>
{/if} <!-- /l-menu --> <!-- /b-header --></div>
</div>
<div class="b-search">
				<!--<div style="float:left;"><span style="padding-left:10px; padding-top:10px;">сайт-визитка теперь <a href="http://vsite.be" target="_blank">бесплатно через вконтакте</a></span></div>-->
				<form id="form_search" action="http://www.google.com.ua/search?" method="get" target="_blank">
					<p>
						<label for="search">
							<span>поиск</span>
							{literal}
							<input type="hidden" name="q" value="site:livephp.net">
							<input type="text" name="q" id="search" size="30" onblur="if (this.value == '') {this.value ='php';}" onfocus="if (this.value == 'php') {this.value = '';}" value="php" />
							{/literal}
						</label>
						<input type="submit" value="" class="b-search-submit" />
					</p>
				</form>
			</div>

