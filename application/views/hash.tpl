<form id="execute" action="{$function}.html" method="post">
<label for="algo">$algo</label><br/>
<select name="algo" id="algo">
<option value="md2" {if $algo=="md2"}selected{/if}>md2</option>
<option value="md4" {if $algo=="md4"}selected{/if}>md4</option>
<option value="md5" {if $algo=="md5"}selected{/if}>md5</option>
<option value="sha1" {if $algo=="sha1"}selected{/if}>sha1</option>
<option value="sha256" {if $algo=="sha256"}selected{/if}>sha256</option>
<option value="sha384" {if $algo=="sha384"}selected{/if}>sha384</option>
<option value="sha512" {if $algo=="sha512"}selected{/if}>sha512</option>
<option value="ripemd128" {if $algo=="ripemd128"}selected{/if}>ripemd128</option>
<option value="ripemd160" {if $algo=="ripemd160"}selected{/if}>ripemd160</option>
<option value="ripemd256" {if $algo=="ripemd256"}selected{/if}>ripemd256</option>
<option value="ripemd320" {if $algo=="ripemd320"}selected{/if}>ripemd320</option>
<option value="whirlpool" {if $algo=="whirlpool"}selected{/if}>whirlpool</option>
<option value="tiger128,3" {if $algo=="tiger128,3"}selected{/if}>tiger128,3</option>
<option value="tiger160,3" {if $algo=="tiger160,3"}selected{/if}>tiger160,3</option>
<option value="tiger192,3" {if $algo=="tiger192,3"}selected{/if}>tiger192,3</option>
<option value="tiger128,4" {if $algo=="tiger128,4"}selected{/if}>tiger128,4</option>
<option value="tiger160,4" {if $algo=="tiger160,4"}selected{/if}>tiger160,4</option>
<option value="tiger192,4" {if $algo=="tiger192,4"}selected{/if}>tiger192,4</option>
<option value="snefru" {if $algo=="snefru"}selected{/if}>snefru</option>
<option value="gost" {if $algo=="gost"}selected{/if}>gost</option>
<option value="adler32" {if $algo=="adler32"}selected{/if}>adler32</option>
<option value="crc32" {if $algo=="crc32"}selected{/if}>crc32</option>
<option value="crc32b" {if $algo=="crc32b"}selected{/if}>crc32b</option>
<option value="haval128,3" {if $algo=="haval128,3"}selected{/if}>haval128,3</option>
<option value="haval160,3" {if $algo=="haval160,3"}selected{/if}>haval160,3</option>
<option value="haval192,3" {if $algo=="haval192,3"}selected{/if}>haval192,3</option>
<option value="haval224,3" {if $algo=="haval224,3"}selected{/if}>haval224,3</option>
<option value="haval256,3" {if $algo=="haval256,3"}selected{/if}>haval256,3</option>
<option value="haval128,4" {if $algo=="haval128,4"}selected{/if}>haval128,4</option>
<option value="haval160,4" {if $algo=="haval160,4"}selected{/if}>haval160,4</option>
<option value="haval192,4" {if $algo=="haval192,4"}selected{/if}>haval192,4</option>
<option value="haval224,4" {if $algo=="haval224,4"}selected{/if}>haval224,4</option>
<option value="haval256,4" {if $algo=="haval256,4"}selected{/if}>haval256,4</option>
<option value="haval128,5" {if $algo=="haval128,5"}selected{/if}>haval128,5</option>
<option value="haval160,5" {if $algo=="haval160,5"}selected{/if}>haval160,5</option>
<option value="haval192,5" {if $algo=="haval192,5"}selected{/if}>haval192,5</option>
<option value="haval224,5" {if $algo=="haval224,5"}selected{/if}>haval224,5</option>
<option value="haval256,5" {if $algo=="haval256,5"}selected{/if}>haval256,5</option>
</select><br/>
<label for="data">$data Пример: Hello</label><br>
<input type="text" value="{if isset($data)&& $data}{$data}{/if}" id="data" name="data"><br>
<br>
<label for="raw_output">$raw_output</label><br>
<input type="checkbox" id="raw_output" name="raw_output" value="true" class="check" {if $raw_output}checked{/if} ><br>
<input type="submit" value="выполнить" id="run" class="button" name="submit">
</form>
