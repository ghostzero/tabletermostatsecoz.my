<form id="execute" action="{$function}.html" method="post">
<label for="input">$input Пример: Alien</label><br/>
<textarea rows="10" cols="50" name="input" id="input">{if isset($input)&& $input}{$input}{/if}</textarea><br/><br/>
<label for="pad_length">$pad_length Пример: 10</label><br/>
<input type="text" name="pad_length" id="pad_length" value="{if isset($pad_length)&& $pad_length}{$pad_length}{/if}"/><br/><br/>
<label for="pad_string">$pad_string Пример: -=</label><br/>
<input type="text" name="pad_string" id="pad_string" value="{if isset($pad_string)&& $pad_string}{$pad_string}{/if}"/><br/><br/>
<label for="pad_type">$pad_type Пример: STR_PAD_LEFT</label><br/>
<select name="pad_type" id="pad_type">
<option value="" {if $pad_type==''}selected{/if}></option>
<option value="STR_PAD_RIGHT" {if $pad_type=='STR_PAD_RIGHT'}selected{/if}>STR_PAD_RIGHT</option>
<option value="STR_PAD_LEFT" {if $pad_type=='STR_PAD_LEFT'}selected{/if}>STR_PAD_LEFT</option>
<option value="STR_PAD_BOTH" {if $pad_type=='STR_PAD_BOTH'}selected{/if}>STR_PAD_BOTH</option>
</select><br/><br/>
<input type="submit" value="выполнить" id="run" class="button" name="submit"/>
</form>
