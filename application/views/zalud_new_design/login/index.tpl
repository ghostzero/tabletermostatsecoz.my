<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width.initial-scale=1.0">
    <title>{trans}My Home Visit{/trans}</title>
    <style>  h1 {
            background: #6A5ACD;
            color: white;
            padding: 6px;
        }

        h2 {
            color: #6A5ACD;
            padding: 8px;
        }

        .first {
            display: flex;
            width: 100%;
            height: 70%;
        }

        .second {
            margin: auto;
            width: 800px;
            height: 500px;
            vertical-align: auto;

            border: solid 2px black;
            background-color: white;
        }

    </style>
</head>

<body>
<h1><font size="8" face= "DejaVu Serif">{trans}MyHomeVisit{/trans}</font>
    <p align="right"> <font size="5" face= "DejaVu Serif">{trans}Das Kundenportal der ZAGmbH{/trans}</font></p>
</h1>

<div class="first">
    <div class="second">

        <table class="tabl1" align="center" width="800px">
            <thead>
            <tr><td colspan="3"><img class="logoMain" src="images/logo.PNG" align="right"/></td> </tr>
            </thead>

            <tbody>
            <tr><td colspan="3">  </td></tr>
            <tr><td colspan="3"><h2 align="center"><font size="5" face="DejaVu Serif">{trans}Willkommen im HomeControl Portal{/trans}</font></h2> </td> </tr>
            <tr><td colspan="3"><h3 align="center"><font size="4" face="DejaVu Serif">{trans}Bitte geben Sie Ihre Zugangsdaten ein{/trans}</font></h3> </td> </tr>
            <tr><td colspan="3">
                    <form action="/login/login" method="post">
                        <p align="center"><strong></strong>
                            <input maxlength="50" size="50" type="text" name="username" id="id_email" required="" placeholder="E-Mail eingeben" class="form-control" ></p>
                        {if isset($form) && isset($form.email) }]
                            {*{ $form.email }*}
                            {if isset($form.email.errors) }
                                <small class="error">
                                    { $form.email.errors.0 }
                                </small>
                            {/if}
                        {/if}
                        <p align="center"><strong></strong>
                            <input type="password" maxlength="50" size="50" name="password" required="" placeholder="Passwort eingeben" class="form-control" id="id_password"></p>
                        {if isset($form.password) }
                            {$form.password}
                            {if isset($form.password.errors)}
                                <small class="error">
                                    {$form.password.errors}
                                </small>
                            {/if}
                        {/if}
                        <p style="text-align: center"><input type="submit" name="Anmeldung" value="Anmeldung"></input> </p>
                    </form>
                </td> </tr>
            <tr><td colspan="3">  </td></tr>
            </tbody>

            <tr align="center">
                <td width="33%"> <p> <a href="Login_Paje.html">{trans}Passwort vergessen?{/trans}</a></p></td>
                <td width="33%"> <p> <a href="Copyright.html">{trans}Copyright ZAGmbH{/trans}</a></p></td>
                <td width="33%"> <p> <a href="Impressum.html">{trans}Impressum{/trans}</a></p></td>
            </tr>

        </table>
    </div>
</div>
</body>
</html>