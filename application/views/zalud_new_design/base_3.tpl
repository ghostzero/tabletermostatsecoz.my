<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width.initial-scale=1.0">
    <title>My Home Visit</title>
    <link href="/css/zalud_new_design/style.css" rel="stylesheet">
    <link href="/css/graph.css" rel="stylesheet">
</head>

{literal}
    <script type="text/javascript">
        $(function () {
            $('#language_chooser').on('change', function () {
//                $(this).closest('form').submit();
                /*$.post( "#language_chooser", { name: "John", time: "2pm" }).done(function( data ) {
                 alert( "Data Loaded: " + data );
                 });*/
                var language_code = $('#language_chooser').find(':selected').attr('value');
                $.post( "/user/set_language", {language_code: language_code}).done(function( data ) {
                    var test1=1;
                    window.location.reload();
                });


            });

            /* $.post( "#language_chooser", { name: "John", time: "2pm" }).done(function( data ) {
             alert( "Data Loaded: " + data );
             });*/

            $('.search_input').keydown(function (e) {
                if (e.keyCode == 13) {
                    var search_term = $.trim($(this).val());
                    if (search_term) {
                        if (search_term.length >= 3) {
                            return true;
                        } else {
                            alert('Please give more than 3 characters');
                            return false;
                        }
                    } else {
                        return true;
                    }
                }
            })
        });
    </script>
{/literal}

<body>
<div data-role="page" id="devices">
    <h1><font size="8" face= "DejaVu Serif">MyHomeVisit</font>
        <p align="right"> <font size="5" face= "DejaVu Serif">Das Kundenportal der ZAGmbH</font></p>
    </h1>
    {block name=mainbody}{/block}

</div>
</body>
</html>