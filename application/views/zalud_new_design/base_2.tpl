<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width.initial-scale=1.0">
    <meta http-equiv="cache-control" content="no-cache" />
    <title>My Home Visit</title>
    <link href="/css/zalud_new_design/style.css" rel="stylesheet">
    <script src="/css/zalud/static/vendor/jquery/jquery.min.js"></script>
    <script src="/css/zalud/static/vendor/popper/popper.min.js"></script>
    <script src="/css/zalud/static/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="/css/zalud/static/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="/css/zalud/static/vendor/chart.js/Chart.min.js"></script>
    <script src="/css/zalud/static/vendor/datatables/jquery.dataTables.js"></script>
    <script src="/css/zalud/static/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="/css/zalud/site-static/js/sb-admin.js"></script>
    <!-- Custom scripts for this page-->
    <script src="/css/zalud/site-static/js/sb-admin-datatables.min.js"></script>

</head>

{literal}
    <script type="text/javascript">
        $(function () {
            $('#language_chooser').on('change', function () {
//                $(this).closest('form').submit();
                /*$.post( "#language_chooser", { name: "John", time: "2pm" }).done(function( data ) {
                 alert( "Data Loaded: " + data );
                 });*/
                var language_code = $('#language_chooser').find(':selected').attr('value');
                $.post( "/user/set_language", {language_code: language_code}).done(function( data ) {
                    var test1=1;
                    window.location.reload();
                });


            });

            /* $.post( "#language_chooser", { name: "John", time: "2pm" }).done(function( data ) {
             alert( "Data Loaded: " + data );
             });*/

            $('.search_input').keydown(function (e) {
                if (e.keyCode == 13) {
                    var search_term = $.trim($(this).val());
                    if (search_term) {
                        if (search_term.length >= 3) {
                            return true;
                        } else {
                            alert('Please give more than 3 characters');
                            return false;
                        }
                    } else {
                        return true;
                    }
                }
            })
        });
    </script>
{/literal}

<body>
<div data-role="page" id="devices">
    <h1><font size="8" face= "DejaVu Serif">MyHomeVisit</font>
        <p align="right"> <font size="5" face= "DejaVu Serif">Das Kundenportal der ZAGmbH</font></p>
    </h1>

    {include file="$tpl_dir/zalud_new_design/includes/lang_chooser.tpl" }

    <div class="ui-field-contain">
            <nav>
            <ul class="site-navigation"><font size="5" face="DejaVu Serif">
                    <li><a href="/user/index">{trans}Overview{/trans}</a> </li>
                    <li><a href="/user/service">{trans}Service{/trans}</a> </li>
                    <li><a href="/user/user">{trans}User{/trans}</a> </li>
                    {if isset($current_role_repeat) && ($current_role_repeat=="admin" || $current_role_repeat=="plumber")}<li><a href="/user/logined_page">{trans}Registration user{/trans}</a></li>{/if}
                    <li><a href="/user/addition_service"><output name="result">{$username}</output></a></Li>
                    <form name="test" method="post" action="input1.php">
                        <p align="right"><font font size="4" face="DejaVu Serif"><a href="/login/logout">{trans}Log out{/trans}</a></font> </p>
                        <p align="right"><input type="text" size="40" placeholder="Suchen und Filtern"></p>
                    </form>
            </ul>

        </nav>
    </div>
    {block name=mainbody}{/block}
</div>
</body>
</html>