{extends file="$tpl_dir/zalud_new_design/base_3.tpl"}

{block name=mainbody}

    <div class="ui-field-contain">
             <table align="left">
            <tr width="100%">
                <td width="500px"> <a href="/user/index"><h3><font size="5" face= "DejaVu Serif"><output name="result"></output>1708000000001</font></h3></a></td>
                <td width="1100px"><h3><font size="5" face= "DejaVu Serif">{trans}Heating Circuits{/trans}</font></h3></td>
                <td width="250px"><h3><font size="5" face= "DejaVu Serif"> <a href="/addition_service/leistungswerte">{trans}Back{/trans}</a></font></h3></td>
            </tr>
        </table>
    </div>



    <div class="content1" id="paramtabl">
        <table class="tabl" align="left" width="100%">

            <tr>
                <td width="150px" height="150px" rowspan="4"><img class="logoMain" src="/Serega_template_1/images/Heizkreise.PNG" align="left"></td>
                <td width="200px">{trans}Heating Circuits{/trans} 1</td>
                <td colspan="6">
                    <div class="ui-slider ui-slider-horizontal ui-corner-all ui-widget ui-widget-content" id="slider1" >
                        <div class="ui-slider-range ui-corner-all ui-widget-header" style="left: 15%; width: 43.4%;"></div>
                        <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 15%;"></span>
                        <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 58.4%;"></span>

                    </div>


                </td>
            </tr>
            <tr>
                <td width="100px"></td>
                <td>{trans}Setpoint:{/trans}</td>
                <td><output name="result10">0</output></td>
                <td>°C</td>
                <td>{trans}Consumption day:{/trans}</td>
                <td><output name="result11">0</output></td>
                <td>kWh</td>
            </tr>


            <tr>
                <td width="100px"></td>
                <td>{trans}Actual value:{/trans}</td>
                <td><output name="result12">0</output></td>
                <td>°C</td>
                <td>{trans}Consumption month:{/trans}</td>
                <td><output name="result13">0</output></td>
                <td>kWh</td>
            </tr>
            <tr>
                <td width="100px"></td>
                <td>{trans}Consumption:{/trans}</td>
                <td><output name="result14">0</output></td>
                <td>kWh</td>
                <td>{trans} Consumption year:{/trans}</td>
                <td><output name="result15">0</output></td>
                <td>kWh</td>
            </tr>


        </table>

        <table class="tabl" align="left" width="100%">

            <tr>
                <td width="150px" height="150px" rowspan="4"><img class="logoMain" src="/Serega_template_1/images/Heizkreise.PNG" align="left"></td>
                <td width="200px">{trans}Heating Circuits{/trans} 2</td>
                <td colspan="6">
                    <div class="ui-slider ui-slider-horizontal ui-corner-all ui-widget ui-widget-content" id="slider1" >
                        <div class="ui-slider-range ui-corner-all ui-widget-header" style="left: 15%; width: 43.4%;"></div>
                        <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 15%;"></span>
                        <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 58.4%;"></span>

                    </div>


                </td>
            </tr>
            <tr>
                <td width="100px"></td>
                <td>{trans}Setpoint:{/trans}</td>
                <td><output name="result16">0</output></td>
                <td>°C</td>
                <td>{trans}Consumption day:{/trans}</td>
                <td><output name="result17">0</output></td>
                <td>kWh</td>
            </tr>


            <tr>
                <td width="100px"></td>
                <td>{trans}Actual value:{/trans}</td>
                <td><output name="result18">0</output></td>
                <td>°C</td>
                <td>{trans}Consumption month:{/trans}</td>
                <td><output name="result19">0</output></td>
                <td>kWh</td>
            </tr>
            <tr>
                <td width="100px"></td>
                <td>{trans}Consumption:{/trans}</td>
                <td><output name="result20">0</output></td>
                <td>kWh</td>
                <td> {trans}Consumption year:{/trans}</td>
                <td><output name="result21">0</output></td>
                <td>kWh</td>
            </tr>


        </table>

    </div>

{/block}