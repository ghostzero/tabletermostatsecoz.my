{extends file="$tpl_dir/zalud_new_design/base_3.tpl"}

{block name=mainbody}
{*{debug}*}

    <table align="left">
        <tr width="100%">
            <td width="700px"><h3><font size="5" face= "DejaVu Serif">{trans}Regulation{/trans} <output name="result"></output></font></h3></td>
            <td width="650px"><h3><font size="5" face= "DejaVu Serif">{trans}Graphic Overview{/trans}</font></h3></td>
            <td width="50px"></td>
            <td width="250px"><h3><font size="5" face= "DejaVu Serif"> <a href="/addition_service/leistungswerte">{trans}Back{/trans}</a></font></h3></td>
        </tr>
        <tr>
            <td width="700px"></td>
            <td width="650px"></td>
            <td width="250px"></td>
            <td>            </td>

        </tr>
    </table>




    <br/><form method="post" action="view_device" accept-charset="utf-8"> <br/>
    <script type="text/javascript" src="/script/loader.js" xmlns="http://www.w3.org/1999/html"></script>
    {if isset($output_array_for_graph['array_names_of_line'])}
        <div id="chart_div" ></div>
    {/if}

    <p> <font size="5" face="DejaVu Serif">{trans}Select the device for shove graphiс{/trans}</font> <select size="1" name="device_id">
            <option disabled>{trans}Select a device{/trans}</option>
            {foreach from=$array_devices_all item=foo}
                {if $device_id==$foo.id}
                    <option  selected="selected" value={$foo.id}>{$foo.modename}</option>
                {else}
                    <option value={$foo.id}>{$foo.modename}</option>
                {/if}
            {/foreach}
        </select>
    </p>



    <p>{trans}Please select parameter:{/trans}</p>
    <table style="width: 100%;" border="2">
        {foreach from=$output_array item=one_group}

        <tr >
                {*<input type="checkbox" id="subscribeNews" name="parameter" value="{$one_group.group.name}">*}
                <td {if $one_group.group.name == 'TempRefCurrent'} rowspan="2" {/if}><label for="subscribeNews">{$one_group.group.name}</label></td>
            {foreach from=$one_group.array_extension key=key_of_group item=one_element_of_group}
                {if $key_of_group==8}
                    </tr>
                    <tr>
                {/if}

                {if $key_of_group<8}
                    <td>
                        <input type="checkbox" id="subscribeNews" name="{$one_group.group.name}_{$one_element_of_group.name}" value="{$one_element_of_group.id}">
                        <label for="subscribeNews">{$one_element_of_group.name}</label>
                    </td>
                {else}
                    <td>
                        <input type="checkbox" id="subscribeNews" name="{$one_group.group.name}_{$one_element_of_group.name}" value="{$one_element_of_group.id}">
                        <label for="subscribeNews">{$one_element_of_group.name}</label>
                    </td>

                {/if}
            {/foreach}
                    </tr>
        {/foreach}
    </table>


    <table>
        <tr style="width: 100%"></tr>
        <tr style="width: 100%"></tr>
        <tr style="width: 100%"></tr>
        <tr style="width: 100%"></tr>
    </table>
    <button type="submit">{trans}<font size="4" face="DejaVu Serif">{trans}Draw Graphiс{/trans}</font>{/trans}</button> <br/></form> <br/>



    {if isset($output_array_for_graph['array_names_of_line'])}
{literal}
    <script>

        //        var time=new Date(2014, 0);
        //        var time_1=new Date(2014, 1);
        //        var time_2=new Date(2014, 2);
        //        var time_3=new Date(2014, 3);


        try{



            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);



        function drawChart() {
            var array_names_of_line =[];
            var array_all_valuse_name =[];
            {/literal}{foreach from=$output_array_for_graph['array_names_of_line'] item=one_row_name}
            array_names_of_line.push('{$one_row_name}');
            {/foreach}
            array_all_valuse_name[0] =array_names_of_line;

            {foreach from=$output_array_for_graph['array_valuse_name'] item=all_row_values name=position_value}
            var array_valuse_name =[];
            {foreach from=$all_row_values item=one_row_values}
            array_valuse_name.push({$one_row_values});
            {/foreach}
            array_all_valuse_name[{$smarty.foreach.position_value.iteration}] = array_valuse_name;
            {/foreach}
            {literal}
            var data = google.visualization.arrayToDataTable(array_all_valuse_name);

            var options = {
                chart: {
                    title: 'Company Performance',
                    subtitle: 'Sales, Expenses, and Profit: 2014-2017',
                },
                bars: 'vertical',
                hAxis: {
//                    format: 'yyyy-MM-dd H:m:s',
                    format: 'yyyy.MM.dd',
//                    format: 'H:m:s',
                    gridlines: {count: 5}
                },
                series: {
                    'Y1': {
                        strokeWidth: 4.0,
                        drawPoints: true,
                        pointSize: 8,
                        highlightCircleSize: 5
                    }
                },
                vAxis: {format: 'decimal'},
                height: 800,
                colors: ['#1b9e77', '#d95f02', '#7570b3'],
                chartArea: {
                    left: 40,
                    top: 10,
                    right: 40,

                    width: '100%',
                    height: 500
                },
                legend: { position: 'bottom', alignment: 'start' }

            };

            var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));

            chart.draw(data, options);
        }
        }catch(e) {

            console.log('Error ' + e.name + ":" + e.message + "\n" + e.stack);
        }



    </script>

{/literal}
    {/if}

{/block}


