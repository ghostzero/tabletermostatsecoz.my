{extends file="$tpl_dir/zalud_new_design/base_2.tpl"}

{block name=mainbody}

    <p>View all users</p>

    <table id="parameters" border="1" cellspacing="2" width="100%" >
        <tbody>
        <tr>
            <th width="5%">№</th>
            <th width="10%" >Username</th>
            <th width="70px" >Email</th>
            <th width="100px" >Password</th>
            <th width="100px" >Last login</th>
            <th width="100px" >Logins</th>
            <th width="70px" >Alies</th>
            <th width="70px" >Address</th>
            <th width="70px" >Owner name</th>
            <th width="70px" >Action</th>
        </tr>

        {$i=0}
        {if isset($array_show_params)}

            {foreach from=$array_show_params item=foo}
                <tr {$i++}>
                    <th width="70px">{$i++}</th>
                    <th width="70px">{$foo.username}</th>
                    <th width="200px">{$foo.email}</th>
                    <th width="70px">{$foo.last_login}</th>
                    <th width="100px">{$foo.logins}</th>
                    <th width="70px">{$foo.alies}</th>
                    <th width="70px">{$foo.address}</th>
                    <th width="70px">{$foo.owner_name}</th>
                    <th width="70px">{$foo.name_role}</th>
                    <th width="70px"><a href="/addition_service/change_parameter_only_user?user_id={$foo.id}">Edit</a></th>
                </tr>

            {/foreach}
        {/if}


        </tbody>
    </table>









{/block}