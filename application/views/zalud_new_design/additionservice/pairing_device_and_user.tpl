{extends file="$tpl_dir/zalud_new_design/base_2.tpl"}

{block name=mainbody}
    {*{debug}*}
    <div class="content1">
        <form method="post" action="/addition_service/pairing_device_and_user">

            <p>Select the device for pairing <select size="1" name="devices">
                    <option disabled>Select a device</option>
                    {foreach from=$array_devices_new item=foo}
                            <option value={$foo.id}>{$foo.modename}</option>
                    {/foreach}
             </select>
            </p>

            <p>Select user for pairing <select size="1" name="user">
                    <option disabled>Select a device</option>
                    {foreach from=$array_users_all item=foo}
                        <option value={$foo.id}>{$foo.username}</option>
                    {/foreach}
                </select>
            </p>


            <input id="acrtion_of_link_device_user" name="acrtion_of_link_device_user" type="hidden" value="create">
            <p align="left"><strong>{trans}Alies{/trans}:</strong>
                <input maxlength="50" size="50" name="alies"></p>
            <p align="left"><strong>{trans}Address{/trans}:</strong>
                <input maxlength="50" size="50" name="address"></p>
            <p align="left"><strong>{trans}Owner Name{/trans}:</strong>
                <input maxlength="50" size="50" name="owner_name"></p>


            <p style="text-align: center"><input type="submit" name="singin" value="Erstellen Sie ein Profil"/> </p>
        </form>


    </div>

    <div class="content2">

        All paired devices
        <table class="base_table" style="width:100%">
            <tr align="left">
                <th>Devise</th>
                <th>User</th>
                <th>Alies</th>
                <th>Address</th>
                <th>Owner name</th>
                <th>Unpairing</th>
            </tr>

            <h1 style="display: none;">
                {$array_binding|@print_r}
            </h1>


            {foreach from=$array_binding item=foo}
                <tr>
                    <td>{$foo.modename}</td>
                    <td>{$foo.username}</td>
                    <td>{$foo.alies}</td>
                    <td>{$foo.address}</td>
                    <td>{$foo.owner_name}</td>
                    <td><input class="RemoveButton" id="RemoveButton"  type="button" name="{$foo.device_id}" value="Remove" /></td>
                </tr>

            {/foreach}


        </table>

    </div>

    {literal}
        <script>

            $("input.RemoveButton").click(function(){


//                var device_id = $("input.RemoveButton").attr('name');
                var device_id = $(this).attr('name');
//                $(this).reset();
//                alert("RemoveButton".device_id);
                $.post('/addition_service/pairing_device_and_user', {device_id:device_id}, function(data){
//                    $(this).parent().parent().remove();
                    window.location.replace("/user/addition_service");
//                    location.reload();
//                    $(this).remove();
//                    alert("delete link  device_id:" +device_id);


                })

            });


           /* $('#RemoveButton').click(function(){
                var device_id = this.attr('name');
                $.post('/addition_service/pairing_device_and_user', {device_id:device_id}, function(data){
                  alert("delete link");
            })*/
        </script>
    {/literal}

{/block}