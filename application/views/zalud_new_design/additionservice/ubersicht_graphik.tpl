{extends file="$tpl_dir/zalud_new_design/base_3.tpl"}

{block name=mainbody}

    <script type="text/javascript" src="/script/loader.js"></script>

    <div class="ui-field-contain">
             <table align="left">
            <tr width="100%">
                <td width="700px"><h3><font size="5" face= "DejaVu Serif"><output name="result"></output>1708000000001</font></h3></td>
                <td width="900px"><h3><font size="5" face= "DejaVu Serif">Leistungswerte Grafick</font></h3></td>
                <td width="250px"><h3><font size="5" face= "DejaVu Serif">Zurück</font></h3></td>
            </tr>
        </table>
    </div>

    <div class="content">
        <h3><font size="5" face= "DejaVu Serif">Graphische Darstellung der letzen 24</font></h3>
    </div>

    <div class="content1" id="graf">
        <table class="tabl8" align="left" width="100%" height="80px">

            <tr>
                <td width="90%" height="70"><div id="chart_div" style="height:100%;width:100%;overflow:hidden;"></div></td>


            </tr>
        </table>

    </div>


{literal}
    <script>

        //        var time=new Date(2014, 0);
        //        var time_1=new Date(2014, 1);
        //        var time_2=new Date(2014, 2);
        //        var time_3=new Date(2014, 3);

        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);



        function drawChart() {
            var array_names_of_line =[];
            var array_all_valuse_name =[];


            {/literal}{foreach from=$output_array_for_graph['array_names_of_line'] item=one_row_name}
            array_names_of_line.push('{$one_row_name}');
            {/foreach}
            array_all_valuse_name[0] =array_names_of_line;

            {foreach from=$output_array_for_graph['array_valuse_name'] item=all_row_values name=position_value}
            var array_valuse_name =[];
            {foreach from=$all_row_values item=one_row_values}
            array_valuse_name.push({$one_row_values});
            {/foreach}
            array_all_valuse_name[{$smarty.foreach.position_value.iteration}] = array_valuse_name;
            {/foreach}
            {literal}
            var data = google.visualization.arrayToDataTable(array_all_valuse_name);

            var options = {
                chart: {
                    title: 'Company Performance',
                    subtitle: 'Sales, Expenses, and Profit: 2014-2017',
                },
                bars: 'vertical',
                hAxis: {
//                    format: 'yyyy-MM-dd H:m:s',
//                    format: 'yyyy-MM-dd',
                    format: 'H:m:s',
                    gridlines: {count: 25}
                },
                vAxis: {format: 'decimal'},
                height: 800,
                colors: ['#1b9e77', '#d95f02', '#7570b3']
            };

            var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));

            chart.draw(data, options);
        }
    </script>
{/literal}


{/block}