{extends file="$tpl_dir/zalud_new_design/base_2.tpl"}

{block name=mainbody}
    {*{debug}*}
    <div class="first">
        <div class="second">


            <table class="tabl1" align="center" width="800px" height="250px">
                <thead>
                <tr><td colspan="3">{*<img class="logoMain" src="images/logo.PNG" align="right"/>*}</td> </tr>
                </thead>

                <tbody>
                <tr><td colspan="3">  </td></tr>
                <tr><td colspan="3"><h2 align="center"><font size="5" face="DejaVu Serif">{trans}User registration{/trans}</font></h2> </td> </tr>
                <tr><td colspan="3">
                        <form action="/user/registration" class="direct_right" method="post" accept-charset="utf-8">
                            <p align="right">{trans}Please select a user:{/trans}</p>
                            <div align="right">

                                <label for="contactChoice1"><strong>{trans}Admin{/trans}</strong></label><input checked type="radio" id="type_of_user_1"
                                                                                                                name="type_of_user" value="admin"><br/>
                                <label for="contactChoice2">{trans}Plumber{/trans}</label><input type="radio" id="type_of_user_2"
                                                                                                 name="type_of_user" value="plumber"><br/>
                                <label for="contactChoice3">{trans}User{/trans}</label><input type="radio" id="type_of_user_3"
                                                                                              name="type_of_user" value="user"><br/>
                            </div>
                            <p align="right"><strong>{trans}Username{/trans}:</strong>
                                <input maxlength="50" size="50" name="username"></p>
                            <p align="right"><strong>{trans}Email{/trans}:</strong>
                                <input  maxlength="50" size="50" name="email"></p>
                            <p align="right"><strong>{trans}Password{/trans}:</strong>
                                <input type="password" maxlength="50" size="50" name="password"></p>
                            <p align="right"><strong>{trans}Repeat Password{/trans}:</strong>
                                <input type="password" maxlength="50" size="50" name="password_confirm"></p>
                            <p style="text-align: center"><button type="submit" name="singin" >{trans}Register{/trans}</button></p>
                        </form>
                    </td> </tr>
                <tr><td colspan="3">  </td></tr>
                </tbody>
            </table>

            <table class="tabl1" align="center" width="800px" height="250px">
                <tbody>
                <tr><td colspan="3"> </tr>
                <tr><td colspan="3"><h2 align="center"><font size="5" face="DejaVu Serif">{trans}Delete User{/trans}</font></h2> </td> </tr>
                <tr><td colspan="3">
                        <form action="/user/delete_user" class="direct_right" method="post" accept-charset="utf-8">
                            <p align="right">{trans}Please select login user:{/trans}
                            <select size="1" name="user">
                                    <option disabled>Select a user</option>
                                    {foreach from=$array_users_all_without_superuser item=foo}
                                        <option value={$foo.id}>{$foo.username}</option>
                                    {/foreach}
                                </select>
                            </p>

                           <p style="text-align: center"><button type="submit" name="singin" >{trans}Delete{/trans}</button></p>
                        </form>
                    </td> </tr>
                <tr><td colspan="3">  </td></tr>
                </tbody>
            </table>

        </div>
    </div>

{/block}