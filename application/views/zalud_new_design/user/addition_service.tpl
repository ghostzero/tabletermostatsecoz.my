{extends file="$tpl_dir/zalud_new_design/base_2.tpl"}

{block name=mainbody}

    <div class="content1">
        <table class="tabl8" align="left" width="100%" height="50px" style="border-spacing: 12px">

            <tr>
                <td width="30px"></td>
                <td width="200px" height="200px"><a href="/addition_service/actueleverte"><img class="logoMain" src="/Serega_template_1/images/Actueleverte.PNG" align="left"></a></td>
                <td width="200px" height="200px"><a href="/addition_service/leistungswerte"><img class="logoMain" src="/Serega_template_1/images/Leistungswerte.PNG" align="left"></a></td>
                <td width="200px" height="200px"><a href="#"><img class="logoMain" src="/Serega_template_1/images/System Monitor.PNG" align="left"></a></td>
                <td width="200px" height="200px"><a href="#"><img class="logoMain" src="/Serega_template_1/images/Inbetriebnahme.PNG" align="left"></a></td>
                <td width="200px" height="200px"><a href="#"><img class="logoMain" src="/Serega_template_1/images/Ablaufdatum.PNG" align="left"></a></td>
                <td width="200px" height="200px"><a href="#"><img class="logoMain" src="/Serega_template_1/images/Position.PNG" align="left"></a></td>
                <td width="10px"></td>
            </tr>

            <tr>
                <td width="30px"></td>
                <td><p align="center"><a href="/addition_service/actueleverte">{trans}Current Values{/trans}</a></p></td>
                <td><p align="center" ><a href="/addition_service/leistungswerte">{trans}Performance Data{/trans}</a></p></td>
                <td><p align="center" ><a href="#">{trans}System Monitor {/trans}</a></p></td>
                <td><p align="center" ><a href="#">{trans}Commissioning Statement {/trans}</a></p></td>
                <td><p align="center" ><a href="#">{trans}Garantie Ablaufdatum{/trans}</a></p></td>
                <td><p align="center" ><a href="#">{trans}Position{/trans}</a></p></td>
                <td width="10px"></td>
            </tr>



        </table>

    </div>
    <table> <tr height="50px"></tr></table>

    <div class="content2">
        <table class="tabl9" align="left" width="100%" height="80px" style="border-spacing: 12px">
            <tr>
                <td width="30px"></td>
                {if isset($current_role_repeat) && ($current_role_repeat=="admin" || $current_role_repeat=="plumber")}
                    <td width="200px" height="200px"><a href="/addition_service/pairing_device_and_user"><img class="user_administration" src="/Serega_template_1/images/reserve.PNG" align="left"></a></td>
                {else}
                    <td width="200px" height="200px"><a href="/addition_service/user_administration_for_user"><img class="logoMain" src="/Serega_template_1/images/reserve.PNG" align="left"></a></td>
                {/if}



                {if isset($current_role_repeat) && ($current_role_repeat=="admin" || $current_role_repeat=="plumber")}
                    <td width="200px" height="200px"><a href="/addition_service/user_administration"><img class="pairing_device_and_user" src="/Serega_template_1/images/reserve.PNG" align="left"></a></td>
                {else}
                    <td width="200px" height="200px"><a href="#"><img class="pairing_device_and_user" src="/Serega_template_1/images/reserve.PNG" align="left"></a></td>
                {/if}
                {if isset($current_role_repeat) && ($current_role_repeat=="admin" || $current_role_repeat=="plumber")}
                    <td width="200px" height="200px"><a href="/addition_service/superuser_user_administration"><img class="pairing_device_and_user" src="/Serega_template_1/images/reserve.PNG" align="left"></a></td>
                {else}
                    <td width="200px" height="200px"><a href="#"><img class="logoMain" src="/Serega_template_1/images/reserve.PNG" align="left"></a></td>
                {/if}
                <td width="200px" height="200px"><a href="#"><img class="logoMain" src="/Serega_template_1/images/reserve.PNG" align="left"></a></td>
                <td width="200px" height="200px"><a href="#"><img class="logoMain" src="/Serega_template_1/images/reserve.PNG" align="left"></a></td>
                <td width="200px" height="200px"><a href="#"><img class="logoMain" src="/Serega_template_1/images/reserve.PNG" align="left"></a></td>
                <td width="10px"></td>
            </tr>
            <tr></tr>
            <tr>
                <td width="30px"></td>
                {if isset($current_role_repeat) && ($current_role_repeat=="admin" || $current_role_repeat=="plumber")}
                    <td><p align="center" ><a href="/addition_service/pairing_device_and_user">Pairing Device and User</a></p></td>
                {else}
                    <td><p align="center" ><a href="/addition_service/user_administration_for_user">user_administration_for_user</a></p></td>
                {/if}


                {if isset($current_role_repeat) && ($current_role_repeat=="admin" || $current_role_repeat=="plumber")}
                    <td><p align="center" ><a href="/addition_service/user_administration">User Administration</a></p></td>
                {else}
                    <td><p align="center" ><a href="#">Reserved</a></p></td>
                {/if}


                {if isset($current_role_repeat) && ($current_role_repeat=="admin" || $current_role_repeat=="plumber")}
                    <td><p align="center" ><a href="/addition_service/superuser_user_administration ">SUPERUSER_USER_ADMINISTRATION</a></p></td>
                {else}
                    <td><p align="center" ><a href="#">Reserved</a></p></td>
                {/if}



                <td><p align="center" ><a href="#">Reserved</a></p></td>
                <td><p align="center" ><a href="#">Reserved</a></p></td>
                <td><p align="center" ><a href="#">Reserved</a></p></td>
                <td><p align="center" ><a href="#">Reserved</a></p></td>
                <td><p align="center" ><a href="#">Reserved</a></p></td>
                <td width="10px"></td>
            </tr>
        </table>
    </div>






{/block}