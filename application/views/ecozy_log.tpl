<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>PHP программирование - тестирование функций онлайн. php строки, php функции, php справочник - все это php сайт</title>
    <link rel="stylesheet" media="screen" href="/css/common.css" type="text/css"/>
    {*<meta http-equiv="refresh" content="0;url=" />*}
</head>
<body>
<p> Output termostat ecozy </p>
<form action="index" method="post">
    Ввидите макадресс для поиска    <input type="text" name="mac" size="40" value="{if isset($mac_current_device)}{$mac_current_device}{/if}">
    {if isset($array_base_row_by_mac) && isset($number_hours)}
    <div>Начальная дата:<select name="select_from_datetime" id="select_from_datetime">
            {foreach from=$array_base_row_by_mac item=one_row}
                <option value="{$one_row['datetime']}" {if isset($seted_select_from_datetime) && $one_row['datetime']==$seted_select_from_datetime} selected="selected" {/if}>{$one_row['datetime']}</option>
            {/foreach}
        </select>
        Конечная дата:
        <select name="number_hours" id="number_hours">
            {for $current_hours=1 to $number_hours}
                <option value="{$current_hours}" {if (isset($seted_number_hours) &&  $current_hours==$seted_number_hours)|| $current_hours==$number_hours   } selected="selected" {/if}>{$current_hours}</option>
            {/for}
        </select>
    </div>
    {/if}
    <input type="submit" value="Отправить" >
</form>

<br/>
<b><a href="/list_mac_adress">Список мак адресов.</a></b>
<p> {$message}</p>
{if isset($mac_current_device)} <b>{$mac_current_device}</b><br> <b><a href="./excell/current_excell{$mac_current_device}.xlsx">Скачать excell файл по данному термостату.</a><br>
    <a target="_blank" href="/statistic_chart_thermostat?mac={$mac_current_device}">Просмотреть график.</a><br><a target="_blank" href="/statistic_chart_thermostat_second?mac={$mac_current_device}">Просмотреть график кривой с полупрозрачной заливкой.</a></b>{else}Не выбран мак адрес термостата.{/if}
<table class="table_subcategory">
    <tr>
        <th class="number_colum">№</th>
        <th class="name_subcategory">Дата и время</th>
        <th class="name_subcategory">Целевая температура</th>
        <th class="name_subcategory">Текущее состояние температуры</th>
        <th class="name_subcategory">Состояние плунжера</th>
    </tr>
    {if isset($array_row_by_mac)}
    {foreach from=$array_row_by_mac item=one_subcategory name=base_one_subcategory}
        {if $one_subcategory['installed_temperature']!=0 && $one_subcategory['datetime']!=0 && $one_subcategory['condition_plunger']==0 && $one_subcategory['current_temperature']==0}
            <tr class="green_text_th">
                <td>{$smarty.foreach.base_one_subcategory.iteration}</td>
                <td ><span>{$one_subcategory['datetime']}</span></td>
                <td ><span>{$one_subcategory['installed_temperature']}</span></td>
                <td>{$one_subcategory['current_temperature']}</td>
                <td>{$one_subcategory['condition_plunger']}</td>
            </tr>
        {else}
            <tr>
                <td>{$smarty.foreach.base_one_subcategory.iteration}</td>
                <td ><span>{$one_subcategory['datetime']}</span></td>
                <td ><span>{$one_subcategory['installed_temperature']}</span></td>
                <td>{$one_subcategory['current_temperature']}</td>
                <td>{$one_subcategory['condition_plunger']}</td>
            </tr>
        {/if}



    {/foreach}{/if}
</table>

{if isset($outputtextresult)}<b>{$outputtextresult}</b>{/if}
</body>
</html>
