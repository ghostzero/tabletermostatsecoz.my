<div class="paginate">
	<!-- first and previous -->
    {if $first_page}
        <a href="{$page->url($first_page)}">&laquo;</a>
    {else}
        &laquo;
    {/if}
    {if $previous_page}
        <a href="{$page->url($previous_page)}">&lsaquo;</a>
    {else}
        &lsaquo;
    {/if}
    
    <!-- main -->
    {section name=pages start=1 loop=$total_pages}
        {assign var='i' value=`$smarty.section.pages.index`}
        {if $i==$current_page}
            <b>{$i}</b>
        {else}
            <a href="{$page->url($i)}">{$i}</a>
        {/if}
    {/section}
    
    <!-- next and last -->
    {if $next_page}
        <a href="{$page->url($next_page)}">&rsaquo;</a>
    {else}
        &rsaquo;
    {/if}
    {if $last_page}
        <a href="{$page->url($last_page)}">&raquo;</a>
    {else}
        &raquo;
    {/if}
</div>
