{include file="$tpl_dir/header.tpl"}
<div class="b-body-bg">
<div class="b-body">
<div class="b-left">	
<div class="l-menu-right">
<ul class="b-menu">
{foreach item=output from=$output_first}
{if $output.category!='feedback'}
<li>{if isset($output.category)&& $output.category}{$output.category}{/if}</li>
{foreach item=method from=$output.methods}
<li><a href="/{if isset($method.name)&& $method.name}{$method.name}{/if}.html">{if isset($method.name)&& $method.name}{$method.name}{/if}</a></li>
{/foreach}
{/if}
{/foreach}
</ul>
</div>
</div>
<div class="b-content">
<h1 class="b-main-descr-title">{if isset($static_page.title)&& $static_page.title}{$static_page.title}{/if}</h1>
<div class="refsect1 description">
<div class="root">
{if isset($static_page.content)&& $static_page.content}{$static_page.content}{/if}
</div>
</div>
<div class="refsect1 description">
{literal}
	<div id="disqus_thread"></div>
			<script type="text/javascript">
		/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
		var disqus_shortname = 'livephp'; // required: replace example with your forum shortname

		// The following are highly recommended additional parameters. Remove the slashes in front to use.
		var disqus_identifier = 'news{/literal}{if isset($shownews.id)&& $shownews.id}{$shownews.id}{/if}{literal}';
		var disqus_url = 'http://livephp.net/static/{/literal}{if isset($static_page.name)&& $static_page.name}{$static_page.name}{/if}{literal}.html';

		/* * * DON'T EDIT BELOW THIS LINE * * */
		(function() {
			var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
			dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
			(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
		})();
	</script>
	<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
	<a href="http://disqus.com" class="dsq-brlink">blog comments powered by <span class="logo-disqus">Disqus</span></a>	
{/literal}
</div>
</div>
{include file="$tpl_dir/rightpartofbody.tpl"}
</div>
</div>
</div>
</div>
{include file="$tpl_dir/footer.tpl"}
