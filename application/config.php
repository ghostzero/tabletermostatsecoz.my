<?php
define ( 'BLOG_CLASSES', APPPATH . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'blog' . DIRECTORY_SEPARATOR );
define ( 'LIBS_CLASSES', APPPATH . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'libs' . DIRECTORY_SEPARATOR );
define ( 'LOG_DIR', APPPATH . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR );
define ( 'COOKIE_CACHE', APPPATH . 'cache' . DIRECTORY_SEPARATOR . 'cookies' . DIRECTORY_SEPARATOR );
define ( 'CURL_VERBOSE_DIR', APPPATH . 'cache' . DIRECTORY_SEPARATOR . 'curl_verbose' . DIRECTORY_SEPARATOR );

define ( 'POSTS_PER_CRONPOSTER_SESSION', 15 );

//TASK AND PROJECT STATUSES
define ( 'ALL', 5 );
define ( 'QUEUE', 0 );
define ( 'COMPLETED', 1 );
define ( 'IN_PROGRESS', 2 );
define ( 'PENDING', 3 );
define ( 'FAILED', - 1 );

//BLOGS, PROJECTS PAGINATION
define ( 'ITEMS_PER_PAGE', 15 );

//WEBMONEY
define ( 'SECRET_KEY', '3jd2jeh3k#235' );
define ( 'PAYMENT_ITEM_PRICE', 9 );

//PROXY
define ( 'USE_PROXY', true );

define ( 'LOG_CRONPOSTER', true );
define ( 'PROJECT_NAME', 'Posterator' );

//TARIFFS
define ( 'BY_POST', 4 );

define ( 'NUM_DEMO_POSTS', 50 );

//META TAGS
define ( 'META_KEYWORDS', 'posterator, кросспостинг, автоматический постинг, автопостинг, блоггер, сервис блогов, отправка блоги, постератор' );
define ( 'META_DESCRIPTION', 'Posterator - это сервис, который предоставляет возможность автоматической отправки сообщений во множество блогов' );

define('STATIC_PAGE_OF_CATEGORY', 213);
?>
