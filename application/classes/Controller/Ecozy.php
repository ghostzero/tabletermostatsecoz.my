<?php
/**
 * Created by PhpStorm.
 * User: ghost
 * Date: 26.10.16
 * Time: 16:14
 */

require_once("./application/classes/Decodezigbee/Ecozy_Server.php");
defined('SYSPATH') or die ('No direct script access.');


class Controller_Ecozy extends Controller_Base
{
    public $current_user;
    public $template = '/ecozy/index.tpl';

    public function action_index(){


    }


    public function action_view_decode_by_one_mac()
    {
//        $result=Mango::factory('Logssqlecozy')->get_all_logs();
//        $result = Mango::factory('Logssqlecozy')->get_one_by_hub_id("ObjectId(57d646b35e7f009a081b04e8)");
//        ORM::factory("Category")->get()
//        ORM::factory("Logsnewsqlecozy")->get_all_colum_from_logs();
        $this->template = View::factory('/ecozy/view_decode_by_one_mac.tpl');

        if (!empty ($_POST ['mac'])) {
            $array_base_row_by_mac = ORM::factory('Decodereport')->get_rows_by_mac_sort_asc($_POST ['mac']);
            $array_row_by_mac = ORM::factory('Decodereport')->get_rows_by_mac($_POST ['mac']);
            if (!empty ($_POST ['select_from_datetime']) && !empty ($_POST ['number_hours'])) {
                $array_row_by_mac = ORM::factory('Decodereport')->get_rows_by_mac_sort_asc_by_datetime_one_hour($_POST ['mac'], $_POST['select_from_datetime'], $_POST['number_hours']);
                $this->template->seted_select_from_datetime = $_POST['select_from_datetime'];
                $this->template->seted_number_hours = $_POST['number_hours'];
            } else
                $array_row_by_mac = ORM::factory('Decodereport')->get_rows_by_mac_sort_asc($_POST ['mac']);

            $old_current_temperature = -1;
            foreach ($array_row_by_mac as $key => $one_row) {
                if ($one_row['current_temperature'] == 0 && $one_row['installed_temperature'] == 0 && $one_row['condition_plunger'] == 0) {
                    unset($array_row_by_mac[$key]);
                    continue;
                }
                if ($one_row['installed_temperature'] != 0) {
                    $old_current_temperature = $one_row['installed_temperature'];
                } elseif ($old_current_temperature != -1) {
                    $array_row_by_mac[$key]['installed_temperature'] = $old_current_temperature;
                }
//                $one_row['installed_temperature']
            }
            $first_element = reset($array_base_row_by_mac);
            $date_first_element = $first_element['datetime'];
            $last_element = end($array_base_row_by_mac);
            $date_last_element = $last_element['datetime'];
            $unix_date_first_element = strtotime($date_first_element);
            $unix_date_last_element = strtotime($date_last_element);
            $number_hours = intval(($unix_date_last_element - $unix_date_first_element) / 3600);
            $this->template->number_hours = $number_hours;
//            $array_row_by_mac = ORM::factory('Decodereport')->get_rows_by_mac($_POST ['mac']);
            $this->create_excell_for_mac_adress($array_row_by_mac, $_POST ['mac']);
            $this->template->array_row_by_mac = $array_row_by_mac;
            $this->template->mac_current_device = $_POST ['mac'];
            $this->template->array_base_row_by_mac = $array_base_row_by_mac;
        }
//        new Mongo
//            $result=Mango::factory('Logecozy')->get_all_logs();
//            $result=Mango::factory("Logs")->load()->as_array();
//        Mango::factory("Logs")
//        $mongoobject = new Mango();
        $this->template->message = "test";
    }


    public function action_view_decode_report_full_by_one_mac()
    {
//        $array_decode_report_full_with_report_by_mac = ORM::factory('')\
        $this->template = View::factory('/ecozy/view_decode_report_full_by_one_mac.tpl');
        if (!empty ($_POST ['mac'])) {
            $array_base_row_by_mac = ORM::factory('Decodereportfull')->get_rows_by_mac_sort_asc($_POST ['mac']);
            $array_row_by_mac = ORM::factory('Decodereportfull')->get_rows_by_mac($_POST ['mac']);
            if (!empty ($_POST ['select_from_datetime']) && !empty ($_POST ['number_hours'])) {
                $array_row_by_mac = ORM::factory('Decodereportfull')->get_rows_by_mac_sort_asc_by_datetime_one_hour($_POST ['mac'], $_POST['select_from_datetime'], $_POST['number_hours']);
                $this->template->seted_select_from_datetime = $_POST['select_from_datetime'];
                $this->template->seted_number_hours = $_POST['number_hours'];
            } else
                $array_row_by_mac = ORM::factory('Decodereportfull')->get_rows_by_mac_sort_asc($_POST ['mac']);

            $old_current_temperature = -1;
            foreach ($array_row_by_mac as $key => $one_row) {
                if ($one_row['current_temperature'] == 0 && $one_row['installed_temperature'] == 0 && $one_row['condition_plunger'] == 0) {
                    unset($array_row_by_mac[$key]);
                    continue;
                }
                if ($one_row['installed_temperature'] != 0) {
                    $old_current_temperature = $one_row['installed_temperature'];
                } elseif ($old_current_temperature != -1) {
                    $array_row_by_mac[$key]['installed_temperature'] = $old_current_temperature;
                }
            }
            $first_element = reset($array_base_row_by_mac);
            $date_first_element = $first_element['datetime'];
            $last_element = end($array_base_row_by_mac);
            $date_last_element = $last_element['datetime'];
            $unix_date_first_element = strtotime($date_first_element);
            $unix_date_last_element = strtotime($date_last_element);
            $number_hours = intval(($unix_date_last_element - $unix_date_first_element) / 3600);
            $this->template->number_hours = $number_hours;
//            $array_row_by_mac = ORM::factory('Decodereport')->get_rows_by_mac($_POST ['mac']);
            $this->create_excell_for_mac_adress($array_row_by_mac, $_POST ['mac']);
            $this->template->array_row_by_mac = $array_row_by_mac;
            $this->template->mac_current_device = $_POST ['mac'];
            $this->template->array_base_row_by_mac = $array_base_row_by_mac;

        }






        if (!empty ($_POST ['mac'])) {

            ORM::factory('Decodereportfull')->get_rows_by_mac_sort_asc('');

//            $this->template->array_base_row_by_mac = $array_decode_report_full_with_report_by_mac;
        }
        $this->template->message = "test";
    }

    public function action_test()
    {

        $before_encode_date = ORM::factory('Logssqlecozy')->get_some_rows("ObjectId(57d646b35e7f009a081b04e8)");
        foreach ($before_encode_date as $key => $value) {
            $one_row = $value->as_array();
            $array_fields = ['hub_id' => $one_row['hub_id'], 'body' => $one_row['body'], 'created' => $one_row['created']];
            $test = 2;
            ORM::factory('Logsnewsqlecozy')->set_one_row($array_fields);
            $test = 3;
        }

        $this->template->message = "testthis";
    }


    public function action_set_all_row_to_logs_new()
    {

//          $before_encode_date=ORM::factory('Logssqlecozy')->get_some_rows("ObjectId(57d646b35e7f009a081b04e8)");

        $before_encode_date = ORM::factory('Logssqlecozy')->get_all_colum_from_logs_by_limit_offset(45000, 45000);
        foreach ($before_encode_date as $key => $value) {
            $one_row = $value->as_array();
            $array_fields = ['hub_id' => $one_row['hub_id'], 'body' => $one_row['body'], 'created' => $one_row['created']];
            ORM::factory('Logsnewsqlecozy')->set_one_row($array_fields);
        }


        $this->template->message = "testthis";
    }

    public function action_test_python()
    {
//        $olimp_choise_state = popen('nohup phpunit ./php-webdriver-master/get_return_v_1_1.php &', 'r');
//        $olimp_choise_state = popen('python3 ./parsellogs/Parserforphp.pyw &', 'r');
        /*$olimp_choise_state = popen('python3 /home/ghost/Документы/work/HAI/from_Alexandr_Drobinin/parsellogs/Parserforphp.pyw &', 'w');
        $view = '';
        while (!feof($olimp_choise_state)) {
            $view = $view . fread($olimp_choise_state, 1024);
            flush();
        }
        fclose($olimp_choise_state);*/

//        $command = escapeshellcmd('python3 ./parsellogs/parserforphp.py');
//        $command = escapeshellcmd('python3 ./parsellogs/parserforphp.py');
//        $output = shell_exec($command);
//        echo $output;
        exec('sudo -u www-data python3 /home/ghost/www/tabletermostatsecoz.my/parsellogs/parserforphp.py');


//        $output = shell_exec('python3 ./parsellogs/parserforphp.py');
//        $output = shell_exec('python3 /home/ghost/Документы/work/HAI/from_Alexandr_Drobinin/parsellogs/parsellogs/Parserforphp.pyw');

        $this->template->message = system("sudo -u user python3 /home/ghost/www/tabletermostatsecoz.my/parsellogs/parserforphp.py");
//        $this->template->message = 'OUTPUT'.$output;

    }

    public function action_test_decode_ecozy()
    {
        $output = "";
//        $command ="45434f5a5907005100002257df2a3800a8010000ded5b370ff30313033303230313031303418e80a000029680808002000002257df2ae800a8010000ded5b370ff30313033303230313031303418e90a000029740808002000";
//        $command = "45434f5a5907005100002257df2b4600b1010000ded5b370ff30313033303230313031303418e00a000029730808002000002257df2b9b0017000000ded5b370ff30313033303230313031303418e30a0000293c0808002000";
//        $command = "45434f5a5907007900002257fede76003d120000ded5b370ff30313033303230313031303418910a000029e20508002019002257fedf0b00050e0000ded5b370ff303130333032303130313034181d0a000029f20508002000002257fedf28003d120000ded5b370ff30313033303230313031303418920a000029e80508002019";
        $one_row = ORM::factory('Logsnewsqlecozy')->get_one_row_by_id(3644);
        preg_match("#.*?,\"reports\":\"(.*?)\",\"thermostats\":\".*?#iu", $one_row['body'], $matches);
        $command = $matches[1];
        $array_fields = ['logs_new_id' => $one_row['id'], 'begin_decode' => true, 'end_decode' => false];
        $id_CheckDecodeReports = ORM::factory('CheckDecodeReports')->set_one_row($array_fields);
        $list_array_output_for_database = Ecozy_Server::factory()->CU_ZigBee_incomming($output, $command);
        foreach ($list_array_output_for_database as $one_row) {
            $installed_temperature = $one_row['installed_temperature'];
            $condition_plunger = $one_row['condition_plunger'];
            $mac = $one_row['mac'];
            $datetime = $one_row['datetime'];
            $current_temperature = $one_row['current_temperature'];
            ORM::factory('Decodereport')->set_one_row($one_row);
        }
        $array_fields = ['end_decode' => true];
        ORM::factory('CheckDecodeReports')->update_one_row_by_id($id_CheckDecodeReports, $array_fields);
        $test = 1;
        $this->template->message = "test";
        $this->template->outputtext = $list_array_output_for_database;

    }

    public function action_test_decode_ecozy_and_to_base()
    {
        $output = "";
        $list_array_output_for_database = '';
//        $command ="45434f5a5907005100002257df2a3800a8010000ded5b370ff30313033303230313031303418e80a000029680808002000002257df2ae800a8010000ded5b370ff30313033303230313031303418e90a000029740808002000";
        $command = "45434f5a5907005100002257df2b4600b1010000ded5b370ff30313033303230313031303418e00a000029730808002000002257df2b9b0017000000ded5b370ff30313033303230313031303418e30a0000293c0808002000";
        $list_array_output_for_database = Ecozy_Server::factory()->CU_ZigBee_incomming($output, $command);
        $array_output_for_database = ['current_temperature' => null, 'installed_temperature' => null, 'condition_plunger' => null, 'mac' => null, 'datetime' => null];
        foreach ($list_array_output_for_database as $one_row) {
            $installed_temperature = $one_row['installed_temperature'];
            $condition_plunger = $one_row['condition_plunger'];
            $mac = $one_row['mac'];
            $datetime = $one_row['datetime'];
            $current_temperature = $one_row['current_temperature'];
//            ORM::factory('Decodereport')->set_one_row($one_row);
            ORM::factory('Decodereportfull')->set_one_row($one_row);
        }
        $test = 1;
        $this->template->message = "test";
        $this->template->outputtextresult = "result";

    }

    public function action_test_decode_ecozy_and_to_base_all()
    {
        $output = "";
        $all_rows_from_logs_new = ORM::factory('Logsnewsqlecozy')->get_all_colum_from_logs();
//        $all_rows_from_logs_new = ORM::factory('Logsnewsqlecozy')->get_all_colum_from_logs_by_offset_limit(0,2500);
//        $all_rows_from_logs_new = ORM::factory('Logsnewsqlecozy')->get_all_colum_from_logs_by_offset_limit(2500, 2500);
//        $all_rows_from_logs_new = ORM::factory('Logsnewsqlecozy')->get_all_colum_from_logs_by_offset_limit(5000,2500);
//        $all_rows_from_logs_new = ORM::factory('Logsnewsqlecozy')->get_all_colum_from_logs_by_offset_limit(7500,2500);
//        $all_rows_from_logs_new = ORM::factory('Logsnewsqlecozy')->get_all_colum_from_logs_by_date();
        foreach ($all_rows_from_logs_new as $one_row_object) {
            $one_row = $one_row_object->as_array();

//        $one_row=ORM::factory('Logsnewsqlecozy')->get_one_colum_from_logs();
            preg_match("#.*?,\"reports\":\"(.*?)\",\"thermostats\":\".*?#iu", $one_row['body'], $matches);
            if (isset($matches[1]))
                $command = $matches[1];
            else
                continue;
                $array_report=['report'=>$command,'logs_new_id'=>$one_row['id']];

//        $command = "45434f5a5907005100002257df2b4600b1010000ded5b370ff30313033303230313031303418e00a000029730808002000002257df2b9b0017000000ded5b370ff30313033303230313031303418e30a0000293c0808002000";
            $array_fields = ['logs_new_id' => $one_row['id'], 'begin_decode' => true, 'end_decode' => false];
            $id_CheckDecodeReports = ORM::factory('CheckDecodeReports')->set_one_row($array_fields);
            $list_array_output_for_database = Ecozy_Server::factory()->CU_ZigBee_incomming($output, $command);
            $array_output_for_database = ['current_temperature' => null, 'installed_temperature' => null, 'condition_plunger' => null, 'mac' => null, 'datetime' => null];
            foreach ($list_array_output_for_database as $one_row) {
                array_push($one_row, $array_report);
                $installed_temperature = $one_row['installed_temperature'];
                $condition_plunger = $one_row['condition_plunger'];
                $mac = $one_row['mac'];
                $datetime = $one_row['datetime'];
                $current_temperature = $one_row['current_temperature'];
//                ORM::factory('Decodereport')->set_one_row($one_row);
                ORM::factory('Decodereportfull')->set_one_row($one_row);
            }
            $array_fields = ['end_decode' => true];
            $id_CheckDecodeReports = ORM::factory('CheckDecodeReports')->update_one_row_by_id($id_CheckDecodeReports, $array_fields);
        }
        $test = 1;
        $this->template->message = "test";
        $this->template->outputtextresult = "result";

    }

    public function action_test_decode_ecozy_and_to_base_all_for_full()
    {
        $output = "";
//        $all_rows_from_logs_new = ORM::factory('Logsnewsqlecozy')->get_all_colum_from_logs();
        $all_rows_from_logs_new = ORM::factory('Logsnewsqlecozy')->get_all_colum_from_logs_by_offset_limit(0,2500);
//        $all_rows_from_logs_new = ORM::factory('Logsnewsqlecozy')->get_all_colum_from_logs_by_offset_limit(2500, 2500);
//        $all_rows_from_logs_new = ORM::factory('Logsnewsqlecozy')->get_all_colum_from_logs_by_offset_limit(5000,2500);
//        $all_rows_from_logs_new = ORM::factory('Logsnewsqlecozy')->get_all_colum_from_logs_by_offset_limit(7500,2500);
//        $all_rows_from_logs_new = ORM::factory('Logsnewsqlecozy')->get_all_colum_from_logs_by_date();
        foreach ($all_rows_from_logs_new as $one_row_object) {
            $one_row = $one_row_object->as_array();

//        $one_row=ORM::factory('Logsnewsqlecozy')->get_one_colum_from_logs();
            preg_match("#.*?,\"reports\":\"(.*?)\",\"thermostats\":\".*?#iu", $one_row['body'], $matches);
            if (isset($matches[1]))
                $command = $matches[1];
            else
                continue;
                $array_report=['report'=>$command,'logs_new_id'=>$one_row['id']];

//        $command = "45434f5a5907005100002257df2b4600b1010000ded5b370ff30313033303230313031303418e00a000029730808002000002257df2b9b0017000000ded5b370ff30313033303230313031303418e30a0000293c0808002000";
            $array_fields = ['logs_new_id' => $one_row['id'], 'begin_decode' => true, 'end_decode' => false];
            $id_CheckDecodeReports = ORM::factory('CheckDecodeReports')->set_one_row($array_fields);
            $list_array_output_for_database = Ecozy_Server::factory()->CU_ZigBee_incomming($output, $command);
            $array_output_for_database = ['current_temperature' => null, 'installed_temperature' => null, 'condition_plunger' => null, 'mac' => null, 'datetime' => null];
            foreach ($list_array_output_for_database as $one_row) {
                $one_row=array_merge($one_row, $array_report);
                $installed_temperature = $one_row['installed_temperature'];
                $condition_plunger = $one_row['condition_plunger'];
                $mac = $one_row['mac'];
                $datetime = $one_row['datetime'];
                $current_temperature = $one_row['current_temperature'];
//                ORM::factory('Decodereport')->set_one_row($one_row);
                ORM::factory('Decodereportfull')->set_one_row($one_row);
            }
            $array_fields = ['end_decode' => true];
            $id_CheckDecodeReports = ORM::factory('CheckDecodeReports')->update_one_row_by_id($id_CheckDecodeReports, $array_fields);
        }
        $test = 1;
        $this->template->message = "test";
        $this->template->outputtextresult = "result";

    }

    public function action_test_excell()
    {
        $array_row_by_mac = ORM::factory('Decodereport')->get_rows_by_mac('70b3d5de000008a3');
        $this->create_excell_for_mac_adress($array_row_by_mac);

    }

    public function action_list_mac_adress()
    {
        $this->template = View::factory('ecozy_mac.tpl');
        $array_macs = ORM::factory('Decodereport')->get_by_group_mac();
        $test = 1;
        $this->template->message = "Вывод списка мак адресов!";
        $this->template->array_macs = $array_macs;
    }

    public function action_statistic_chart_thermostat()
    {
        $this->template = View::factory('statistic_chart_thermostat.tpl');
        if (!empty ($_GET ['mac'])) {
            $array_row_by_mac = ORM::factory('Decodereport')->get_rows_by_mac_sort_asc($_GET ['mac']);
            $old_current_temperature = -1;
            foreach ($array_row_by_mac as $key => $one_row) {                                         
                if ($one_row['installed_temperature'] != 0) {
                    $old_current_temperature = $one_row['installed_temperature'];
                } elseif ($old_current_temperature != -1) {
                    $array_row_by_mac[$key]['installed_temperature'] = $old_current_temperature;
                }
                if ($one_row['condition_plunger'] != 0) {
                    $array_row_by_mac[$key]['condition_plunger'] = $array_row_by_mac[$key]['condition_plunger'] / 4;
                }
            }
            $this->template->array_row_by_mac = $array_row_by_mac;
            /*hub_id
            datetime
            mac
            condition_plunger
            installed_temperature
            current_temperature
            id*/
        }
//        $this->template->array_result=
    }

    public function action_statistic_chart_thermostat_second()
    {
        $this->template = View::factory('statistic_chart_thermostat_second.tpl');
        if (!empty ($_GET ['mac'])) {
            $array_base_row_by_mac = ORM::factory('Decodereport')->get_rows_by_mac_sort_asc($_GET ['mac']);
            if (!empty ($_POST ['select_from_datetime']) && !empty ($_POST ['number_hours'])) {
                $array_row_by_mac = ORM::factory('Decodereport')->get_rows_by_mac_sort_asc_by_datetime_one_hour($_GET ['mac'], $_POST['select_from_datetime'], $_POST['number_hours']);
                $this->template->seted_select_from_datetime = $_POST['select_from_datetime'];
                $this->template->seted_number_hours = $_POST['number_hours'];
            } else
                $array_row_by_mac = ORM::factory('Decodereport')->get_rows_by_mac_sort_asc($_GET ['mac']);
            $old_current_temperature = -1;
            $datetime = [];
            foreach ($array_row_by_mac as $key => $one_row) {
                if ($one_row['installed_temperature'] != 0) {
                    $old_current_temperature = $one_row['installed_temperature'];
                } elseif ($old_current_temperature != -1) {
                    $array_row_by_mac[$key]['installed_temperature'] = $old_current_temperature;
                }
                if ($one_row['condition_plunger'] != 0) {
                    $array_row_by_mac[$key]['condition_plunger'] = $array_row_by_mac[$key]['condition_plunger'] / 4;
                }
                $datetime[] = $one_row['datetime'];
            }
            $first_element = reset($array_base_row_by_mac);
            $date_first_element = $first_element['datetime'];
            $last_element = end($array_base_row_by_mac);
            $date_last_element = $last_element['datetime'];
            $unix_date_first_element = strtotime($date_first_element);
            $unix_date_last_element = strtotime($date_last_element);
            $number_hours = intval(($unix_date_last_element - $unix_date_first_element) / 3600);


            $this->template->number_hours = $number_hours;
            $this->template->array_row_by_mac = $array_row_by_mac;
            $this->template->array_base_row_by_mac = $array_base_row_by_mac;
            $this->template->mac_current_device = $_GET ['mac'];
            /*hub_id
            datetime
            mac
            condition_plunger
            installed_temperature
            current_temperature
            id*/
        }
//        $this->template->array_result=
    }


    public function create_excell_for_mac_adress($array_assignment, $mac = null)
    {
//        $array_assignment = DataBaseWilliamhill::factory()->get_array_assignment();
//        $objPHPExcel = new PHPExcel();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Ecozy")
            ->setLastModifiedBy("Ecozy")
            ->setTitle("statistic termostat mac")
            ->setSubject("statistic termostat mac")
            ->setDescription("statistic termostat mac")
            ->setKeywords("statistic termostat mac")
            ->setCategory("statistic termostat mac");
        $base_elemnt = $objPHPExcel->setActiveSheetIndex(0);
        $base_elemnt->setCellValue('A1', '№')
            ->setCellValue('B1', 'Текущее состояние температуры')
            ->setCellValue('C1', 'Установленная температура')
            ->setCellValue('D1', 'Состояние плунжера')
            ->setCellValue('E1', 'Дата и время');
        $number_cell = 2;
        foreach ($array_assignment as $one_assignment) {
            $base_elemnt->setCellValue('A' . $number_cell, $number_cell - 1)
                ->setCellValue('B' . $number_cell, $one_assignment['current_temperature'])
                ->setCellValue('C' . $number_cell, $one_assignment['installed_temperature'])
                ->setCellValue('D' . $number_cell, $one_assignment['condition_plunger'])
                ->setCellValue('E' . $number_cell, $one_assignment['datetime']);
            $number_cell++;
        }
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//        $objWriter->save(str_replace('.php', '.xlsx', __FILE__));
        $objWriter->save(DOCROOT . '/excell/current_excell' . $mac . '.xlsx');
//        $objWriter->save(DOCROOT . '/excell/array_assignment.xlsx');
//        file_put_contents(DOCROOT . '/tmp_file/array_assignment.txt', $array_assignment);
    }


    public function set_array_assignment_to_file_excell()
    {
        $array_assignment = DataBaseWilliamhill::factory()->get_array_assignment();
//        $objPHPExcel = new PHPExcel();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            ->setLastModifiedBy("Maarten Balliauw")
            ->setTitle("PHPExcel Test Document")
            ->setSubject("PHPExcel Test Document")
            ->setDescription("Test document for PHPExcel, generated using PHP classes.")
            ->setKeywords("office PHPExcel php")
            ->setCategory("Test result file");
        $base_elemnt = $objPHPExcel->setActiveSheetIndex(0);

        $base_elemnt->setCellValue('A1', 'id')
            ->setCellValue('B1', 'factor_probability')
            ->setCellValue('C1', 'win_count')
            ->setCellValue('D1', 'total_count')
            ->setCellValue('E1', 'difference');
        $number_cell = 2;
        foreach ($array_assignment as $one_assignment) {
            $base_elemnt->setCellValue('A' . $number_cell, $one_assignment['id'])
                ->setCellValue('B' . $number_cell, $one_assignment['factor_probability'])
                ->setCellValue('C' . $number_cell, $one_assignment['win_count'])
                ->setCellValue('D' . $number_cell, $one_assignment['total_count'])
                ->setCellValue('E' . $number_cell, $one_assignment['total_count'] - $one_assignment['win_count']);
            $number_cell++;
        }
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//        $objWriter->save(str_replace('.php', '.xlsx', __FILE__));
        $objWriter->save(DOCROOT . '/tmp_file/array_assignment.xlsx');
//        file_put_contents(DOCROOT . '/tmp_file/array_assignment.txt', $array_assignment);
    }


}





?>