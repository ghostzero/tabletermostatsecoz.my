<?php
/**
 * Created by PhpStorm.
 * User: vagrant
 * Date: 17.05.18
 * Time: 11:09
 */



class Controller_Device_Base extends Controller_Base {
    protected $auth;
    protected $user;
    public $template;

    public function before() {
        $this->template = 'zalud/device/' . $this->request->action().'.tpl';
        parent::before ();
        $this->auth = Auth::instance ();
        $this->user = $this->auth->get_user ();
        $this->template->user_array =$this->user_array;
    }



    public function action_index() {

        /*print_r($_GET);
        if(isset($_GET["set"])){
            $json_template = file_get_contents('php://input');
            ORM::factory ( 'Messages' )->set_one_message_from_raspberry('',$json_template);

        }
        exit;*/
    }

    public function action_pairing()
    {
        //ORM::factory ( 'Device' )->pairing_device_and_user(3, 3);
        //ORM::factory ( 'Device' )->clean_pairing_device(3);

        ORM::factory ( 'Synonyms' )->set_synonym(3,3,129,"test!");
    }

    public function action_clean_tables(){



    }

    public function action_set_to_base_messange(){
        $json_template = file_get_contents('php://input');
        ORM::factory ( 'Messages' )->set_one_message_from_raspberry($json_template);


    }


    public function action_push_from_raspberry(){
        //        $json_template="{\"HeaderData\":{\"HeadCRC\":0,\"ModeName\":[136,207,117,0,128,0],\"SerialNamber\":[108,138,196,117,108,138,196,117,136,207,117,0]},\"InputData\":{\"AinGroup1\":{\"Ain1\":0,\"Ain2\":0,\"Ain3\":0,\"Ain4\":0,\"Ain5\":0,\"Ain6\":0,\"Ain7\":0,\"Ain8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"AinGroup2\":{\"Ain1\":0,\"Ain2\":0,\"Ain3\":0,\"Ain4\":0,\"Ain5\":0,\"Ain6\":0,\"Ain7\":0,\"Ain8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"DinGroup1\":{\"Din1\":0,\"Din2\":0,\"Din3\":0,\"Din4\":0,\"Din5\":0,\"Din6\":0,\"Din7\":0,\"Din8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"DinGroup2\":{\"Din1\":0,\"Din2\":0,\"Din3\":0,\"Din4\":0,\"Din5\":0,\"Din6\":0,\"Din7\":0,\"Din8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"InputDataCRC\":0,\"PWMinGroup1\":{\"PWMin1\":0,\"PWMin2\":0,\"PWMin3\":0,\"PWMin4\":0,\"PWMin5\":0,\"PWMin6\":0,\"PWMin7\":0,\"PWMin8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"PWMinGroup2\":{\"PWMin1\":0,\"PWMin2\":0,\"PWMin3\":0,\"PWMin4\":0,\"PWMin5\":0,\"PWMin6\":0,\"PWMin7\":0,\"PWMin8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"TinGroup1\":{\"Tin1\":0,\"Tin2\":0,\"Tin3\":0,\"Tin4\":0,\"Tin5\":0,\"Tin6\":0,\"Tin7\":0,\"Tin8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"TinGroup2\":{\"Tin1\":0,\"Tin2\":0,\"Tin3\":0,\"Tin4\":0,\"Tin5\":0,\"Tin6\":0,\"Tin7\":0,\"Tin8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]}},\"OutputData\":{\"AoutGroup1\":{\"Aout1\":0,\"Aout2\":0,\"Aout3\":0,\"Aout4\":0,\"Aout5\":0,\"Aout6\":0,\"Aout7\":0,\"Aout8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"AoutGroup2\":{\"Aout1\":0,\"Aout2\":0,\"Aout3\":0,\"Aout4\":0,\"Aout5\":0,\"Aout6\":0,\"Aout7\":0,\"Aout8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"DoutGroup1\":{\"Dout1\":0,\"Dout2\":0,\"Dout3\":0,\"Dout4\":0,\"Dout5\":0,\"Dout6\":0,\"Dout7\":0,\"Dout8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"DoutGroup2\":{\"Dout1\":0,\"Dout2\":0,\"Dout3\":0,\"Dout4\":0,\"Dout5\":0,\"Dout6\":0,\"Dout7\":0,\"Dout8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"OutputCRC\":24347,\"PWMoutGroup1\":{\"PWMout1\":0,\"PWMout2\":0,\"PWMout3\":0,\"PWMout4\":0,\"PWMout5\":0,\"PWMout6\":20,\"PWMout7\":0,\"PWMout8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"PWMoutGroup2\":{\"PWMout1\":0,\"PWMout2\":0,\"PWMout3\":0,\"PWMout4\":0,\"PWMout5\":0,\"PWMout6\":0,\"PWMout7\":0,\"PWMout8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]}},\"StatusData\":{\"StatusCRC\":15010,\"TempRefCurrent\":{\"TempRef1\":120,\"TempRef10\":253,\"TempRef11\":0,\"TempRef12\":0,\"TempRef13\":0,\"TempRef14\":0,\"TempRef15\":0,\"TempRef16\":0,\"TempRef2\":252,\"TempRef3\":129,\"TempRef4\":116,\"TempRef5\":0,\"TempRef6\":0,\"TempRef7\":0,\"TempRef8\":0,\"TempRef9\":105},\"TimeRpi\":{\"Date\":0,\"Hours\":0,\"Minutes\":0,\"Month\":0,\"Seconds\":0,\"TimeFormat\":0,\"WeekDay\":0,\"Year\":0,\"serial\":[0,0,0,0,120,252,129,116,0,0,0,0]}}}";
//        $json_template="{\"HeaderData\":{\"HeadCRC\":23,\"ModeName\":[136,207,117,23,128,0],\"SerialNamber\":[108,138,196,117,108,138,196,117,136,207,117,0]},\"InputData\":{\"AinGroup1\":{\"Ain1\":23,\"Ain2\":23,\"Ain3\":23,\"Ain4\":23,\"Ain5\":23,\"Ain6\":23,\"Ain7\":23,\"Ain8\":23,\"serial\":[23,23,23,23,23,23,23,23,23,23,23,0]},\"AinGroup2\":{\"Ain1\":23,\"Ain2\":23,\"Ain3\":23,\"Ain4\":23,\"Ain5\":23,\"Ain6\":23,\"Ain7\":23,\"Ain8\":23,\"serial\":[23,23,23,23,23,23,23,23,23,23,23,0]},\"DinGroup1\":{\"Din1\":23,\"Din2\":23,\"Din3\":23,\"Din4\":23,\"Din5\":23,\"Din6\":23,\"Din7\":23,\"Din8\":23,\"serial\":[23,23,23,23,23,23,23,23,23,23,23,0]},\"DinGroup2\":{\"Din1\":23,\"Din2\":23,\"Din3\":23,\"Din4\":23,\"Din5\":23,\"Din6\":23,\"Din7\":23,\"Din8\":23,\"serial\":[23,23,23,23,23,23,23,23,23,23,23,0]},\"InputDataCRC\":23,\"PWMinGroup1\":{\"PWMin1\":23,\"PWMin2\":23,\"PWMin3\":23,\"PWMin4\":23,\"PWMin5\":23,\"PWMin6\":23,\"PWMin7\":23,\"PWMin8\":23,\"serial\":[23,23,23,23,23,23,23,23,23,23,23,0]},\"PWMinGroup2\":{\"PWMin1\":23,\"PWMin2\":23,\"PWMin3\":23,\"PWMin4\":23,\"PWMin5\":23,\"PWMin6\":23,\"PWMin7\":23,\"PWMin8\":23,\"serial\":[23,23,23,23,23,23,23,23,23,23,23,0]},\"TinGroup1\":{\"Tin1\":23,\"Tin2\":23,\"Tin3\":23,\"Tin4\":23,\"Tin5\":23,\"Tin6\":23,\"Tin7\":23,\"Tin8\":23,\"serial\":[23,23,23,23,23,23,23,23,23,23,23,0]},\"TinGroup2\":{\"Tin1\":23,\"Tin2\":23,\"Tin3\":23,\"Tin4\":23,\"Tin5\":23,\"Tin6\":23,\"Tin7\":23,\"Tin8\":23,\"serial\":[23,23,23,23,23,23,23,23,23,23,23,0]}},\"OutputData\":{\"AoutGroup1\":{\"Aout1\":23,\"Aout2\":23,\"Aout3\":23,\"Aout4\":23,\"Aout5\":23,\"Aout6\":23,\"Aout7\":23,\"Aout8\":23,\"serial\":[23,23,23,23,23,23,23,23,23,23,23,0]},\"AoutGroup2\":{\"Aout1\":23,\"Aout2\":23,\"Aout3\":23,\"Aout4\":23,\"Aout5\":23,\"Aout6\":23,\"Aout7\":23,\"Aout8\":23,\"serial\":[23,23,23,23,23,23,23,23,23,23,23,0]},\"DoutGroup1\":{\"Dout1\":23,\"Dout2\":23,\"Dout3\":23,\"Dout4\":23,\"Dout5\":23,\"Dout6\":23,\"Dout7\":23,\"Dout8\":23,\"serial\":[23,23,23,23,23,23,23,23,23,23,23,0]},\"DoutGroup2\":{\"Dout1\":23,\"Dout2\":23,\"Dout3\":23,\"Dout4\":23,\"Dout5\":23,\"Dout6\":23,\"Dout7\":23,\"Dout8\":23,\"serial\":[23,23,23,23,23,23,23,23,23,23,23,0]},\"OutputCRC\":24347,\"PWMoutGroup1\":{\"PWMout1\":23,\"PWMout2\":23,\"PWMout3\":23,\"PWMout4\":23,\"PWMout5\":23,\"PWMout6\":223,\"PWMout7\":23,\"PWMout8\":23,\"serial\":[23,23,23,23,23,23,23,23,23,23,23,0]},\"PWMoutGroup2\":{\"PWMout1\":23,\"PWMout2\":23,\"PWMout3\":23,\"PWMout4\":23,\"PWMout5\":23,\"PWMout6\":23,\"PWMout7\":23,\"PWMout8\":23,\"serial\":[23,23,23,23,23,23,23,23,23,23,23,0]}},\"StatusData\":{\"StatusCRC\":150123,\"TempRefCurrent\":{\"TempRef1\":1223,\"TempRef10\":253,\"TempRef11\":23,\"TempRef12\":23,\"TempRef13\":23,\"TempRef14\":23,\"TempRef15\":23,\"TempRef16\":23,\"TempRef2\":252,\"TempRef3\":129,\"TempRef4\":116,\"TempRef5\":23,\"TempRef6\":23,\"TempRef7\":23,\"TempRef8\":23,\"TempRef9\":105},\"TimeRpi\":{\"Date\":23,\"Hours\":23,\"Minutes\":23,\"Month\":23,\"Seconds\":23,\"TimeFormat\":23,\"WeekDay\":23,\"Year\":23,\"serial\":[23,23,23,23,1223,252,129,116,23,23,23,23]}}}";
        $json_template = file_get_contents('php://input');


//        $exampl_Jsonuserhandlere = new Jsonuserhandler($json_template);
//        $arry_decode_json=$exampl_Jsonuserhandlere->set_json_string($json_template);
//       $this->template->array_decode_json = json_encode($exampl_Jsonuserhandlere->jsonSerialize(), JSON_PRETTY_PRINT);


        $this->template->array_decode_json = json_decode($json_template);
        ORM::factory ( 'Device' )->set_total_info_about_device($json_template);





        /*$this->template->array_decode_json =1;
         ORM::factory ( 'Typeofextensionmodule' )->set_by_group_for(15,"TempRef");*/
        /*if ($this->user) {
            Request::instance ()->redirect ( 'admin/login.html' );
        }
        $status = $this->auth->login (  $_POST ['username'], $_POST ['password'] );
        if ($status) {
            Request::instance ()->redirect ( 'admin/login.html' );
        }
        $this->template->administrative = 1;*/

    }

    public function action_view_all_devices(){
        $array_devices=ORM::factory('Device')->get_all_devices();
        $this->template->array_devices=$array_devices;
    }

    public function action_view_one_devices(){
        if(isset($_GET['device_id'])){
            $array_group = ORM::factory('Typeofgroupsextensionmodule')->get_groups_by();
            $output_array=array();
            foreach ($array_group as $key=> $one_group){
                $group_id=$one_group['id'];
                $current_group=ORM::factory('Typeofextensionmodule')->get_array_extension_module($group_id);
                $output_array[$key]=['group' => $one_group,'array_extension'=>$current_group];
             }

            $this->template->output_array=$output_array;
            $this->template->device_id=$_GET['device_id'];
//            $device_id=$_GET['device_id'];
//            $array_parameter=ORM::factory('Device')->get_all_parameters_by_id();


        }

    }


    public function action_draw_graph(){
        $test=1;
        $device_id=null;
        $array_parameters=[];
        $array_parameters_result=[];
        if($_POST['device_id']){
            foreach ($_POST as $key_post => $post_value){
                if($key_post=='device_id'){
                    $device_id=$post_value;
                }else{
                    $array_parameters[]=$post_value;
                    $array_parameters_result[$key_post]=ORM::factory("Valueofextensionmodule")->get_value_of_extension_module_by_device_id_type_of_extension_module_id($device_id,$post_value);
                    $copy_array_parameters_result[$key_post] = $array_parameters_result[$key_post];
                    foreach ($array_parameters_result[$key_post] as $key_array_parameters_result => $value_array_parameters_result){
                        $array_timestamp=ORM::factory('Timestamp')->get_timestamp_by_id_timestamp($array_parameters_result[$key_post][$key_array_parameters_result]['timestamp_id']);
                        $copy_array_parameters_result[$key_post][$key_array_parameters_result]['unixtime'] = $array_timestamp[0]['unixtime'];
//                        $copy_array_parameters_result[$key_post][$key_array_parameters_result]['unixtime'] = $this->microtimetodatetime($array_timestamp[0]['unixtime']);
//                        array_merge($copy_array_parameters_result[$key_post][$key_array_parameters_result],['unixtime'=>$array_timestamp[0]['unixtime']]);
//                        array_pu($copy_array_parameters_result[$key_post][$key_array_parameters_result],['unixtime'=>$array_timestamp[0]['unixtime']]);
                        $test_4=4;
                    }
                   $test_3=3;
                }
            }
//            ORM::factory("Valueofextensionmodule")->;
        }else{
            Request::current()->redirect('/user/index');
        }

        $array_names_of_line=[];
        $array_valuse_name=[];
        $array_names_of_line[]="DateTime";

        foreach ($copy_array_parameters_result as $key_copy_array_parameters_result => $value_copy_array_parameters_result){
            $array_names_of_line[]=$key_copy_array_parameters_result;
            $number_values_name=0;
            foreach ($value_copy_array_parameters_result as $key_one_parce_element => $value_one_parce_element ){

                $array_one_column=[];
//                $array_valuse_name[$number_values_name]['unixtime']=$this->microtimetodatetime($value_one_parce_element['unixtime']);
//                $array_valuse_name[$number_values_name]['unixtime']=$value_one_parce_element['unixtime'];
                $array_valuse_name[$number_values_name]['unixtime']=$this->set_time_two_string($value_one_parce_element['unixtime']);
                $array_valuse_name[$number_values_name][$key_copy_array_parameters_result]=$value_one_parce_element['value'];

                /*$template_array=[];
                 * switch ($key_one_parce_element){
                    case "unixtime":{
                        $template_array['unixtime']=$value_one_parce_element;
                        break;
                    }
                    default:{

                        break;
                    }

                }*/

                $number_values_name++;
            }

        }

        $output_array_for_graph=['array_names_of_line'=>$array_names_of_line,'array_valuse_name'=>$array_valuse_name];
        $this->template->output_array_for_graph=$output_array_for_graph;
//      $this->template->copy_array_parameters_result=$copy_array_parameters_result;


        $test1=1;
    }

    public function action_print_table_with_data(){
        if ($_POST){
            $test1=1;

        }else{
            $test2=2;

        }
    }



    public function microtimetodatetime($content){
        $t = $content/1000;
        $micro = sprintf("%06d",($t - floor($t)) * 1000000);
//        $d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
//        $d = new DateTime( date('Y-m-d H:i:s:'.$micro, $t) );
        $d = new DateTime( date('Y-m-d H:i:s', $t) );

//        return $d->format("Y-m-d H:i:s.u"); // note at point on "u"
        return $d->format("Y-m-d H:i:s"); // note at point on "u"
    }












    public function action_truncate_tables(){
        ORM::factory("Valueofextensionmodule")->truncate_base_tables();
        ORM::factory("Device")->truncate_base_tables();
//        ORM::factory("Typeofgroupsextensionmodule")->truncate_base_tables();
//        ORM::factory("Typeofextensionmodule")->truncate_base_tables();
        ORM::factory("Crcdata")->truncate_base_tables();
        ORM::factory("Serialnamber")->truncate_base_tables();
        ORM::factory("Serialofgroup")->truncate_base_tables();
        ORM::factory("Timestamp")->truncate_base_tables();


    }

    public function action_test(){
//        ORM::factory("Timestamp")->set_timestamp_by_device_id(3);
        /*$type_of_groups_extension_module_id=ORM::factory("Typeofgroupsextensionmodule")->get_id_group_by_name_group("PWMinGroup1");
        $name_extension_module="PWMin1";
        $group_id=ORM::factory("Typeofextensionmodule")->get_id_value_of_extension_module_by_tpe_of_groups_extension_module_id_and_name_extension_module($type_of_groups_extension_module_id, $name_extension_module);*/




        $array1 = array("color" => "red", 2, 4);
        $array2 = array("a", "b", "color" => "green", "shape" => "trapezoid", 4);
        $result = array_merge($array1, $array2);








          $test=1;
    }

    public function action_test_two(){

        /*$t_1 = microtime(true);
        $micro_1 = sprintf("%06d",($t_1 - floor($t_1)) * 1000000);
        $d_1 = new DateTime( date('Y-m-d H:i:s.'.$micro_1, $t_1) );*/

//        print $d->format("Y-m-d H:i:s.u"); // note at point on "u";



        $unit_ms=1528111689407;
        $t = $unit_ms/1000;
//        $t = $unit_ms;
        $micro = sprintf("%06d",($t - floor($t)) * 1000);
        $year =date('Y',$t);
        $month =date('m',$t);
        $day =date('d',$t);
        $hour =date('H',$t);
        $minutes =date('H',$t);
        $seconds =date('s',$t);
        $millisesonds =date('u',$t);
        $d = new DateTime( date('Y-m-d H:i:s', $t) );
        $test=1;

    }

    public function set_time_two_string($unix_time_in_milliseconds){
//        $micro = sprintf("%06d",($unix_time_in_milliseconds - floor($unix_time_in_milliseconds)) * 1000);
        $t=$unix_time_in_milliseconds/1000;
        $year =date('Y',$t);
        $month =date('m',$t);
        $day =date('d',$t);
        $hour =date('H',$t);
        $minutes =date('i',$t);
        $seconds =date('s',$t);
        $output="new Date(".$year.", ".$month.", ".$day.", ".$hour.", ".$minutes.", ".$seconds.")";
        return $output;
    }




    public function action_test_second(){
//        ORM::factory("Timestamp")->set_timestamp_by_device_id(3);

//          ORM::factory("Timestamp")->set_one_row_for_timestamp(3);
//        $resutl=ORM::factory("Timestamp")->get_timestamp_by_id_timestamp(3);
//        $resutl=ORM::factory("Timestamp")->set_one_row_for_timestamp_unixtime_millisecond(3);


























        $test=1;



    }








    public function action_login() {
        if (! $this->user) {
            Request::instance ()->redirect ( 'admin/index.html' );
        }
        $this->template->user=$this->user->username;
        $this->template->administrative = 1;

    }
    public function action_logout() {
        $this->auth->logout ();
        Request::instance ()->redirect ( 'admin/index.html' );
    }
    public function action_fid() {
        $this->template->input = 'fid';

    }

}