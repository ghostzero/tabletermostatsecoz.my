<?php

/**
 * Created by PhpStorm.
 * User: vagrant
 * Date: 04.07.18
 * Time: 8:15
 */
class Controller_Additionservice extends Controller_Base
{

    public $template;

    public function before()
    {
//		$this->template = 'zalud/login/' . $this->request->action().'.tpl';
        $this->template = 'zalud_new_design/additionservice/' . $this->request->action() . '.tpl';


        parent::before();
        $this->template->type_page = "login";
        $this->template->user_array = $this->user_array;
//		$this->auth = Auth::instance ();
//		$this->user = $this->auth->get_user ();


    }


    public function action_actueleverte()
    {
        $this->template->device_id = "";

        //$array_devices_all = ORM::factory('Device')->get_device_all();

        if ($this->current_role == "admin" || $this->current_role == "plumber") {
            $array_devices_all = ORM::factory('Device')->get_device_all();
        } else {
            $array_devices_all = ORM::factory('Device')->get_device_by_id($this->user->get('id'));
        }

        $this->template->array_devices_all = $array_devices_all;
        $this->template->user_id = $this->user->get('id');

        //$array_groups_Ain = ORM::factory("Typeofextensionmodule")->get_groups_by_name("Ain");
        //$this->template->array_groups_Ain=$array_groups_Ain;


        if (isset($_POST["DB_write"]) and isset($_POST["count_colume"])) {

            $user_id = $_POST["user_id"];
            $device_id = $_POST["device_id"];

            $toDB = [];

            for ($i = 1; $i <= $_POST["count_colume"]; $i++) {

                $type_of_extension_module_id = $_POST["type_of_extension_module_id_" . $i];
                $value = $_POST["synonym_" . $i];

                //echo "synonym_id_".$i.": ".$_POST["synonym_id_".$i]."<br>";
                //echo "type_of_extension_module_id_".$i.": ".$_POST["type_of_extension_module_id_".$i]."<br>";
                //echo "synonym_".$i.": ".$_POST["synonym_".$i]."<br>";
                //echo "value_".$i.": ".$_POST["value_".$i]."<br><br>";

                if ($this->current_role == "admin" || $this->current_role == "plumber") {

                    ORM::factory("Synonyms")->set_synonym_for_user($user_id, $device_id, $type_of_extension_module_id, $value);
                } else {

                    ORM::factory("Synonyms")->set_synonym_for_another($device_id, $type_of_extension_module_id, $value);
                }


            }

        }


        if (isset($_POST['all_info'])) {

            $array_groups_Ain = ORM::factory("Typeofextensionmodule")->get_groups();
            $this->template->array_groups_Ain = $array_groups_Ain;

        } else {
            $array_groups_Ain = ORM::factory("Typeofextensionmodule")->get_groups_by_name("Ain");
            $this->template->array_groups_Ain = $array_groups_Ain;
        }


        $array_show_params = [];

        if (isset($_POST['device_id'])) {
            $this->template->device_id = $_POST['device_id'];


            $array_value = ORM::factory("Valueofextensionmodule")->get_value_MAX_timestamp_id($_POST['device_id']);
            //$array_synonyms_user = ORM::factory("Synonyms")->get_synonyms($this->user->get('id'));


            if ($this->current_role == "admin" || $this->current_role == "plumber") {
                $array_synonyms_user = ORM::factory("Synonyms")->get_synonyms_by_devise_id_all_user($_POST['device_id']);
            } else {
                $array_synonyms_user = ORM::factory("Synonyms")->get_synonyms_by_devise_id($this->user->get('id'), $_POST['device_id']);
            }


            foreach ($array_value as $key_post => $post_value) {
                foreach ($array_groups_Ain as $key_group => $post_value_group) {
                    if ($post_value["type_of_extension_module_id"] == $post_value_group["id"]) {
                        $temp['synonym_id'] = "";
                        $temp['name'] = $post_value_group['name'];
                        $temp['value'] = $post_value['value'];
                        $temp['synonym'] = "";
                        $temp['type_of_extension_module_id'] = $post_value_group["id"];

                        foreach ($array_synonyms_user as $key_synonyms => $post_synonyms) {
                            if ($post_synonyms["type_of_extension_module_id"] == $post_value_group["id"]) {
                                $temp['synonym'] = $post_synonyms["name"];
                                $temp['synonym_id'] = $post_synonyms['id'];

                            }
                        }


                        array_push($array_show_params, $temp);
                        break;
                    }
                }

            }
            $this->template->array_show_params = $array_show_params;

        }

        /*


        $array_synonyms_user_temp = ORM::factory("Synonyms")->get_synonyms($this->user->get('id'));

        $array_synonyms_user = array();

        foreach ($array_synonyms_user_temp as $key_post => $post_value){

            $type_of_extension_module_id = ORM::factory("Typeofextensionmodule")->get_groups_by($post_value["type_of_extension_module_id"]);

            //$post_value[""]=
        }


        $this->template->array_synonyms_user = $array_synonyms_user;
        */

    }

    public function action_leistungswerte()
    {


    }

    public function action_systemmonitor()
    {


    }


    public function action_inbetriebnahme()
    {


    }

    public function action_ablaufdatum()
    {


    }

    public function action_position()
    {


    }

    public function action_reserve()
    {


    }


    public function action_user_administration()
    {
        $array_all_admin = ORM::factory("Rolesusers")->get_user_by_type_roles("user");
        $this->template->array_show_params = $array_all_admin;

    }


    public function action_user_administration_for_user()
    {
        $array_all_admin = ORM::factory("Rolesusers")->get_user_with_role_by_id($this->user_array['id']);
        $this->template->array_show_params = $array_all_admin;

    }


    public function action_superuser_user_administration()
    {
        $array_all_admin = ORM::factory("Rolesusers")->get_all_not_admin();
        $this->template->array_show_params = $array_all_admin;
    }


    public function action_change_parameter_user()
    {

        if (isset($_GET['user_id'])) {
            $array_all_admin = ORM::factory("User")->get_user_with_role($_GET['user_id']);
            $this->template->array_show_params = $array_all_admin;
        } else {
            Request::current()->redirect('/addition_service/superuser_user_administration');
        }
    }


    public function action_change_parameter_only_user()
    {


        /*
         *
         *
         *
         *
         *
         *
         *
         * */


        if (isset($_GET['user_id'])) {
            $array_all_admin = ORM::factory("User")->get_user_with_role($_GET['user_id']);
            $array_for_output = ['email' => $array_all_admin['email'], 'username' => $array_all_admin ['username'], 'password' => '', 'alies' => $array_all_admin ['alies'], 'owner_name' => $array_all_admin ['owner_name'], 'address' => $array_all_admin ['address']];
            $this->template->array_show_params = $array_for_output;
            $this->template->current_user_id = $_GET['user_id'];
        } else {
            Request::current()->redirect('/addition_service/superuser_user_administration');
        }


    }


    public function action_do_changed_parameter_for_user()
    {
        if (isset($_POST['user_id']) && isset($_POST['email']) && isset($_POST['username']) && isset($_POST['password']) && isset($_POST['alies']) && isset($_POST['owner_name']) && isset($_POST['address']) && isset($_POST['admin_password'])) {
//            isset() && isset() && isset() && isset() && isset() && isset() && isset()

            $array_fields = ['email' => $_POST['email'],
                'username' => $_POST['username'],
                'password' => $_POST['password'],
                'alies' => $_POST['alies'],
                'owner_name' => $_POST['owner_name'],
                'address' => $_POST['address'],
//                'admin_password' => $_POST['admin_password'],
            ];

            $output_array=$this->clean_array($array_fields);
            if(isset($output_array['password'])){

                $output_array['password']=Auth::instance()->hash($output_array['password']);
            }
            ORM::factory("User")->update_one_row_by_id($_POST['user_id'],$output_array);
            $test = 'asdf';
            $current_user=ORM::factory('User')->get_user($_POST['user_id']);
            $test = 'asdf';
            $this->send_message_change_user_parameter($current_user[0]['email'],'Test message from Zalud',$array_fields);

        }



        Request::current()->redirect('/addition_service/change_parameter_only_user?user_id='.$_POST['user_id']);
        $test = 'asdf';

    }

    function send_message_change_user_parameter($email, $subject, $raw_message){
        $config = Kohana::$config->load('email');
        Email::connect($config);
//        $to = 'omelya1998chirk@gmail.com';
        $to = $email;
//        $subject = 'Test message from Zalud';
        $from = 'test.zalud.webserver@gmail.com';
//        $message = 'Проверка связи Test message from Zalud';
        $message=print_r($raw_message,true);
        Email::send($to, $from, $subject, $message, $html = false);
    }




    function clean_array($begin_array){
        $cleaned_array=[];
        foreach ($begin_array as $key_begin_array => $one_begin_array){
            if($one_begin_array!==''){
                $cleaned_array[$key_begin_array] = $one_begin_array;
            }
        }
        return $cleaned_array;
   }


    public function action_test_send_email()
    {

        $config = Kohana::$config->load('email');
        Email::connect($config);

        $to = 'omelya1998chirk@gmail.com';
        $subject = 'Test message from Zalud';
        $from = 'test.zalud.webserver@gmail.com';
        $message = 'Проверка связи Test message from Zalud';

        Email::send($to, $from, $subject, $message, $html = false);
    }


    public function action_garantie_ablaufdatum()
    {


    }


    public function action_ubersicht_graphik()
    {
        $this->template->output_array_for_graph = array();

        /*
        $flag_role=false;
        foreach ($this->user_array['roles'] as $one_role){
            if($one_role=='admin') {$flag_role=true; break;}
        }
        */


    }


    public function action_hainkreize()
    {


    }


    public function set_time_two_string($unix_time_in_milliseconds)
    {
//        $micro = sprintf("%06d",($unix_time_in_milliseconds - floor($unix_time_in_milliseconds)) * 1000);
        $t = $unix_time_in_milliseconds / 1000;
        $year = date('Y', $t);
        $month = date('m', $t);
        $day = date('d', $t);
        $hour = date('H', $t);
        $minutes = date('i', $t);
        $seconds = date('s', $t);
        $output = "new Date(" . $year . ", " . $month . ", " . $day . ", " . $hour . ", " . $minutes . ", " . $seconds . ")";
        return $output;
    }

    public function action_view_device()
    {
        $this->template->device_id = "";

        $this->template->output_array_for_graph = array();

        $device_id = null;
        $array_parameters = [];
        $array_parameters_result = [];

        $copy_array_parameters_result = [];

        if (isset($_POST['device_id'])) {
            $this->template->device_id = $_POST['device_id'];

            foreach ($_POST as $key_post => $post_value) {
                if ($key_post == 'device_id') {
                    $device_id = $post_value;
                } else {
                    $array_parameters[] = $post_value;
                    $array_parameters_result[$key_post] = ORM::factory("Valueofextensionmodule")->get_value_of_extension_module_by_device_id_type_of_extension_module_id($device_id, $post_value);
                    $copy_array_parameters_result[$key_post] = $array_parameters_result[$key_post];
                    foreach ($array_parameters_result[$key_post] as $key_array_parameters_result => $value_array_parameters_result) {
                        $array_timestamp = ORM::factory('Timestamp')->get_timestamp_by_id_timestamp($array_parameters_result[$key_post][$key_array_parameters_result]['timestamp_id']);
                        $copy_array_parameters_result[$key_post][$key_array_parameters_result]['unixtime'] = $array_timestamp[0]['unixtime'];
//                        $copy_array_parameters_result[$key_post][$key_array_parameters_result]['unixtime'] = $this->microtimetodatetime($array_timestamp[0]['unixtime']);
//                        array_merge($copy_array_parameters_result[$key_post][$key_array_parameters_result],['unixtime'=>$array_timestamp[0]['unixtime']]);
//                        array_pu($copy_array_parameters_result[$key_post][$key_array_parameters_result],['unixtime'=>$array_timestamp[0]['unixtime']]);
                        $test_4 = 4;
                    }
                    $test_3 = 3;
                }
            }


            //////////////////////////////////////////

            $array_names_of_line = [];
            $array_valuse_name = [];
            $array_names_of_line[] = "DateTime";


            foreach ($copy_array_parameters_result as $key_copy_array_parameters_result => $value_copy_array_parameters_result) {
                $array_names_of_line[] = $key_copy_array_parameters_result;
                $number_values_name = 0;
                foreach ($value_copy_array_parameters_result as $key_one_parce_element => $value_one_parce_element) {

                    $array_one_column = [];
//                $array_valuse_name[$number_values_name]['unixtime']=$this->microtimetodatetime($value_one_parce_element['unixtime']);
//                $array_valuse_name[$number_values_name]['unixtime']=$value_one_parce_element['unixtime'];
                    $array_valuse_name[$number_values_name]['unixtime'] = $this->set_time_two_string($value_one_parce_element['unixtime']);
                    $array_valuse_name[$number_values_name][$key_copy_array_parameters_result] = $value_one_parce_element['value'];

                    /*$template_array=[];
                     * switch ($key_one_parce_element){
                        case "unixtime":{
                            $template_array['unixtime']=$value_one_parce_element;
                            break;
                        }
                        default:{

                            break;
                        }

                    }*/

                    $number_values_name++;
                }

            }


            $output_array_for_graph = ['array_names_of_line' => $array_names_of_line, 'array_valuse_name' => $array_valuse_name];
            $this->template->output_array_for_graph = $output_array_for_graph;
        } else {
        }


        if ($this->current_role == "admin" || $this->current_role == "plumber") {
            $array_devices_all = ORM::factory('Device')->get_device_all();
        } else {
            $array_devices_all = ORM::factory('Device')->get_device_by_id($this->user->get('id'));
        }
//        $array_devices_all = ORM::factory('Device')->get_device_all();
        $this->template->array_devices_all = $array_devices_all;

        //if(isset($_GET['device_id'])){
        $array_group = ORM::factory('Typeofgroupsextensionmodule')->get_groups_by();
        $output_array = array();
        foreach ($array_group as $key => $one_group) {
            $group_id = $one_group['id'];
            $current_group = ORM::factory('Typeofextensionmodule')->get_array_extension_module($group_id);
            $output_array[$key] = ['group' => $one_group, 'array_extension' => $current_group];
        }

        $this->template->output_array = $output_array;
        //$this->template->device_id=$_GET['device_id'];
//            $device_id=$_GET['device_id'];
//            $array_parameter=ORM::factory('Device')->get_all_parameters_by_id();


        //}


    }


    public function action_pairing_device_and_user()
    {
        if (isset($_POST["devices"]) && $_POST["devices"] && $_POST["user"]) {
            ORM::factory('Device')->pairing_device_and_user($_POST["devices"], $_POST["user"]);
            ORM::factory('Synonyms')->set_user_id_by_device_id($_POST["devices"], $_POST["user"]);
            if (isset($_POST["alies"]) && isset($_POST["owner_name"]) && isset($_POST["address"])) {
                ORM::factory('User')->set_address_alies_owner_nameowner_name_by_id($_POST["user"], $_POST["alies"], $_POST["owner_name"], $_POST["address"]);
            }
        }

        if (isset($_POST["device_id"]) && $_POST["device_id"]) {
            ORM::factory('Device')->clean_pairing_device($_POST["device_id"]);
            exit;
        }


        $array_devices_new = ORM::factory('Device')->get_device_without_binding();
        $this->template->array_devices_new = $array_devices_new;

        $array_devices_binding = ORM::factory('Device')->get_device_with_binding();
        $this->template->array_devices_binding = $array_devices_binding;

        $array_users_all = ORM::factory('User')->get_users_all();
        $this->template->array_users_all = $array_users_all;

        $array_binding = array();
        foreach ($array_devices_binding as $value) {
            $array = ORM::factory('User')->get_user($value["user_id"]);
            $tmp["user_id"] = $value["user_id"];
            $tmp["username"] = $array[0]["username"];
            $tmp["alies"] = $array[0]["alies"];
            $tmp["owner_name"] = $array[0]["owner_name"];
            $tmp["address"] = $array[0]["address"];
            $tmp["device_id"] = $value["id"];
            $tmp["modename"] = $value["modename"];


            array_push($array_binding, $tmp);

        }
        $this->template->array_binding = $array_binding;


    }


}