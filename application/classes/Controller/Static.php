<?php
defined ( 'SYSPATH' ) or die ( 'No direct script access.' );

class Controller_Static extends Controller_Base {
	public $template = 'static.tpl';
	public function action_index() {
		$static_name = $this->request->param ( 'path' );
		$static_page = ORM::factory ( 'Static' )->get_one_static ( $static_name );
		$this->template->static_page = $static_page;
		$this->template->title='PHP шпаргалка :: '.$static_page['title'];
		$this->template->name_frequent_use_methods = User::frequent_use_method ();
		$this->template->random_methods = User::get_random_methods ();
		$category_name = ORM::factory ( 'Category' )->get_categories ();
					
					foreach ( $category_name as $key => $value ) {
						$output [$key] ['category'] = $value ['title'];
						$output [$key] ['methods'] = ORM::factory ( 'Category' )->get_methods ( $value ['name'] );
					
					}
					$this->template->output_first = $output;
	}
}
?>
