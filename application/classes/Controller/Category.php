<?php
defined('SYSPATH') or die ('No direct script access.');

class Controller_Category extends Controller_Base
{
    public $template;

    public function action_index()
    {
        $category = $this->request->param('path');
        if (empty ($category))
            $category = 'news';
        switch ($category) {
            case 'static' : {
                $this->template = View::factory('static.tpl');
                $this->template->title = 'php шпаргалка';
                $page = max(1, Arr::get($_GET, 'page', 1));
                $per_page = ITEMS_PER_PAGE;
                $offset = $per_page * ($page - 1);
                $list_static = ORM::factory('Static')->get_short_all_statics($per_page, $offset);
                $pag_data = array(
                    'total_items' => ORM::factory('Static')->count_statics(),
                    'items_per_page' => $per_page,
                    'view' => 'pagination/list_projects');
                $this->template->list_static = $list_static;
                $this->template->offset = $offset;
                $this->template->pagination = Pagination::factory($pag_data)->render();
                //$this->template->short_all_static = ORM::factory ( 'Static' )->get_short_all_static ();
                $this->template->name_frequent_use_methods = User::frequent_use_method();
                $this->template->random_methods = User::get_random_methods();

                break;
            }
            case 'news' : {
                $this->template = View::factory('news.tpl');
                $this->template->title = 'php функции онлайн';
                $page = max(1, Arr::get($_GET, 'page', 1));
                $per_page = ITEMS_PER_PAGE;
                $offset = $per_page * ($page - 1);
                $list_news = ORM::factory('News')->get_short_all_news($per_page, $offset);
                $pag_data = array(
                    'total_items' => ORM::factory('News')->count_news(),
                    'items_per_page' => $per_page,
                    'view' => 'pagination/list_projects');
                $this->template->list_news = $list_news;
                $this->template->offset = $offset;
                $this->template->pagination = Pagination::factory($pag_data)->render();
                //$this->template->short_all_news = ORM::factory ( 'News' )->get_short_all_news ();
                $this->template->name_frequent_use_methods = User::frequent_use_method();
                $this->template->random_methods = User::get_random_methods();
                $category_name = ORM::factory('Category')->get_categories();

                foreach ($category_name as $key => $value) {
                    $output [$key] ['href'] = $value ['href'];
                    $output [$key] ['category'] = $value ['title'];
                    $output [$key] ['methods'] = ORM::factory('Category')->get_methods($value ['name']);

                }
                $this->template->output_first = $output;
                $function = ' ';
                foreach ($list_news as $news) {
//                    $temp=explode(' ', $news ['title']);
                    $temp = explode(' ', $news ['title']);
                    reset($temp);
                    $function = $function . ' ' . $temp[0];

//                    $function = array_shift(explode(' ', $news ['title']));
                    if (!next($news)) {
                        $function = $function . '.';
                    } else $function = $function . ' ';

                }
                $this->template->methods = User::get_random_methods();
                $this->template->category = $category;
                $this->template->output_first = $output;
                $this->template->output_second = $output;
                $this->template->description = $function;
                $this->template->function = 'php примеры';

                break;
            }
            case 'all' : {
                $this->template = View::factory('all.tpl');
                $this->template->title = 'php программирование - тестирование функций онлайн. php строки, функции, массивы - все это на php сайте';
                $category_name = ORM::factory('Category')->get_categories();

                foreach ($category_name as $key => $value) {
                    $output [$key] ['href'] = $value ['href'];
                    $output [$key] ['category'] = $value ['title'];
                    $output [$key] ['methods'] = ORM::factory('Category')->get_methods($value ['name']);

                }
                $this->template->output_first = $output;
                $this->template->output_second = $output;
                $this->template->name_frequent_use_methods = User::frequent_use_method();
                $this->template->random_methods = User::get_random_methods();
                break;
            }
            default : {
                $this->template = View::factory('category.tpl');
                $this->template->title = ucfirst('PHP category :: ' . $category);
                $this->template->category = $category;
                $this->template->methods = ORM::factory('Category')->get_methods($category);
                $name_frequent_use_methods = User::frequent_use_method();
                $this->template->name_frequent_use_methods = $name_frequent_use_methods;
                $this->template->random_methods = User::get_random_methods();
                break;

            }
        }
    }
}

?>
