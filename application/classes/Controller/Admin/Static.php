<?php
defined ( 'SYSPATH' ) or die ( 'No direct script access.' );

class Controller_Admin_Static extends Controller_Template {
	protected $auth;
	protected $user;
	public function before() {
		$this->template = 'static/' . $this->request->action;
		parent::before ();
		$this->auth = Auth::instance ();
		$this->user = $this->auth->get_user ();
		$this->template->user = $this->user->username;
		if (! $this->user) {
			Request::instance ()->redirect ( 'admin/index.html' );
		}
		$this->template->administrative = 1;
	
	}
	public function action_index() {
		$page = max ( 1, Arr::get ( $_GET, 'page', 1 ) );
		$per_page = ITEMS_PER_PAGE;
		$offset = $per_page * ($page - 1);
		$list_statics = ORM::factory ( 'Static' )->list_statics ( $per_page, $offset );
		$pag_data = array (
				'total_items' => ORM::factory ( 'Static' )->count_statics (), 
				'items_per_page' => $per_page, 
				'view' => 'pagination/list_projects' );
		$this->template->list_statics = $list_statics;
		$this->template->offset = $offset;
		$this->template->pagination = Pagination::factory ( $pag_data )->render ();
	}
	public function action_edit() {
		
		if (! $_POST) {
			$this->template->ckeditor = 'e';
			$this->template->one_news = ORM::factory ( 'Static' )->get_one_static_admin ( $_GET ['id'] );
		} else {
			
			ORM::factory ( 'Static' )->edit_static ( $_POST ['id'], $_POST ['name'], $_POST ['editor1'],$_POST ['title'],$_POST ['status']);
			Request::instance ()->redirect ( 'admin/static/index.html' );
		}
	
	}
	public function action_delete($id_news) {
		ORM::factory ( 'Static' )->delete_one_static ( $_GET ['id'] );
		Request::instance ()->redirect ( 'admin/static/index.html' );
	
	}
	public function action_create() {
		$this->template->ckeditor = 'e';
		if ($_POST) {
			if (ORM::factory ( 'Static' )->safe_static ( $_POST ['name'], $_POST ['editor1'],$_POST ['title'],$_POST ['status'] ))
				Request::instance ()->redirect ( 'admin/static/index.html' );
		}
	}

    public function action_nullstatus() {
		ORM::factory('Static')->check_status();
		Request::instance ()->redirect ( 'admin/static/index.html' );
	}
}
?>