<?php
defined ( 'SYSPATH' ) or die ( 'No direct script access.' );

class Controller_Admin_News extends Controller_Template {
	protected $auth;
	protected $user;
	//public $id_news;
	public $template = 'news/index';
	
	public function before() {
		$this->template = 'news/' . $this->request->action;
		parent::before ();
		$this->auth = Auth::instance ();
		$this->user = $this->auth->get_user ();
		$this->template->user = $this->user->username;
		if (! $this->user) {
			Request::instance ()->redirect ( 'admin/index.html' );
		}
		$this->template->administrative = 1;
	
	}
	
	public function action_index() {
		/*if ($_POST ['edit']) {
			//$this->edit ( $_POST ['select_news'] );
		
		$log=Request::factory( 'admin/news/edit.html' );
		$log->method='POST';
		$log->post=array('select_news'=>$_POST ['select_news']);
		$log->execute();
		}
		if ($_POST ['delete']) {
			$this->delete ( $_POST ['select_news'] );
		
		//Request::instance ()->redirect ( 'admin/news/delete.html' );
		

		}
		if ($_POST ['create']) {
			Request::instance ()->redirect ( 'admin/news/create.html' );
		
		}
		*/
		$page = max ( 1, Arr::get ( $_GET, 'page', 1 ) );
		$per_page = ITEMS_PER_PAGE;
		$offset = $per_page * ($page - 1);
		$list_news = ORM::factory ( 'News' )->list_news ( $per_page, $offset );
		$pag_data = array (
				'total_items' => ORM::factory ( 'News' )->count_news (), 
				'items_per_page' =>$per_page, 
				'view' => 'pagination/list_projects'
				 );
		$this->template->list_news = $list_news;
		$this->template->offset = $offset;
		$this->template->pagination = Pagination::factory ( $pag_data )->render ();
	
	}
	public function action_edit() {
		
		if (! $_POST) {
			//$this->template->ckeditor = 'e';
			$this->template->one_news = ORM::factory ( 'News' )->get_one_news_admin ( $_GET ['id'] );
		} else {
			$date = $_POST ['Date_Year'] . '-' . $_POST ['Date_Month'] . '-' . $_POST ['Date_Day'].' '.date('H:i:s');
			ORM::factory ( 'News' )->edit_news ( $_POST ['id'], $_POST ['title'], $_POST ['content'], $date );
			Request::instance ()->redirect ( 'admin/news/index.html' );
		}
	
	}
	public function action_delete($id_news) {
		ORM::factory ( 'News' )->delete_one_news ( $_GET['id'] );
		Request::instance ()->redirect ( 'admin/news/index.html' );
	
	}
	public function action_create() {
		//$this->template->ckeditor = 'e';
		if ($_POST) {
			$date = $_POST ['Date_Year'] . '-' . $_POST ['Date_Month'] . '-' . $_POST ['Date_Day'].' '.date('H:i:s');
			if (ORM::factory ( 'News' )->safe_news ( $_POST ['title'], $_POST ['content'], $date ))
				Request::instance ()->redirect ( 'admin/news/index.html' );
		}
	}
}
?>