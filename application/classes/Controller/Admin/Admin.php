<?php
defined ( 'SYSPATH' ) or die ( 'No direct script access.' );

//class Controller_Admin_Admin extends Controller_Template {
class Controller_Admin_Admin extends Controller_Base {
	protected $auth;
	protected $user;
	public $template;
	
	public function before() {
		$this->template = 'zalud/admin/' . $this->request->action().'.tpl';
		parent::before ();
		$this->auth = Auth::instance ();
		$this->user = $this->auth->get_user ();
	}
	
	public function action_index() {



		/*if ($this->user) {
			Request::instance ()->redirect ( 'admin/login.html' );
		}
		$status = $this->auth->login (  $_POST ['username'], $_POST ['password'] );
		if ($status) {
			Request::instance ()->redirect ( 'admin/login.html' );
		}
		$this->template->administrative = 1;*/
	}
	public function action_login() {
		if (! $this->user) {
			Request::instance ()->redirect ( 'admin/index.html' );
		}
		$this->template->user=$this->user->username;
		$this->template->administrative = 1;
	
	}
	public function action_logout() {
		$this->auth->logout ();
		Request::instance ()->redirect ( 'admin/index.html' );
	}
	public function action_fid() {
		$this->template->input = 'fid';
	}

}
?>