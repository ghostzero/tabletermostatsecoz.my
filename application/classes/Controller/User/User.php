<?php
defined ( 'SYSPATH' ) or die ( 'No direct script access.' );

//class Controller_Admin_Admin extends Controller_Template {
class Controller_User_User extends Controller_Base {
//	protected $auth;
//	protected $user;
	public $template;
    public $user_array;
	
	public function before() {
//		$this->template = 'zalud/user/' . $this->request->action().'.tpl';
		$this->template = 'zalud_new_design/user/' . $this->request->action().'.tpl';
		parent::before ();
        $this->template->user_array =$this->user_array;



	}
	
	public function action_index() {

	    if($this->current_role=="user"){
	        $array_user = ORM::factory('User')->get_user($this->user_array['id']);
            $array_device = ORM::factory('Device')->get_device_by_id($this->user_array['id']);
            if(count($array_device)!=0){
                    $this->template->flag_view_device=true;
                $this->template->output_array_user=$array_user;
                $this->template->output_array_device=$array_device;
            }else{
                $this->template->flag_view_device=false;
            }

            $test=1;



//            $info_about_device=

        }


//        $this->template = View::factory('zalud/user/index.tpl');

        /*$roles = $this->auth->get_user()->roles->find_all()->as_array();
        $user_roles_1 = ORM::factory('User')->user_roles($this->user->id);
        $user_roles_2 = $this->user->roles;
        # Получаем список ролей пользователя
        $user_roles_3 = Model::factory('User')->user_roles($this->user->id);
        $user_roles_4 = $this->user->roles->find_all()->as_array();
        $user_roles_5 = $this->user->roles->find_all()->as_array(NULL,'name');
        $user_roles_6 = $this->user->roles->find_all();
        $test1="asdf";*/


		/*if ($this->user) {
			Request::instance ()->redirect ( 'admin/login.html' );
		}
		$status = $this->auth->login (  $_POST ['username'], $_POST ['password'] );
		if ($status) {
			Request::instance ()->redirect ( 'admin/login.html' );
		}
		$this->template->administrative = 1;*/



	}

    public function action_service() {

    }

    public function action_user() {

    }


    public function action_addition_service() {

    }

    public function action_delete_user() {
        if(isset($_POST['user']) && $_POST['user']){
            ORM::factory('User')->delete_user_by_id($_POST['user']);

        }
        Request::current()->redirect('/user/index');
    }


    public function action_logined_page() {

        $array_users_all = ORM::factory('User')->get_users_not_superadmin();
        $this->template->array_users_all_without_superuser = $array_users_all;

    }


    public function action_registration()
    {
        // Если есть данные, присланные методом POST
        if ($_POST)
        {
            // Создаем переменную, отвечающую за связь с моделью данных User
            $model = ORM::factory('User');
            // Вносим в эту переменную значения, переданные из POST
            $model->values(array(
                'username' => $_POST['username'],
                'email' => $_POST['email'],
                'password' => $_POST['password'],
                'password_confirm' => $_POST['password_confirm'],
            ));

            $role_user=$_POST['type_of_user'];

            try
            {
// Пытаемся     сохранить пользователя (то есть, добавить в базу)
                $model->save();
// Назначаем ему роли
                $model->add('roles', ORM::factory('Role')->where('name', '=', 'login')->find());
                $model->add('roles', ORM::factory('Role')->where('name', '=', $role_user)->find());
// И отправляем его на страницу пользователя
//                $this->request->redirect('login/logined/');
//                Request::instance ()->redirect ( 'login/logined/' );
                Request::current()->redirect('/login/logined');
            }
            catch (ORM_Validation_Exception $e)
            {
// Это если возникли какие-то ошибки
                echo $e;
            }
        }

        $test_2=2;
// Загрузка формы логина
//        $this->response->body(View::factory('register'));
    }

    public function action_set_language()
    {
            if(isset($_POST['language_code'])){
                $session=Session::instance();
                $session->set('language_code',$_POST['language_code']);
            }

//        $uri=$this->request->uri();
//        Request::current()->redirect('/user/index');
//        Request::current()->redirect($this->request->_uri);
//        $request_object = new Request();
//        $current_url = URL::base(true)." | ".$request_object->uri()." | ".URL::query();
//        Good work
//        $current_url = URL::base(true)." | ".$this->request->uri()." | ".URL::query();
//        Request::current()->redirect('/');
//        Request::current()->redirect("/".$this->request->uri());
//        Request::current()->redirect("/");

        $test_1=1;
    }





	public function action_login() {
		if (! $this->user) {
			Request::instance ()->redirect ( 'admin/index.html' );
		}

		$this->template->user=$this->user->username;
		$this->template->administrative = 1;
	
	}

	public function action_logout() {
		$this->auth->logout ();
		Request::instance ()->redirect ( 'admin/index.html' );

	}
	public function action_fid() {
		$this->template->input = 'fid';
	
	}

}
?>