<?php
defined ( 'SYSPATH' ) or die ( 'No direct script access.' );

class User extends Kohana_Controller {
	public static function count($function) {
		$method = ORM::factory ( 'Function' )->get_method ( $function );
		$count = $method->count;
		$new_method = ORM::factory ( 'Function' )->get_method ( $function );
		$new_method->count = ++ $count;
		$new_method->save ();
	
	}
	public static function frequent_use_method() {
		$sql = 'SELECT name FROM `function` order by count desc limit 20';
		$result = DB::query ( Database::SELECT, $sql )->execute ()
			->as_array ();
		return $result;
	
	}
	public static function get_random_methods() {
		$sql = 'SELECT name FROM `function` ORDER BY rand() LIMIT 10';
		$result = DB::query ( Database::SELECT, $sql )->execute ()
			->as_array ();
		return $result;
	
	}
	public static function get_static_page() {
		$page = ORM::factory ( 'Static' )->where ( 'status', '=', true )
			->find ()->name;
		if (!$page) {
			$sql = 'SELECT name FROM `static` ORDER BY rand() LIMIT 1';
			$page = DB::query ( Database::SELECT, $sql )->execute()->get('name');
		}
		return $page;
	}
	
	public static function is_PCRE($PCRE_value){
		$re='#[\\|\#|\"|\{|\[|\(].*?[\\|\#|\"|\}|\]|\)][i|x|m|s|e]#i';
		//Если значение соответствует виду параметра PCRE
		if(preg_match($re, $PCRE_value)){ 
		//то передать верно
		return true;
		}else{
		//иначе ложь
		return false;
		//Все-если
		}
	}
	
	public function after() {
	}

}

?>