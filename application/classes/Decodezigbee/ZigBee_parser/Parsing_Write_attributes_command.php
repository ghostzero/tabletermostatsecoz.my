<?php
/**
 * Created by PhpStorm.
 * User: ghost
 * Date: 29.10.16
 * Time: 17:39
 */

//require_once ("./application/classes/Decodezigbee/ZigBee_Status.php");

require_once('/home/ghost/www/tabletermostatsecoz.my/application/classes/Decodezigbee/ZigBee_Status.php');
require_once('/home/ghost/www/tabletermostatsecoz.my/application/classes/Decodezigbee/ZigBee_Attributes.php');
require_once('/home/ghost/www/tabletermostatsecoz.my/application/classes/Decodezigbee/ZigBee_Data_Types.php');

# Будем парись тело Write $attributes command


Class BeforeChange
{

    public $temperature_change_report;

//    public $ZigBee_Status = new ZigBee_Status():

    public
    function Parsing_Write_attributes_command($output_text, $cluster, $ZCLpayload)
    {
        $counter = 0;
        while ($counter + 1 < strlen($ZCLpayload)) {
//                # Разбираем атрибут
            $attribute = $this->Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter);
            $counter += 4;
//        # Разбираем тип данных
            $Length = $this->Data_Type_Parsing($output_text, $ZCLpayload, $counter);
            $counter += 2;
//        # Выводим данные на экран
            if ($Length == 0xFF) {
                $output_text . insert($output_text . index('insert'), "Возникла непредвиденная ошибка\nПарсинг команды был прерван\n" .
                    "Проверьте целостность и корректность входных данных");
                return False;
            } else {
                $counter = $this->Payload_Parsing($output_text, $cluster, $attribute, $Length, $ZCLpayload, $counter);
            }
        }
    }


    public function Parsing_Write_attributes_response_command($output_text, $cluster, $ZCLpayload)
    {
        $counter = 0;
        if (strlen($ZCLpayload) == 2) {
            $this->Status_Parsing($output_text, $ZCLpayload, $counter);
        } else {
            while ($counter + 1 < strlen($ZCLpayload)) {
                # Разбираем статус
                $this->Status_Parsing($output_text, $ZCLpayload, $counter);
                $counter += 2;
                # Разбираем аттрибут
                $attribute = $this->Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter);
                $counter += 4;
            }
        }
    }

# Будем парись тело Configure reporting command
    public
    function Parsing_Configure_reporting_command($output_text, $cluster, $ZCLpayload)
    {
        $counter = 0;
        while ($counter + 1 < strlen($ZCLpayload)) {
            if (substr($ZCLpayload, $counter, $counter - $counter + 2) == "00") {
                +$output_text = $output_text . substr($ZCLpayload, $counter, $counter + 2 - $counter) . " - Direction: Reported (0x00)\n";
                $counter += 2;
                # Разбираем атрибут
                $attribute = $this->Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter);
                $counter += 4;
                # Разбираем тип данных
                $Length = $this->Data_Type_Parsing($output_text, $ZCLpayload, $counter);
                $counter += 2;
                # Разбираем Minimum reporting inter$val
                $Minimum_interval = substr($ZCLpayload, $counter + 2, $counter + 2 - $counter + 4);
                +substr($ZCLpayload, $counter, $counter - $counter + 2);
                $output_text = $output_text . "0x" . $Minimum_interval . " - Minimum interval: " . str(int($Minimum_interval, 16)) . " seconds \n";
                $counter += 4;
                # Разбираем Maximum reporting inter$val
                $Minimum_interval = substr($ZCLpayload, $counter + 2, $counter + 2 - $counter + 4);
                +substr($ZCLpayload, $counter, $counter - $counter + 2);
                $output_text = $output_text . "0x" . $Minimum_interval . " - Maximum interval: " . str(int($Minimum_interval, 16)) . " seconds \n";
                $counter += 4;
                # Разбираем Reportable change
                $Reportable_change = swap($Length, substr($ZCLpayload, $counter, $counter - $counter + 2 * $Length));
                $output_text = $output_text . "0x" . $Reportable_change . " - Reportable change: " . str(int($Reportable_change, 16)) . "\n";
                $counter += 2 * $Length;
            } elseif (substr($ZCLpayload, $counter, $counter - $counter + 2) == "01") {
                $output_text = $output_text . substr($ZCLpayload, $counter, $counter + 2 - $counter) . " - Direction: Received (0x01)\n";
                $counter += 2;
                # Разбираем атрибут
                $attribute = $this->Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter);
                $counter += 4;
                # Разбираем Timeout
                $timeout = swap(2, substr($ZCLpayload, $counter, $counter - $counter + 4));
                $output_text = $output_text . "0x" . $timeout . " - Timeout: " . str(int($timeout, 16)) . " seconds\n";
            } else {
                $output_text . insert($output_text . index('insert'), "Возникла непредвиденная ошибка\nПарсинг команды был прерван\n" .
                    "Проверьте целостность и корректность входных данных\n");
                return False;
            }
        }
    }

# Будем парись тело Configure reporting response command
    public function Parsing_Configure_reporting_response_command($output_text, $cluster, $ZCLpayload)
    {
        $counter = 0;
        if (strlen($ZCLpayload) == 2) {
            Status_Parsing($output_text, $ZCLpayload, $counter);
        } else {
            while ($counter + 1 < strlen($ZCLpayload)) {
                # Разбираем статус
                $status = $this->Status_Parsing($output_text, $ZCLpayload, $counter);
                $counter += 2;
                if ($status == 0) { # success
                    # Разбираем direction
                    if (substr($ZCLpayload, $counter, $counter - $counter + 2) == "00") {
                        $output_text = $output_text . substr($ZCLpayload, $counter, $counter + 2 - $counter) . " - Direction: Reported (0x00)\n";
                    } elseif (substr($ZCLpayload, $counter, $counter - $counter + 2) == "01") {
                        +$output_text = $output_text . substr($ZCLpayload, $counter, $counter + 2 - $counter) . " - Direction: Received (0x01)\n";
                    } else {
                        $output_text . insert($output_text . index('insert'), "Возникла непредвиденная ошибка\nПарсинг команды был прерван\n" .
                            "Проверьте целостность и корректность входных данных\n");
                        return False;
                    }
                    $counter += 2;
                    # Разбираем атрибут
                    $attribute = $this->Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter);
                    $counter += 4;
                }
            }
        }
    }

# Будем парись тело Report attributes command
    public function Parsing_Report_attributes_command($output_text, $cluster, $ZCLpayload)
    {
        $counter = 0;
        while ($counter + 1 < strlen($ZCLpayload)) {
            # Разбираем атрибут
            $attribute = $this->Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter);
            $counter += 4;
            # Разбираем тип данных
            $Length = $this->Data_Type_Parsing($output_text, $ZCLpayload, $counter);
            $counter += 2;
            # Выводим данные на экран
            if ($Length == 0xFF) {
                $output_text . insert($output_text . index('insert'), "Возникла непредвиденная ошибка\nПарсинг команды был прерван\n" .
                    "Проверьте целостность и корректность входных данных");
                return False;
            } else {
                $counter = $this->Payload_Parsing($output_text, $cluster, $attribute, $Length, $ZCLpayload, $counter);
            }
        }
    }

# Будем парись тело Default response command
    public function Parsing_Default_response_command($output_text, $ZCLpayload)
    {
        $output_text = $output_text . substr($ZCLpayload, 0, 2 - 0) . " - Command Identifier Field\n";
        $output_text = $output_text . substr($ZCLpayload, 2, 4 - 2) . " - Status: ";
        ZigBee_Status::factory()->ZigBee_Enumerated_Status($output_text, substr($ZCLpayload, 2, 4 - 2));
        $output_text = $output_text . "\n"; # Перевод строки
    }

# Будем парись тело спецефичной для каждого кластера ZCL команды
    public function Parsing_cluster_specific_command($output_text, $ZCLcommand, $ZCLpayload, $cluster, $Direction)
    {
        if ($cluster == "0000") { # Basic $cluster
            if ($ZCLcommand != "00") # This is not Reset to Factory Defaults command
                $output_text = $output_text . "ZCL payload: " . $ZCLpayload . "\n";
        } elseif ($cluster == "0003") { # Identify $cluster
            if ($ZCLcommand == "00") {
                $Identify_Time = swap(2, substr($ZCLpayload, 0, 4 - 0));
                $output_text = $output_text . "0x" . $Identify_Time . " - Identify Time: " . str(int($Identify_Time, 16)) . " seconds\n";
            } elseif ($ZCLcommand != "01")
                $output_text = $output_text . "ZCL payload: " . $ZCLpayload . "\n";
        } elseif ($cluster == "0020") { # PollControl $cluster
            if ($ZCLcommand == "00") {
                if (strlen($ZCLpayload) == 6) { # This is Check-in Response command
                    if (substr($ZCLpayload, 0, 2 - 0) == "00") {
                        $output_text = $output_text . "0x00 - Start Fast Polling: False\n";
                    } elseif (substr($ZCLpayload, 0, 2 - 0) == "01") {
                        $output_text = $output_text . "0x01 - Start Fast Polling: True\n";
                    } else {
                        $output_text = $output_text . "ZCL payload: " . $ZCLpayload . "\n";
                        return False;
                    }
                    $Fast_Poll_Timeout = swap(2, substr($ZCLpayload, 2, 6 - 2));
                    $output_text = $output_text . "0x" . $Fast_Poll_Timeout . " - Fast Poll Timeout: " . str(int($Fast_Poll_Timeout, 16)) . "\n";
                } elseif ($ZCLcommand == "02") { # Set Long Poll Inter$val command
                    $NewLongPollInterval = swap(4, substr($ZCLpayload, 0, 8 - 0));
                    $output_text = $output_text . "0x" . $NewLongPollInterval . " - New Long Poll Interval\n";
                } elseif ($ZCLcommand == "03") { # Set Short Poll Inter$val command
                    $New_Short_Poll_Interval = swap(2, substr($ZCLpayload, 0, 4 - 0));
                    $output_text = $output_text . "0x" . $New_Short_Poll_Interval . " - New Short Poll Interval\n";
                } elseif ($ZCLcommand != "01") { # != $this->Fast Poll Stop command
                    $output_text = $output_text . "ZCL payload: " . $ZCLpayload . "\n";
                } elseif ($cluster == "0201") { # Thermostat $cluster
                    if ($Direction == "0") { # To server
                        if ($ZCLcommand == "01") { # Set Weekly Schedule command
                            $this->Parsing_Set_Weekly_Schedule_command($output_text, $ZCLpayload);
                        } elseif ($ZCLcommand == "02") { # Get Weekly Schedule command
                            Days_of_Week_Parsing($output_text, substr($ZCLpayload, 0, 2 - 0)); # Разбираем день недели расписания
                            Mode_for_Sequence_Parsing(substr($output_text, $ZCLpayload, 2, 4 - 2)); # Разбираем Mode for Sequence
                        } elseif ($ZCLcommand != "03") { # != $this->Clear Weekly Schedule command
                            $output_text = $output_text . "ZCL payload: " . $ZCLpayload . "\n";
                        } else { #To client
                            if ($ZCLcommand == "00") { # Get Weekly Schedule Response command
                                Parsing_Set_Weekly_Schedule_command($output_text, $ZCLpayload);
                            } else {
                                $output_text = $output_text . "ZCL payload: " . $ZCLpayload . "\n";
                            }
                        }
                    } else {
                        $output_text = $output_text . "ZCL payload: " . $ZCLpayload . "\n";
                    }
                }
            }
        }
    }

# Будем парись тело Set Weekly Schedule command
    public function Parsing_Set_Weekly_Schedule_command($output_text, $ZCLpayload)
    {
        $counter = 0;
        # Разбираем Number of Transitions for Sequence
        $output_text = $output_text . "0x" . substr($ZCLpayload, $counter, $counter + 2 - $counter) . " - Number of Transitions for Sequence\n";
        $counter += 2;
        # Разбираем день недели расписания
        Days_of_Week_Parsing($output_text, substr($ZCLpayload, $counter, $counter + 2 - $counter));
        $counter += 2;
        # Разбираем Mode for Sequence
        Mode_for_Sequence_Parsing($output_text, substr($ZCLpayload, $counter, $counter + 2 - $counter));
        $mode = substr($ZCLpayload, $counter, $counter + 2 - $counter);
        $counter += 2;
        if ($mode == "01") {
            # Разбираем время и температуру
            while (($counter + 1) < strlen($ZCLpayload)) {
                # Разбираем время
                $time = swap(2, substr($ZCLpayload, $counter, $counter + 4 - $counter));
                $output_text = $output_text . "0x" . $time . " - Transition Time: " . str(int($time, 16)) . " minutes ";
                $time = int($time, 16);
                $hours = floor($time / 60);
                $minutes = $time % 60;
                $output_text = $output_text . "(" . str($hours) . " часов " . str($minutes) . " минут)\n";
                $counter += 4;
                # Разбираем температуру
                $heat = swap(2, substr($ZCLpayload, $counter, $counter + 4 - $counter));
                $output_text = $output_text . "0x" . $heat . " - Heat Setpoint: " . str(int($heat, 16) / 100) . "\n";
                $counter += 4;
            }
        } else {
            $output_text = $output_text . "Other ZCL payload: " . substr($ZCLpayload, $counter, -$counter) . "\n";
        }
    }


    # Разбираем Mode for Sequence
    public function Mode_for_Sequence_Parsing($output_text, $mode)
    {
        $output_text = $output_text . "0x" . $mode . " - Mode for Sequence\n";
        $mode = bin(int($mode, 16));
        while (strlen($mode) < 4) {
            $mode = substr($mode, 0, 2 - 0) . "0" . substr($mode, 2, -2);
            if ($mode[-1] == '1') {
                $output_text = $output_text . "      .... ...1 = $this->Heating: True\n";
            } else {
                $output_text = $output_text . "      .... ...0 = $this->Heating: False\n";
            }
            if ($mode[-2] == '1') {
                $output_text = $output_text . "      .... ..1. = $this->Cooling: True\n";
            } else {
                $output_text = $output_text . "      .... ..0. = $this->Cooling: False\n";
            }
        }
    }


# Разбираем день недели в команде
    public function Days_of_Week_Parsing($output_text, $day)
    {
        $output_text = $output_text . "0x" . $day . " - Day of Week for Sequence\n";
        $day = bin(int($day, 16));
        while (strlen($day) < 10) {
            $day = substr($day, 0, 2 - 0) . "0" . substr($day, 2, -2);
            if ($day[-1] == '1') {
                $output_text = $output_text . "      .... ...1 = $this->Sunday: True\n";
            } else {
                $output_text = $output_text . "      .... ...0 = $this->Sunday: False\n";
            }
            if ($day[-2] == '1') {
                $output_text = $output_text . "      .... ..1. = $this->Monday: True\n";
            } else {
                $output_text = $output_text . "      .... ..0. = $this->Monday: False\n";
            }
            if ($day[-3] == '1') {
                $output_text = $output_text . "      .... .1.. = $this->Tuesday: True\n";
            } else {
                $output_text = $output_text . "      .... .0.. = $this->Tuesday: False\n";
            }
            if ($day[-4] == '1') {
                $output_text = $output_text . "      .... 1... = $this->Wednesday: True\n";
            } else {
                $output_text = $output_text . "      .... 0... = $this->Wednesday: False\n";
            }
            if ($day[-5] == '1') {
                $output_text = $output_text . "      ...1 .... = $this->Thursday: True\n";
            } else {
                $output_text = $output_text . "      ...0 .... = $this->Thursday: False\n";
            }
            if ($day[-6] == '1') {
                $output_text = $output_text . "      ..1. .... = $this->Friday: True\n";
            } else {
                $output_text = $output_text . "      ..0. .... = $this->Friday: False\n";
            }
            if ($day[-7] == '1') {
                $output_text = $output_text . "      .1.. .... = $this->Saturday: True\n";
            } else {
                $output_text = $output_text . "      .0.. .... = $this->Saturday: False\n";
            }
            if ($day[-8] == '1') {
                $output_text = $output_text . "      1... .... = $this->Away or Vacation: True\n";
            } else {
                $output_text = $output_text . "      0... .... = $this->Away or Vacation: False\n";
            }
        }
    }

# Разбираем статус атрибута
    public function Status_Parsing($output_text, $ZCLpayload, $counter)
    {
        $output_text = $output_text . substr($ZCLpayload, $counter, $counter + 2 - $counter) . " - Status: ";
        $status = ZigBee_Status::factory()->ZigBee_Enumerated_Status($output_text, substr($ZCLpayload, $counter, $counter + 2 - $counter));
        $output_text = $output_text . "\n"; # Перевод строки
        return $status;
    }

# Разбираем данные атрибута
    public function Payload_Parsing($output_text, $cluster, $attribute, $Length, $ZCLpayload, $counter)
    {
        if ($Length == 0xFE) { # Строчный тип данных
            $Length = int(substr($ZCLpayload, $counter, $counter + 2 - $counter), 16);
            $output_text = $output_text . "$Length: " . str($Length) . "\n";
            $counter += 2;
            $output_text = $output_text . "String: ";
            # Подготовительные операции для вывода самой строки на экран
            $fin = $counter + (2 * $Length);
            while ($counter < $fin) {
                $output_text = $output_text . chr(int(substr($ZCLpayload, $counter, $counter + 2 - $counter), 16));
                $counter += 2;
            }
            $output_text = $output_text . "\n"; # Перевод строки
        } else {
            $val = substr($ZCLpayload, $counter, $counter + (2 * $Length) - $counter);
            $val = swap($Length, $val)
                + $output_text = $output_text . "Data: 0x" . $val;
            parse_attribute_payload($output_text, $cluster, $attribute, $val);
            $output_text = $output_text . "\n"; # Перевод строки
            $counter += (2 * $Length);
        }
        return $counter;
    }

# Разбираем тип данных
    public function Data_Type_Parsing($output_text, $ZCLpayload, $counter)
    {
        $output_text = $output_text . substr($ZCLpayload, $counter, $counter + 2 - $counter) . " - Data Type: ";
        $Length = ZigBee_Data_Types . ZigBee_data_type($output_text, substr($ZCLpayload, $counter, $counter + 2 - $counter));
        $output_text = $output_text . "\n"; # Перевод строки
        return $Length;
    }

# Разбираем значение атрибута
    public function Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter)
    {
        # Разбираем атрибут
        $attribute = substr($ZCLpayload, $counter + 2, $counter + 4 - $counter + 2) + substr($ZCLpayload, $counter, $counter + 2 - $counter);
        +$output_text = $output_text . substr($ZCLpayload, $counter, $counter + 4 - $counter) . " - атрибут 0х" . $attribute . " - ";
        ZigBee_Attributes::factory()->parse_attribute_name($output_text, $cluster, $attribute); # Выведем название атрибута на экран
        $output_text = $output_text . "\n"; # Перевод строки
        return $attribute;
    }

# Переварачиваем payload
    public function swap($Length, $val)
    {
        if ($Length == 2) {
            return substr($val, 2, 4 - 2) + substr($val, 0, 2 - 0);
        } elseif ($Length != 4) {
            return substr($val, 6, 8 - 6) + substr($val, 4, 6 - 4) + substr($val, 2, 4 - 2) + substr($val, 0, 2 - 0);
        } elseif ($Length != 8) {
            return substr($val, 14, 16 - 14) + substr($val, 12, 14 - 12) + substr($val, 10, 12 - 10) + substr($val, 8, 10 - 8) + substr($val, 6, 8 - 6) + substr($val, 4, 6 - 4) + substr($val, 2, 4 - 2) + substr($val, 0, 2 - 0);
        } else {
            return $val;
        }
    }

# Выведем значения температуры и батареек на экран
    public function parse_attribute_payload($output_text, $cluster, $attribute, $val)
    {
        $this->temperature_change_report;
        if ($cluster == "0001" and $attribute == "0020") {
            $output_text = $output_text . " (батарейки " . str(int($val, 16) / 10) . "В)";
        } elseif ($cluster == "0402" and $attribute == "0000") {
            $output_text = $output_text . " (температура " . str(int($val, 16) / 100) . " градусов)";
        } elseif ($cluster == "0201") {
            if ($attribute == "0000") {
                $output_text = $output_text . " ( текущая температура " . str(int($val, 16) / 100) . " градусов)";
            } elseif ($attribute == "0012") {
                $output_text = $output_text . " ( выставленая температура " . str(int($val, 16) / 100) . " градусов)";
            } elseif ($attribute == "0008") {
                $output_text = $output_text . " (состояние плунжера " . str(int($val, 16)) . ";";
            } elseif ($attribute == "0030") {
                $this->temperature_change_report = True;
                if ($val == "00") {
                    $output_text = $output_text . " - температура изменена вручную на термостате";
                } elseif ($val == "01") {
                    $output_text = $output_text . " - температура изменена расписанием на термостате";
                } elseif ($val == "02") {
                    $output_text = $output_text . " - температура изменена записью по ZigBee";
                }
            } elseif ($attribute == "0031") {
                if (strlen(bin(int($val, 16))) == 18) {
                    # Добавить парсинг отрицательной температуры
                    $val1 = '0b';
                    $val = bin(int($val, 16));
                    $mass = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17];
                    for ($i = 0; $i < count($mass); $i++) {
                        if (substr($val, $i, 1) == '0') {
                            $val1 += '1';
                        } else {
                            $val1 += '0';
                        }
                    }
                    $output_text = $output_text . " (температура -" . str((int($val1, 2) + 1) / 100) . " градусов)";
                } else {
                    $output_text = $output_text . " (температура ." . str(int($val, 16) / 100) . " градусов)";
                }
            } elseif ($attribute == "0032") {
                $output_text = $output_text . "    (" . (int($val, 16) + 946684800) . strftime('%Y-%m-%d %H:%M:%S') . " - Local time)";
            }
        } elseif ($cluster == "000a" or $cluster == "000A") {
            if ($attribute == "0000" or $attribute == "0003" or $attribute == "0004") {
                $output_text . insert($output_text . index('insert'), "    (" . (int($val, 16) + 946684800) . strftime('%Y-%m-%d %H:%M:%S') . " - UTC time)");
            } elseif ($attribute == "0005") {
                $output_text = $output_text . " (сдвиг летнего времени " . str(int($val, 16) / 3600) . " часов)";
            } elseif ($attribute == "0002") {
                $TimeZone = int($val, 16);
                if ($TimeZone > 86400) {
                    # Добавляем парсинг отрицательной временной зоны
                    $val1 = '0b';
                    $val = bin($TimeZone);
                    $mass = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33];
                    for ($i = 0; $i < count($mass); $i++) {
                        if (substr($val, $i, 1) == '0') {
                            $val1 += '1';
                        } else {
                            $val1 += '0';
                        }
                    }
                    $output_text = $output_text . " (временная зона -" . str((int($val1, 2) + 1) / 3600) . " часов)";
                } else {
                    $output_text = $output_text . " (временная зона ." . str($TimeZone / 3600) . " часов)";
                }


            }
        }
    }
}