<?php
/**
 * Created by PhpStorm.
 * User: ghost
 * Date: 29.10.16
 * Time: 15:11
 */
# разбираем общие команды
class  Profile_wide_command
{
    public
    function Profile_wide_command($output_text, $ZCLcommand)
    {
        if ($ZCLcommand == "00") {
            $output_text = $output_text . "Read attributes";
        } elseif ($ZCLcommand == "01") {
            $output_text = $output_text . "Read attributes response";
        } elseif ($ZCLcommand == "02") {
            $output_text = $output_text . "Write attributes";
        } elseif ($ZCLcommand == "03") {
            $output_text = $output_text . "Write attributes undivided";
        } elseif ($ZCLcommand == "04") {
            $output_text = $output_text . "Write attributes response";
        } elseif ($ZCLcommand == "05") {
            $output_text = $output_text . "Write attributes no response";
        } elseif ($ZCLcommand == "06") {
            $output_text = $output_text . "Configure reporting";
        } elseif ($ZCLcommand == "07") {
            $output_text = $output_text . "Configure reporting response";
        } elseif ($ZCLcommand == "08") {
            $output_text = $output_text . "Read reporting configuration";
        } elseif ($ZCLcommand == "09") {
            $output_text = $output_text . "Read reporting configuration response";
        } elseif ($ZCLcommand == "0A" or ZCLcommand == "0a") {
            $output_text = $output_text . "Report attributes";
        } elseif ($ZCLcommand == "0B" or ZCLcommand == "0b") {
            $output_text = $output_text . "Default response";
        } elseif ($ZCLcommand == "0C" or ZCLcommand == "0c") {
            $output_text = $output_text . "Discover attributes";
        } elseif ($ZCLcommand == "0D" or ZCLcommand == "0d") {
            $output_text = $output_text . "Discover attributes response";
        } elseif ($ZCLcommand == "0E" or ZCLcommand == "0e") {
            $output_text = $output_text . "Read attributes structured";
        } elseif ($ZCLcommand == "0F" or ZCLcommand == "0f") {
            $output_text = $output_text . "Write attributes structured";
        } elseif ($ZCLcommand == "10") {
            $output_text = $output_text . "Write attributes structured response";
        } else {
            $output_text = $output_text . "Unknown";
        }
    }
}

