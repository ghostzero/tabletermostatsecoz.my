<?php

/**
 * Created by PhpStorm.
 * User: ghost
 * Date: 28.10.16
 * Time: 15:15
 */
class Parsing_profile_wide_command
{
  # Будем парись тело общей ZCL команды
    public function Parsing_profile_wide_command($output_text, $ZCLcommand, $ZCLpayload, $cluster)
    {
        if ($ZCLcommand == "00") {                                # Read attributes
            $this->Parsing_Read_attributes_command($output_text, $cluster, $ZCLpayload);
        } elseif ($ZCLcommand == "01") {                        # Read attributes response
            $this->Parsing_Read_attributes_response_command($output_text, $cluster, $ZCLpayload);
        } elseif ($ZCLcommand == "02" or $ZCLcommand == "03") {    # Write attributes or Write attributes undivided
            $this->Parsing_Write_attributes_command($output_text, $cluster, $ZCLpayload);
        } elseif ($ZCLcommand == "04") {                        # Write attributes response
            $this->Parsing_Write_attributes_response_command($output_text, $cluster, $ZCLpayload);
        } elseif ($ZCLcommand == "05") {                        # Write attributes no response
            $this->Parsing_Write_attributes_command($output_text, $cluster, $ZCLpayload);
        } elseif ($ZCLcommand == "06") {                        # Configure reporting
            $this->Parsing_Configure_reporting_command($output_text, $cluster, $ZCLpayload);
        } elseif ($ZCLcommand == "07") {                        # Configure reporting response
            $this->Parsing_Configure_reporting_response_command($output_text, $cluster, $ZCLpayload);
        } elseif ($ZCLcommand == "0A" or $ZCLcommand == "0a") {    # Report attributes
            $this->Parsing_Report_attributes_command($output_text, $cluster, $ZCLpayload);
        } elseif ($ZCLcommand == "0B" or $ZCLcommand == "0b") {    # Default response
            $this->Parsing_Default_response_command($output_text, $ZCLpayload);
        } else {
            # Unknown Просто выведем тело команды на экран
            $output_text = $output_text . "ZCL payload: " . $ZCLpayload . "\n";
        }
    }
}