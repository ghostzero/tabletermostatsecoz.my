<?php

/**
 * Created by PhpStorm.
 * User: ghost
 * Date: 29.10.16
 * Time: 16:35
 */
class Parsing_Read_attributes_response_command
{


# Будем парись тело Read attributes response command
    public function Parsing_Read_attributes_response_command($output_text, $cluster, $ZCLpayload)
    {
        $counter = 0;
        while ($counter + 1 < len($ZCLpayload)) {
            # Разбираем атрибут
            $attribute = Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter);
            $counter += 4;
            # Разбираем статус чтения атрибута
            $status = Status_Parsing($output_text, $ZCLpayload, $counter);
            $counter += 2;
            if ($status == 0) {
                # Разбираем тип данных
                $Length = Data_Type_Parsing($output_text, $ZCLpayload, $counter);
                $counter += 2;
                # Выводим данные на экран
                if ($Length == 0xFF) {
                    $output_text = $output_text . "Возникла непредвиденная ошибка\nПарсинг команды был прерван\n" + "Проверьте целостность и корректность входных данных";
                    return False;
                } else {
                    $counter = Payload_Parsing($output_text, $cluster, $attribute, $Length, $ZCLpayload, $counter);
                }
            }
        }
    }

    public function Parsing_Write_attributes_command($output_text, $cluster, $ZCLpayload)
    {
        $counter = 0;
        while ($counter + 1 < len($ZCLpayload)) {
//                # Разбираем атрибут
            $attribute = $this->Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter);
            $counter += 4;
//        # Разбираем тип данных
            $Length = Data_Type_Parsing($output_text, $ZCLpayload, $counter);
            $counter += 2;
//        # Выводим данные на экран
            if ($Length == 0xFF) {
                $output_text . insert($output_text . index('insert'), "Возникла непредвиденная ошибка\nПарсинг команды был прерван\n" +
                    "Проверьте целостность и корректность входных данных");
                return False;
            } else {
                $counter = Payload_Parsing($output_text, $cluster, $attribute, $Length, $ZCLpayload, $counter);
            }
        }
    }

    public function Parsing_Write_attributes_response_command($output_text, $cluster, $ZCLpayload)
    {
        $counter = 0;
        if (len($ZCLpayload) == 2) {
            $this->Status_Parsing($output_text, $ZCLpayload, $counter);
        } else {
            while ($counter + 1 < len($ZCLpayload)) {
                # Разбираем статус
                $this->Status_Parsing($output_text, $ZCLpayload, $counter);
                $counter += 2;
                # Разбираем аттрибут
                $attribute = $this->Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter);
                $counter += 4;
            }
        }
    }


    # Будем парись тело Configure reporting command
    public function Parsing_Configure_reporting_command($output_text, $cluster, $ZCLpayload)
    {
        $counter = 0;
        while ($counter + 1 < len($ZCLpayload)) {
            if (substr($ZCLpayload, $counter, $counter + 2 - $counter) == "00") {
                $output_text = $output_text . substr($ZCLpayload, $counter, $counter + 2 - $counter) + " - Direction: Reported(0x00)\n";
                $counter += 2;
                # Разбираем атрибут
                $attribute = $this->Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter);
                $counter += 4;
                # Разбираем тип данных
                $Length = Data_Type_Parsing($output_text, $ZCLpayload, $counter);
                $counter += 2;
                # Разбираем Minimum reporting interval
                $Minimum_interval = substr($ZCLpayload, $counter + 2, $counter + 4 - $counter + 2) + substr($ZCLpayload, $counter, $counter + 2 - $counter);
                $output_text = $output_text . "0x" + $Minimum_interval + " - Minimum interval: " + str(int($Minimum_interval, 16)) + " seconds \n";
                $counter += 4;
                # Разбираем Maximum reporting interval
                $Minimum_interval = substr($ZCLpayload, $counter + 2, $counter + 4 - $counter + 2) + substr($ZCLpayload, $counter, $counter + 2 - $counter);
                $output_text = $output_text . "0x" + $Minimum_interval + " - Maximum interval: " + str(int($Minimum_interval, 16)) + " seconds \n";
                $counter += 4;
                # Разбираем Reportable change
                $Reportable_change = swap($Length, substr($ZCLpayload, $counter, $counter + 2 * $Length - $counter));
                $output_text = $output_text . "0x" + $Reportable_change + " - Reportable change: " + str(int($Reportable_change, 16)) + "\n";
                $counter += 2 * $Length;
            } elseif (substr($ZCLpayload, $counter, $counter + 2 - $counter) == "01") {
                $output_text = $output_text . substr($ZCLpayload, $counter, $counter + 2 - $counter) + " - Direction: Received(0x01)\n";
                $counter += 2;
                # Разбираем атрибут
                $attribute = Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter);
                $counter += 4;
                # Разбираем Timeout
                $timeout = swap(2, substr($ZCLpayload, $counter, $counter + 4 - $counter));
                $output_text = $output_text . "0x" + $timeout + " - Timeout: " + str(int($timeout, 16)) + " seconds\n";
            } else {
                $output_text = $output_text . "Возникла непредвиденная ошибка\nПарсинг команды был прерван\n" +
                    "Проверьте целостность и корректность входных данных\n";
                return False;
            }
        }
    }


}