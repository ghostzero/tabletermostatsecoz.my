<?php

/**
 * Created by PhpStorm.
 * User: ghost
 * Date: 29.10.16
 * Time: 15:17
 */
class Cluster_specific_command{
    # разбираем команды, которые спецефичны для некоторых кластеров
    public
    function Cluster_specific_command($output_text, $ZCLcommand, $cluster, $Direction)
    {
        if ($cluster == "0000") {     # Basic cluster
            if ($ZCLcommand == "00") {     # Reset to Factory Defaults
                $output_text = $output_text . "Reset to Factory Defaults";
            } else {
                $output_text = $output_text . "Unknown";
            }
        } elseif ($cluster == "0003") { # Identify cluster
            if ($ZCLcommand == "00") {
                if ($Direction == "0") {     # To server
                    $output_text = $output_text . "Identify";
                } else {
                    $output_text = $output_text . "Identify Query Response";
                }
            } elseif ($ZCLcommand == "01") {
                $output_text = $output_text . "Identify Query";
            } else {
                $output_text = $output_text . "Unknown";
            }
        } elseif ($cluster == "0020") { # PollControl cluster
            if ($ZCLcommand == "00") {
                if ($Direction == "0") {     # To server
                    $output_text = $output_text . "Check-in Response";
                } else {
                    $output_text = $output_text . "Check-in";
                }
            } elseif ($ZCLcommand == "01") {
                $output_text = $output_text . "Fast Poll Stop";
            } elseif ($ZCLcommand == "02") {
                $output_text = $output_text . "Set Long Poll Interval";
            } elseif ($ZCLcommand == "03") {
                $output_text = $output_text . "Set Short Poll Interval";
            } else {
                $output_text = $output_text . "Unknown";
            }
        } elseif ($cluster == "0201") { # Thermostat cluster
            if ($ZCLcommand == "00") {
                if ($Direction == "0") {     # To server
                    $output_text = $output_text . "Setpoint Raise/Lower";
                } else {
                    $output_text = $output_text . "Get Weekly Schedule Response";
                }
            } elseif ($ZCLcommand == "01") {
                if ($Direction == "0") {     # To server
                    $output_text = $output_text . "Set Weekly Schedule";
                } else {
                    $output_text = $output_text . "Get Relay Status Log Response";
                }
            } elseif ($ZCLcommand == "02") {
                $output_text = $output_text . "Get Weekly Schedule";
            } elseif ($ZCLcommand == "03") {
                $output_text = $output_text . "Clear Weekly Schedule";
            } elseif ($ZCLcommand == "04") {
                $output_text = $output_text . "Get Relay Status Log";
            } else {
                $output_text = $output_text . "Unknown";
            }
        } else {
            $output_text = $output_text . "Unknown";
        }
    }


}