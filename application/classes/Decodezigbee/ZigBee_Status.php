<?php

/**
 * Created by PhpStorm.
 * User: ghost
 * Date: 28.10.16
 * Time: 11:23
 */
Class ZigBee_Status
{
    public function __construct()
    {
    }

    public static function factory()
    {
        return new ZigBee_Status ();
    }

    # Парсим статус в ZigBee команде
    public function ZigBee_Enumerated_Status($output_text, $status)
    {

        if ($status == "00") {
            $output_text=$output_text."SUCCESS";
            # "orange" "red" "OliveDrab1" "cyan" "light gray" "lime green" "DarkOrchid1"
            return 0x00;
        } elseif ($status == "01") {
            $this->Bad_status($output_text, "FAILURE");
            return 0x01;
        } elseif ($status == "7e" or $status == "7E") {
            $this->Bad_status($output_text, "NOT_AUTHORIZED");
            return 0x7e;
        } elseif ($status == "7F" or $status == "7F") {
            $this->Bad_status($output_text, "RESERVED_FIELD_NOT_ZERO");
            return 0x7f;
        } elseif ($status == "80") {
            $this->Bad_status($output_text, "MALFORMED_COMMAND");
            return 0x80;
        } elseif ($status == "81") {
            $this->Bad_status($output_text, "UNSUP_CLUSTER_COMMAND");
            return 0x81;
        } elseif ($status == "82") {
            $this->Bad_status($output_text, "UNSUP_GENERAL_COMMAND");
            return 0x82;
        } elseif ($status == "83") {
            $this->Bad_status($output_text, "UNSUP_MANUF_CLUSTER_COMMAND");
            return 0x83;
        } elseif ($status == "84") {
            $this->Bad_status($output_text, "UNSUP_MANUF_GENERAL_COMMAND");
            return 0x84;
        } elseif ($status == "85") {
            $this->Bad_status($output_text, "INVALID_FIELD");
            return 0x85;
        } elseif ($status == "86") {
            $this->Bad_status($output_text, "UNSUPPORTED_ATTRIBUTE");
            return 0x86;
        } elseif ($status == "87") {
            $this->Bad_status($output_text, "INVALID_VALUE");
            return 0x87;
        } elseif ($status == "88") {
            $this->Bad_status($output_text, "READ_ONLY");
            return 0x88;
        } elseif ($status == "89") {
            $this->Bad_status($output_text, "INSUFFICIENT_SPACE");
            return 0x89;
        } elseif ($status == "8a" or $status == "8A") {
            $this->Bad_status($output_text, "DUPLICATE_EXISTS");
            return 0x8a;
        } elseif ($status == "8b" or $status == "8B") {
            $this->Bad_status($output_text, "NOT_FOUND");
            return 0x8b;
        } elseif ($status == "8c" or $status == "8C") {
            $this->Bad_status($output_text, "UNREPORTABLE_ATTRIBUTE");
            return 0x8c;
        } elseif ($status == "8d" or $status == "8D") {
            $this->Bad_status($output_text, "INVALID_DATA_TYPE");
            return 0x8d;
        } elseif ($status == "8e" or $status == "8E") {
            $this->Bad_status($output_text, "INVALID_SELECTOR");
            return 0x8e;
        } elseif ($status == "8f" or $status == "8F") {
            $this->Bad_status($output_text, "WRITE_ONLY");
            return 0x8f;
        } elseif ($status == "90") {
            $this->Bad_status($output_text, "INCONSISTENT_STARTUP_STATE");
            return 0x90;
        } elseif ($status == "91") {
            $this->Bad_status($output_text, "DEFINED_OUT_OF_BAND");
            return 0x91;
        } elseif ($status == "92") {
            $this->Bad_status($output_text, "INCONSISTENT");
            return 0x92;
        } elseif ($status == "93") {
            $this->Bad_status($output_text, "ACTION_DENIED");
            return 0x93;
        } elseif ($status == "94") {
            $this->Bad_status($output_text, "TIMEOUT");
            return 0x94;
        } elseif ($status == "95") {
            $this->Bad_status($output_text, "ABORT");
            return 0x95;
        } elseif ($status == "96") {
            $this->Bad_status($output_text, "INVALID_IMAGE");
            return 0x96;
        } elseif ($status == "97") {
            $this->Bad_status($output_text, "WAIT_FOR_DATA");
            return 0x97;
        } elseif ($status == "98") {
            $this->Bad_status($output_text, "NO_IMAGE_AVAILABLE");
            return 0x98;
        } elseif ($status == "99") {
            $this->Bad_status($output_text, "REQUIRE_MORE_IMAGE");
            return 0x99;
        } elseif ($status == "c0" or $status == "C0") {
            $this->Bad_status($output_text, "HARDWARE_FAILURE");
            return 0xc0;
        } elseif ($status == "c1" or $status == "C1") {
            $this->Bad_status($output_text, "SOFTWARE_FAILURE");
            return 0xc1;
        } elseif ($status == "c2" or $status == "C2") {
            $this->Bad_status($output_text, "CALIBRATION_ERROR");
            return 0xc2;
        } else {
            $this->Bad_status($output_text, "UNKNOWN");
            return 0xff;
        }
    }

    # Выделим цветом ошибки (все кроме 0)
    public function Bad_status($output_text, $status)
    {
        $tag1 = $output_text;
        $output_text = $output_text . $status;
        $tag2 = $output_text;
        //	output_text . tag_add("error", tag1, tag2)
        $output_text = $output_text . $tag1 . $tag2;
        return $output_text;
    }


}