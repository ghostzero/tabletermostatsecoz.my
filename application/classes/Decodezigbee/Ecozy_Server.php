<?php
/**
 * Created by PhpStorm.
 * User: ghost
 * Date: 30.10.16
 * Time: 14:20
 */
//import datetime
//   import ZigBee_Profiles, ZigBee_Clusters, ZigBee_parser

require_once("ZigBee_Profiles.php");
require_once("ZigBee_Clusters.php");
require_once("ZigBee_Parser.php");

class Ecozy_Server extends Kohana_Controller
{
    public function __construct()
    {
    }

    public static function factory()
    {
        return new Ecozy_Server ();
    }


    public $array_pointer = 0; # глобальный счетчик, указывает на наше положение в строке

# Проверяем заголовок команды. Данные должны начиналься подстрокой "ECOZY"
    public function check_command_header($output_text, $command)
    {
        if ($command . startswith("45434f5a59") == True) {
            $output_text = $output_text . "ECOZY - заголовок пакета\n";
            return True;
        } else {
            $output_text = $output_text . "Неверные входные данные\n"; # Слишком мало входных данных
            return False;
        }
    }

# Пришедшая команда от концентратора на сервер должна быть '07'
    public function check_cu_command($output_text, $command)
    {
        if (substr($command, 10, 12 - 10) == "07") {
            $output_text . insert($output_text . index('insert'), substr($command, 10, 12 - 10) . " - Передача данных от концентратора для журналирования\n");
            return True;
        } else {
            $output_text = $output_text . "Неверная команда от концентратора\n"; # Неверная команда от концентратора
            return False;
        }
    }

# Вычисляем длину пришедшей команды
    public function main_command_len($output_text, $command)
    {
        if (len($command) < 16) {
            $output_text = $output_text . "Недостаточно данных\n"; # Слишком мало входных данных
            return False;
        }

        $length = int(substr($command, 12, 16 - 12), 16);
        $output_text . insert($output_text . index('insert'), str($length) . " - Длина тела сообщения, байты\n");
        return True;
    }

# Вычисляем количество связей телефонов
    public function Phone_ID_numbers($output_text, $command)
    {
        $this->array_pointer;
        if (strlen($command) < 17) {
            $output_text = $output_text . "Недостаточно данных\n"; # Слишком мало входных данных
            return False;
        }

        $this->array_pointer = 0; # сбрасываем счетчик, у нас новый цыкл
        $IDnumbers = substr($command, 16, 1) . substr($command, 17, 1); # Вычисляем количество связей телефонов
        $IDnumbers = $this->int($IDnumbers, 16);
        $output_text = $output_text . $IDnumbers . " - Количество связей телефонов\n";
        $this->array_pointer = 18; # указываем следующий байт в принятой строке для парсинка
        $output_text = $output_text . "\n"; # Перевод строки

        if ($IDnumbers == 0) {
            return ['output_text' => $output_text, 'command' => $command];
        } else {
            $id = 1; # парсим связи телефонов
            while ($id <= $IDnumbers) {
                $this->Phone_ID_parsing($output_text, $command);
                $output_text = $output_text . "\n"; # Перевод строки
                $id += 1;
            }
        }

        if (strlen($command) == 17) {
            return False; # данные закончились
        }
        return ['output_text' => $output_text, 'command' => $command];

//        return True;
    }

# разбираем связи телефонов
    public
    function Phone_ID_parsing($output_text, $command)
    {
        $this->array_pointer;
        $IdNumber = substr($command, $this->array_pointer, 1) . substr($command, $this->array_pointer + 1, 1); # читаем номер связи
        $this->array_pointer += 2;
        $IdLength = substr($command, $this->array_pointer, 1) . substr($command, $this->array_pointer + 1, 1); # читаем длину связи
        $this->array_pointer += 2;
        $IdNumber = $this->int($IdNumber, 16); # пребразуем строчный вид в число
        $IdLength = $this->int($IdLength, 16); # пребразуем строчный вид в число
        $output_text = $output_text . strval($IdNumber) . " - Номер связи телефона\n";
        $output_text = $output_text . strval($IdLength) . " - Длина связи телефона\n";
        $fin = $this->array_pointer + (2 * $IdLength);
        while ($this->array_pointer < $fin) {
            $output_text = $output_text . $this->int(substr($command, $this->array_pointer, $this->array_pointer + 2 - $this->array_pointer), 16);
            $this->array_pointer += 2;
        }

        $output_text = $output_text . " - ID связи телефона\n"; # Перевод строки
    }

# Разбираем входящие ZigBee пакеты
    public function CU_ZigBee_incomming($output_text, $command)
    {
        $array = $this->Phone_ID_numbers($output_text, $command);
        if (!$array) return false;
        $output_text = $array['output_text'];
        $command = $array['command'];
        $this->array_pointer;
        $list_array_output_for_database = [];
        while (True) {
//            $array_output_for_database = ['current_temperature' => null, 'installed_temperature' => null, 'condition_plunger' => null, 'mac' => null,'datetime'=>null];
            /* Параметры ввода данных в таблицу.
             * 1.Время отрибут «time» номера 00; 02; 03;04;05;07
                2. Батарейка power configuration  атрибут № 20
                3. Установлении расписании, вывод расписания.
                4. Установка температуры .
                5. Источник установки температуры( 1-ручной на термостате, 2 удалённо(сервер), 3- по расписанию ).
             * */


//            $array_output_for_database = ['current_temperature' => null, 'installed_temperature' => null, 'condition_plunger' => null, 'mac' => null, 'datetime' => null, ''];

            $array_output_for_database = ['condition_plunger' => null,
                'current_temperature' => null,
                'installed_temperature' => null,
                'mac' => null,
                'datetime' => null,
                'status_battery' => null,
                'some_temperature' => null,
                'status_change_temperature' => null,
                'negative_temperature' => null,
                'local_time' => null,
                'UTC_time' => null,
                'begin_summer_DstStart' => null,
                'finish_summer_DstStart' => null,
                'begin_summer_DstStart' => null,
                'finish_summer_DstStart' => null,
                'timezone' => null,
                'timezone' => null,
            ];
            $zigbeeparserObject = ZigBee_Parser::factory();
            $zigbeeparserObject->temperature_change_report = False;
//            $tag1 = $output_text . index('insert');
            $CommLength = substr($command, $this->array_pointer, $this->array_pointer + 4 - $this->array_pointer);
            $this->array_pointer += 4;
            $CommLength = $this->int($CommLength, 16);
            $output_text = $output_text . $CommLength . " - Длина ZigBee команды\n";
            $MessageTime = substr($command, $this->array_pointer, $this->array_pointer + 8 - $this->array_pointer);
            $this->array_pointer += 8;
            $MessageTime = $this->int($MessageTime, 16);
            $output_text = $output_text . $MessageTime . " - Время команды " . gmdate("Y-m-d\TH:i:s\Z", $MessageTime) . "\n";
            $array_output_for_database ['datetime'] = gmdate('Y-m-d H:i:s', $MessageTime);
            $IdNumber = substr($command, $this->array_pointer, $this->array_pointer + 2 - $this->array_pointer);
            $this->array_pointer += 2;
            if ($IdNumber == "00") {
                $output_text = $output_text . "0x" . $IdNumber . " - ID сообщения (репортинг)\n";
            } else {
                $output_text = $output_text . "0x" . $IdNumber . " - ID сообщения\n";
            }

            $MACaddress = substr($command, $this->array_pointer, $this->array_pointer + 16 - $this->array_pointer);
            $this->array_pointer += 16;
            $MACaddress = $this->swap64($MACaddress); # MAC адрес перевернуть байты
            if (substr($MACaddress, 0, 8 - 0) == '70b3d5de') {
                $output_text = $output_text . $MACaddress . " - MAC адрес термостата\n";
            } else {
                $tag1 = $output_text;
                $output_text = $output_text . $MACaddress . " - MAC адрес термостата\n";
                $tag2 = $output_text;
//                $output_text . tag_add("error", $tag1, $tag2);
//                $output_text . tag_config("error", $foreground = "red");
            }
            $array_output_for_database['mac'] = $MACaddress;
//            MAC адрес
            $MessageDirect = substr($command, $this->array_pointer, $this->array_pointer + 2 - $this->array_pointer);
            $this->array_pointer += 2;
            if ($MessageDirect == "00") {
                $output_text = $output_text . $MessageDirect . " - Message direct: запрос от телефона термостату\n";
            } elseif ($MessageDirect == "FF" || $MessageDirect == "ff") {
                $output_text = $output_text . $MessageDirect . " - Message direct: ответ от термостата\n";
            } else {
                $output_text = $output_text . $MessageDirect . " - Message direct\n";
            }
            $SourceEndpoint = substr($command, $this->array_pointer, $this->array_pointer + 4 - $this->array_pointer);
            $this->array_pointer += 4;
            $output_text = $output_text . $SourceEndpoint . " - " . substr($SourceEndpoint, 1, 2 - 1) . " конечная точка отправителя\n";
            $DestinationEndpoint = substr($command, $this->array_pointer, $this->array_pointer + 4 - $this->array_pointer);
            $this->array_pointer += 4;
            $output_text = $output_text . $DestinationEndpoint . " - " . substr($DestinationEndpoint, 1, 2 - 1) . " конечная точка получателя\n";
            $ClusterID = substr($command, $this->array_pointer, $this->array_pointer + 8 - $this->array_pointer);
            $this->array_pointer += 8;
            $cluster = chr($this->int((substr($ClusterID, 0, 1) . substr($ClusterID, 1, 1)), 16)) . chr($this->int((substr($ClusterID, 2, 1) . substr($ClusterID, 3, 1)), 16)) . chr($this->int((substr($ClusterID, 4, 1) . substr($ClusterID, 5, 1)), 16)) . chr($this->int((substr($ClusterID, 6, 1) . substr($ClusterID, 7, 1)), 16));
            $output_text = $output_text . $ClusterID . " - " . $cluster . " кластер " . ZigBee_clusters::factory()->ZigBee_clusters($cluster) . "\n";
            $ProfileID = substr($command, $this->array_pointer, $this->array_pointer + 8 - $this->array_pointer);
            $this->array_pointer += 8;
            $output_text = $output_text . $ProfileID . " - " . $this->substrbystep($ProfileID, 1, 2) . " профайл " . ZigBee_Profiles::factory()->ZigBee_profile($this->substrbystep($ProfileID, 1, 2)) . "\n";
            # разбираем заголовок ZigBee сообщения
            $HeaderRes = $zigbeeparserObject->ZigBee_header_parsing($output_text, substr($command, $this->array_pointer), $cluster);
            # $HeaderRes[0] - Manufacturer_Specific, $HeaderRes[1] - Direction, $HeaderRes[2] - Frame type, $HeaderRes[3] - ZCL Command
            $CommLength -= 22; # отнимаем длину заголовка сообщения концентратора
            if ($HeaderRes[0] == False) { # Manufacturer spesific
                $CommLength -= 3; # отнимаем длину заголовка ZCL сообщения
                $this->array_pointer += 6;
            } else {
                $CommLength -= 5; # отнимаем длину заголовка ZCL сообщения
                $this->array_pointer += 10;
            }

            $ZCLpayload = substr($command, $this->array_pointer, $this->array_pointer + 2 * $CommLength - $this->array_pointer);

            # Попытаемся разобрать тело команды
            if ($HeaderRes[0] == True) { # Manufacturer spesific
                # Не разбираем никакие команды, специфичные от производителя. Просто выведем тело команды на экран
                $output_text = $output_text . "ZCL payload: " . $ZCLpayload . "\n";
                $toDataBaseZCLpayload = $ZCLpayload;
            } else {
                if ($HeaderRes[2] == "00") { # Profile-wide $command
                    $output_text = $zigbeeparserObject->Parsing_profile_wide_command($output_text, $HeaderRes[3], $ZCLpayload, $cluster);

                } elseif ($HeaderRes[2] == "01") { # Cluster-specific $command
                    $output_text = $zigbeeparserObject->Parsing_cluster_specific_command($output_text, $HeaderRes[3], $ZCLpayload, $cluster, $HeaderRes[1]);
                } else {
                    # Хрен его знает что это, явная ошибка. Просто выведем тело команды на экран
                    $output_text = $output_text . "ZCL payload: " . $ZCLpayload . "\n";
                }
                $toDataBaseZCLpayload = $output_text;
            }

            if ($zigbeeparserObject->temperature_change_report == True) {
//                print('123');
                $tag2 = $output_text;
//                            $output_text . tag_add("color_mark", $tag1, $tag2);
//                            $output_text . tag_config("color_mark", background = "OliveDrab1");
            }

            $output_text = $output_text . "\n"; # Перевод строки

            $this->array_pointer += 2 * $CommLength;
            $test2 = 2;
            $array_output_for_database['condition_plunger'] = $zigbeeparserObject->get_condition_plunger();
            $array_output_for_database['installed_temperature'] = $zigbeeparserObject->get_installed_temperature();
            $array_output_for_database['current_temperature'] = $zigbeeparserObject->get_current_temperature();
            $array_output_for_database['status_battery'] = $zigbeeparserObject->getStatusBattery();
            $array_output_for_database['some_temperature'] =$zigbeeparserObject->getSomeTemperature();
            $array_output_for_database['status_change_temperature'] =$zigbeeparserObject->getStatusChangeTemperature();
            $array_output_for_database['negative_temperature'] = $zigbeeparserObject->getNegativeTemperature();
            $array_output_for_database['local_time'] = $zigbeeparserObject->getLocalTime();
            $array_output_for_database['UTC_time'] = $zigbeeparserObject->getUTCTime();
            $array_output_for_database['begin_summer_DstStart'] =$zigbeeparserObject->getBeginSummerDstStart();
            $array_output_for_database['finish_summer_DstStart'] = $zigbeeparserObject->getFinishSummerDstStart();
            $array_output_for_database['begin_summer_DstStart'] =$zigbeeparserObject->getBeginSummerDstStart();
            $array_output_for_database['finish_summer_DstStart'] = $zigbeeparserObject->getFinishSummerDstStart();
            $list_array_output_for_database[] = $array_output_for_database;
            $test = 1;
            if (strlen($command) <= ($this->array_pointer + 1)) {
                break;
            }
        }
        return $list_array_output_for_database;
    }

# Переверовачиваем байты в MAC адресе
    public
    function swap64($eui64)
    {
        $ret = "";
        $array_swap64 = [14, 12, 10, 8, 6, 4, 2, 0];
        foreach ($array_swap64 as $one_swap64) {
            $ret .= substr($eui64, $one_swap64, 2);
        }
        /*for ($i = 0; $i < count($array_swap64); $i++) {
            $ret += $eui64[$i];
            $ret += $eui64[$i + 1];
            $ret . upper();

        }*/
        return $ret;
    }

    public
    static function int($number, $system)
    {
        switch ($system) {
            case 16: {
                return base_convert($number, $system, 10);
                break;
            }

        }
    }

    public
    function substrbystep($string, $begin, $step)
    {
        $length = strlen($string);
        $i = 0;
        $current_length = $begin + $step * $i;
        $result = "";
        while ($current_length <= $length) {
            $result .= substr($string, $begin + $step * $i, 1);
            $i++;
            $current_length = $begin + $step * $i + 1;
        }
        return $result;

    }


}