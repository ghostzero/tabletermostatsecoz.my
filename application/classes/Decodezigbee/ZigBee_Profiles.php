<?php

/**
 * Created by PhpStorm.
 * User: ghost
 * Date: 28.10.16
 * Time: 10:09
 */
class ZigBee_Profiles
{
//     Проверко профайла на совпадение с объявленными public профайлами в ZigBee

    public function __construct()
    {
    }

    public static function factory()
    {
        return new ZigBee_Profiles ();
    }

    public function ZigBee_profile($profile)
    {
        if ($profile == "0101") {
            return "Industrial Plant Monitoring (IPM)";
        } elseif ($profile == "0104") {    //use
            return "Home Automation (HA)";
        } elseif ($profile == "0105") {
            return "Commercial Building Automation (CBA)";
        } elseif ($profile == "0107") {
            return "	Telecom Applications (TA)";
        } elseif ($profile == "0108") {
            return "Personal Home & Hospital Care (PHHC)";
        } elseif ($profile == "0109") {
            return "	Advanced Metering Initiative (AMI)";
        } else {
            return "Unknown";
        }
    }

}