<?php
/**
 * Created by PhpSt||m.
 * User: ghost
 * Date: 28.10.16
 * Time: 10:33
 */
# Проверка кластера на совпадение с объявленными в ZCL и HA
class ZigBee_clusters
{

    public function __construct()
    {
    }

    public static function factory()
    {
        return new ZigBee_clusters ();
    }
    public
    function ZigBee_clusters($cluster)
    {
        # General clusters
        if ($cluster == "0000") {
            return "Basic";   //
        } elseif ($cluster == "0001") {
            return "Power Configuration";//
        } elseif ($cluster == "0002") {
            return "Device Temperature Configuration";
        } elseif ($cluster == "0003") {
            return "Identify";             //
        } elseif ($cluster == "0004") {
            return "Groups";
        } elseif ($cluster == "0005") {
            return "Scenes";
        } elseif ($cluster == "0006") {
            return "On/Off";
        } elseif ($cluster == "0007") {
            return "On/Off Switch Configuration";
        } elseif ($cluster == "0008") {
            return "Level Control ";
        } elseif ($cluster == "0009") {
            return "Alarms";
        } elseif ($cluster == "000a" || $cluster == "000A") {
            return "Time";                   //
        } elseif ($cluster == "000f" || $cluster == "000F") {
            return "Binary Input (Basic) ";
        } elseif ($cluster == "0016") {
            return "Partition";
        } elseif ($cluster == "001a" || $cluster == "001A") {
            return "Power Profile ";
        } elseif ($cluster == "001b" || $cluster == "001B") {
            return "EN50523Appliance Control";
        } elseif ($cluster == "0020") {
            return "Poll Control";             //
            # Closures clusters
        } elseif ($cluster == "0100") {
            return "Shade Configuration";
        } elseif ($cluster == "0101") {
            return "Do|| Lock";
        } elseif ($cluster == "0102") {
            return "Window Covering";
            # HVAC clusters
        } elseif ($cluster == "0200") {
            return "Pump Configuration and Control";
        } elseif ($cluster == "0201") {
            return "Thermostat";                               //
        } elseif ($cluster == "0202") {
            return "Fan Control";
        } elseif ($cluster == "0204") {
            return "Thermostat User Interface Configuration";//
            # Lighting cluster
        } elseif ($cluster == "0300") {
            return "Col|| Control";
            # Measurement & Sensing clusters
        } elseif ($cluster == "0400") {
            return "Illuminance Measurement";
        } elseif ($cluster == "0401") {
            return "Illuminance Level Sensing";
        } elseif ($cluster == "0402") {
            return "Temperature Measurement";
        } elseif ($cluster == "0403") {
            return "Pressure Measurement";
        } elseif ($cluster == "0404") {
            return "Flow Measurement";
        } elseif ($cluster == "0405") {
            return "Relative Humidity Measurement";
        } elseif ($cluster == "0406") {
            return "Occupancy Sensing";
            # Security and Safety clusters
        } elseif ($cluster == "0500") {
            return "IAS Zone";
        } elseif ($cluster == "0501") {
            return "IAS ACE";
        } elseif ($cluster == "0502") {
            return "IAS WD";
            # Smart Energy cluster
        } elseif ($cluster == "0702") {
            return "Metering";
            # Home Automation clusters
        } elseif ($cluster == "0b00") {
            return "EN50523 Appliance Identification";
        } elseif ($cluster == "0b01" || $cluster == "0B01") {
            return "Meter Identification";
        } elseif ($cluster == "0b02" || $cluster == "0B02") {
            return "EN50523 Appliance events and Alert";
        } elseif ($cluster == "0b03" || $cluster == "0B03") {
            return "Appliance statistics";
        } elseif ($cluster == "0b04" || $cluster == "0B04") {
            return "Electricity Measurement";
        } elseif ($cluster == "0b05" || $cluster == "0B05") {
            return "Diagnostics";
        } else {
            return "Unknown";
        }
    }
}





