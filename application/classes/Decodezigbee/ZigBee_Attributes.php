<?php

/**
 * Created by PhpStorm.
 * User: ghost
 * Date: 28.10.16
 * Time: 12:34
 */
class    ZigBee_Attributes
{
    public function __construct()
    {
    }

    public static function factory()
    {
        return new ZigBee_Attributes ();
    }


    public function parse_attribute_name($output_text, $cluster, $attribute)
    {
        if ($cluster == "0000") { # Basic $cluster
            if ($attribute == "0000") {
                $output_text = $output_text . "ZCLVersion";
            } elseif ($attribute == "0001") {
                $output_text = $output_text . "ApplicationVersion";
            } elseif ($attribute == "0002") {
                $output_text = $output_text . "StackVersion";
            } elseif ($attribute == "0003") {
                $output_text = $output_text . "HWVersion";
            } elseif ($attribute == "0004") {
                $output_text = $output_text . "ManufacturerName";
            } elseif ($attribute == "0005") {
                $output_text = $output_text . "ModelIdentifier";
            } elseif ($attribute == "0006") {
                $output_text = $output_text . "DateCode";
            } elseif ($attribute == "0007") {
                $output_text = $output_text . "PowerSource";
            } elseif ($attribute == "0010") {
                $output_text = $output_text . "LocationDescription";
            } elseif ($attribute == "0011") {
                $output_text = $output_text . "PhysicalEnvironment";
            } elseif ($attribute == "0012") {
                $output_text = $output_text . "DeviceEnabled";
            } elseif ($attribute == "0013") {
                $output_text = $output_text . "AlarmMask";
            } elseif ($attribute == "0014") {
                $output_text = $output_text . "DisableLocalConfig";
            } else {
                $output_text = $output_text . "Unknown";
            }
        } elseif ($cluster == "0001") { # PowerConfiguration $cluster
            if ($attribute == "0020") {
                $output_text = $output_text . "BatteryVoltage";
            } elseif ($attribute == "0030") {
                $output_text = $output_text . "BatteryManufacturer";
            } elseif ($attribute == "0031") {
                $output_text = $output_text . "BatterySize";
            } elseif ($attribute == "0032") {
                $output_text = $output_text . "BatteryAHrRating";
            } elseif ($attribute == "0033") {
                $output_text = $output_text . "BatteryQuantity";
            } elseif ($attribute == "0034") {
                $output_text = $output_text . "BatteryRatedVoltage";
            } elseif ($attribute == "0035") {
                $output_text = $output_text . "BatteryAlarmMask";
            } elseif ($attribute == "0036") {
                $output_text = $output_text . "BatteryVoltageMinThreshold";
            } else {
                $output_text = $output_text . "Unknown";
            }
        } elseif ($cluster == "0003") { # Identify $cluster
            if ($attribute == "0000") {
                $output_text = $output_text . "IdentifyTime";
            } else {
                $output_text = $output_text . "Unknown";
            }
        } elseif ($cluster == "000a" or $cluster == "000A") { # Time $cluster
            if ($attribute == "0000") {
                $output_text = $output_text . "Time";
            } elseif ($attribute == "0001") {
                $output_text = $output_text . "TimeStatus";
            } elseif ($attribute == "0002") {
                $output_text = $output_text . "TimeZone";
            } elseif ($attribute == "0003") {
                $output_text = $output_text . "DstStart";
            } elseif ($attribute == "0004") {
                $output_text = $output_text . "DstEnd";
            } elseif ($attribute == "0005") {
                $output_text = $output_text . "DstShift";
            } elseif ($attribute == "0006") {
                $output_text = $output_text . "StandardTime";
            } elseif ($attribute == "0007") {
                $output_text = $output_text . "LocalTime";
            } elseif ($attribute == "0008") {
                $output_text = $output_text . "LastSetTime";
            } elseif ($attribute == "0009") {
                $output_text = $output_text . "ValidUntilTime";
            } else {
                $output_text = $output_text . "Unknown";
            }
        } elseif ($cluster == "0020") { # PollControl $cluster
            if ($attribute == "0000") {
                $output_text = $output_text . "CheckinInterval";
            } elseif ($attribute == "0001") {
                $output_text = $output_text . "LongPollInterval";
            } elseif ($attribute == "0002") {
                $output_text = $output_text . "ShortPollInterval";
            } elseif ($attribute == "0003") {
                $output_text = $output_text . "FastPollTimeout";
            } elseif ($attribute == "0004") {
                $output_text = $output_text . "CheckinIntervalMin";
            } elseif ($attribute == "0005") {
                $output_text = $output_text . "LongPollIntervalMin";
            } elseif ($attribute == "0006") {
                $output_text = $output_text . "FastPollTimeoutMax";
            } else {
                $output_text = $output_text . "Unknown";
            }
        } elseif ($cluster == "0201") { # Thermostat $cluster
            if ($attribute == "0000") {
                $output_text = $output_text . "LocalTemperature";
            } elseif ($attribute == "0001") {
                $output_text = $output_text . "OutdoorTemperature";
            } elseif ($attribute == "0002") {
                $output_text = $output_text . "Ocupancy";
            } elseif ($attribute == "0003") {
                $output_text = $output_text . "AbsMinHeatSetpointLimit";
            } elseif ($attribute == "0004") {
                $output_text = $output_text . "AbsMaxHeatSetpointLimit";
            } elseif ($attribute == "0005") {
                $output_text = $output_text . "AbsMinCoolSetpointLimit";
            } elseif ($attribute == "0006") {
                $output_text = $output_text . "AbsMaxCoolSetpointLimit";
            } elseif ($attribute == "0007") {
                $output_text = $output_text . "PICoolingDemand";
            } elseif ($attribute == "0008") {
                $output_text = $output_text . "PIHeatingDemand";
            } elseif ($attribute == "0009") {
                $output_text = $output_text . "HVACSystemTypeConfiguration";
            } elseif ($attribute == "0010") {
                $output_text = $output_text . "LocalTemperatureCalibration";
            } elseif ($attribute == "0011") {
                $output_text = $output_text . "OccupiedCoolingSetpoint";
            } elseif ($attribute == "0012") {
                $output_text = $output_text . "OccupiedHeatingSetpoint";
            } elseif ($attribute == "0013") {
                $output_text = $output_text . "UnoccupiedCoolingSetpoint";
            } elseif ($attribute == "0014") {
                $output_text = $output_text . "UnoccupiedHeatingSetpoint";
            } elseif ($attribute == "0015") {
                $output_text = $output_text . "MinHeatSetpointLimit";
            } elseif ($attribute == "0016") {
                $output_text = $output_text . "MaxHeatSetpointLimit";
            } elseif ($attribute == "0017") {
                $output_text = $output_text . "MinCoolSetpointLimit";
            } elseif ($attribute == "0018") {
                $output_text = $output_text . "MaxCoolSetpointLimit";
            } elseif ($attribute == "0019") {
                $output_text = $output_text . "MinSetpointDeadBand";
            } elseif ($attribute == "001a" || $attribute == "001A") {
                $output_text = $output_text . "RemoteSensing";
            } elseif ($attribute == "001b" || $attribute == "001B") {
                $output_text = $output_text . "ControlSequenceOfOperation";
            } elseif ($attribute == "001c" || $attribute == "001C") {
                $output_text = $output_text . "SystemMode";
            } elseif ($attribute == "001d" || $attribute == "001D") {
                $output_text = $output_text . "AlarmMask";
            } elseif ($attribute == "001e" || $attribute == "001E") {
                $output_text = $output_text . "ThermostatRunningMode";
            } elseif ($attribute == "0020") {
                $output_text = $output_text . "StartOfWeek";
            } elseif ($attribute == "0021") {
                $output_text = $output_text . "NumberOfWeeklyTransitions";
            } elseif ($attribute == "0022") {
                $output_text = $output_text . "NumberOfDailyTransitions";
            } elseif ($attribute == "0023") {
                $output_text = $output_text . "TemperatureSetpointHold";
            } elseif ($attribute == "0024") {
                $output_text = $output_text . "TemperatureSetpointHoldDuration";
            } elseif ($attribute == "0025") {
                $output_text = $output_text . "ThermostatProgrammingOperationMode";
            } elseif ($attribute == "0029") {
                $output_text = $output_text . "ThermostatRunningState";
            } elseif ($attribute == "0030") {
                $output_text = $output_text . "SetpointChangeSource";
            } elseif ($attribute == "0031") {
                $output_text = $output_text . "SetpointChangeAmount";
            } elseif ($attribute == "0032") {
                $output_text = $output_text . "SetpointChangeSourceTimestamp";
            } else {
                $output_text = $output_text . "Unknown";
            }
        } elseif ($cluster == "0204") { # Thermostat User Interface Configuration $cluster
            if ($attribute == "0000") {
                $output_text = $output_text . "TemperatureDisplayMode";
            } elseif ($attribute == "0001") {
                $output_text = $output_text . "KeypadLockout";
            } elseif ($attribute == "0002") {
                $output_text = $output_text . "ScheduleProgrammingVisibility";
            } else {
                $output_text = $output_text . "Unknown";
            }
        } elseif ($cluster == "0402") { # Temperature Measurement $cluster
            if ($attribute == "0000") {
                $output_text = $output_text . "MeasuredValue";
            } elseif ($attribute == "0001") {
                $output_text = $output_text . "MinMeasuredValue";
            } elseif ($attribute == "0002") {
                $output_text = $output_text . "MaxMeasuredValue";
            } elseif ($attribute == "0003") {
                $output_text = $output_text . "Tolerance";
            } else {
                $output_text = $output_text . "Unknown";
            }
        } else {
            $output_text = $output_text . "Unknown";
        }
    }
}