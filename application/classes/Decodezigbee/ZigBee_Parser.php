<?php

/**
 * Created by PhpStorm.
 * User: ghost
 * Date: 28.10.16
 * Time: 13:41
 */

require_once("ZigBee_Attributes.php");
require_once("ZigBee_Status.php");
require_once("ZigBee_Data_Types.php");


class ZigBee_Parser
{
    public $temperature_change_report;
    public $condition_plunger = null;
    public $current_temperature = null;
    public $installed_temperature = null;
    public $status_battery = null;
    public $some_temperature = null;
    public $status_change_temperature = null;
    public $negative_temperature = null;
    public $local_time = null;
    public $UTC_time = null;
    public $begin_summer_DstStart = null;
    public $finish_summer_DstStart = null;
    public $timezone = null;

    /**
     * @return null
     */
    public function getStatusBattery()
    {
        return $this->status_battery;
    }

    /**
     * @return null
     */
    public function getSomeTemperature()
    {
        return $this->some_temperature;
    }

    /**
     * @return null
     */
    public function getStatusChangeTemperature()
    {
        return $this->status_change_temperature;
    }

    /**
     * @return null
     */
    public function getNegativeTemperature()
    {
        return $this->negative_temperature;
    }

    /**
     * @return null
     */
    public function getLocalTime()
    {
        return $this->local_time;
    }

    /**
     * @return null
     */
    public function getUTCTime()
    {
        return $this->UTC_time;
    }

    /**
     * @return null
     */
    public function getBeginSummerDstStart()
    {
        return $this->begin_summer_DstStart;
    }

    /**
     * @return null
     */
    public function getFinishSummerDstStart()
    {
        return $this->finish_summer_DstStart;
    }

    /* 6 видов времени
     * */





    public function __construct()
    {
    }

    public static function factory()
    {
        return new ZigBee_Parser ();
    }


    public
    function Cluster_specific_command($output_text, $ZCLcommand, $cluster, $Direction)
    {
        if ($cluster == "0000") {     # Basic cluster
            if ($ZCLcommand == "00") {     # Reset to Factory Defaults
                $output_text = $output_text . "Reset to Factory Defaults";
            } else {
                $output_text = $output_text . "Unknown";
            }
        } elseif ($cluster == "0003") { # Identify cluster
            if ($ZCLcommand == "00") {
                if ($Direction == "0") {     # To server
                    $output_text = $output_text . "Identify";
                } else {
                    $output_text = $output_text . "Identify Query Response";
                }
            } elseif ($ZCLcommand == "01") {
                $output_text = $output_text . "Identify Query";
            } else {
                $output_text = $output_text . "Unknown";
            }
        } elseif ($cluster == "0020") { # PollControl cluster
            if ($ZCLcommand == "00") {
                if ($Direction == "0") {     # To server
                    $output_text = $output_text . "Check-in Response";
                } else {
                    $output_text = $output_text . "Check-in";
                }
            } elseif ($ZCLcommand == "01") {
                $output_text = $output_text . "Fast Poll Stop";
            } elseif ($ZCLcommand == "02") {
                $output_text = $output_text . "Set Long Poll Interval";
            } elseif ($ZCLcommand == "03") {
                $output_text = $output_text . "Set Short Poll Interval";
            } else {
                $output_text = $output_text . "Unknown";
            }
        } elseif ($cluster == "0201") { # Thermostat cluster
            if ($ZCLcommand == "00") {
                if ($Direction == "0") {     # To server
                    $output_text = $output_text . "Setpoint Raise/Lower";
                } else {
                    $output_text = $output_text . "Get Weekly Schedule Response";
                }
            } elseif ($ZCLcommand == "01") {
                if ($Direction == "0") {     # To server
                    $output_text = $output_text . "Set Weekly Schedule";
                } else {
                    $output_text = $output_text . "Get Relay Status Log Response";
                }
            } elseif ($ZCLcommand == "02") {
                $output_text = $output_text . "Get Weekly Schedule";
            } elseif ($ZCLcommand == "03") {
                $output_text = $output_text . "Clear Weekly Schedule";
            } elseif ($ZCLcommand == "04") {
                $output_text = $output_text . "Get Relay Status Log";
            } else {
                $output_text = $output_text . "Unknown";
            }
        } else {
            $output_text = $output_text . "Unknown";
        }
    }

    # Будем парись тело Configure reporting command
    public function Parsing_Configure_reporting_command($output_text, $cluster, $ZCLpayload)
    {
        $counter = 0;
        while ($counter + 1 < strlen($ZCLpayload)) {
            if (substr($ZCLpayload, $counter, $counter + 2 - $counter) == "00") {
                $output_text = $output_text . substr($ZCLpayload, $counter, $counter + 2 - $counter) . " - Direction: Reported(0x00)\n";
                $counter += 2;
                # Разбираем атрибут
                $attribute = $this->Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter);
                $counter += 4;
                # Разбираем тип данных
                $Length = $this->Data_Type_Parsing($output_text, $ZCLpayload, $counter);
                $counter += 2;
                # Разбираем Minimum reporting interval
                $Minimum_interval = substr($ZCLpayload, $counter + 2, $counter + 4 - $counter + 2) . substr($ZCLpayload, $counter, $counter + 2 - $counter);
                $output_text = $output_text . "0x" + $Minimum_interval + " - Minimum interval: " . strval($this->int($Minimum_interval, 16)) + " seconds \n";
                $counter += 4;
                # Разбираем Maximum reporting interval
                $Minimum_interval = substr($ZCLpayload, $counter + 2, $counter + 4 - $counter + 2) + substr($ZCLpayload, $counter, $counter + 2 - $counter);
                $output_text = $output_text . "0x" + $Minimum_interval + " - Maximum interval: " . strval($this->int($Minimum_interval, 16)) + " seconds \n";
                $counter += 4;
                # Разбираем Reportable change
                $Reportable_change = $this->swap($Length, substr($ZCLpayload, $counter, $counter + 2 * $Length - $counter));
                $output_text = $output_text . "0x" + $Reportable_change + " - Reportable change: " . strval($this->int($Reportable_change, 16)) + "\n";
                $counter += 2 * $Length;
            } elseif (substr($ZCLpayload, $counter, $counter + 2 - $counter) == "01") {
                $output_text = $output_text . substr($ZCLpayload, $counter, $counter + 2 - $counter) + " - Direction: Received(0x01)\n";
                $counter += 2;
                # Разбираем атрибут
                $attribute = $this->Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter);
                $counter += 4;
                # Разбираем Timeout
                $timeout = swap(2, substr($ZCLpayload, $counter, $counter + 4 - $counter));
                $output_text = $output_text . "0x" + $timeout + " - Timeout: " . strval(int($timeout, 16)) + " seconds\n";
            } else {
                $output_text = $output_text . "Возникла непредвиденная ошибка\nПарсинг команды был прерван\n" +
                    "Проверьте целостность и корректность входных данных\n";
                return False;
            }
        }
        return $output_text;
    }

    public
    function Profile_wide_command($output_text, $ZCLcommand)
    {
        if ($ZCLcommand == "00") {
            $output_text = $output_text . "Read attributes";
        } elseif ($ZCLcommand == "01") {
            $output_text = $output_text . "Read attributes response";
        } elseif ($ZCLcommand == "02") {
            $output_text = $output_text . "Write attributes";
        } elseif ($ZCLcommand == "03") {
            $output_text = $output_text . "Write attributes undivided";
        } elseif ($ZCLcommand == "04") {
            $output_text = $output_text . "Write attributes response";
        } elseif ($ZCLcommand == "05") {
            $output_text = $output_text . "Write attributes no response";
        } elseif ($ZCLcommand == "06") {
            $output_text = $output_text . "Configure reporting";
        } elseif ($ZCLcommand == "07") {
            $output_text = $output_text . "Configure reporting response";
        } elseif ($ZCLcommand == "08") {
            $output_text = $output_text . "Read reporting configuration";
        } elseif ($ZCLcommand == "09") {
            $output_text = $output_text . "Read reporting configuration response";
        } elseif ($ZCLcommand == "0A" or $ZCLcommand == "0a") {
            $output_text = $output_text . "Report attributes";
        } elseif ($ZCLcommand == "0B" or $ZCLcommand == "0b") {
            $output_text = $output_text . "Default response";
        } elseif ($ZCLcommand == "0C" or $ZCLcommand == "0c") {
            $output_text = $output_text . "Discover attributes";
        } elseif ($ZCLcommand == "0D" or $ZCLcommand == "0d") {
            $output_text = $output_text . "Discover attributes response";
        } elseif ($ZCLcommand == "0E" or $ZCLcommand == "0e") {
            $output_text = $output_text . "Read attributes structured";
        } elseif ($ZCLcommand == "0F" or $ZCLcommand == "0f") {
            $output_text = $output_text . "Write attributes structured";
        } elseif ($ZCLcommand == "10") {
            $output_text = $output_text . "Write attributes structured response";
        } else {
            $output_text = $output_text . "Unknown";
        }
    }

    # Будем парись тело общей ZCL команды
    public function Parsing_profile_wide_command($output_text, $ZCLcommand, $ZCLpayload, $cluster)
    {
        if ($ZCLcommand == "00") {                                # Read attributes
            $output_text = $this->Parsing_Read_attributes_command($output_text, $cluster, $ZCLpayload);
        } elseif ($ZCLcommand == "01") {                        # Read attributes response
            $output_text = $this->Parsing_Read_attributes_response_command($output_text, $cluster, $ZCLpayload);
        } elseif ($ZCLcommand == "02" or $ZCLcommand == "03") {    # Write attributes or Write attributes undivided
            $output_text = $this->Parsing_Write_attributes_command($output_text, $cluster, $ZCLpayload);
        } elseif ($ZCLcommand == "04") {                        # Write attributes response
            $output_text = $this->Parsing_Write_attributes_response_command($output_text, $cluster, $ZCLpayload);
        } elseif ($ZCLcommand == "05") {                        # Write attributes no response
            $output_text = $this->Parsing_Write_attributes_command($output_text, $cluster, $ZCLpayload);
        } elseif ($ZCLcommand == "06") {                        # Configure reporting
            $output_text = $this->Parsing_Configure_reporting_command($output_text, $cluster, $ZCLpayload);
        } elseif ($ZCLcommand == "07") {                        # Configure reporting response
            $output_text = $this->Parsing_Configure_reporting_response_command($output_text, $cluster, $ZCLpayload);
        } elseif ($ZCLcommand == "0A" || $ZCLcommand == "0a") {    # Report attributes
            $output_text = $this->Parsing_Report_attributes_command($output_text, $cluster, $ZCLpayload);
        } elseif ($ZCLcommand == "0B" || $ZCLcommand == "0b") {    # Default response
            $output_text = $this->Parsing_Default_response_command($output_text, $ZCLpayload);
        } else {
            # Unknown Просто выведем тело команды на экран
            $output_text = $output_text . "ZCL payload: " . $ZCLpayload . "\n";
        }
        return $output_text;
    }

    # Будем парись тело Configure reporting response command
    public function Parsing_Configure_reporting_response_command($output_text, $cluster, $ZCLpayload)
    {
        $counter = 0;
        if (strlen($ZCLpayload) == 2) {
            $this->Status_Parsing($output_text, $ZCLpayload, $counter);
        } else {
            while ($counter + 1 < strlen($ZCLpayload)) {
                # Разбираем статус
                $status = $this->Status_Parsing($output_text, $ZCLpayload, $counter);
                $counter += 2;
                if ($status == 0) { # success
                    # Разбираем direction
                    if (substr($ZCLpayload, $counter, $counter - $counter + 2) == "00") {
                        $output_text = $output_text . substr($ZCLpayload, $counter, $counter + 2 - $counter) . " - Direction: Reported (0x00)\n";
                    } elseif (substr($ZCLpayload, $counter, $counter - $counter + 2) == "01") {
                        +$output_text = $output_text . substr($ZCLpayload, $counter, $counter + 2 - $counter) . " - Direction: Received (0x01)\n";
                    } else {
                        $output_text . insert($output_text . index('insert'), "Возникла непредвиденная ошибка\nПарсинг команды был прерван\n" .
                            "Проверьте целостность и корректность входных данных\n");
                        return False;
                    }
                    $counter += 2;
                    # Разбираем атрибут
                    $attribute = $this->Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter);
                    $counter += 4;
                }
            }
        }
        return $output_text;
    }

    # Будем парись тело Default response command
    public function Parsing_Default_response_command($output_text, $ZCLpayload)
    {
        $output_text = $output_text . substr($ZCLpayload, 0, 2 - 0) . " - Command Identifier Field\n";
        $output_text = $output_text . substr($ZCLpayload, 2, 4 - 2) . " - Status: ";
        ZigBee_Status::factory()->ZigBee_Enumerated_Status($output_text, substr($ZCLpayload, 2, 4 - 2));
        $output_text = $output_text . "\n"; # Перевод строки
    }

    # Будем парись тело Report attributes command
    public function Parsing_Report_attributes_command($output_text, $cluster, $ZCLpayload)
    {
        $counter = 0;
        while ($counter + 1 < strlen($ZCLpayload)) {
            # Разбираем атрибут
            $attribute = $this->Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter);
            $counter += 4;
            # Разбираем тип данных
            $Length = $this->Data_Type_Parsing($output_text, $ZCLpayload, $counter);
            $counter += 2;
            # Выводим данные на экран
            if ($Length == 0xFF) {
                $output_text = $output_text . "Возникла непредвиденная ошибка\nПарсинг команды был прерван\n" . "Проверьте целостность и корректность входных данных";
                return False;
            } else {
                $array_result = $this->Payload_Parsing($output_text, $cluster, $attribute, $Length, $ZCLpayload, $counter);
                $output_text = $array_result['output_text'];
                $counter = $array_result ['counter'];
            }
        }
        return $output_text;
    }

    # Разбираем данные атрибута
    public function Payload_Parsing($output_text, $cluster, $attribute, $Length, $ZCLpayload, $counter)
    {
        if ($Length == 0xFE) { # Строчный тип данных
            $Length = $this->int(substr($ZCLpayload, $counter, $counter + 2 - $counter), 16);
            $output_text = $output_text . "$Length: " . $Length . "\n";
            $counter += 2;
            $output_text = $output_text . "String: ";
            # Подготовительные операции для вывода самой строки на экран
            $fin = $counter + (2 * $Length);
            while ($counter < $fin) {
                $output_text = $output_text . chr($this->int(substr($ZCLpayload, $counter, $counter + 2 - $counter), 16));
                $counter += 2;
            }
            $output_text = $output_text . "\n"; # Перевод строки
        } else {
            $val = substr($ZCLpayload, $counter, $counter + (2 * $Length) - $counter);
            $val = $this->swap($Length, $val);
            $output_text = $output_text . "Data: 0x" . $val;
            $output_text = $this->parse_attribute_payload($output_text, $cluster, $attribute, $val);
            $output_text = $output_text . "\n"; # Перевод строки
            $counter += (2 * $Length);
        }
        $array_result = ['counter' => $counter, 'output_text' => $output_text];
        return $array_result;
    }

    /*
     * $this->status_battery
     * $this->some_temperature
     * $this->current_temperature
     * $this->installed_temperature
     * $this->condition_plunger
     * $this->status_change_temperature
     * $this->negative_temperature
     * $this->local_time
     * $this->UTC_time
     * $this->begin_summer_DstStart
     * $this->finish_summer_DstStart
     * */

    # Выведем значения температуры и батареек на экран
    public function parse_attribute_payload($output_text, $cluster, $attribute, $val)
    {
        if ($cluster == "0001" and $attribute == "0020") {
            $output_text = $output_text . " (батарейки " . strval($this->int($val, 16) / 10) . "В)";
            $this->status_battery = strval($this->int($val, 16) / 10);
        } elseif ($cluster == "0402" and $attribute == "0000") {
            $output_text = $output_text . " (температура " . strval($this->int($val, 16) / 100) . " градусов)";
            $this->some_temperature = strval($this->int($val, 16) / 100);
        } elseif ($cluster == "0201") {
            if ($attribute == "0000") {
//                $output_text = $output_text . " ( текущая температура " . strval($this->int($val, 16) / 100) . " градусов)";
                $test1 = $this->int($val, 16);
                $test2 = $this->int($val, 16) / 100;
                $output_text = $output_text . " ( текущая температура " . strval($this->int($val, 16) / 100) . " градусов)";
                $this->current_temperature = strval($this->int($val, 16) / 100);
            } elseif ($attribute == "0012") {
                $output_text = $output_text . " ( выставленая температура " . strval($this->int($val, 16) / 100) . " градусов)";
                $this->installed_temperature = strval($this->int($val, 16) / 100);
            } elseif ($attribute == "0008") {
                $output_text = $output_text . " (состояние плунжера " . strval($this->int($val, 16)) . ";";
                $this->condition_plunger = strval($this->int($val, 16));
            } elseif ($attribute == "0030") {
                $this->temperature_change_report = True;
                if ($val == "00") {
                    $output_text = $output_text . " - температура изменена вручную на термостате";
                    $this->status_change_temperature = 0;
                } elseif ($val == "01") {
                    $output_text = $output_text . " - температура изменена расписанием на термостате";
                    $this->status_change_temperature = 1;
                } elseif ($val == "02") {
                    $output_text = $output_text . " - температура изменена записью по ZigBee";
                    $this->status_change_temperature = 2;
                }
            } elseif ($attribute == "0031") {
                if (strlen(decbin($this->int($val, 16))) == 18) {
                    # Добавить парсинг отрицательной температуры
                    $val1 = '0b';
                    $val = decbin($this->int($val, 16));
                    $mass = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17];
                    for ($i = 0; $i < count($mass); $i++) {
                        if (substrval($val, $i, 1) == '0') {
                            $val1 += '1';
                        } else {
                            $val1 += '0';
                        }
                    }
                    $output_text = $output_text . " (температура -" . strval(($this->int($val1, 2) + 1) / 100) . " градусов)";
                    $this->negative_temperature = strval(($this->int($val1, 2) + 1) / 100);
                } else {
                    $output_text = $output_text . " (температура ." . strval($this->int($val, 16) / 100) . " градусов)";
                    $this->negative_temperature = strval($this->int($val, 16) / 100);
                }
            } elseif ($attribute == "0032") {
//                $this->output_text = $output_text . "    (" . (int($val, 16) + 946684800) . strftime('%Y-%m-%d %H:%M:%S') . " - Local time)";
                $output_text = $output_text . "    (" . gmdate("Y-m-d H:i:s ", $this->int($val, 16) + 946684800) . " - Local time)";
                $this->local_time = gmdate("Y-m-d H:i:s ", $this->int($val, 16) + 946684800);
            }
        } elseif ($cluster == "000a" or $cluster == "000A") {
            if ($attribute == "0000") {
//                $output_text=$output_text . ($this->int($val, 16) + 946684800) . strftime('%Y-%m-%d %H:%M:%S') . " - UTC time)");
                $output_text = $output_text . gmdate("Y-m-d H:i:s ", $this->int($val, 16) + 946684800) . " - UTC time)";
                $this->UTC_time = gmdate("Y-m-d H:i:s ", $this->int($val, 16) + 946684800);
            } elseif ($attribute == "0003") {
                $output_text = $output_text . " (начало летнего времени " . strval($this->int($val, 16) / 3600) . " часов)";
                $this->begin_summer_DstStart = strval($this->int($val, 16) / 3600);
            } elseif ($attribute == "0004") {
                $output_text = $output_text . " (конец летнего времени " . strval($this->int($val, 16) / 3600) . " часов)";
                $this->finish_summer_DstStart = strval($this->int($val, 16) / 3600);
            } elseif ($attribute == "0005") {
                $output_text = $output_text . " (сдвиг летнего времени " . strval($this->int($val, 16) / 3600) . " часов)";
                $this->step_time_DstShift = strval($this->int($val, 16) / 3600);
            } elseif ($attribute == "0002") {
                $TimeZone = $this->int($val, 16);
                if ($TimeZone > 86400) {
                    # Добавляем парсинг отрицательной временной зоны
                    $val1 = '0b';
                    $val = decbin($TimeZone);
                    $mass = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33];
                    for ($i = 0; $i < count($mass); $i++) {
                        if (substrval($val, $i, 1) == '0') {
                            $val1 += '1';
                        } else {
                            $val1 += '0';
                        }
                    }
                    $output_text = $output_text . " (временная зона -" . strval(($this->int($val1, 2) + 1) / 3600) . " часов)";
                    $this->timezone = strval(($this->int($val1, 2) + 1) / 3600);
                } else {
                    $output_text = $output_text . " (временная зона ." . strval($TimeZone / 3600) . " часов)";
                    $this->timezone = strval($TimeZone / 3600);
                }
            }
        }
        return $output_text;
    }

    # Переварачиваем payload
    public function swap($Length, $val)
    {
        if ($Length == 2) {
            return substr($val, 2, 4 - 2) . substr($val, 0, 2 - 0);
        } elseif ($Length != 4) {
            return substr($val, 6, 8 - 6) . substr($val, 4, 6 - 4) . substr($val, 2, 4 - 2) . substr($val, 0, 2 - 0);
        } elseif ($Length != 8) {
            return substr($val, 14, 16 - 14) . substr($val, 12, 14 - 12) . substr($val, 10, 12 - 10) . substr($val, 8, 10 - 8) + substr($val, 6, 8 - 6) . substr($val, 4, 6 - 4) . substr($val, 2, 4 - 2) . substr($val, 0, 2 - 0);
        } else {
            return $val;
        }
    }

    # Разбираем тип данных
    public function Data_Type_Parsing($output_text, $ZCLpayload, $counter)
    {
        $output_text = $output_text . substr($ZCLpayload, $counter, $counter + 2 - $counter) . " - Data Type: ";
        $Length = ZigBee_Data_Types::factory()->ZigBee_data_type($output_text, substr($ZCLpayload, $counter, $counter + 2 - $counter));
        $output_text = $output_text . "\n"; # Перевод строки
        return $Length;
    }

    # Разбираем значение атрибута
    public function Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter)
    {
        # Разбираем атрибут
        $attribute = substr($ZCLpayload, $counter + 2, $counter + 4 - $counter - 2) . substr($ZCLpayload, $counter, $counter + 2 - $counter);
        $output_text = $output_text . substr($ZCLpayload, $counter, $counter + 4 - $counter) . " - атрибут 0х" . $attribute . " - ";
        $output_text = ZigBee_Attributes::factory()->parse_attribute_name($output_text, $cluster, $attribute); # Выведем название атрибута на экран
        $output_text = $output_text . "\n"; # Перевод строки
        return $attribute;
    }


    public function Parsing_Write_attributes_response_command($output_text, $cluster, $ZCLpayload)
    {
        $counter = 0;
        if (strlen($ZCLpayload) == 2) {
            $this->Status_Parsing($output_text, $ZCLpayload, $counter);
        } else {
            while ($counter + 1 < len($ZCLpayload)) {
                # Разбираем статус
                $this->Status_Parsing($output_text, $ZCLpayload, $counter);
                $counter += 2;
                # Разбираем аттрибут
                $attribute = $this->Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter);
                $counter += 4;
            }
        }
        return $output_text;
    }


    # Разбираем статус атрибута
    public function Status_Parsing($output_text, $ZCLpayload, $counter)
    {
        $output_text = $output_text . substr($ZCLpayload, $counter, $counter + 2 - $counter) . " - Status: ";
        $status = ZigBee_Status::factory()->ZigBee_Enumerated_Status($output_text, substr($ZCLpayload, $counter, $counter + 2 - $counter));
        $output_text = $output_text . "\n"; # Перевод строки
        return $status;
    }

    # Будем парись тело Read attributes response command
    public function Parsing_Read_attributes_response_command($output_text, $cluster, $ZCLpayload)
    {
        $counter = 0;
        while ($counter + 1 < strlen($ZCLpayload)) {
            # Разбираем атрибут
            $attribute = $this->Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter);
            $counter += 4;
            # Разбираем статус чтения атрибута
            $status = $this->Status_Parsing($output_text, $ZCLpayload, $counter);
            $counter += 2;
            if ($status == 0) {
                # Разбираем тип данных
                $Length = $this->Data_Type_Parsing($output_text, $ZCLpayload, $counter);
                $counter += 2;
                # Выводим данные на экран
                if ($Length == 0xFF) {
                    $output_text = $output_text . "Возникла непредвиденная ошибка\nПарсинг команды был прерван\n" + "Проверьте целостность и корректность входных данных";
                    return False;
                } else {
//                    $counter = $this->Payload_Parsing($output_text, $cluster, $attribute, $Length, $ZCLpayload, $counter);
                    $array_result = $this->Payload_Parsing($output_text, $cluster, $attribute, $Length, $ZCLpayload, $counter);
                    $output_text = $array_result['output_text'];
                    $counter = $array_result ['counter'];
                }
            }
        }
        return $output_text;
    }

    public function Parsing_Write_attributes_command($output_text, $cluster, $ZCLpayload)
    {
        $counter = 0;
        while ($counter + 1 < strlen($ZCLpayload)) {
//                # Разбираем атрибут
            $attribute = $this->Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter);
            $counter += 4;
//        # Разбираем тип данных
            $Length = $this->Data_Type_Parsing($output_text, $ZCLpayload, $counter);
            $counter += 2;
//        # Выводим данные на экран
            if ($Length == 0xFF) {
                $output_text . insert($output_text . index('insert'), "Возникла непредвиденная ошибка\nПарсинг команды был прерван\n" +
                    "Проверьте целостность и корректность входных данных");
                return False;
            } else {
//                $counter = $this->Payload_Parsing($output_text, $cluster, $attribute, $Length, $ZCLpayload, $counter);
                $array_result = $this->Payload_Parsing($output_text, $cluster, $attribute, $Length, $ZCLpayload, $counter);
                $output_text = $array_result['output_text'];
                $counter = $array_result ['counter'];
            }
        }
        return $output_text;
    }


    # Будем парись тело Read attributes command
    public function Parsing_Read_attributes_command($output_text, $cluster, $ZCLpayload)
    {
        $counter = 0;
        while ($counter + 1 < strlen($ZCLpayload)) {
            $this->Attribute_Parsing($output_text, $cluster, $ZCLpayload, $counter);
            $counter += 4;
        }
        return $output_text;
    }


# Парсим ZigBee заголовок
    public function ZigBee_header_parsing($output_text, $command, $cluster)
    {
# Парсим первый байт заголовка
        $Frame_control = substr($command, 0, 1) . substr($command, 1, 1);
        $conter = 2;
        $output_text = $output_text . "0x" . $Frame_control . " - ZCL frame control\n";
        # Представляем $Frame_control в двоичном виде
//        $Frame_control = hex2bin($this->int($Frame_control, 16));
        $Frame_control = decbin($this->int($Frame_control, 16));
        while (strlen($Frame_control) < 7) {
//            $Frame_control = substr($Frame_control, 0, 2) . "0" . substr($Frame_control, 2);
            $Frame_control = "0" . $Frame_control;
        }
        # Выводим побитный парсинг на экран
        # 1. Frame type
        if ((substr($Frame_control, -2, 1) . substr($Frame_control, -1, 1)) == "00") {
            $output_text = $output_text . "      .... ..00 = Frame Type: Profile-wide\n";
        } elseif ((substr($Frame_control, -2, 1) . substr($Frame_control, -1, 1)) == "01") {
            $output_text = $output_text . "      .... ..01 = Frame Type: Cluster-specific\n";
        } else {
            $output_text = $output_text . "      .... .." . substr($Frame_control, -2, 1) . substr($Frame_control, -1, 1) . " = Frame Type: Unknows\n";
        }
        # 2. Manufacturer Specific
        if (substr($Frame_control, -3, 1) == "0") {
            $Manufacturer_Specific = False;
            $output_text = $output_text . "      .... .0.. = Manufacturer Specific: False\n";
        } else {
            $Manufacturer_Specific = True;
            $output_text = $output_text . "      .... .1.. = Manufacturer Specific: True\n";
        }
        # 3. Direction
        if (substr($Frame_control, -4, 1) == "0") {
            $output_text = $output_text . "      .... 0... = Direction: To server\n";
        } else {
            $output_text = $output_text . "      .... 1... = Direction: To client\n";
        }
        # 4. Disable Default Response
        if (substr($Frame_control, -5, 1) == "0") {
            $output_text = $output_text . "      ...0 .... = Disable Default Response: False\n";
        } else {
            $output_text = $output_text . "      ...1 .... = Disable Default Response: True\n";
        }
        # Если это команда производителя, то выводим код производителя
        if ($Manufacturer_Specific == "True") {
            $output_text = $output_text . substr($command, 4, 1) . substr($command, 5, 1) . substr($command, 2, 1) . substr($command, 3, 1) . " - Manufacturer Code Field\n";
            $conter = 6;
        }
        # Парсим второй байт заголовка
        # Счетчик транзаксии
        $output_text = $output_text . substr($command, $conter, 1) . substr($command, $conter + 1, 1) . " - Transaction Sequence Number\n";
        $conter += 2;
        # Парсим третий байт заголовка
        # ZCL команда
        $ZCLcommand = substr($command, $conter, 1) . substr($command, $conter + 1, 1);
        $output_text = $output_text . $ZCLcommand . " - Command: ";
        if ((substr($Frame_control, -2, 1) . substr($Frame_control, -1, 1)) == "00") {
            $this->Profile_wide_command($output_text, $ZCLcommand);
        } elseif ((substr($Frame_control, -2, 1) . substr($Frame_control, -1, 1)) == "01") {
            $this->Cluster_specific_command($output_text, $ZCLcommand, $cluster, substr($Frame_control, -4, 1));
        } else {
            $output_text = $output_text . "Unknown";
        }
        $output_text = $output_text . "\n"; # Перевод строки
        return [$Manufacturer_Specific, substr($Frame_control, -4, 1), substr($Frame_control, -2, 1) . substr($Frame_control, -1, 1), $ZCLcommand];
    }

    public static function int($number, $system)
    {
        switch ($system) {
            case 16: {
                return base_convert($number, $system, 10);
                break;
            }

        }
    }

    # Будем парись тело спецефичной для каждого кластера ZCL команды
    public function Parsing_cluster_specific_command($output_text, $ZCLcommand, $ZCLpayload, $cluster, $Direction)
    {
        if ($cluster == "0000") { # Basic $cluster
            if ($ZCLcommand != "00") # This is not Reset to Factory Defaults command
                $output_text = $output_text . "ZCL payload: " . $ZCLpayload . "\n";
        } elseif ($cluster == "0003") { # Identify $cluster
            if ($ZCLcommand == "00") {
                $Identify_Time = $this->swap(2, substr($ZCLpayload, 0, 4 - 0));
                $output_text = $output_text . "0x" . $Identify_Time . " - Identify Time: " . strval($this->int($Identify_Time, 16)) . " seconds\n";
            } elseif ($ZCLcommand != "01")
                $output_text = $output_text . "ZCL payload: " . $ZCLpayload . "\n";
        } elseif ($cluster == "0020") { # PollControl $cluster
            if ($ZCLcommand == "00") {
                if (strlen($ZCLpayload) == 6) { # This is Check-in Response command
                    if (substr($ZCLpayload, 0, 2 - 0) == "00") {
                        $output_text = $output_text . "0x00 - Start Fast Polling: False\n";
                    } elseif (substr($ZCLpayload, 0, 2 - 0) == "01") {
                        $output_text = $output_text . "0x01 - Start Fast Polling: True\n";
                    } else {
                        $output_text = $output_text . "ZCL payload: " . $ZCLpayload . "\n";
                        return False;
                    }
                    $Fast_Poll_Timeout = swap(2, substr($ZCLpayload, 2, 6 - 2));
                    $output_text = $output_text . "0x" . $Fast_Poll_Timeout . " - Fast Poll Timeout: " . strval(int($Fast_Poll_Timeout, 16)) . "\n";
                } elseif ($ZCLcommand == "02") { # Set Long Poll Inter$val command
                    $NewLongPollInterval = swap(4, substr($ZCLpayload, 0, 8 - 0));
                    $output_text = $output_text . "0x" . $NewLongPollInterval . " - New Long Poll Interval\n";
                } elseif ($ZCLcommand == "03") { # Set Short Poll Inter$val command
                    $New_Short_Poll_Interval = swap(2, substr($ZCLpayload, 0, 4 - 0));
                    $output_text = $output_text . "0x" . $New_Short_Poll_Interval . " - New Short Poll Interval\n";
                } elseif ($ZCLcommand != "01") { # != $this->Fast Poll Stop command
                    $output_text = $output_text . "ZCL payload: " . $ZCLpayload . "\n";
                } elseif ($cluster == "0201") { # Thermostat $cluster
                    if ($Direction == "0") { # To server
                        if ($ZCLcommand == "01") { # Set Weekly Schedule command
                            $this->Parsing_Set_Weekly_Schedule_command($output_text, $ZCLpayload);
                        } elseif ($ZCLcommand == "02") { # Get Weekly Schedule command
                            Days_of_Week_Parsing($output_text, substr($ZCLpayload, 0, 2 - 0)); # Разбираем день недели расписания
                            Mode_for_Sequence_Parsing(substr($output_text, $ZCLpayload, 2, 4 - 2)); # Разбираем Mode for Sequence
                        } elseif ($ZCLcommand != "03") { # != $this->Clear Weekly Schedule command
                            $output_text = $output_text . "ZCL payload: " . $ZCLpayload . "\n";
                        } else { #To client
                            if ($ZCLcommand == "00") { # Get Weekly Schedule Response command
                                Parsing_Set_Weekly_Schedule_command($output_text, $ZCLpayload);
                            } else {
                                $output_text = $output_text . "ZCL payload: " . $ZCLpayload . "\n";
                            }
                        }
                    } else {
                        $output_text = $output_text . "ZCL payload: " . $ZCLpayload . "\n";
                    }
                }
            }
        }
        return $output_text;
    }

    public function get_condition_plunger()
    {
        return $this->condition_plunger;
    }

    public function get_installed_temperature()
    {
        return $this->installed_temperature;
    }

    public function get_current_temperature()
    {
        return $this->current_temperature;
    }
}


