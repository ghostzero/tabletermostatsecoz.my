<?php
defined ( 'SYSPATH' ) or die ( 'No direct script access.' );

class Jsonuserhandler extends Kohana_Controller {


    public function __construct(array $array) {
        $this->array = $array;
    }

    public function jsonSerialize() {
        return $this->array;
   }


}

?>