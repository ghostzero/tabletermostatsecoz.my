<?php
/**
 * Created by PhpStorm.
 * User: vagrant
 * Date: 24.05.18
 * Time: 16:03
 */
defined('SYSPATH') or die ('No direct script access.');
class Model_Language extends Kohana_Model{

    static function get_words($content , $lang, $output_language)
    {
        $content=trim($content);
        //Выборка словаря из базы,используя кеширование
        $language = DB::select()->from('language_'.$lang)->cached(40)->execute()->as_array('eng',$output_language);

        if($output_language=='eng') return $content;

        if (!empty($language[$content]))
        {
            return $language[$content];
        }
    }
}



?>