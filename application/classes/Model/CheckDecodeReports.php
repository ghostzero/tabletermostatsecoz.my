<?php
/**
 * Created by PhpStorm.
 * User: ghost
 * Date: 01.11.16
 * Time: 11:32
 */
class Model_CheckDecodeReports extends ORM
{
    protected $_table_name = 'check_decode_reports';

    public function set_one_row($array_fields)
    {
        $sql = "insert into `".$this->_table_name."` (`";
        $partvalues = " values ('";
        foreach ($array_fields as $key => $value) {
            $sql = $sql . $key. "`, `";
            $partvalues = $partvalues . $value . "', '";
        }
        $sql= substr($sql, 0, -3);
        $partvalues = substr($partvalues, 0, -3);
        $resultsql = $sql.') '.$partvalues.') ';
        $test = 1;

        list($insert_id, $affected_rows)= DB::query(Database::INSERT, $resultsql)->execute();
        return $insert_id;
    }


        public function update_one_row_by_id($id,$array_fields)
    {
        $sql = "update `".$this->_table_name."` set ";
        foreach ($array_fields as $key => $value) {
            $sql = $sql.' `'. $key. "`='".$value."', ";
        }
        $sql= substr($sql, 0, -2)." where `id`='".$id."'    ";

        $test = 1;
           list($insert_id, $affected_rows)= DB::query(Database::UPDATE, $sql)->execute();
        return $insert_id;
    }

}