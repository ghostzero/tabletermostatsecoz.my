<?php

/**
 * Created by PhpStorm.
 * User: ghost
 * Date: 27.10.16
 * Time: 7:14
 */
class Model_Logssqlecozy extends ORM
{

    protected $_table_name = 'logs';

//    protected $_primary_key = 'id';


    public function get_all_logs()
    {
        $result = $this->factory('Logssqlecozy')->find_all()->as_array();

        return $result;
    }

    public function get_one_by_hub_id($hub_id)
    {
        $all_rows = $this->factory('Logssqlecozy')->where('hub_id', '=', $hub_id)->find_all()->as_array();
        $output = [];
        foreach ($all_rows as $k => $v) {
            $output[] = $v->as_array();
        }

        $result = $output;

        return $result;
    }

    public function resort_by_new_table($hub_id)
    {
        $all_rows = $this->factory('Logssqlecozy')->where('hub_id', '=', $hub_id)->find_all()->as_array();
        $output = [];
        foreach ($all_rows as $k => $v) {
            $output[] = $v->as_array();
        }

        $result = $output;

        return $result;
    }

    public function get_all_colum_from_logs()
    {
        $result = $this->factory('Logssqlecozy')->find_all()->as_array();
        return $result;
    }

        public function get_all_colum_from_logs_by_limit_offset($number_offset, $number_limit)
    {
        $result = $this->factory('Logssqlecozy')->order_by('created', 'DESC')->limit($number_limit)->offset($number_offset)->find_all()->as_array();
        return $result;
    }

    public function get_some_rows($hub_id)
    {
        $result = $this->factory('Logssqlecozy')->where('hub_id', '=', $hub_id)->find_all()->as_array();
        return $result;
    }

    public function insert_t()
    {
        $result = $this->factory('Logssqlecozy')->find_all()->as_array();
        return $result;
    }


}