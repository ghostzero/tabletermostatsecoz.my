<?php
class Model_User extends Model_Auth_User {
	public $_RULES = array(
			'password'	=> array(
				'not_empty'						=> NULL,
				'min_length'					=> array(5),
				'max_length'					=> array(42),
			),
			
			'email'	=> array(
				'not_empty'						=> NULL,
				'min_length'					=> array(4),
				'max_length'					=> array(127),
				'email'							=> NULL,
				'Model_User::unique_email' 		=> NULL
			)
			
			);
	
	
	
	public function set_validate_params(&$array) 
	{
		// initialise the library validation rules and setup Some		
		$array = Validate::factory($array)
						->rules('password', $this->_RULES['password'])
						->rules('email', $this->_RULES['email'])
						->filter(TRUE, 'trim');
						
		return $array;
	}


    public function set_address_alies_owner_nameowner_name_by_id($user_id, $address, $alies, $owner_name){

        $sql = "UPDATE `users` SET `address`='".$address."', `alies`='".$alies."', `owner_name`='".$owner_name."'  WHERE `id`='".$user_id."'";
                $category_name = DB::query(Database::UPDATE, $sql)->execute();
        return $category_name;

    }

    public function user_roles($user_id)
    {
        $result = array();

        $db = DB::select(array('roles.name', 'name'));

        # SELECT -> ROLES_USERS
        $db->from('roles_users');
        $db->where('roles_users.user_id', '=', $user_id);

        # SELECT -> ROLES
        $db->join('roles');
        $db->on('roles.id', '=', 'roles_users.role_id');

        $roles = $db->execute()->as_array();

        foreach($roles as $role)
        {
            array_push($result, $role['name']);
        }
    }


    public function get_users_all(){
        $sql = "select * from `users`";
//        $sql = "select * from `decode_report_full` where `mac`='" . $mac . "' order by `datetime` asc";
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $category_name;
    }


    public function delete_user_by_id($user_id){
        $sql = "delete from `users` where `id`='".$user_id."'";
//        $sql = "select * from `decode_report_full` where `mac`='" . $mac . "' order by `datetime` asc";
        $category_name = DB::query(Database::DELETE, $sql)->execute();
//        return $category_name;
    }


    public function get_users_not_superadmin(){
        $sql = "select * from `users` where `username`<>'admin'";
//        $sql = "select * from `decode_report_full` where `mac`='" . $mac . "' order by `datetime` asc";
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $category_name;
    }



    public function get_user($id){
        $sql = "select * from `users` WHERE id=".$id;
//        $sql = "select * from `decode_report_full` where `mac`='" . $mac . "' order by `datetime` asc";
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $category_name;
    }





    public function get_user_with_role($id){
//        $sql = "select * from `users` WHERE id=".$id;
//        $sql = "select * from `decode_report_full` where `mac`='" . $mac . "' order by `datetime` asc";
        $sql = " SELECT B.*, A.role_id as role_id, C.name as name_role FROM roles_users A
                 LEFT OUTER JOIN users B ON ( B.id = A.user_id )
                 LEFT OUTER JOIN roles C ON ( C.id = A.role_id )
                 WHERE C.name NOT LIKE '%login%' and B.id='".$id."' ";
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $category_name[0];
    }


    public function update_one_row_by_id($id,$array_fields)
    {
        $sql = "update `".$this->_table_name."` set ";
        foreach ($array_fields as $key => $value) {
            $sql = $sql.' `'. $key. "`='".$value."', ";
        }
        $sql= substr($sql, 0, -2)." where `id`='".$id."'    ";

        $test = 1;
        list($insert_id, $affected_rows)= DB::query(Database::UPDATE, $sql)->execute();
        echo $sql;
        return $insert_id;
    }


}
?>