<?php
class Model_News extends ORM {
	protected $_table_name = 'news';
	protected $_primary_key = 'id';
	
	public function all_news() {
		$all_news = $this->factory ( 'News' )
			->order_by ( 'date', 'desc' )
			->find_all ()
			->as_array ();
		foreach ( $all_news as $k => $one_news ) {
			$result [] = array (
					'title' => $one_news->title, 
					'date' => $one_news->date );
		}
		return $result;
	}
	
	public function parse_code($content)
	{
		preg_match('/\[code\](.*?)\[\/code\]/siu', $content, $matches);
		$php_code = $matches[1];
		/*$replace = array("'<div class=refsect1 >'.highlight_string('<?php '.stripslashes('$1').'?>', true).'</div>'");*/
		$content = preg_replace('/\[code\].*?\[\/code\]/siu', '[php code]', $content);	
		$content = nl2br(htmlspecialchars($content));
		$content = str_replace('[php code]', '<div class="refsect1">'.highlight_string('<?php '.$php_code.'?>', true).'</div>', $content);
		return $content;
	}
	
	public function get_short_all_news($per_page, $offset) {
		$all_news = $this->factory ( 'News' )
			->limit ( $per_page )
			->offset ( $offset )
			->order_by ( 'date', 'desc' )
			->find_all ()
			->as_array ();
		
		foreach ( $all_news as $k => $one_news ) {
			$content = $one_news->content;
			$content = $this->parse_code($content);
			$result [] = array (
					'id' => $one_news->id, 
					'title' => $one_news->title, 
					'content' => $content, 
					'date' => $one_news->date );
		}
		return $result;
	}
	public function get_one_news($id) {
		$result = $this->factory ( 'News' )
			->where ( 'id', '=', $id )
			->find ()
			->as_array ();
	
		$result['content'] = $this->parse_code($result['content']);
		return $result;
	}
	public function get_one_news_admin($id) {
		$result = $this->factory ( 'News' )
			->where ( 'id', '=', $id )
			->find ()
			->as_array ();		
		return $result;
	}
	
	public function count_news() {
		$result = $this->factory ( 'News' )
			->count_all ();
		return $result;
	}
	
	public function list_news($per_page, $offset) {
		$result = $this->factory ( 'News' )
			->limit ( $per_page )
			->offset ( $offset )
			->order_by ( 'date', 'desc' )
			->find_all ()
			->as_array ();
		return $result;
	}
	public function delete_one_news($id_news) {
		$this->factory ( 'News' )
			->delete ( $id_news );
	
	}
	public function safe_news($title, $content, $date) {
		$news = $this->factory ( 'News' );
		$news->title = $title;
		$news->content = $content;
		$news->date = $date;
		$news->save ();
		return true;
	
	}
	public function edit_news($id, $title, $content, $date) {
		$news = $this->factory ( 'News' )
			->where ( 'id', '=', $id )
			->find ();
		$news->title = $title;
		$news->content = $content;
		$news->date = $date;
		$news->save ();
		return true;
	
	}

}
?>
