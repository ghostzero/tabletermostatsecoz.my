<?php


class Model_Category extends ORM
{

    protected $_table_name = 'category';
    protected $_primary_key = 'id';

    protected $_rules = array(
        'name' => array(
            'not_empty' => NULL,
            'max_length' => array(
                32)));

    public function get_categories()
    {
        $all_category = $this->factory('Category')
            ->find_all()
            ->as_array();
        foreach ($all_category as $k => $v) {
            if ($v->name == STATIC_PAGE_OF_CATEGORY) {
                $result [] = array(
                    'name' => $v->name,
                    'href' => '/static/' . User::get_static_page(),
                    'title' => $v->title);
            } else {
                $result [] = array(
                    'name' => $v->name,
                    'href' => '/category/' . $v->href,
                    'title' => $v->title);
            }

        }
        return $result;
    }

    public function get_methods($category_name)
    {
        $result = null;
        $category_id = $this->factory('Category')
            ->where('name', '=', $category_name)
            ->find()->id;
        $all_method = ORM::factory('Function')->where('category_id', '=', $category_id)
            ->order_by('name')
            ->find_all()
            ->as_array();
        foreach ($all_method as $k => $v) {
            $result [] = array(
                "name" => $v->name,
                "description" => $v->description);

        }
        return $result;
    }

    public function get_methods_for_function($function)
    {
        $category_name = $this->get_category($function);
        return $this->get_methods($category_name);
    }

    public function get_category($function)
    {
        $sql = 'select `name` from `category` where `id`=(select `category_id` from `function` where `name` ="' . $function . '" limit 1)';
        $category_name = DB::query(Database::SELECT, $sql)->execute()
            ->as_array();
        return $category_name;
    }
}

?>
