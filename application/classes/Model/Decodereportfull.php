<?php

/**
 * Created by PhpStorm.
 * User: ghost
 * Date: 31.10.16
 * Time: 3:19
 */
class Model_Decodereportfull extends ORM
{
    protected $_table_name = 'decode_report_full';

    public function set_one_row($array_fields)
    {
//        $baseCall = $this->factory('Logssqlecozy');
//        $sql = 'select `name` from `category` where `id`=(select `category_id` from `function` where `name` ="' . $function . '" limit 1)';
//        $sql = "insert into `logs_new` (`hub_id`,`body`,`created`) values (`"++"``,`"++"``,`"++")";
        $sql = "insert into `" . $this->_table_name . "` (`";
        $partvalues = " values ('";
        foreach ($array_fields as $key => $value) {
            $sql = $sql . $key . "`, `";
            $partvalues = $partvalues . $value . "', '";
        }
        $sql = substr($sql, 0, -3);
        $partvalues = substr($partvalues, 0, -3);
        $resultsql = $sql . ') ' . $partvalues . ') ';
        $test = 1;

        $category_name = DB::query(Database::INSERT, $resultsql)->execute();

    }

    public function get_by_group_mac()
    {
        $sql = "select `mac` from `decode_report_full` group by `mac` order by `mac`";
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $category_name;

    }


    public function get_rows_by_mac($mac)
    {
        $sql = "select * from `decode_report_full` where `mac`='" . $mac . "' order by `datetime` asc";
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $category_name;

    }

    public function get_rows_by_mac_sort_asc($mac)
    {
        $sql = "select * from `decode_report_full` where `mac`='" . $mac . "' order by `datetime` asc";
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $category_name;

    }

    public function get_rows_by_mac_sort_asc_by_datetime_from_to($mac, $select_from_datetime, $select_to_datetime)
    {
        $sql = "select * from `decode_report_full` where `datetime`>='" . $select_from_datetime . "' and `datetime`<='" . $select_to_datetime . "' and `mac`='" . $mac . "' order by `datetime` asc";
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();

        return $category_name;

    }

    public function get_rows_by_mac_sort_asc_by_datetime_one_hour($mac, $select_from_datetime, $number_hour)
    {
        $select_to_datetime = date("Y-m-d H:i:s", strtotime($select_from_datetime) + $number_hour * 3600 + 1);
        $sql = "select * from `decode_report_full` where `datetime`>='" . $select_from_datetime . "' and `datetime`<='" . $select_to_datetime . "' and `mac`='" . $mac . "' order by `datetime` asc";
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $category_name;

    }

    public function get_rows_by_mac_for_excell($mac)
    {
        $sql = "select * from `decode_report_full` where `mac`='" . $mac . "' order by `mac`";
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $category_name;

    }


    public function get_rows_by_mac_group_by_datetime($mac)
    {
        $sql = "select * from `decode_report_full` where `mac`='" . $mac . "' group by `datetime` order by `mac`";
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $category_name;

    }
}