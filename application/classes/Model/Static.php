<?php
class Model_Static extends ORM {
	protected $_table_name = 'static';
	protected $_primary_key = 'id';
	
	public function all_statics() {
		$all_statics = $this->factory ( 'Static' )
			->order_by ( 'date', 'desc' )
			->find_all ()
			->as_array ();
		foreach ( $all_statics as $k => $one_static ) {
			$result [] = array (
					'title' => $one_static->title, 
					'date' => $one_static->date );
		}
		return $result;
	}
	
	public function get_short_all_statics($per_page, $offset) {
		$all_static = $this->factory ( 'Static' )
			->limit ( $per_page )
			->offset ( $offset )
			->order_by ( 'date', 'desc' )
			->find_all ()
			->as_array ();
		
		foreach ( $all_static as $k => $one_static ) {
			$content = nl2br ( $one_static->content );
			$result [] = array (
					'id' => $one_static->id, 
					'title' => $one_static->title, 
					'content' => $content, 
					'date' => $one_static->date );
		}
		return $result;
	}
	public function get_one_static($static_name) {
		$result = $this->factory ( 'Static' )
			->where ( 'name', '=', $static_name )
			->find ()
			->as_array ();
		return $result;
	}
	public function get_one_static_admin($id) {
		$result = $this->factory ( 'Static' )
			->where ( 'id', '=', $id )
			->find ()
			->as_array ();
		return $result;
	}
	public function count_statics() {
		$result = $this->factory ( 'Static' )
			->count_all ();
		return $result;
	}
	public function list_statics($per_page, $offset) {
		$result = $this->factory ( 'Static' )
			->limit ( $per_page )
			->offset ( $offset )
			->find_all ()
			->as_array ();
		return $result;
	}
	public function delete_one_static($id_news) {
		$this->factory ( 'Static' )
			->delete ( $id_news );
	
	}
	public function safe_static($name, $content, $title, $status = '0') {
		if ($status == null)
			$status = 0;
		if ($status)
			$this->check_status ();
		$static = $this->factory ( 'static' );
		$static->name = $name;
		$static->content = $content;
		$static->title = $title;
		$static->status = $status;
		
		$static->save ();
		return true;
	
	}
	public function edit_static($id, $name, $content, $title, $status = '0') {
		if ($status == null)
			$status = 0;
		if ($status)
			$this->check_status ();
		$static = $this->factory ( 'Static' )
			->where ( 'id', '=', $id )
			->find ();
		$static->name = $name;
		$static->content = $content;
		$static->title = $title;
		$static->status = $status;
		$static->save ();
		return true;
	
	}
	public function check_status() {
		$sql = 'UPDATE static SET status=0';
		$result = DB::query ( Database::UPDATE, $sql )->execute ();
	
	}
}