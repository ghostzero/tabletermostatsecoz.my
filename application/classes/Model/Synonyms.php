<?php

class Model_Synonyms extends ORM
{
    protected $_table_name = 'synonyms';
/*
    public function set_synonym($user_id, $device_id, $type_of_extension_module_id, $name, $id=0){

        $sql = "REPLACE INTO synonyms (id, user_id, device_id, type_of_extension_module_id, name) VALUES (".$id.",".$user_id.",".$device_id.",".$type_of_extension_module_id.",'".$name."');";

        $category_name = DB::query(Database::UPDATE, $sql)->execute();
        return $category_name;

    }
*/
    public function set_synonym_for_user($user_id, $device_id, $type_of_extension_module_id, $name, $id=0){

        $name = preg_replace ("/^[^a-zA-ZА-Яа-я0-9\s]*$/","",$name);


        $sql = "select * from `".$this->_table_name."` where user_id=".$user_id." and device_id=".$device_id." and type_of_extension_module_id=".$type_of_extension_module_id;
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();

        if(count($category_name) > 0){

            $sql = "UPDATE synonyms SET `name`='".$name."' where user_id=".$user_id." and device_id=".$device_id." and type_of_extension_module_id=".$type_of_extension_module_id;
            $category_name = DB::query(Database::UPDATE, $sql)->execute();
        }else{

            $sql = "INSERT INTO synonyms (`user_id`, `type_of_extension_module_id`, `name`, `device_id`) VALUES ('".$user_id."', '".$type_of_extension_module_id."', '".$name."', '".$device_id."' );";
            $category_name = DB::query(Database::INSERT, $sql)->execute();
        }

        return $category_name;

    }


    public function set_synonym_for_another($device_id, $type_of_extension_module_id, $name, $id=0){

        $name = preg_replace ("/^[^a-zA-ZА-Яа-я0-9\s]*$/","",$name);


//        $sql = "select * from `".$this->_table_name."` where user_id=".$user_id." and device_id=".$device_id." and type_of_extension_module_id=".$type_of_extension_module_id;
        $sql = "select * from `".$this->_table_name."` where device_id=".$device_id." and type_of_extension_module_id=".$type_of_extension_module_id;
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();

        if(count($category_name) > 0){

//            $sql = "UPDATE synonyms SET `name`='".$name."' where user_id=".$user_id." and device_id=".$device_id." and type_of_extension_module_id=".$type_of_extension_module_id;
            $sql = "UPDATE synonyms SET `name`='".$name."' where  device_id=".$device_id." and  type_of_extension_module_id=".$type_of_extension_module_id;
            $category_name = DB::query(Database::UPDATE, $sql)->execute();
        }else{

//            $sql = "INSERT INTO synonyms (`user_id`, `type_of_extension_module_id`, `name`, `device_id`) VALUES ('".$user_id."', '".$type_of_extension_module_id."', '".$name."', '".$device_id."' );";
            $sql = "INSERT INTO synonyms (`type_of_extension_module_id`, `name`, `device_id`) VALUES ('".$type_of_extension_module_id."', '".$name."', '".$device_id."' );";
            $category_name = DB::query(Database::INSERT, $sql)->execute();
        }

        return $category_name;

    }

    public function get_synonyms($user_id){

        $sql = "select * from `".$this->_table_name."` where user_id=".$user_id;
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $category_name;

    }

    public function set_user_id_by_device_id($device_id, $user_id){

        $sql = "UPDATE `synonyms` SET `user_id`='".$user_id."' WHERE `device_id`='".$device_id."'";
        $category_name = DB::query(Database::UPDATE, $sql)->execute();
        return $category_name;

    }

    public function get_synonyms_by_devise_id_all_user($device_id){

        $sql = "select * from `".$this->_table_name."` where device_id=".$device_id;
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $category_name;

    }

    public function get_synonyms_by_devise_id($user_id, $device_id){

        $sql = "select * from `".$this->_table_name."` where user_id=".$user_id." and device_id=".$device_id;
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $category_name;

    }

}
?>