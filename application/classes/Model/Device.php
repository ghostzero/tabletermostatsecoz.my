<?php

/**
 * Created by PhpStorm.
 * User: vagrant
 * Date: 16.05.18
 * Time: 15:46
 */
class Model_Device extends ORM
{
    protected $_table_name = 'device';


    public function clean_pairing_device($device_id){

        $sql = "UPDATE device SET user_id=NULL WHERE id=".$device_id;
        $category_name = DB::query(Database::UPDATE, $sql)->execute();
        return $category_name;

    }

    public function pairing_device_and_user($device_id, $user_id){

        $sql = "UPDATE device SET user_id=".$user_id." WHERE id=".$device_id;
        $category_name = DB::query(Database::UPDATE, $sql)->execute();
        return $category_name;

    }

    public function remove_of_link_device_and_user($device_id){

        $sql = "UPDATE device SET user_`id`=NULL WHERE `id`='".$device_id."'";
        $category_name = DB::query(Database::UPDATE, $sql)->execute();
        return $category_name;

    }

    public function get_device_without_binding(){
        $sql = "select * from `".$this->_table_name."` where (user_id is null or user_id = '')";
//        $sql = "select * from `decode_report_full` where `mac`='" . $mac . "' order by `datetime` asc";
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $category_name;
    }

    public function get_device_with_binding(){
        $sql = "select * from `".$this->_table_name."` where user_id is not null";
//        $sql = "select * from `decode_report_full` where `mac`='" . $mac . "' order by `datetime` asc";
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $category_name;
    }

    public function get_device_all(){
        $sql = "select * from `".$this->_table_name."` ";
//        $sql = "select * from `decode_report_full` where `mac`='" . $mac . "' order by `datetime` asc";
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $category_name;
    }


    public function get_device_by_id($user_id){
        $sql = "select * from `".$this->_table_name."` where `user_id`='".$user_id."'";
//        $sql = "select * from `decode_report_full` where `mac`='" . $mac . "' order by `datetime` asc";
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $category_name;
    }



    public function get_id_role($name_role)
    {
        $sql = "select `id` from `roles` where `name`='" . $name_role . "'";
//        $sql = "select * from `decode_report_full` where `mac`='" . $mac . "' order by `datetime` asc";
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $category_name;
    }


    public function get_all_devices(){
        $sql = "select * from `".$this->_table_name."` ";
//        $sql = "select * from `decode_report_full` where `mac`='" . $mac . "' order by `datetime` asc";
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();
        return $category_name;
    }

    public function get_device(){

    }

    public function get_all_parameters_by_device_id($device_id){


    }


    public function set_total_info_about_device($string_json_array)
    {
        $array_from_string_json_array_different_case = json_decode($string_json_array, true);
//        $array_from_string_json_array=array_change_key_case($array_from_string_json_array_different_case,CASE_LOWER);
//        $array_HeaderData = array_change_key_case($array_from_string_json_array_different_case['HeaderData'], CASE_LOWER);
//        $array_InputData = array_change_key_case($array_from_string_json_array_different_case['InputData'], CASE_LOWER);
//        $array_OutputData = array_change_key_case($array_from_string_json_array_different_case['OutputData'], CASE_LOWER);
//        $array_StatusData = array_change_key_case($array_from_string_json_array_different_case['StatusData'], CASE_LOWER);



        $array_HeaderData = $array_from_string_json_array_different_case['HeaderData'];
        $array_InputData = $array_from_string_json_array_different_case['InputData'];
        $array_OutputData = $array_from_string_json_array_different_case['OutputData'];
        $array_StatusData = $array_from_string_json_array_different_case['StatusData'];


//        `id``headcrc``modename``outputcrc``inputdatacrc``statuscrc`

        $this->set_only_to_table_device($array_HeaderData, $array_InputData, $array_OutputData, $array_StatusData);



//        $this->set_one_row();

        $test = 1;


    }





    public function set_only_to_table_device($array_HeaderData, $array_InputData, $array_OutputData, $array_StatusData)
    {

//        var_dump($array_HeaderData);
//        print_r($array_HeaderData);
        $headcrc = $array_HeaderData['HeadCRC'];
        $modename_array = $array_HeaderData['ModeName'];
        $serialnamber = $array_HeaderData['SerialNamber'];
        $modename = '';
        $count=1;
        foreach ($serialnamber as $key => $one_modename) {
//            $modename = $modename + $one_modename * pow(255, $key);
            if($modename==''){
                $modename = $modename . dechex($one_modename);
            }else { $modename = $modename.":" . dechex($one_modename);}


            if($count >= 6) break;
                $count+=1;
        }
        /*foreach ($modename_array as $key => $one_modename) {
            $modename = $modename + $one_modename * pow(255, $key);
        }*/
//        printf($modename);
//        echo $modename;
//        exit;
        $outputcrc = $array_OutputData['OutputCRC'];
        $inputdatacrc = $array_InputData['InputDataCRC'];
        $statuscrc = $array_StatusData['StatusCRC'];
        $array_temprefcurrent = $array_StatusData['TempRefCurrent'];
        $array_timerpi = $array_StatusData['TimeRpi'];


        //        $modename=

        $check_return_device = $this->get_device_by_modename($modename);
//        $modename_mistake= "123408737789";
//        $check_return_device=$this->get_device_by_modename($modename_mistake);
        $count_rows_devices = count($check_return_device);

        $device_id = null;
        if ($count_rows_devices == 0) {
            $array_fields = ['modename' => $modename];
            $device_id = $this->set_one_row($array_fields);
        } else {
            $device_id = $check_return_device[0]['id'];
        }

        $timestamp_id = ORM::factory("Timestamp")->set_one_row_for_timestamp_unixtime_millisecond($device_id);




//        $array_fields = ['headcrc' => $headcrc, 'modename' => $modename, 'outputcrc' => $outputcrc, 'inputdatacrc' => $inputdatacrc, 'statuscrc' => $statuscrc];
//        $array_fields = ['modename' => $modename];
//        $array_timerpi=

//        $device_id=$this->set_one_row($array_fields);
        //Занести в базу данных SerialNamber
//        $timestamp_id =
        ORM::factory("Serialnamber")->set_serialnamber_for_device($device_id, $timestamp_id, $serialnamber);
        //Занести в базу данных crcdata
        ORM::factory("Crcdata")->set_crcdata_for_device($device_id, $timestamp_id, $headcrc, $outputcrc, $inputdatacrc, $statuscrc);

        //Занести в базу данных value_of_extension_module
        ORM::factory("Timerpi")->set_timerpi($device_id, $timestamp_id, $array_timerpi);







        $this->set_to_base_array_InputData($array_InputData, $device_id, $timestamp_id);

        $this->set_to_base_array_InputData($array_OutputData, $device_id, $timestamp_id);

        $this->set_to_base_array_InputData($array_StatusData, $device_id, $timestamp_id);


//            ORM::factory("Valueofextensionmodule")->set_one_value_of_extension_module($device_id, $timestamp_id, $type_of_extension_module_id, $value);


//        Время в юникс тайм для формирования.


//        Серийники каждой группы
//        Значения групп


    }


    public function parcing_array_by_group()
    {

    }

    public function set_to_base_array_InputData($array_InputData, $device_id, $timestamp_id)
    {


        /*$name_of_type_of_groups_extension_module = '';

        $type_of_groups_extension_module_id=ORM::factory("Typeofgroupsextensionmodule")->get_id_group_by_name_group("PWMinGroup1");


        $name_extension_module="PWMin1";


        $group_id=ORM::factory("Typeofextensionmodule")->get_id_value_of_extension_module_by_type_of_groups_extension_module_id_and_name_extension_module($type_of_groups_extension_module_id, $name_extension_module);*/


        foreach ($array_InputData as $key_array_InputData => $one_value_InputData) {
            $name_of_type_of_groups_extension_module = $key_array_InputData;
//            $type_of_groups_extension_module_id=ORM::factory("Typeofgroupsextensionmodule")->get_id_group_by_name_group("PWMinGroup1");
            $type_of_groups_extension_module_id = ORM::factory("Typeofgroupsextensionmodule")->get_id_group_by_name_group($name_of_type_of_groups_extension_module);
            if ($type_of_groups_extension_module_id != false) {
                foreach ($one_value_InputData as $key_type_of_extension_module => $value_type_of_extension_module) {
                    if ($key_type_of_extension_module == "serial") {
//                        continue;
                        ORM::factory("Serialofgroup")->set_serial_of_group_for_group($device_id,$timestamp_id, $type_of_groups_extension_module_id, $value_type_of_extension_module);


                    } elseif ($name_of_type_of_groups_extension_module == "TimeRpi") {

                    } else {
                        $type_of_extension_module_id = ORM::factory("Typeofextensionmodule")->get_id_value_of_extension_module_by_type_of_groups_extension_module_id_and_name_extension_module($type_of_groups_extension_module_id, $key_type_of_extension_module);
                        if($type_of_extension_module_id == false ){
                            $check_point =1;
                        }

                        ORM::factory("Valueofextensionmodule")->set_one_value_of_extension_module($device_id, $timestamp_id, $type_of_extension_module_id, $value_type_of_extension_module);
                    }


                }
            }


        }


        /*foreach ($array_InputData as $key => $value){
//            ORM::factory('')->

        }*/


    }


    public function set_to_base_array_OutputData()
    {

    }


    public function set_to_base_array_StatusData()
    {

    }


    public function clean_tables()
    {

    }


    public function get_device_by_modename($modename)
    {
        $sql = "select * from `device` where `modename`='" . $modename . "'";
        $category_name = DB::query(Database::SELECT, $sql)->execute()->as_array();
        array_reverse($category_name);
        return $category_name;
    }

//    public function


    public function action_test()
    {
//        ORM::factory("Timestamp")->set_timestamp_by_device_id(3);
//        $id_group = ORM::factory("Typeofgroupsextensionmodule")->get_id_group_by_name_group('AinGroup1');
        $type_of_extension_module_id = ORM::factory("Typeofextensionmodule")->get_id_group_by_name_group('AinGroup1');
        $test = 1;

    }


    public function set_one_row($array_fields)
    {
//        $baseCall = $this->factory('Logssqlecozy');
//        $sql = 'select `name` from `category` where `id`=(select `category_id` from `function` where `name` ="' . $function . '" limit 1)';
//        $sql = "insert into `logs_new` (`hub_id`,`body`,`created`) values (`"++"``,`"++"``,`"++")";
        $sql = "insert into `" . $this->_table_name . "` (`";
        $partvalues = " values ('";
        foreach ($array_fields as $key => $value) {
            $sql = $sql . $key . "`, `";
            $partvalues = $partvalues . $value . "', '";
        }
        $sql = substr($sql, 0, -3);
        $partvalues = substr($partvalues, 0, -3);
        $resultsql = $sql . ') ' . $partvalues . ') ';
        $test = 1;

        $category_name = DB::query(Database::INSERT, $resultsql)->execute();
        return $category_name[0];
    }


    public function truncate_base_tables()
    {
        $sql = "TRUNCATE TABLE `" . $this->_table_name . "`";
//        $category_name = DB::query(Database::INSERT, $resultsql)->execute();
        DB::query(NULL, $sql)->execute();

    }


}

?>