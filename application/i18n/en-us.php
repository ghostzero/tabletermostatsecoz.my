<?php
/**
 * Created by PhpStorm.
 * User: vagrant
 * Date: 16.05.18
 * Time: 11:16
 */

return array(
    "German" => "Deutsch",
    "English" => "Englisch",


    "Log out" => "Ausloggen",


    "Click on log out if you have finished your session" => "Klicken Sie auf Abmelden, wenn Sie Ihre Sitzung beendet haben",


    "Cancel" => "Abbrechen",


    "Please give more than 3 characters" => "Bitte geben Sie mehr als 3 Zeichen",


    "Overview" => "Übersicht",


    "All Devices" => "Alle Geräte",


    "Search results" => "Suchergebnisse",


    "Search results for: " => "Suchergebnisse für:",


    "No device found" => "Kein Gerät gefunden",


    "Last week" => "Letzte Woche",


    "Line" => "Linie",


    "Devices" => "Geräte",


    "Service" => "Service",


    "User" => "Benutzer",


    "Language" => "Sprache",


    "Search for S/N, email, name" => "Suche nach S/N, E-Mail, Name",


    "Last accessed" => "Zuletzt gesehen",


    "Log in" => "Einloggen",


    "Welcome in HomeControl Portal" => "Willkommen im HomeControl Portal",


    "Please enter your credentials" => "Bitte geben Sie Ihre Zugangsdaten ein",

    "Password is wrong. Either you already reset it or you just forgot it :/" => "Das eingegebene Passwort ist falsch. Sie haben es entweder bereits geändert oder Sie haben es vergessen :/",


    "E-Mail Adresse" => "",


    "Password" => "Passwort",


    "Forgot your password?" => "Passwort vergessen?",


    "New password" => "Ein neues Passwort",


    "Repeat new password" => "Das neue Passwort wiederholen",

    "Password is wrong. You will find the right one in your email." => "Das Passwort ist falsch. Sie finden das richtige Passwort in Ihrer E-Mail.",

    "Save and log in"=> "Speichern und einloggen",);
