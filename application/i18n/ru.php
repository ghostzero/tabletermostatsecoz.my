<?php
return array (
			'Email must be unique' => 'Почтовый ящик должен быть уникальным', 
			'Invalid captcha' => 'Капча введена неверно', 
			'Confirm password must be equal to password' => 'Проверочный пароль должен совпадать с оригинальным', 
			'not_empty' => 'поле ":field" не должно быть пустым', 
			'matches' => 'поле ":field" должно совпадать с :param1', 
			'regex' => 'поле ":field" не соответствует нужному формату', 
			'exact_length' => 'поле ":field"должно быть только длинной :param1 символов', 
			'min_length' => 'поле ":field" должно как миниму содержать :param1 символов', 
			'max_length' => 'поле ":field" должно быть не более чем :param1 символов', 
			'in_array' => 'поле ":field" должно быть одним из доступных', 
			'digit' => 'поле ":field" должно быть цифровым', 
			'decimal' => 'поле ":field" must be a decimal with :param1 places', 
			'range' => 'поле ":field" должно быть от :param1 до :param2', 
			'email' => 'неверный формат адреса электронной почты', 
			'invalid email' => 'неверный формат email', 
			'url' => 'поле ":field" должно быть url адресом', 
			'confirmation' => 'пользовательское соглашение', 
			'integers' => 'в поле ":field" должны быть целые числа' );

?>
