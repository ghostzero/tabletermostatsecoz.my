<?php
defined ( 'SYSPATH' ) or die ( 'No direct access allowed.' );

return array ('status' => array (QUEUE => 'queue', ALL => 'all', COMPLETED => 'completed', IN_PROGRESS => 'in_progress', PENDING => 'pending', FAILED => 'failed' ) );
