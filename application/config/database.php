<?php
defined ( 'SYSPATH' ) or die ( 'No direct access allowed.' );

return array (
	'default' => array ('type' => 'MySQLi',
								'connection' => array (
													'hostname' => 'localhost',
													'database' => 'zaludbaseone',
													'username' => 'zalud',
													'password' => 'test123456',
													'persistent' => FALSE ),
								'table_prefix' => '',
								'charset' => 'utf8',
								'caching' => FALSE,
								'profiling' => TRUE )

/*,
	'mongodbecozy' => array (
                'type' => 'MongoDB',
		        'server' => 'mongodb://localhost:27017',
                'database' => 'ecozy')*/

	);

