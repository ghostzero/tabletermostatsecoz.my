<?php defined('SYSPATH') or die('No direct script access.');
include_once ('config.php');
// -- Environment setup --------------------------------------------------------


// Load the core Kohana class
require SYSPATH . 'classes/Kohana/Core' . EXT;

if (is_file(APPPATH . 'classes/Kohana' . EXT)) {
    // Application extends the core
    require APPPATH . 'classes/Kohana' . EXT;
} else {
    // Load empty core extension
    require SYSPATH . 'classes/Kohana' . EXT;
}

/**
 * Set the default time zone.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/timezones
 */
date_default_timezone_set('America/Chicago');

/**
 * Set the default locale.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/function.setlocale
 */
setlocale(LC_ALL, 'en_US.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @link http://kohanaframework.org/guide/using.autoloading
 * @link http://www.php.net/manual/function.spl-autoload-register
 */
spl_autoload_register(array('Kohana', 'auto_load'));
spl_autoload_register(array('Kohana', 'auto_load_lowercase'));

/**
 * Optionally, you can enable a compatibility auto-loader for use with
 * older modules that have not been updated for PSR-0.
 *
 * It is recommended to not enable this unless absolutely necessary.
 */
//spl_autoload_register(array('Kohana', 'auto_load_lowercase'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @link http://www.php.net/manual/function.spl-autoload-call
 * @link http://www.php.net/manual/var.configuration#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

/**
 * Set the mb_substitute_character to "none"
 *
 * @link http://www.php.net/manual/function.mb-substitute-character.php
 */
mb_substitute_character('none');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
I18n::lang('en-us');

if (isset($_SERVER['SERVER_PROTOCOL'])) {
    // Replace the default protocol.
    HTTP::$protocol = $_SERVER['SERVER_PROTOCOL'];
}

/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
if (isset($_SERVER['KOHANA_ENV'])) {
    Kohana::$environment = constant('Kohana::' . strtoupper($_SERVER['KOHANA_ENV']));
}

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - integer  cache_life  lifetime, in seconds, of items cached              60
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 * - boolean  expose      set the X-Powered-By header                        FALSE
 */
Kohana::init(array(
//    'base_url' => '/kohana/',
    'base_url' => FALSE,
));

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH . 'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(
     'auth'       => MODPATH.'auth',       // Basic authentication
    // 'cache'      => MODPATH.'cache',      // Caching with multiple backends
    // 'codebench'  => MODPATH.'codebench',  // Benchmarking tool
     'database'   => MODPATH.'database',   // Database access
    // 'image'      => MODPATH.'image',      // Image manipulation
     'minion'     => MODPATH.'minion',     // CLI Tasks
    'pagination' => MODPATH . 'pagination',  // Paging of results
     'orm'        => MODPATH.'orm',        // Object Relationship Mapping
     'unittest'   => MODPATH.'unittest',   // Unit testing
     'userguide'  => MODPATH.'userguide',  // User guide and API documentation
    'phpexcel' => MODPATH . 'phpexcel',  // phpexcel
    'smarty' => MODPATH . 'smarty',  // smarty template module.
    'email'       => MODPATH.'email', // E-mail
//    'captcha' => MODPATH . 'captcha'
    'mangodb'  => MODPATH.'mangodb'
));

/**
 * Cookie Salt
 * @see  http://kohanaframework.org/3.3/guide/kohana/cookies
 *
 * If you have not defined a cookie salt in your Cookie class then
 * uncomment the line below and define a preferrably long salt.
 */
Cookie::$salt = 1;

/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */
/*Route::set('default', '(<controller>(/<action>(/<id>)))')
	->defaults(array(
		'controller' => 'welcome',
		'action'     => 'index',
	));*/

/*Route::set('static', 'static/<path>.html', array(
    'path' => '[a-zA-Z0-9_/]+'))->defaults(array(
    'controller' => 'static',
    'action' => 'index'));

Route::set('admininto', 'admin/<controller>(/<action>.html)', array(
    'controller' => '(news|static)'))->defaults(array(
    'action' => 'index',
    'directory' => 'admin'));

Route::set('admin', 'admin(/<action>.html)')->defaults(array(
    'controller' => 'admin',
    'action' => 'index',
    'directory' => 'admin'));

Route::set('category', 'category/<path>.html', array(
    'path' => '[a-zA-Z0-9_/]+'))->defaults(array(
    'controller' => 'category',
    'action' => 'index'));
Route::set('ajax', 'ajax.html')->defaults(array(
    'controller' => 'handler',
    'action' => 'ajax'));*/
/*Route::set('default', '(<controller>(/<action>(/<id>)))')->defaults(array(
    'controller' => 'ecozy',
    'action' => 'index'));*/


/*Route::set('admin', 'admin/<controller>(/<action>(/<id>))')
    ->defaults(array(
        'directory' => 'admin',
        'controller' => 'admin',
        'action' => 'index'
    ));*/


Route::set('admin', 'admin(/<action>(/<id>))')->defaults(array(
    'controller' => 'admin',
    'action' => 'index',
    'directory' => 'admin'));


Route::set('user', 'user(/<action>(/<id>))')->defaults(array(
    'controller' => 'user',
    'action' => 'index',
    'directory' => 'user'));


Route::set('device', 'device(/<action>(/<id>))')->defaults(array(
    'controller' => 'base',
    'action' => 'index',
    'directory' => 'device'));





Route::set('login', 'login(/<action>(/<id>))')->defaults(array(
    'controller' => 'login',
    'action' => 'index',
//    'directory' => 'user'
));


Route::set('addition_service', 'addition_service(/<action>(/<id>))')->defaults(array(
    'controller' => 'additionservice',
    'action' => 'actueleverte',
//    'directory' => 'user'
));


Route::set('default', '(<controller>(/<action>(/<id>)))')->defaults(array(
    'controller' => 'login',
    'action' => 'index',
//    'directory' => 'login'
));

/*Route::set('default', '(<action>)')->defaults(array(
//    'directory' => 'admin',
//    'controller' => 'admin',
    'controller' => 'ecozy',
    'action' => 'index'));*/






/*Route::set('function', '<path>.html', array(
    'path' => '[a-zA-Z0-9_/]+'))->defaults(array(
    'controller' => 'handler',
    'action' => 'index'));*/




/*Route::set('default', '(<controller>(/<action>(/<id>)))')->defaults(array(
    'controller' => 'category',
    'action' => 'index'));*/

i18n::lang('ru-RU');


if (!defined('SUPPRESS_REQUEST')) {
    /**
     * Execute the main request. A source of the URI can be passed, eg: $_SERVER['PATH_INFO'].
     * If no source is specified, the URI will be automatically detected.
     */

    /*echo Request::instance ()->execute ()
        ->send_headers ()->response;*/


    /*try {

        $request = Request::instance();
        $request->execute();
    } catch (Exception $e) {
        $date = date("Y-m-d");
        $time = date("H:i:s");
        $error_string = "\n[$time] {$e->getMessage()} in {$e->getFile()} on line {$e->getLine()}";
        echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />Во время работы системы произошла ошибка, программисты уже работают над ее исправлением. Спасибо за понимание.';
        //mail("posterators@gmail.com", $error_string, $error_string);
        file_put_contents(LOG_DIR . $date . ".log", $error_string, FILE_APPEND);
        exit;
    }

    if ($request->send_headers()->response) {
        echo $request->response;
    }*/
}

