<?php
defined ( 'SYSPATH' ) or die ( 'No direct script access.' );
include_once ('config.php');
//-- Environment setup --------------------------------------------------------


/**
 * Set the default time zone.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/timezones
 */
date_default_timezone_set ( 'Europe/Kiev' );

/**
 * Set the default locale.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/setlocale
 */
setlocale ( LC_ALL, 'ru_RU.utf-8' );

/**
 * Enable the Kohana auto-loader.
 *
 * @see  http://kohanaframework.org/guide/using.autoloading
 * @see  http://php.net/spl_autoload_register
 */
spl_autoload_register ( array (
		'Kohana', 'auto_load' ) );

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @see  http://php.net/spl_autoload_call
 * @see  http://php.net/manual/var.configuration.php#unserialize-callback-func
 */
ini_set ( 'unserialize_callback_func', 'spl_autoload_call' );

//-- Configuration and initialization -----------------------------------------


/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 */
Kohana::init ( array (
		'base_url' => '/', 
		'index_file' => '', 
		'errors' => true ) );

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
//Kohana::$log->attach(new Kohana_Log_File(APPPATH.'logs'));


/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach ( new Kohana_Config_File () );

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules ( array (
		'auth' => MODPATH . 'auth',  // Basic authentication
		// 'cache'      => MODPATH.'cache',      // Caching with multiple backends
		// 'codebench'  => MODPATH.'codebench',  // Benchmarking tool
		'database' => MODPATH . 'database',  // Database access
		// 'image'      => MODPATH.'image',      // Image manipulation
		'orm' => MODPATH . 'orm',  // Object Relationship Mapping
		// 'oauth'      => MODPATH.'oauth',      // OAuth authentication
		'pagination' => MODPATH . 'pagination',  // Paging of results
		// 'unittest'   => MODPATH.'unittest',   // Unit testing
		//'userguide'  => MODPATH.'userguide',  // User guide and API documentation
		'smarty' => MODPATH . 'smarty',  // smarty template module. 	
		'captcha' => MODPATH . 'captcha' ) ); //'ckeditor'=> MODPATH.'ckeditor',


/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */
Route::set ( 'static', 'static/<path>.html', array (
		'path' => '[a-zA-Z0-9_/]+' ) )->defaults ( array (
		'controller' => 'static', 
		'action' => 'index' ) );

Route::set ( 'admininto', 'admin/<controller>(/<action>.html)', array (
		'controller' => '(news|static)' ) )->defaults ( array (
		'action' => 'index', 
		'directory' => 'admin' ) );

Route::set ( 'admin', 'admin(/<action>.html)' )->defaults ( array (
		'controller' => 'admin', 
		'action' => 'index', 
		'directory' => 'admin' ) );

Route::set ( 'category', 'category/<path>.html', array (
		'path' => '[a-zA-Z0-9_/]+' ) )->defaults ( array (
		'controller' => 'category', 
		'action' => 'index' ) );
Route::set ( 'ajax', 'ajax.html' )->defaults ( array (
		'controller' => 'handler', 
		'action' => 'ajax' ) );

Route::set ( 'function', '<path>.html', array (
		'path' => '[a-zA-Z0-9_/]+' ) )->defaults ( array (
		'controller' => 'handler', 
		'action' => 'index' ) );
Route::set ( 'default', '(<controller>(/<action>(/<id>)))' )->defaults ( array (
		'controller' => 'category', 
		'action' => 'index' ) );

/*Route::set('error', 'error(/<action>(/<id>))', array('id' => '.+'))
->defaults(array(
	'controller'	=> 'error',
	'action'		=> '404',
	'id'			=> FALSE,
));
*/

i18n::lang ( 'ru-RU' );

if (! defined ( 'SUPPRESS_REQUEST' )) {
	/**
	 * Execute the main request. A source of the URI can be passed, eg: $_SERVER['PATH_INFO'].
	 * If no source is specified, the URI will be automatically detected.
	 */
	
	/*echo Request::instance ()->execute ()
		->send_headers ()->response;*/
	

	try
	{
		$request = Request::instance();
		$request->execute();
	}
	catch (Exception $e)
	{
		$date = date("Y-m-d");
		$time = date("H:i:s");
		$error_string = "\n[$time] {$e->getMessage()} in {$e->getFile()} on line {$e->getLine()}";
		echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />Во время работы системы произошла ошибка, программисты уже работают над ее исправлением. Спасибо за понимание.';
		//mail("posterators@gmail.com", $error_string, $error_string);
		file_put_contents(LOG_DIR.$date.".log", $error_string, FILE_APPEND);
		exit;
	}
	
	if ( $request->send_headers()->response )
	{
		echo $request->response;
	}	
}
