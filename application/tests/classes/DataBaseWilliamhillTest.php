<?php


/**
 * Created by PhpStorm.
 * User: ghost
 * Date: 25.07.15
 * Time: 14:56
 */

//namespace DataBaseWilliamhillTest;

defined('SYSPATH') or die('No direct access allowed!');
require_once(DOCROOT . '/php-webdriver-master/user_classes/all/DataBaseWilliamhill.class.php');
//require_once('/home/ghost/www/betsline.my/php-webdriver-master/user_classes/all/DataBaseWilliamhill.class.php');


class DataBaseWilliamhillTest extends Kohana_Unittest_TestCase
{
    protected $db;

    function __construct()
    {
        $this->db = new DataBaseWilliamhill();
    }

    /*public function  setUp()
    {
        $this->db = new DataBaseWilliamhill();
    }*/

    public function test_set_into_statistic_bot_next_day()
    {
        $set = ['begin_amount_money_in_account' => 1, 'end_amount_money_in_account' => 1, 'count_chosed_football_games' => 1, 'count_setted_football_games' => 1, 'from_datetime_football_games' => 1, 'to_datetime_football_games' => 1, 'run_cycle_of_bot_next_day_id' => 1];
        $this->db->set_into_statistic_bot_next_day($set);
    }


    public function test_set_into_statistic_bot_next_day_v_1_1()
    {
        $array_statistic_bot_next_day = ['begin_amount_money_in_account' => 14.54, 'end_amount_money_in_account' => 8.67, 'count_chosed_football_games' => 60, 'count_setted_football_games' => 60, 'from_datetime_football_games' => '2015-08-25 19:30:00', 'to_datetime_football_games' => '2015-08-25 21:45:00'];
        $this->db->set_into_statistic_bot_next_day_v_1_1($array_statistic_bot_next_day);
    }

    public function test_set_into_statistic_bot_next_day_2()
    {
        $username = 2;
        $this->assertEquals($username, 1);
    }


    /*public function test_convert_form_los_angeles_to_moscow_date_time()
    {
        $this->assertEquals(2, DataBasePinnacle::factory()->convert_form_los_angeles_to_moscow_date_time(1, 1));
    }*/


}