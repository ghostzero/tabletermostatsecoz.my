<?php
/**
 * Created by PhpStorm.
 * User: ghost
 * Date: 27.10.16
 * Time: 12:22
 */
defined('SYSPATH') or die('No direct access allowed!');

class ModelCheckDevice extends Kohana_Unittest_TestCase
{
    /*protected $model;

    function __construct()
    {
        $this->model = new Model_MethodWilliamHillTest();
    }*/

    public function test_set_one_row()
    {
        $json_template="{\"HeaderData\":{\"HeadCRC\":0,\"ModeName\":[136,207,117,0,128,0],\"SerialNamber\":[108,138,196,117,108,138,196,117,136,207,117,0]},\"InputData\":{\"AinGroup1\":{\"Ain1\":0,\"Ain2\":0,\"Ain3\":0,\"Ain4\":0,\"Ain5\":0,\"Ain6\":0,\"Ain7\":0,\"Ain8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"AinGroup2\":{\"Ain1\":0,\"Ain2\":0,\"Ain3\":0,\"Ain4\":0,\"Ain5\":0,\"Ain6\":0,\"Ain7\":0,\"Ain8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"DinGroup1\":{\"Din1\":0,\"Din2\":0,\"Din3\":0,\"Din4\":0,\"Din5\":0,\"Din6\":0,\"Din7\":0,\"Din8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"DinGroup2\":{\"Din1\":0,\"Din2\":0,\"Din3\":0,\"Din4\":0,\"Din5\":0,\"Din6\":0,\"Din7\":0,\"Din8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"InputDataCRC\":0,\"PWMinGroup1\":{\"PWMin1\":0,\"PWMin2\":0,\"PWMin3\":0,\"PWMin4\":0,\"PWMin5\":0,\"PWMin6\":0,\"PWMin7\":0,\"PWMin8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"PWMinGroup2\":{\"PWMin1\":0,\"PWMin2\":0,\"PWMin3\":0,\"PWMin4\":0,\"PWMin5\":0,\"PWMin6\":0,\"PWMin7\":0,\"PWMin8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"TinGroup1\":{\"Tin1\":0,\"Tin2\":0,\"Tin3\":0,\"Tin4\":0,\"Tin5\":0,\"Tin6\":0,\"Tin7\":0,\"Tin8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"TinGroup2\":{\"Tin1\":0,\"Tin2\":0,\"Tin3\":0,\"Tin4\":0,\"Tin5\":0,\"Tin6\":0,\"Tin7\":0,\"Tin8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]}},\"OutputData\":{\"AoutGroup1\":{\"Aout1\":0,\"Aout2\":0,\"Aout3\":0,\"Aout4\":0,\"Aout5\":0,\"Aout6\":0,\"Aout7\":0,\"Aout8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"AoutGroup2\":{\"Aout1\":0,\"Aout2\":0,\"Aout3\":0,\"Aout4\":0,\"Aout5\":0,\"Aout6\":0,\"Aout7\":0,\"Aout8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"DoutGroup1\":{\"Dout1\":0,\"Dout2\":0,\"Dout3\":0,\"Dout4\":0,\"Dout5\":0,\"Dout6\":0,\"Dout7\":0,\"Dout8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"DoutGroup2\":{\"Dout1\":0,\"Dout2\":0,\"Dout3\":0,\"Dout4\":0,\"Dout5\":0,\"Dout6\":0,\"Dout7\":0,\"Dout8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"OutputCRC\":24347,\"PWMoutGroup1\":{\"PWMout1\":0,\"PWMout2\":0,\"PWMout3\":0,\"PWMout4\":0,\"PWMout5\":0,\"PWMout6\":20,\"PWMout7\":0,\"PWMout8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]},\"PWMoutGroup2\":{\"PWMout1\":0,\"PWMout2\":0,\"PWMout3\":0,\"PWMout4\":0,\"PWMout5\":0,\"PWMout6\":0,\"PWMout7\":0,\"PWMout8\":0,\"serial\":[0,0,0,0,0,0,0,0,0,0,0,0]}},\"StatusData\":{\"StatusCRC\":15010,\"TempRefCurrent\":{\"TempRef1\":120,\"TempRef10\":253,\"TempRef11\":0,\"TempRef12\":0,\"TempRef13\":0,\"TempRef14\":0,\"TempRef15\":0,\"TempRef16\":0,\"TempRef2\":252,\"TempRef3\":129,\"TempRef4\":116,\"TempRef5\":0,\"TempRef6\":0,\"TempRef7\":0,\"TempRef8\":0,\"TempRef9\":105},\"TimeRpi\":{\"Date\":0,\"Hours\":0,\"Minutes\":0,\"Month\":0,\"Seconds\":0,\"TimeFormat\":0,\"WeekDay\":0,\"Year\":0,\"serial\":[0,0,0,0,120,252,129,116,0,0,0,0]}}}";
//        $exampl_Jsonuserhandlere = new Jsonuserhandler($json_template);
//        $arry_decode_json=$exampl_Jsonuserhandlere->set_json_string($json_template);
//       $this->template->array_decode_json = json_encode($exampl_Jsonuserhandlere->jsonSerialize(), JSON_PRETTY_PRINT);
//        $this->template->array_decode_json = json_decode($json_template);
        ORM::factory ( 'Device' )->set_total_info_about_device($json_template);
        $test = 1;
    }

    public function test_update_one_row_by_id()
    {
        /*$amount_money_in_account=$this->model->get_amount_money_in_account();
        $this->assertEquals($amount_money_in_account, 0.70);*/
        $array_fields = ['end_decode' => true];
        $id = 2;
        $id_CheckDecodeReports = ORM::factory('CheckDecodeReports')->update_one_row_by_id($id, $array_fields);
        $test = 1;
    }

    public function test_set_into_statistic_bot_next_day_2()
    {
        $username = 2;
        $this->assertEquals($username, 2);
    }

    public function test_add_current_temprature()
    {
        $array_row_by_mac = ORM::factory('Decodereport')->get_rows_by_mac_sort_asc('70b3d5de00000c33');
        $old_current_temperature = -1;
        foreach ($array_row_by_mac as $key => $one_row) {
            if ($one_row['current_temperature'] != 0) {
                $old_current_temperature = $one_row['current_temperature'];
            } elseif ($old_current_temperature != -1) {
                $array_row_by_mac[$key]['current_temperature'] = $old_current_temperature;
            }
        }
        $test=1;
    }


}