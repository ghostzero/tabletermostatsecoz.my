<?php

/**
 * Created by PhpStorm.
 * User: ghost
 * Date: 24.08.15
 * Time: 12:51
 */
require_once('/home/ghost/www/betsline.my/php-webdriver-master/phpQuery/phpQuery.php');
class ModelMethodWilliamHillTest extends Kohana_Unittest_TestCase
{
    protected $model;

    function __construct()
    {
        $this->model = new Model_MethodWilliamHillTest();
    }

    public function test_get_amount_money_in_account()
    {
        $amount_money_in_account=$this->model->get_amount_money_in_account();
        $this->assertEquals($amount_money_in_account, 0.70);
    }

}