<?php
/**
 * Created by PhpStorm.
 * User: ghost
 * Date: 27.10.16
 * Time: 12:22
 */
defined('SYSPATH') or die('No direct access allowed!');

class ModelCheckDecodeReports extends Kohana_Unittest_TestCase
{
    /*protected $model;

    function __construct()
    {
        $this->model = new Model_MethodWilliamHillTest();
    }*/

    public function test_set_one_row()
    {
        /*$amount_money_in_account=$this->model->get_amount_money_in_account();
        $this->assertEquals($amount_money_in_account, 0.70);*/
        $array_fields = ['logs_new_id' => 2, 'begin_decode' => true, 'end_decode' => false];
        $id_CheckDecodeReports = ORM::factory('CheckDecodeReports')->set_one_row($array_fields);
        $test = 1;
    }

    public function test_update_one_row_by_id()
    {
        /*$amount_money_in_account=$this->model->get_amount_money_in_account();
        $this->assertEquals($amount_money_in_account, 0.70);*/
        $array_fields = ['end_decode' => true];
        $id = 2;
        $id_CheckDecodeReports = ORM::factory('CheckDecodeReports')->update_one_row_by_id($id, $array_fields);
        $test = 1;
    }

    public function test_set_into_statistic_bot_next_day_2()
    {
        $username = 2;
        $this->assertEquals($username, 2);
    }

    public function test_add_current_temprature()
    {
        $array_row_by_mac = ORM::factory('Decodereport')->get_rows_by_mac_sort_asc('70b3d5de00000c33');
        $old_current_temperature = -1;
        foreach ($array_row_by_mac as $key => $one_row) {
            if ($one_row['current_temperature'] != 0) {
                $old_current_temperature = $one_row['current_temperature'];
            } elseif ($old_current_temperature != -1) {
                $array_row_by_mac[$key]['current_temperature'] = $old_current_temperature;
            }
        }
        $test=1;
    }


}